using System;
using System.Collections.Generic;
using System.Linq;

namespace BarnardsStar
{
    public static class GameVersion
    {
        public const int VersionNumber = 20;
        public const string MinorVersionSuffix = "b";
        // Update MostRecentPatchNotesURL below if applicable!

        public static string VersionStr => VersionNumber + MinorVersionSuffix;
        
        public const int MinimumWatchableVersion = 20;

        public const int SeasonNumber = 4;
        public static readonly DateTime SeasonStartDate = new DateTime(2024, 6, 14, 12, 0, 0);
        
        public static int CurrentGameVersionInt()
        {
            return VersionStrToInt(VersionStr);
        }

        public static int VersionStrToInt(string versionStr)
        {
            int len = versionStr.Length;

            char versionSuffix = versionStr.ToCharArray(len - 1, 1)[0];
            int minorNumber = (int)versionSuffix - (int)'a' + 1;

            string majorVersionStr = versionStr.Substring(0, len - 1);
            int.TryParse(majorVersionStr, out var majorVersion);

            return majorVersion * 100 + minorNumber;
        }
    }

    // Stuff that doesn't have as much to do with the game version, but is important constants nonetheless 
    public static class GameConstants
    {
        public const string MostRecentPatchNotesURL = "https://bstar.app/patch-notes/20a/";
        public const string MostRecentPatchPopup = "20a";

        // Min/max number of players supported in a single game
        public const int MinPlayersSupported = 2;
        public const int MaxPlayersSupported = 6;

        public static IEnumerable<int> AllSupportedNumPlayers()
        {
            return Enumerable.Range(MinPlayersSupported, MaxPlayersSupported - MinPlayersSupported + 1);
        }
    }

    public static class Headers
    {
        public const string SteamAppId = "X-Steam-AppId",
            ClientVersion = "X-Client-Version",
            ClientVersionStr = "X-Client-Version-Str",
            BstarPlatform = "X-Bstar-Platform",
            BstarUserAgent = "X-Bstar-User-Agent",
            UseFakeLogin = "Use-Fake-Login",
            AdminPanelLogin = "Auth-Panel-Login",
            SteamAuth = "X-Steam-Auth";
    }
}
