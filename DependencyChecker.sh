#! /bin/bash

set -e

# change to script directory
cd "${0%/*}"

depsFound=0

check() {
	find . -name "*.cs" | xargs grep -r "$1"
	if [[ $? -ne 1 ]]; then
		depsFound=1
	fi
}

check UnityEngine
check 'BarnardsStar.Logic'

if [[ depsFound -eq 1 ]]; then
	echo
	echo 'Inappropriate dependencies found!'
	echo "BStarGameLogic shouldn't depend on anything specific to Unity or the BStar Unity client."
	exit 1
fi
