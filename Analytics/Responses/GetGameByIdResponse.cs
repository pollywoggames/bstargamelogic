namespace BarnardsStar.Analytics
{
    public class GetGameByIdResponse
    {
        public CompletedGame Game { get; set; }
    }
}