using System.Collections.Generic;

namespace BarnardsStar.Analytics
{
    public class GetWeeklyGamesResponse
    {
        public List<CompletedGame> Games { get; set; }
    }
}