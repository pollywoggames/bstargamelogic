namespace BarnardsStar.Analytics
{
    public class GetGameByIdRequest
    {
        public string GameId { get; set; }
    }
}