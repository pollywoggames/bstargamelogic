using System;
using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;

namespace BarnardsStar.Analytics
{
    public class CompletedGameParticipant
    {
        // The player's current username
        // May be different from the username they had when the game was played
        public string Name { get; set; }
        
        // The color this player chose -- red or blue
        public TeamColor TeamColor { get; set; }
        
        // The fighters this player chose
        public List<FighterPrototype> Fighters;

        // The player's elo before the game started
        public double StartingElo { get; set; }
    }

    public enum GameEndType
    {
        Timeout,
        Surrender,
        WinOrLoss,
    }
    
    public class CompletedGame
    {
        // The unique 6 digit ID for this game
        public string GameId { get; set; }
        
        // Information about each player
        public List<CompletedGameParticipant> Players { get; set; }

        public string WinnerId;
        
        // When the game was created by the first player
        public DateTime GameCreationDateTime { get; set; }
        
        // When the game was joined by the second player
        public DateTime GameJoinDateTime { get; set; }
        
        // When the game ended. This could have happened because of a win, a surrender, or a timeout
        public DateTime GameEndDateTime { get; set; }
        
        // Which map and layout the game was played on
        public MapName MapName { get; set; }
        public int MapLayout { get; set; }

        // Whether the game ended in surrender, timeout, or a win/loss
        public GameEndType GameEndType { get; set; }
    
        // A list of all the turns that were taken in this game
        public List<GameTurn> GameTurns { get; set;  }
    }
}