﻿using System;

namespace BarnardsStar.Characters
{
    public enum FighterPrototype
    {
        // If you add a new fighter to this list, make sure to add it to FloorGen->GetPossibleEnemyPrototypes too
        // DO NOT CHANGE THE ORDER OF THIS LIST. They get serialized as numbers. Add new items to the end of the list.
        
        // When adding a new fighter:
        // - Add to AllFighters if they're pickable
        // - Add to FloorGen.GetPossible[Faction]Prototypes ("RealFighter" list)
        // - Add to MenuOptions things for locking/unlocking specific characters
        // - Add unlock criteria to AchievementTracker.AllUnlockCriteria

        // Enforcer
        HumanEnforcer = 0,
        // Sniper
        HumanSniper = 1,
        // Commando
        HumanCommando = 2,
        // Rocketeer
        HumanBazookaman = 3,
        // Amphibian
        HumanAmphibian = 4,
        // Mad Scientist
        HumanChemistSupport = 5,
        // Medic
        HumanMedic = 6,
        // Ranger
        HumanEngineer = 7,
        HumanPunk = 8,
        HumanArchitect = 9,
        
        HumanMiniMelee = 10,
        HumanMeleeCreep = 11,
        HumanRangedCreep = 12,

        // Antivirus
        RobotTank = 13,
        // Overwatch
        RobotOverwatch = 14,
        // Exterminator
        RobotExterminator = 15,
        // Daemon
        RobotSummoner = 16,
        // Firewall
        RobotOiler = 17,
        // Automechanic
        RobotChemistSupport = 18,
        // Defragmenter
        RobotMedic = 19,
        // Physics Engine
        RobotEngineer = 20,
        RobotCatalyst = 21,
        RobotOmnibus = 22,
        
        // Minion
        RobotMinion = 23,

        RobotMiniMelee = 24,
        RobotMeleeCreep = 25,
        RobotRangedCreep = 26,

        // Grump
        CritterGrump = 27,
        // Gunslinger
        CritterGunslinger = 28,
        // Bounty Hunter
        CritterBountyHunter = 29,
        // Psion
        CritterPsion = 30,
        // Blobber
        CritterBlobber = 31,
        // Acid Junkie
        CritterChemistSupport = 32,
        // Gargoyle
        CritterMedic = 33,
        // Overlord
        CritterEngineer = 34,
        CritterBeast = 35,
        CritterLurker = 36,
        
        // Housefly
        CritterFly = 37,
        
        CritterMiniMelee = 38,
        CritterMeleeCreep = 39,
        CritterRangedCreep = 40,
    }

    public static class FighterPrototypeExtensions
    {
        // returns false for creeps and minions
        public static bool IsRealFighter(this FighterPrototype fighterPrototype)
        {
            return fighterPrototype switch
            {
                FighterPrototype.HumanMiniMelee => false,
                FighterPrototype.HumanMeleeCreep => false,
                FighterPrototype.HumanRangedCreep => false,
                FighterPrototype.RobotMinion => false,
                FighterPrototype.RobotMiniMelee => false,
                FighterPrototype.RobotMeleeCreep => false,
                FighterPrototype.RobotRangedCreep => false,
                FighterPrototype.CritterFly => false,
                FighterPrototype.CritterMiniMelee => false,
                FighterPrototype.CritterMeleeCreep => false,
                FighterPrototype.CritterRangedCreep => false,
                _ => true
            };
        }
    }
}