using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities;
using BarnardsStar.Hazards;
using BarnardsStar.Upgrades;
using BarnardsStar.Weapons;

namespace BarnardsStar.Characters
{
    public class FighterBlueprint
    {
        public string Name;
        public int SpriteOffset;
        public Team team;
        public FighterPrototype proto;

        public int Actions, Health, MoveSpeed, ShieldStrength;
        
        public BSWeapon EquippedWeapon;
        public List<BSAbility> abilities;
        public List<HazardType> immunities;

        public MovementAnimationType moveType;
        public bool UseFemaleSFX;
        public bool DisableMuzzleFlash;
        
        public List<IPotentialUpgrade> potentialUpgrades = new List<IPotentialUpgrade>();
        
        // Metadata for procedural level generation.
        // "Carry" = damage-focused fighter. Each fighter is considered either a "carry" or a "support"
        public bool IsDamageCarry;

        public FighterBlueprint(FighterPrototype proto, string n, Team team, int SpriteOffset)
        {
            Name = n;
            this.team = team;
            this.proto = proto;
            this.SpriteOffset = SpriteOffset;

            // Default values
            Actions = 1;
            Health = AllFighters.BaseHealth;
            ShieldStrength = AllFighters.BaseShieldStrength;
            MoveSpeed = AllFighters.BaseMoveSpeed;
            moveType = MovementAnimationType.Bob;
            EquippedWeapon = BSWeapon.UnarmedAttack;
            UseFemaleSFX = false;

            abilities = new List<BSAbility>();
            immunities = new List<HazardType>();
        }

        public FighterBlueprint WithHealth(int x)
        {
            Health = x;
            return this;
        }

        public FighterBlueprint WithMoveSpeed(int x)
        {
            MoveSpeed = x;
            return this;
        }

        public FighterBlueprint WithMoveType(MovementAnimationType type)
        {
            moveType = type;
            return this;
        }

        public FighterBlueprint WithWeapon(BSWeapon weapon)
        {
            EquippedWeapon = weapon;
            return this;
        }

        public FighterBlueprint WithAbility(BSAbility a)
        {
            abilities.Add(a);
            return this;
        }

        public FighterBlueprint WithImmunity(HazardType immuneto)
        {
            immunities.Add(immuneto);
            return this;
        }

        public FighterBlueprint WithUseFemaleSFX(bool u)
        {
            UseFemaleSFX = u;
            return this;
        }

        public FighterBlueprint WithDisableMuzzleFlash(bool m)
        {
            DisableMuzzleFlash = m;
            return this;
        }

        public FighterBlueprint WithHealthPotentialUpgrade(int amount)
        {
            potentialUpgrades.Add(new PotentialUpgrade(new UpgradeType(amount, GeneralUpgradeType.Health)));
            return this;
        }
        
        public FighterBlueprint WithMovePotentialUpgrade(int amount)
        {
            potentialUpgrades.Add(new PotentialUpgrade(new UpgradeType(amount, GeneralUpgradeType.Moves)));
            return this;
        }
        
        public FighterBlueprint WithPotentialUpgrade(UpgradeType upgradeType, string details = null, string noun = null)
        {
            return WithPotentialUpgrade(new PotentialUpgrade(upgradeType, details, noun));
        }

        public FighterBlueprint WithPotentialUpgrade(IPotentialUpgrade pu)
        {
            pu.index = potentialUpgrades.Count;
            this.potentialUpgrades.Add(pu);
            return this;
        }

        public FighterBlueprint SetIsDamageCarry()
        {
            this.IsDamageCarry = true;
            return this;
        }

        public virtual void InitFields(Fighter f, List<UpgradeType> upgrades)
        {
            f.Name = Name;
            f.Prototype = proto;
            f.SpriteOffset = SpriteOffset;
            f.team = team;

            f.SetMaxActions(Actions);
            f.SetMaxMoves(MoveSpeed + UpgradeType.GetMoveUpgradeAmount(upgrades));
            f.SetMaxHealth(Health + UpgradeType.GetHealthUpgradeAmount(upgrades));
            f.SetMaxShield(ShieldStrength);

            f.MoveType = moveType;
            f.UseFemaleSFX = UseFemaleSFX;
            f.DisableMuzzleFlash = DisableMuzzleFlash;

            f.weapon = UpgradeType.GetSwappedWeapon(upgrades) ?? EquippedWeapon;

            f.AbilityTypes = new List<BSAbility>(abilities);
            f.Immunities = new List<HazardType>(immunities);
        }

        public List<IPotentialUpgrade> LookupUpgradeTypes(List<UpgradeType> upgradeTypes)
        {
            var ret = new List<IPotentialUpgrade>();

            foreach (var upgradeType in upgradeTypes)
            {
                // check if it's already subsumed by any other upgrade objects
                if (ret.Any(upgradeObj => upgradeObj.Matches(upgradeType)))
                    continue;

                ret.Add(potentialUpgrades.Find(pu => pu.Matches(upgradeType)) ?? new PotentialUpgrade(upgradeType));
            }
            
            return ret;
        }
    }
}
