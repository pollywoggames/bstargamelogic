using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BarnardsStar.Abilities;
using BarnardsStar.Abilities.PassiveAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Upgrades;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons;
using Newtonsoft.Json;
using Logger = BarnardsStar.Utilities.Logger;
using Vector3 = System.Numerics.Vector3;

namespace BarnardsStar.Characters
{
    [DebuggerDisplay("Fighter {id}, coords: {coords}")]
    public class Fighter : IDamageable, IMovable, IEquatable<Fighter>
    {
        // 0.20833333 is 5/24 (5 pixels up on a 24x24 pixel grid)
        public static readonly Point OffsetOnGrid = new Point(0, 0.20833333f);
        public const float WakeupRadius = 5f;
        public const int MaxRespawnTurns = 2;

        public FighterPrototype Prototype;
        public string Name;
        public string id;
        public Player Owner;
        
        [JsonIgnore] public bool hasVirtualActor => virtualActor != null && !virtualActor.IsDestroyed();
        [JsonIgnore] public IFighterVirtualActor virtualActor;
        
        public BSWeapon weapon;
        public List<BSAbility> AbilityTypes;
        public List<AbstractAbility> Abilities;
        public List<HazardType> Immunities;

        public List<UpgradeType> Upgrades = new List<UpgradeType>();

        public int MaxActions;
        public int ActionsLeft;
        public int MaxHealth;
        public int CurrHealth;
        public int MaxMoves;
        public float MovesLeft;
        public int MaxShield;
        public int CurrShield;
        
        // Whether the fighter has the Hypertunnel ability, allowing traversal through cover
        public bool Hypertunnel;
        // Whether the fighter has the Fly ability, allowing traversal over cover, pits, and enemies
        public bool Flier;
        // Whether the fighter has the surfing ability, where movement on the given type of hazard only costs 1/2 a point
        public HazardType? Surfing;
        
        public HazardType? LeavesTrail;
        public bool MoveAfterAttack;
        
        // deprecated
        // TODO remove after next patch
        public bool ReadyAttack;
        // new one
        public int ReadyAttacks;
        [JsonIgnore] public bool HasReadyAttacks => ReadyAttack || ReadyAttacks > 0;
        
        public bool ReflectProjectiles;
        public int? TurnsTilRespawn;
        public int NextTurnsTilRespawn;
        public Team team;
        
        // how many kills this fighter has gotten this game
        public int numKills;

        public Dictionary<StatusEffect, int> statusEffects;

        public MovementAnimationType MoveType;
        public int SpriteOffset;
        public bool UseFemaleSFX;
        public bool DisableMuzzleFlash;
        public bool WasSummoned; // don't respawn
        public bool Asleep;
        public int? tpChan; // teleporter channel for tp's this fighter will place (only applicable to PlaceTeleporter ability)  

        [JsonIgnore] public bool TempDisableMovementPreviews;
        [JsonIgnore] public bool TempDisableMovementPreviewScore;

        // Virtual actor hooks for update/animation
        [JsonIgnore]
        public Action OnReadyAttackChange,
                      OnReflectProjectilesChange,
                      OnHealthChange,
                      OnShieldChange,
                      OnActionsLeftChange,
                      OnMovesLeftChange,
                      OnAsleepChange;
        
        // Game hooks for abilities
        [JsonIgnore]
        public Action OnKill, OnAttack;

        // Other runtime stuff
        private int animationStack;
        private Weapon _weaponObj;

        private Coords _coords;
        private Point _pos;

        public Coords? OverrideCoords;
        public Coords coords
        {
            get {
                if (OverrideCoords.HasValue) return OverrideCoords.Value;
                if (hasVirtualActor) _coords = virtualActor.GetCoords();
                return _coords;
            }
            set { 
                if (hasVirtualActor) virtualActor.SetCoords(value);
                _coords = value;
            }
        }

        public Point pos
        {
            get {
                if (hasVirtualActor) _pos = virtualActor.GetPosition();
                return _pos;
            }
            set {
                if (hasVirtualActor) virtualActor.SetPosition(value);
                _pos = value;
            }
        }

        [JsonIgnore] public bool ShieldUp => CurrShield > 0;
        [JsonIgnore] public int EffectiveHP => CurrHealth + CurrShield;
        [JsonIgnore] public bool IsDead => CurrHealth <= 0;

        [JsonIgnore] public IContextProvider contextProvider => Owner.contextProvider; 
        [JsonIgnore] public IAnimationContext animCtx => Owner.animCtx;
        [JsonIgnore] public IRunContext runCtx => Owner.runCtx;
        [JsonIgnore] public Game game => Owner.game;
        [JsonIgnore] public bool IsAnimating => animationStack > 0;

        public Fighter() { }
        
        // dummy constructor for getting IAbilityDescriptor info only
        public Fighter(FighterBlueprint blueprint)
        {
            Name = blueprint.Name;
        }

        public Fighter(Player owner, FighterPrototype prototype, IFighterVirtualActor virtualActor, bool initForGame = true)
        {
            Owner = owner;
            Prototype = prototype;
            this.virtualActor = virtualActor;

            var idx = 0;
            if (owner != null) // for testing mainly 
            {
                idx = owner.NextFighterId;
                owner.NextFighterId++;
            }

            id = $"{prototype}-{owner.Idx}-{idx}";

            TurnsTilRespawn = null;

            if (owner?.game != null)
            {
                var gameSettings = owner.game.gameSettings;
                NextTurnsTilRespawn = Math.Min(gameSettings.StartingRespawnTimer, gameSettings.MaxRespawnTimer);
            }
            else
            {
                NextTurnsTilRespawn = 0;
            }

            AbilityTypes = new List<BSAbility>();
            statusEffects = new Dictionary<StatusEffect, int>();

            if (initForGame)
                InitForGame();
        }

        public void InitForGame()
        {
            InitFieldsFromBlueprintAndUpgrades();
            InitAbilities();
            virtualActor.Init(this, true);
        }

        private FighterBlueprint GetFighterBlueprint()
        {
            if (AllFighters.Instance.All.TryGetValue(Prototype, out var fb))
                return fb;

            return new FighterBlueprint(Prototype, id, team, 0);
        }

        public void ReapplyUpgrades()
        {
            InitFieldsFromBlueprintAndUpgrades();
            ApplyAbilityUpgrades();
        }

        public void InitFieldsFromBlueprintAndUpgrades()
        {
            var bp = GetFighterBlueprint();
            bp.InitFields(this, Upgrades);
            _weaponObj = null;
        }

        public void ApplyAbilityUpgrades()
        {
            ApplyAbilityUpgrade(GetWeapon(), UpgradeType.GetWeaponUpgrades(Upgrades));
            
            for (var i = 0; i < Abilities.Count; i++)
            {
                var ability = Abilities[i];
                ApplyAbilityUpgrade(ability, UpgradeType.GetAbilityUpgrades(Upgrades, i));
            }
        }

        private void ApplyAbilityUpgrade(IAbility ability, AbilityUpgrade upgrade)
        {
            if (upgrade.Range > 0)
            {
                if (ability.CanReceiveRangeUpgrade())
                    ability.ApplyRangeUpgrade(upgrade.Range);
                else
                    Logger.Warn($"Fighter {id} got range upgrade but {ability.GetTypeName()} can't receive a range upgrade!");
            }
            
            if (upgrade.Damage > 0)
            {
                if (ability.CanReceiveDamageUpgrade())
                    ability.ApplyDamageUpgrade(upgrade.Damage);
                else
                    Logger.Warn($"Fighter {id} got Damage upgrade but {ability.GetTypeName()} can't receive a Damage upgrade!");
            }
            
            if (upgrade.Cooldown > 0)
            {
                if (ability.CanReceiveCooldownUpgrade())
                    ability.ApplyCooldownUpgrade(upgrade.Cooldown);
                else
                    Logger.Warn($"Fighter {id} got Cooldown upgrade but {ability.GetTypeName()} can't receive a Cooldown upgrade!");
            }
            
            if (upgrade.Misc > 0)
            {
                if (ability.CanReceiveMiscUpgrade())
                    ability.ApplyMiscUpgrade(upgrade.Misc);
                else
                    Logger.Warn($"Fighter {id} got Misc upgrade but {ability.GetTypeName()} can't receive a Misc upgrade!");
            }
        }

        public void InvokeStateChangeCallbacks()
        {
            OnReadyAttackChange?.Invoke();
            OnReflectProjectilesChange?.Invoke();
            OnHealthChange?.Invoke();
            OnShieldChange?.Invoke();
            OnActionsLeftChange?.Invoke();
            OnMovesLeftChange?.Invoke();
            OnAsleepChange?.Invoke();
            
            foreach (var statusEffect in statusEffects.Keys)
                virtualActor.ShowStatusEffect(statusEffect);
        }

        public void ClearActionsAndMovesLeft()
        {
            try
            {
                SetActionsLeft(0);
                SetMovesLeft(0);
            }
            catch (Exception e)
            {
                Logger.Warn(e);
            }
        }

        public void ResetStateForRespawn()
        {
            ClearAllStatusEffects();
            SetHealth(MaxHealth);
            
            // reset all cooldowns
            foreach (var ability in Abilities) ability.ResetCooldown();
            
            var nextCoords = Owner.ClearCoordsBySpawnPoint();

            if (hasVirtualActor)
            {
                virtualActor.SetCoords(nextCoords);
                virtualActor.ResetStateForRespawn();
            }
        }

        internal void ClearCaches()
        {
            RemoveEventHandlerSubscriptions();
            _weaponObj = null;
            animationStack = 0;
        }
        
        public Fighter Clone()
        {
            var clone = (Fighter) MemberwiseClone();
            
            var newAbilityList = new List<AbstractAbility>();
            foreach (var ability in clone.Abilities)
            {
                newAbilityList.Add(ability.Clone(clone));
            }
            clone.Abilities = newAbilityList;

            var newStatusEffectsList = new Dictionary<StatusEffect, int>();
            foreach (var se in statusEffects.Keys)
            {
                newStatusEffectsList[se] = statusEffects[se];
            }
            clone.statusEffects = newStatusEffectsList;

            if (Upgrades != null)
                clone.Upgrades = Upgrades.Select(u => u).ToList();
            
            return clone;
        }

        public override string ToString()
        {
            return $"`{id}`";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Fighter) obj);
        }

        public bool Equals(Fighter other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return id == other.id;
        }

        public static bool operator ==(Fighter a, Fighter b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Fighter a, Fighter b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (id != null ? id.GetHashCode() : 0);
        }

        public Weapon GetWeapon()
        {
            if (_weaponObj == null)
            {
                _weaponObj = Weapon.New(weapon, this);
                ApplyAbilityUpgrade(_weaponObj, UpgradeType.GetWeaponUpgrades(Upgrades));
            }

            return _weaponObj;
        }

        public void ClearCachedWeaponObj()
        {
            _weaponObj = null;
        }

        public void InitAbilities()
        {
            Abilities = new List<AbstractAbility>();
            foreach (var abilityType in AbilityTypes)
            {
                var abObj = AbstractAbility.New(abilityType, this);
                if (abObj != null)
                    Abilities.Add(abObj);
            }

            ApplyAbilityUpgrades();
        }

        public T FindAbilityOfType<T>() where T : AbstractAbility
        {
            foreach (var ab in Abilities)
                if (ab is T t)
                    return t;

            return null;
        }

        public AbstractAbility GetAbility(BSAbility abilityType)
        {
            return Abilities.Find(a => a.type == abilityType);
        }
        
        public AbstractAbility GetAbilityIdx(int idx)
        {
            return Abilities[idx];
        }

        public Player GetOwner()
        {
            return Owner;
        }

        public void HideHealthBar()
        {
            if (hasVirtualActor) virtualActor.HideHealthBar();
        }

        public void ShowHealthBar()
        {
            if (hasVirtualActor) virtualActor.ShowHealthBar();
        }

        public IEnumerable<Coords> GetTargetableGridCoords()
        {
            return new[] {coords};
        }

        public int GetEffectiveHP()
        {
            return EffectiveHP;
        }

        public Point GetPosition()
        {
            return pos;
        }

        public void SetPosition(Point p)
        {
            SetPosition(p, false);
        }

        public void SetPosition(Point p, bool isTeleport)
        {
            if (!TempDisableMovementPreviews && contextProvider.IsPreviewing())
            {
                contextProvider.RegisterPreview(
                    new MovementPreviewAction(
                        this,
                        new SingleMove((p - OffsetOnGrid).ToCoords(), isTeleport, TempDisableMovementPreviewScore)));
            }

            pos = p;
        }

        public float GetRotation()
        {
            return 0;
        }

        public void SetRotation(float q)
        {
            virtualActor.SetRotation(q);
        }

        public float GetBackwardsRotationAngle()
        {
            return virtualActor.IsFacingLeft() ? -20f : 20f;
        }
        
        public float GetForwardsRotationAngle()
        {
            return -1 * GetBackwardsRotationAngle();
        }

        public void TipForwards()
        {
            SetRotation(GetForwardsRotationAngle());
        }
        
        public void TipBackwards()
        {
            SetRotation(GetBackwardsRotationAngle());
        }

        public void ResetRotation()
        {
            SetRotation(0);
        }

        public Vector3 GetScale()
        {
            return Vector3.One;
        }

        public void SetScale(Vector3 scale)
        {
            virtualActor.SetScale(scale);
        }

        public void Destroy()
        {
            Logger.Error("You really shouldn't be calling Destroy() on a Fighter instance ...");
        }

        public void SetMaxHealth(int m)
        {
            MaxHealth = m;
            CurrHealth = MaxHealth;
            
            virtualActor.InitHealthBar();
        }

        public void SetMaxShield(int s)
        {
            if (MaxShield < s)
                MaxShield = s;
            
            virtualActor.InitHealthBar();
        }

        public void SetMaxMoves(int m)
        {
            MaxMoves = m;
            MovesLeft = m;
        }

        public void SetMaxActions(int a)
        {
            MaxActions = a;
            ActionsLeft = a;
        }

        public void SetReadyAttacks(int r)
        {
            if (r < 0) r = 0;
            
            ReadyAttack = r > 0;
            ReadyAttacks = r;
            OnReadyAttackChange?.Invoke();
        }

        public void SetReflectProjectiles(bool r)
        {
            ReflectProjectiles = r;
            OnReflectProjectilesChange?.Invoke();
        }

        public void SetWasSummoned(bool wasSummoned)
        {
            WasSummoned = wasSummoned;
            if (WasSummoned)
            {
                // summoned fighters don't get moves/actions their first turn alive
                SetMovesLeft(0);
                SetActionsLeft(0);
            }
        }

        public void SetHealth(int h)
        {
            if (h < 0)
                h = 0;
            else if (h > MaxHealth)
                h = MaxHealth;
            
            // register preview for healing
            if (h > CurrHealth)
            {
                int diff = h - CurrHealth;

                if (contextProvider.IsPreviewing())
                {
                    // use negative diff bc it's healing, not damage
                    contextProvider.RegisterPreview(new DamagePreview(game, -diff, this, false));
                }

                game.SomethingHappened(new BSEvent(BSEventType.HealByAnyMethod, this) { amount = diff });
            }

            CurrHealth = h;
            OnHealthChange?.Invoke();
        }

        public void SetCurrShield(int s, bool scorePreview = false)
        {
            if (s < 0)
            {
                s = 0;
            }
            else if (s > MaxShield)
            {
                s = MaxShield;
            }

            if (s > CurrShield && contextProvider.IsPreviewing())
            {
                contextProvider.RegisterPreview(new GainShieldPreview(this, s - CurrShield, scorePreview));
            }

            CurrShield = s;
            OnShieldChange?.Invoke();
        }

        public void SetActionsLeft(int a)
        {
            if (a < 0)
            {
                a = 0;
            }
            else if (a > MaxActions)
            {
                a = MaxActions;
            }
            
            ActionsLeft = a;
            
            if (ActionsLeft == 0 && !MoveAfterAttack)
                SetMovesLeft(0);

            if (game.CheatEnabled(Cheat.InfiniteActions))
                ActionsLeft = MaxActions;
            
            OnActionsLeftChange?.Invoke();
        }

        public void SetMovesLeft(float m, bool overrideMax = false)
        {
            if (m < 0)
            {
                m = 0;
            }
            else if (!overrideMax && m > MaxMoves)
            {
                m = MaxMoves;
            }

            if (overrideMax && m > MovesLeft && contextProvider.IsPreviewing())
            {
                contextProvider.RegisterPreview(new MovementSpeedBuffPreview(this, m - MovesLeft));
            }

            MovesLeft = m;

            if (game != null && game.CheatEnabled(Cheat.InfiniteActions))
                MovesLeft = MaxMoves;

            OnMovesLeftChange?.Invoke();
        }

        public void SetAsleep(bool asleep)
        {
            if (Asleep && !asleep && contextProvider.IsPreviewing())
            {
                contextProvider.RegisterPreview(new WakeUpPreview(this));
            }
            
            Asleep = asleep;
            
            OnAsleepChange?.Invoke();
            
            SetMovesLeft(CalcMaxMoves());
        }

        public void ResetMovesAndActions()
        {
            SetMovesLeft(MaxMoves);
            SetActionsLeft(MaxActions);
        }

        public void RemoveEventHandlerSubscriptions()
        {
            OnReadyAttackChange = null;
            OnReflectProjectilesChange = null;
            OnHealthChange = null;
            OnShieldChange = null;
            OnActionsLeftChange = null;
            OnMovesLeftChange = null;
            OnAsleepChange = null;

            OnAttack = null;
            OnKill = null;
        }

        public IEnumerator DealDamage(int damage, Fighter origin)
        {
            yield return DealDamage(damage, origin, false, false);
        }

        public IEnumerator DealDamage(int damage, Fighter origin, bool suppressKnockback)
        {
            yield return DealDamage(damage, origin, suppressKnockback, false);
        }

        public IEnumerator DealDamage(int damage, Fighter origin, bool suppressKnockback, bool ignoreShield)
        {
            if (damage == 0 || CurrHealth <= 0)
            {
                yield break;
            }
            
            if (hasVirtualActor) virtualActor.ShowHealthBar();

            var hasShield = false;
            if (CurrShield > 0 && !ignoreShield)
            {
                hasShield = true;
                int absorbed = damage <= CurrShield ? damage : CurrShield;
                SetCurrShield(CurrShield - absorbed);
                var healthDamage = damage - absorbed;

                // Not all of it was absorbed
                if (healthDamage > 0)
                {
                    SetHealth(CurrHealth - healthDamage);
                }
            }
            else
            {
                // No shield to absorb it, health takes the full brunt of it
                SetHealth(CurrHealth - damage);
            }

            if (hasShield && CurrShield <= 0)
            {
                FindAbilityOfType<Shield>()?.SetOnCooldown();
                game.soundCtx.PlayOneShot(Sound.ShieldBreak);
            }

            if (hasVirtualActor)
            {
                virtualActor.DamageAnimation(origin, suppressKnockback);
                virtualActor.HideHealthBar();
            }
            
            if (contextProvider.IsPreviewing())
                contextProvider.RegisterPreview(new DamagePreview(game, damage, this, CurrHealth <= 0, ignoreShield));

            if (CurrHealth > 0)
            {
                PlayDamageSFX();
            }
            else
            {
                PlayDeathSFX();

                foreach (var ability in Abilities)
                {
                    if (ability.HasOnDeathTrigger)
                        yield return ability.OnDeath(origin);
                }
                
                yield return game.FighterDied(this, origin);
            }
        }

        public IEnumerator DealSelfDamage(int damage)
        {
            var actorOriginalPos = pos;
            
            // Move down a bit and tilt, then take damage
            virtualActor.SetRotation(GetBackwardsRotationAngle());
            var dst = pos + (Point.down * .4f);
            yield return animCtx.MoveSmoothly(this, dst, .25f);
            
            // 1 damage to self
            yield return DealDamage(damage, this, false, true);
            
            // check if dead
            if (CurrHealth <= 0)
                yield break;
            
            // tilt/move back
            virtualActor.SetRotation(0);
            yield return animCtx.MoveSmoothly(this, actorOriginalPos, .35f);
        }

        public IEnumerator FallDownHole(Fighter killer)
        {
            if (contextProvider.IsPreviewing())
                contextProvider.RegisterPreview(new DamagePreview(game, GetEffectiveHP(), this, true));

            game.SomethingHappened(new BSEvent(BSEventType.DealDamageWithPushPull, killer));
            game.SomethingHappened(new BSEvent(BSEventType.KillFighterWithPushPullDownPit, killer));
            
            SetCurrShield(0);
            SetHealth(0);
            
            PlayDeathSFX();
            
            var waitGroup = new WaitGroup(runCtx);
            waitGroup.Start(game.FighterDied(this, killer));
            waitGroup.Start(virtualActor.AnimateFallDownHole());
            yield return waitGroup.Wait();
        }

        private void PlayDamageSFX()
        {
            Sound s = team switch
            {
                Team.Human => UseFemaleSFX ? Sound.HumanFemaleDamage : Sound.HumanMaleDamage,
                Team.Robot => Sound.RobotDamage,
                Team.Critter =>
                    // for critters, UseFemaleSFX is set for the big boys
                    UseFemaleSFX ? Sound.BigCritterDamage : Sound.CritterDamage,
                _ => throw new ArgumentOutOfRangeException()
            };

            game.soundCtx.PlayOneShot(s, game.randy.Next(.9f, 1.1f));
        }

        private void PlayDeathSFX()
        {
            Sound s = team switch
            {
                Team.Human => UseFemaleSFX ? Sound.HumanFemaleDeath : Sound.HumanMaleDeath,
                Team.Robot => Sound.RobotDeath,
                Team.Critter =>
                    // for critters, UseFemaleSFX is set for the big boys
                    UseFemaleSFX ? Sound.BigCritterDeath : Sound.CritterDeath,
                _ => throw new ArgumentOutOfRangeException()
            };

            game.soundCtx.PlayOneShot(s, game.randy?.Next(.9f, 1.1f) ?? 1f);
        }

        public void StartAnimation()
        {
            animationStack++;
        }

        public void StopAnimation()
        {
            animationStack--;

            if (animationStack < 0)
            {
                animationStack = 0;
            }
        }

        public IEnumerator AnimateCoroutine(IEnumerator coroutine)
        {
            StartAnimation();
            yield return runCtx.Run(coroutine);
            StopAnimation();
        }

        public void DidReflectProjectile(Point projectileOrigin)
        {
            SetReflectProjectiles(false);
            FindAbilityOfType<ReflectProjectiles>().SetOnCooldown();
            virtualActor.ReflectProjectileAnimation(projectileOrigin);
        }

        public IEnumerator GetPushed(Coords direction, Fighter pusher)
        {
            if (CanGetPushedInDirection(direction))
            {
                yield return RealPush(direction);

                if (game.map._teleportersByCoords.TryGetValue(coords, out var tp) && tp.type != TeleporterType.Burrow)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.PushOrPullFighterIntoTeleporter, pusher?.Owner));
                }
                
                yield return game.OnFighterPushed(this, pusher);
            }
            else
            {
                yield return PushButBounceBack(direction, pusher);
            }
        }

        private IEnumerator RealPush(Coords direction)
        {
            virtualActor.EnableMovingParticles(direction);
            var dstGridCoords = coords + direction;
            var dst = dstGridCoords.ToPoint() + OffsetOnGrid;
            yield return animCtx.MoveSmoothly(this, dst, .17f);
            virtualActor.DisableMovingParticles();
        }

        private IEnumerator PushButBounceBack(Coords direction, Fighter pusher)
        {
            var dstGridCoords = coords + direction;
            var dst = dstGridCoords.ToPoint() + OffsetOnGrid;
            var delta = dst - pos;
            var halfway = delta * .5f + pos;
            var positionBefore = coords.ToPoint() + OffsetOnGrid;

            yield return animCtx.MoveSmoothly(this, halfway, .1f);

            yield return DealDamage(1, pusher, true);
            if (game.TryGetDamageable(dstGridCoords, out var thingGettingPushedInto))
            {
                yield return thingGettingPushedInto.DealDamage(2, pusher);
            }
            else if (game.map.GetTerrainType(dstGridCoords) == TerrainType.Cover)
            {
                yield return game.map.DamageCover(dstGridCoords, 1, pos, pusher);
            }

            game.SomethingHappened(BSEventType.DealDamageWithPushPull);
            
            yield return animCtx.MoveSmoothly(this, positionBefore, .1f);
        }

        public bool CanGetPushedInDirection(Coords direction)
        {
            var dst = coords + direction;
            return !game.CollideAt(dst);
        }

        public bool HasStatusEffect(StatusEffect eff)
        {
            return statusEffects.TryGetValue(eff, out var strength) && strength > 0;
        }

        public void AddStatusEffect(StatusEffect eff, int strength, Fighter source, bool suppressFx = false)
        {
            var effectDetails = eff.Details();

            // if the fighter is already dead
            if (IsDead)
                return;

            // check if it can't go above one
            if (!effectDetails.CanGoAboveOne && statusEffects.TryGetValue(eff, out int currVal) && currVal >= 1)
                return;

            // if the effect has a special rule for being able to add it
            if (!effectDetails.CheckCanAdd(this))
                return;

            if (!suppressFx)
            {
                if (effectDetails.vfxType.HasValue)
                    animCtx.PlayVFX(effectDetails.vfxType.Value, pos);
                
                if (effectDetails.sound.HasValue)
                    Owner.game.soundCtx.PlayOneShot(effectDetails.sound.Value);
            }

            if (eff == StatusEffect.Stunned || eff == StatusEffect.Electrified)
                game.SomethingHappened(new BSEvent(BSEventType.StunnedEnemy, source));
            
            if (Owner.IsTurn && effectDetails.IsEndOfTurn && effectDetails.ExtraIfTheirTurn)
            {
                // If it's that fighter's turn, give status +1 so it lasts through next turn
                strength++;
            }

            if (contextProvider.IsPreviewing())
                contextProvider.RegisterPreview(new StatusEffectPreview(this, eff, strength));

            var movementDeltaBefore = MovementDelta();
            var movesAlreadyUsed = MaxMoves + movementDeltaBefore - MovesLeft;
            
            if (statusEffects.ContainsKey(eff))
            {
                statusEffects[eff] += strength;
            }
            else
            {
                statusEffects.Add(eff, strength);
            }

            if (eff == StatusEffect.Stunned || eff == StatusEffect.Electrified)
            {
                SetMovesLeft(0);
                SetActionsLeft(0);

                if (ReadyAttack)
                    SetReadyAttacks(0);
                if (ReflectProjectiles)
                    SetReflectProjectiles(false);
            }
            else if (effectDetails.MovementDelta != 0)
            {
                SetMovesLeft(CalcMaxMoves() - movesAlreadyUsed);
            }
            
            if (hasVirtualActor)
                virtualActor.ShowStatusEffect(eff);
        }
        
        public void ClearAllStatusEffects()
        {
            statusEffects.Clear();
            TempDisableMovementPreviews = false;
            TempDisableMovementPreviewScore = false;
            
            if (hasVirtualActor)
                virtualActor.ClearAllStatusEffects();
        }

        public void TickDownStatusEffects(bool isEndOfTurn)
        {
            TempDisableMovementPreviews = false;
            TempDisableMovementPreviewScore = false;

            // Make it a separate list so that we can add/remove status effects during this loop 
            var activeStatusEffects = statusEffects.Keys.ToList();
            foreach (var se in activeStatusEffects)
            {
                if (isEndOfTurn != se.Details().IsEndOfTurn)
                    continue;
                
                statusEffects[se]--;
                
                if (statusEffects[se] <= 0)
                {
                    se.Details().TickDownToZeroEffect(this);
                    
                    statusEffects.Remove(se);
                    if (hasVirtualActor)
                        virtualActor.ClearStatusEffect(se);
                }
            }
        }

        public float MovementDelta()
        {
            return statusEffects.Select(se => se.Key.Details().MovementDelta).Sum();
        }

        public float CalcMaxMoves()
        {
            float movementDelta = MovementDelta();
            if (Asleep) movementDelta = 0;

            return (float) Math.Round(MaxMoves + movementDelta);
        }

        public bool LosesActionNextTurn()
        {
            return statusEffects.Keys.Any(se => se.Details().TakesAwayActions);
        }

        public void SetRespawnTimer()
        {
            // In weird cases (getting hit by 2 shotgun pellets at once) this function might get called 2x for the same
            // death, and increment the TurnsTilRespawn too much. This check prevents that.
            if (TurnsTilRespawn.HasValue) return;
            
            TurnsTilRespawn = NextTurnsTilRespawn;

            if (Owner.IsTurn)
                TurnsTilRespawn++;

            NextTurnsTilRespawn++;

            int maxRespawnTurns;
            if (Owner.playerSettings?.maxRespawnTimer.HasValue ?? false)
            {
                // If the player settings has maxRespawnTimer set, we'll just use that with no overrides
                maxRespawnTurns = Owner.playerSettings.maxRespawnTimer.Value;
            }
            else
            {
                // else use the one on the game settings
                maxRespawnTurns = Owner.game.gameSettings.MaxRespawnTimer;
            }
            
            if (NextTurnsTilRespawn > maxRespawnTurns)
                NextTurnsTilRespawn = maxRespawnTurns;
        }

        public bool IsFacingLeft()
        {
            return hasVirtualActor && virtualActor.IsFacingLeft();
        }
        
        public bool IsFriendlyTo(Fighter fighter)
        {
            return Owner.IsFriendlyTo(fighter.Owner);
        }

        public bool IsEnemyOf(Fighter fighter)
        {
            return Owner.IsEnemyOf(fighter.Owner);
        }

        // For checking whether this fighter has any abilities that can be Taunt-ed (and whether it's the weapon,
        // or one of the abilities, and which ability it is.)
        //
        // -1: nope
        // 0: weapon
        // 1: ability 1
        // 2: ability 2
        //
        // The second item in the tuple is which coords are targeted - is it the target itself? Or, is the target behind
        // cover, in which case the tauntee can only target the cover in front of them?
        public (int, Coords) GetProjectileAttackTargeting(Coords target)
        {
            if (!GetWeapon().IsProjectileAttack() && !Abilities.Any(a => a.IsProjectileAttack()))
                return (-1, default);

            var _weapon = GetWeapon();
            if (_weapon.IsProjectileAttack())
            {
                var targetList = _weapon.GetTargets();
                if (targetList.HasTargetedOrCovered(target))
                    return (0, GetTargetedCoords(targetList, target, coords));
            }

            for (var i = 0; i < Abilities.Count; i++)
            {
                var ab = Abilities[i];
                if (ab.IsProjectileAttack() && !ab.OnCooldown)
                {
                    var abilityTargets = ab.GetTargets();
                    if (abilityTargets.HasTargetedOrCovered(target))
                    {
                        return (i + 1, GetTargetedCoords(abilityTargets, target, coords));
                    }
                }
            }

            return (-1, default);
        }
        
        private Coords GetTargetedCoords(TargetList targetList, Coords coordsTryTarget, Coords projectileAttackerCoords)
        {
            // Determine whether the actor themselves, or the cover in front of them, is going to be targeted.
            // (for purposes of the Taunt ability.)

            if (targetList.validTargets.Any(t => t.coords == coordsTryTarget))
            {
                // Can just target the actor directly
                return coordsTryTarget;
            }
            
            // Else, just move backwards towards the enemy 1 square (so it targets the cover they're behind)
            Coords delta = (projectileAttackerCoords - coordsTryTarget).normalized;
            return coordsTryTarget + delta;
            
            // // Other implementation where we walk back towards the enemy fighter until we find a valid target space...
            // // I wrote this and then realized it's probably unnecessary. But keeping it in case it's needed in the future.
            // Coords current = actorCoords + delta;
            // while (current != enemyCoords)
            // {
            //     if (targetList.validTargets.Any(t => t.coords == current))
            //     {
            //         return current;
            //     }
            //     
            //     current += delta;
            // }
        }

        public List<UpgradeType> GetUpgradesFor(IAbility ability)
        {
            if (Upgrades == null)
                return new List<UpgradeType>();
            
            if (ability is Weapon)
            {
                return Upgrades.Where(u =>
                    u.mainType == GeneralUpgradeType.Weapon || u.mainType == GeneralUpgradeType.SwapWeapon).ToList();
            }
            
            // else, it's an ability
            var idx = AbilityTypes.IndexOf((ability as AbstractAbility).type);
            return Upgrades.Where(u => u.mainType == GeneralUpgradeType.Ability && u.abilityIdx == idx).ToList();
        }
    }
}
