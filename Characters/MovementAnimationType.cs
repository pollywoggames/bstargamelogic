﻿namespace BarnardsStar.Characters
{
    public enum MovementAnimationType
    {
        Smooth,
        Bob,
        Wobble,
        Roll,
        HeavyBob
    }
}