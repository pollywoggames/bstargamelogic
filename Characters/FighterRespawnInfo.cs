﻿using System;
using BarnardsStar.Primitives;

namespace BarnardsStar.Characters
{
    public class FighterRespawnInfo
    {
        public bool facingLeft;
        public Point loc;
        public FighterPrototype proto;
        public int turnsTilRespawn;

        public FighterRespawnInfo(FighterPrototype proto, Point loc, bool facingLeft)
        {
            this.loc = loc;
            this.proto = proto;
            this.facingLeft = facingLeft;
            turnsTilRespawn = 0;
        }
    }
}
