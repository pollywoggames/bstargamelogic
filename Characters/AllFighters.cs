using System.Collections.Generic;
using BarnardsStar.Abilities;
using BarnardsStar.Hazards;
using BarnardsStar.Upgrades;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons;

namespace BarnardsStar.Characters
{
    public class AllFighters
    {
        public const int BaseHealth = 5, BaseShieldStrength = 0, BaseMoveSpeed = 4;

        private static AllFighters _instance;

        public Dictionary<FighterPrototype, FighterBlueprint> All;
        public Dictionary<Team, List<FighterPrototype>> HighDamageFighters, SupportFighters;
        public List<FighterBlueprint> Humans, Robots, Critters;

        public static AllFighters Instance => _instance ??= new AllFighters();

        public static FighterBlueprint Lookup(FighterPrototype prototype)
        {
            if (Instance.All.TryGetValue(prototype, out var fb))
                return fb;

            Logger.Warn("AllFighters.Lookup can't find FighterBlueprint for fighter: " + prototype);
            return new FighterBlueprint(prototype, "name?", Team.Human, 0);
        }

        public AllFighters()
        {
            var others = new List<FighterBlueprint>();
            
            // When adding a new fighter, be sure to add them under 
            // [MenuItem("Tools/Achievements/Unlock All Characters")],
            // as well as creating individual methods for unlocking them
            // in MenuOptions.cs.

            #region Humans
            
            // ********************** Human units *************************
            Humans = new List<FighterBlueprint>();

            Humans.Add(new FighterBlueprint(FighterPrototype.HumanEnforcer, "Enforcer", Team.Human, 3)
                .WithWeapon(BSWeapon.Pistol)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithAbility(BSAbility.KnockoutPunch)
                .WithAbility(BSAbility.Shield)
            
                .WithMovePotentialUpgrade(1)
                // upgrade pistol damage to 3
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Weapon, AbilityUpgradeType.Damage))
                // upgrade knockout punch damage to 3
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 0))
                // upgrade shield amount to 3
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 1), noun: "amount")
            );

            Humans.Add(new FighterBlueprint(FighterPrototype.HumanSniper, "Sniper", Team.Human, 2)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithWeapon(BSWeapon.Rifle)
                .WithAbility(BSAbility.ReadyAttack)
                .WithAbility(BSAbility.HealOnKill)
                .WithUseFemaleSFX(true)
            
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Weapon, AbilityUpgradeType.Damage))
                // Upgrade Ready Attack to be able to go off 2x per turn
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 0),
                    details: "to be able to trigger 2x per turn", noun: "")
                // upgrade HealOnKill to heal 2 -> 4
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 1), noun: "healing")
                // upgrade HealOnKill to overheal, giving a shield up to 2
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 1),
                    details: "to overheal, giving a shield up to 2", noun: "")
            
                .SetIsDamageCarry()
            );

            Humans.Add(new FighterBlueprint(FighterPrototype.HumanCommando, "Commando", Team.Human, 1)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Machete)
                .WithAbility(BSAbility.ChargeAttack)
                .WithAbility(BSAbility.ActiveRegeneration)
            
                .WithMovePotentialUpgrade(1)
                // Upgrade damage for machete and charge simultaneously
                .WithPotentialUpgrade(new MultiPotentialUpgrade(new List<PotentialUpgrade>()
                {
                    new PotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Weapon, AbilityUpgradeType.Damage)),
                    new PotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 0)),
                }) { descriptionOverride = "Upgrade <b>Machete</b> and <b>Charge</b> damage from 3 to 4" })
                // Upgrade charge cooldown
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Cooldown, 0))
                // Upgrade regen heal amount
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 1), noun: "healing")
            
                .SetIsDamageCarry()
            );

            Humans.Add(new FighterBlueprint(FighterPrototype.HumanBazookaman, "Rocketeer", Team.Human, 0)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithAbility(BSAbility.Bazooka)
                .WithAbility(BSAbility.PersonalCover)
            
                // change unarmed out for pistol
                .WithPotentialUpgrade(new UpgradeType(0, GeneralUpgradeType.SwapWeapon, newWeapon: BSWeapon.Pistol))
                // upgrade bazooka damage
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 0))
                // upgrade bazooka cooldown
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Cooldown, 0))
                // upgrade bazooka range
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Range, 0))
            
                .SetIsDamageCarry()
            );

            Humans.Add(new FighterBlueprint(FighterPrototype.HumanAmphibian, "Amphibian", Team.Human, 4)
                .WithHealth(BaseHealth + 1)
                .WithWeapon(BSWeapon.Revolver)
                .WithAbility(BSAbility.SprayWater)
                .WithAbility(BSAbility.Electrify)
                .WithUseFemaleSFX(true)
            
                .WithHealthPotentialUpgrade(1)
                // swap gun with Magnum
                .WithPotentialUpgrade(new UpgradeType(0, GeneralUpgradeType.SwapWeapon, newWeapon: BSWeapon.Magnum))
                // Upgrade Spray Water to larger AOE
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 0),
                    noun: "spray area", details: "to be 3 wide for half of its length")
                // upgrade electrify +2 damage
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 1))
            );
            
            Humans.Add(new FighterBlueprint(FighterPrototype.HumanChemistSupport, "Mad Scientist", Team.Human, 5)
                .WithWeapon(BSWeapon.Sidearm)
                .WithAbility(BSAbility.ElectrolysisGrenade)
                .WithAbility(BSAbility.WaterGrenade)
            
                .WithMovePotentialUpgrade(1)
                .WithPotentialUpgrade(new UpgradeType(0, GeneralUpgradeType.SwapWeapon, newWeapon: BSWeapon.Magnum))
                // Upgrade Electrolysis Grenade AOE
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 0),
                    noun: "water area", details: "to a cross shape")
                // Upgrade Water Grenade AOE
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 1),
                    noun: "", details: "to greatly increase water area")
            );
            
            Humans.Add(new FighterBlueprint(FighterPrototype.HumanMedic, "Medic", Team.Human, 6)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithAbility(BSAbility.ElectricPunch)
                .WithAbility(BSAbility.MedKit)
                .WithUseFemaleSFX(true)
            
                .WithHealthPotentialUpgrade(1)
                .WithMovePotentialUpgrade(1)
                // Upgrade Electric Punch damage by 2
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 0), details: "from 2 to 4")
                // Upgrade Med Kit range by 2
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Range, 1))
            );
            
            Humans.Add(new FighterBlueprint(FighterPrototype.HumanEngineer, "Ranger", Team.Human, 7)
                .WithWeapon(BSWeapon.Shotgun)
                .WithAbility(BSAbility.LandMine)
                .WithAbility(BSAbility.SmokeGrenade)
            
                .WithMovePotentialUpgrade(1)
                // upgrade shotgun range
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Weapon, AbilityUpgradeType.Range))
                // upgrade land mine damage
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 0), details: "from 2 to 3")
                // upgrade smoke nade AOE
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 1),
                    noun: "", details: "to greatly increase coverage area")
            
                .SetIsDamageCarry()
            );
            
            Humans.Add(new FighterBlueprint(FighterPrototype.HumanPunk, "Punk", Team.Human, 8)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithWeapon(BSWeapon.MachineGun)
                .WithUseFemaleSFX(true)
                .WithAbility(BSAbility.DisplacementCharge)
                .WithAbility(BSAbility.OverloadAbility)
            
                .WithHealthPotentialUpgrade(1)
                // upgrade shotgun range
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Weapon, AbilityUpgradeType.Range))
                // Displacement Charge range
                .WithPotentialUpgrade(new UpgradeType(2, GeneralUpgradeType.Ability, AbilityUpgradeType.Range, 0))
                // upgrade Overload so it doesn't shock self
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Misc, 1),
                    noun: "", details: "not to shock self")
            
                .SetIsDamageCarry()
            );
            
            Humans.Add(new FighterBlueprint(FighterPrototype.HumanArchitect, "Architect", Team.Human, 9)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithAbility(BSAbility.TeleportGunAbility)
                .WithAbility(BSAbility.GlueGun)
                .WithUseFemaleSFX(true)
            
                .WithMovePotentialUpgrade(1)
                // Upgrades Teleport Gun range and teleport distance
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Range, 0),
                    noun: "range and teleport distance", details: "by 1")
                // Upgrade TP gun cooldown
                .WithPotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Cooldown, 0))
                // Upgrade Glue Gun range and damage
                .WithPotentialUpgrade(new MultiPotentialUpgrade(new List<PotentialUpgrade>()
                {
                    new PotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Range, 1)),
                    new PotentialUpgrade(new UpgradeType(1, GeneralUpgradeType.Ability, AbilityUpgradeType.Damage, 1)),
                }) { descriptionOverride = "Upgrade <b>Glue Gun</b> damage and range by 1" })
            );
            
            #endregion

            #region Robots
            
            // ************************* Robot units **********************
            Robots = new List<FighterBlueprint>();

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotTank, "Antivirus", Team.Robot, 4)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Pistol)
                .WithMoveType(MovementAnimationType.Roll)
                .WithAbility(BSAbility.DeployCover)
                .WithAbility(BSAbility.Shield)
                .WithDisableMuzzleFlash(true)
            );

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotOverwatch, "Overwatch", Team.Robot, 2)
                .WithWeapon(BSWeapon.Rifle)
                .WithAbility(BSAbility.BusterShot)
                .WithAbility(BSAbility.ReadyAttack)
            
                .SetIsDamageCarry()
            );

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotExterminator, "Exterminator", Team.Robot, 0)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.PlasmaSword)
                .WithAbility(BSAbility.Taunt)
                .WithAbility(BSAbility.ReflectProjectiles)
            
                .SetIsDamageCarry()
            );

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotSummoner, "Daemon", Team.Robot, 3)
                .WithWeapon(BSWeapon.Pistol)
                .WithMoveType(MovementAnimationType.Smooth)
                .WithAbility(BSAbility.SummonMinion)
                .WithDisableMuzzleFlash(true)
            );

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotOiler, "Firewall", Team.Robot, 1)
                .WithAbility(BSAbility.Flamethrower)
                .WithAbility(BSAbility.OilBarrelThrow)
            
                .SetIsDamageCarry()
            );
            
            Robots.Add(new FighterBlueprint(FighterPrototype.RobotChemistSupport, "Chembot", Team.Robot, 6)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithImmunity(HazardType.Oil)
                .WithWeapon(BSWeapon.PlasmaSword)
                .WithAbility(BSAbility.OilDash)
                .WithAbility(BSAbility.FireGrenade)
            );
            
            Robots.Add(new FighterBlueprint(FighterPrototype.RobotMedic, "Defragmenter", Team.Robot, 7)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithAbility(BSAbility.SketchySyringe)
                .WithAbility(BSAbility.ShieldBot)
            );
            
            Robots.Add(new FighterBlueprint(FighterPrototype.RobotEngineer, "Physics Engine", Team.Robot, 8)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Forklift)
                .WithAbility(BSAbility.Kinesis)
                .WithAbility(BSAbility.Shockwave)
            
                .SetIsDamageCarry()
            );

            Robots.Add(new FighterBlueprint(FighterPrototype.RobotCatalyst, "Catalyst", Team.Robot, 10)
                .WithWeapon(BSWeapon.PsiBlast)
                .WithAbility(BSAbility.Stasis)
                .WithAbility(BSAbility.SpeedBuff)
            );
            
            Robots.Add(new FighterBlueprint(FighterPrototype.RobotOmnibus, "Omnibus", Team.Robot, 9)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithWeapon(BSWeapon.MachineGun)
                .WithAbility(BSAbility.PlaceTeleporter)
            
                .SetIsDamageCarry()
            );

            #endregion
            
            #region Critters
            
            // ************************* Critter units **********************
            Critters = new List<FighterBlueprint>();

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterGrump, "Grump", Team.Critter, 4)
                .WithHealth(BaseHealth + 1)
                .WithWeapon(BSWeapon.SmashAttack)
                .WithMoveSpeed(BaseMoveSpeed)
                .WithMoveType(MovementAnimationType.HeavyBob)
                .WithAbility(BSAbility.PassiveRegeneration)
                // we co-opt FemaleSFX for critters to make the big boys' voices really low
                .WithUseFemaleSFX(true)
                
                .SetIsDamageCarry()
            );

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterGunslinger, "Gunslinger", Team.Critter, 2)
                .WithWeapon(BSWeapon.DiagonalRifle)
                .WithAbility(BSAbility.ReadyAttack)
                .WithAbility(BSAbility.SelfHaste)

                .SetIsDamageCarry()
            );

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterBountyHunter, "Bounty Hunter", Team.Critter, 1)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Shotgun)
                .WithAbility(BSAbility.Hook)
                .WithAbility(BSAbility.SelfSmoke)
            
                .SetIsDamageCarry()
            );

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterPsion, "Psion", Team.Critter, 3)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.PsiBlast)
                .WithMoveType(MovementAnimationType.Smooth)
                .WithAbility(BSAbility.Hypertunnel)
                .WithAbility(BSAbility.MoveAfterAttack)
                .WithDisableMuzzleFlash(true)
            );

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterBlobber, "Blobber", Team.Critter, 0)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithMoveType(MovementAnimationType.Wobble)
                .WithAbility(BSAbility.AcidRain)
                .WithAbility(BSAbility.AcidBomb)
                .WithImmunity(HazardType.Acid)
            );

            Critters.Add(new FighterBlueprint(FighterPrototype.CritterChemistSupport, "Acid Junkie", Team.Critter, 5)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithAbility(BSAbility.AcidDash)
                .WithAbility(BSAbility.AcidSurfing)
                .WithImmunity(HazardType.Acid)
            );
            
            Critters.Add(new FighterBlueprint(FighterPrototype.CritterMedic, "Gargoyle", Team.Critter, 6)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.SavagePeck)
                .WithAbility(BSAbility.Transfusion)
                .WithAbility(BSAbility.Fly)
            
                .SetIsDamageCarry()
            );
            
            Critters.Add(new FighterBlueprint(FighterPrototype.CritterEngineer, "Overlord", Team.Critter, 7)
                .WithMoveType(MovementAnimationType.Wobble)
                .WithWeapon(BSWeapon.PsiBlast)
                .WithAbility(BSAbility.AcidBubble)
                .WithAbility(BSAbility.SummonFly)
            );
            
            Critters.Add(new FighterBlueprint(FighterPrototype.CritterBeast, "Beast", Team.Critter, 8)
                .WithHealth(BaseHealth + 1)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.BashSelfDamageAttack)
                .WithMoveType(MovementAnimationType.HeavyBob)
                .WithAbility(BSAbility.BodySlam)
                .WithAbility(BSAbility.ExplodeIntoAcidOnDeath)
                // we co-opt FemaleSFX for critters to make the big boys' voices really low
                .WithUseFemaleSFX(true)
            
                .SetIsDamageCarry()
            );
            
            Critters.Add(new FighterBlueprint(FighterPrototype.CritterLurker, "Lurker", Team.Critter, 9)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.DashStrike)
                .WithAbility(BSAbility.Burrow)
            
                .SetIsDamageCarry()
            );
            
            #endregion
            
            #region Campaign Enemies

            // ****** Human campaign enemies ******
            
            others.Add(new FighterBlueprint(FighterPrototype.HumanMiniMelee, "Gnome", Team.Human, 10)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithHealth(3)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.HumanMeleeCreep, "Private", Team.Human, 11)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Machete)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.HumanRangedCreep, "Space Cadet", Team.Human, 12)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Pistol)
            );
            
            // ****** Robot campaign enemies ******
            
            others.Add(new FighterBlueprint(FighterPrototype.RobotMiniMelee, "Worker Process", Team.Robot, 11)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithHealth(3)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.RobotMeleeCreep, "Task Manager", Team.Robot, 12)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Machete)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.RobotRangedCreep, "Virus", Team.Robot, 13)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Pistol)
            );
            
            // ****** Critter campaign enemies ******
            
            others.Add(new FighterBlueprint(FighterPrototype.CritterMiniMelee, "Janitor", Team.Critter, 11)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithHealth(3)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.CritterMeleeCreep, "Grunt", Team.Critter, 12)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Machete)
            );
            
            others.Add(new FighterBlueprint(FighterPrototype.CritterRangedCreep, "Hooligan", Team.Critter, 13)
                .WithMoveSpeed(BaseMoveSpeed + 1)
                .WithWeapon(BSWeapon.Pistol)
            );
            
            #endregion

            #region Summonable Units

            // *************************** Summonable units ************************
            
            others.Add(new FighterBlueprint(FighterPrototype.RobotMinion, "Minion", Team.Robot, 5)
                .WithHealth(5)
                .WithMoveSpeed(3)
                .WithMoveType(MovementAnimationType.Smooth)
                .WithWeapon(BSWeapon.Sidearm)
                .WithAbility(BSAbility.OilRain)
                .WithDisableMuzzleFlash(true)
            );

            others.Add(new FighterBlueprint(FighterPrototype.CritterFly, "Housefly", Team.Critter, 10)
                .WithHealth(5)
                .WithMoveSpeed(5)
                .WithMoveType(MovementAnimationType.Smooth)
                .WithWeapon(BSWeapon.UnarmedAttack)
                .WithAbility(BSAbility.AcidDrizzle)
            );
            
            #endregion
            
            // ***************************

            // Populate the "All" lookup dict
            var listAll = new List<FighterBlueprint>();
            listAll.AddRange(Humans);
            listAll.AddRange(Robots);
            listAll.AddRange(Critters);
            listAll.AddRange(others);
            All = new Dictionary<FighterPrototype, FighterBlueprint>();
            foreach (var fb in listAll)
            {
                All.Add(fb.proto, fb);
            }
            
            // populate the high damage/support fighters lists
            HighDamageFighters = new Dictionary<Team, List<FighterPrototype>>
            {
                [Team.Human] = new List<FighterPrototype>(),
                [Team.Robot] = new List<FighterPrototype>(),
                [Team.Critter] = new List<FighterPrototype>(),
            };
            SupportFighters = new Dictionary<Team, List<FighterPrototype>>
            {
                [Team.Human] = new List<FighterPrototype>(),
                [Team.Robot] = new List<FighterPrototype>(),
                [Team.Critter] = new List<FighterPrototype>(),
            };
            foreach (var fb in listAll)
            {
                if (fb.IsDamageCarry)
                    HighDamageFighters[fb.team].Add(fb.proto);
                else
                    SupportFighters[fb.team].Add(fb.proto);
            }
        }

        public List<FighterBlueprint> FightersByTeam(Team t)
        {
            switch (t)
            {
                default:
                case Team.Human:
                    return Humans;
                case Team.Robot:
                    return Robots;
                case Team.Critter:
                    return Critters;
            }
        }
    }
}