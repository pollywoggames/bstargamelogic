﻿using System;
using BarnardsStar.Primitives;

namespace BarnardsStar.Characters
{
    public class SerializedSpawnPoint
    {
        public Coords coords;
        public bool facingLeft;
        public TeamColor teamColor;
    }
}
