using System.Collections;
using BarnardsStar.Players;

namespace BarnardsStar.Characters
{
    public interface IDamageable
    {
        Player GetOwner();
        IEnumerator DealDamage(int damage, Fighter origin);
        int GetEffectiveHP();
    }
}