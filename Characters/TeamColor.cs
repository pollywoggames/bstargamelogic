﻿using System;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Games;

#if UNITY_2020_3_OR_NEWER
using UnityEngine;
#endif

namespace BarnardsStar.Characters
{
    [Serializable]
    public enum TeamColor
    {
        // Regular team colors
        Blue,
        Red,
        Green,
        Purple,
        Yellow,
        Brown,
        
        // Extra team colors for up to 3v3
        Blue2,
        Blue3,
        Red2,
        Red3
    }

    public static class TeamColorExtensions
    {
        public static readonly List<TeamColor> NormalTeamColors = new List<TeamColor>()
        {
            TeamColor.Blue,
            TeamColor.Red,
            TeamColor.Green,
            TeamColor.Purple,
            TeamColor.Yellow,
            TeamColor.Brown,
        };

        public static readonly List<TeamColor> IconPickableTeamColors = new List<TeamColor>()
        {
            TeamColor.Blue,
            TeamColor.Red,
            TeamColor.Green,
            TeamColor.Purple,
            TeamColor.Yellow,
            TeamColor.Brown
        };

        public static readonly List<TeamColor> BlueTeamColors = new List<TeamColor>()
        {
            TeamColor.Blue,
            TeamColor.Blue2,
            TeamColor.Blue3,
        };
        
        public static readonly List<TeamColor> RedTeamColors = new List<TeamColor>()
        {
            TeamColor.Red,
            TeamColor.Red2,
            TeamColor.Red3,
        };
        
        public static readonly List<TeamColor> GreenTeamColors = new List<TeamColor>()
        {
            TeamColor.Green,
            TeamColor.Yellow,
        };

        /// <summary>
        /// Normalize extra team colors (e.g. blue2) to regular team colors (e.g. blue)
        /// </summary>
        public static TeamColor ToRegularTeamColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue2 => TeamColor.Blue,
                TeamColor.Blue3 => TeamColor.Blue,
                TeamColor.Red2 => TeamColor.Red,
                TeamColor.Red3 => TeamColor.Red,
                _ => teamColor
            };
        }
        
        public static VFXColor ToVFXColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => VFXColor.Blue,
                TeamColor.Red => VFXColor.Red,
                TeamColor.Green => VFXColor.Green,
                TeamColor.Purple => VFXColor.Purple,
                TeamColor.Yellow => VFXColor.Yellow,
                TeamColor.Brown => VFXColor.Brown,
                TeamColor.Blue2 => VFXColor.Blue,
                TeamColor.Blue3 => VFXColor.Blue,
                TeamColor.Red2 => VFXColor.Red,
                TeamColor.Red3 => VFXColor.Red,
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }

#if UNITY_2020_3_OR_NEWER
        public static Color GetUnityVFXColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => new Color(.3f, .67f, 1f),
                TeamColor.Red => new Color(1f, 0, 0),
                TeamColor.Green => new Color(0f, 1f, 0),
                TeamColor.Purple => new Color(.78f, 0f, 1f),
                TeamColor.Yellow => new Color(1f, 1f, 0),
                TeamColor.Brown => new Color(1f, .3f, 0),
                TeamColor.Blue2 => new Color(.3f, .67f, 1f),
                TeamColor.Blue3 => new Color(.3f, .67f, 1f),
                TeamColor.Red2 => new Color(1f, 0, 0),
                TeamColor.Red3 => new Color(1f, 0, 0),
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }
#endif

        public static string GetTextColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => "A4DBE8",
                TeamColor.Red => "FF7276",
                TeamColor.Green => "44CC66",
                TeamColor.Purple => "CC44CC",
                TeamColor.Yellow => "d5d90b",
                TeamColor.Brown => "785011",
                TeamColor.Blue2 => "A4DBE8",
                TeamColor.Blue3 => "A4DBE8",
                TeamColor.Red2 => "FF7276",
                TeamColor.Red3 => "FF7276",
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }
        
        public static string GetBrightTextColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => "33BBFF",
                TeamColor.Red => "ff4444",
                TeamColor.Green => "33FF33",
                TeamColor.Purple => "c45eff",
                TeamColor.Yellow => "ffff00",
                TeamColor.Brown => "C29A6E",
                TeamColor.Blue2 => "33BBFF",
                TeamColor.Blue3 => "33BBFF",
                TeamColor.Red2 => "ff4444",
                TeamColor.Red3 => "ff4444",
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }

        public static (float, float, float) GetUIColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => (.4f, .7f, 1f),
                TeamColor.Red => (1f, .4f, .4f),
                TeamColor.Green => (.4f, .9f, .7f),
                TeamColor.Purple => (.85f, .4f, 1f),
                TeamColor.Yellow => (1f, 1f, 0),
                TeamColor.Brown => (.76f, .6f, .43f),
                TeamColor.Blue2 => (.4f, .7f, 1f),
                TeamColor.Blue3 => (.4f, .7f, 1f),
                TeamColor.Red2 => (1f, .4f, .4f),
                TeamColor.Red3 => (1f, .4f, .4f),
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }
        
        public static (float, float, float) GetBrightUIColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => (0f, .5f, 1f),
                TeamColor.Red => (1f, 0f, 0f),
                TeamColor.Green => (0f, 1f, .2f),
                TeamColor.Purple => (.8f, 0f, .8f),
                TeamColor.Yellow => (1f, 1f, 0),
                TeamColor.Brown => (.5f, .3f, 0),
                TeamColor.Blue2 => (0f, .5f, 1f),
                TeamColor.Blue3 => (0f, .5f, 1f),
                TeamColor.Red2 => (1f, 0f, 0f),
                TeamColor.Red3 => (1f, 0f, 0f),
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }

        public static MovablePropType ToShieldBotColor(this TeamColor teamColor)
        {
            return teamColor switch
            {
                TeamColor.Blue => MovablePropType.ShieldBotBlue,
                TeamColor.Red => MovablePropType.ShieldBotRed,
                TeamColor.Green => MovablePropType.ShieldBotGreen,
                TeamColor.Purple => MovablePropType.ShieldBotPurple,
                TeamColor.Yellow => MovablePropType.ShieldBotYellow,
                TeamColor.Brown => MovablePropType.ShieldBotBrown,
                TeamColor.Blue2 => MovablePropType.ShieldBotBlue,
                TeamColor.Blue3 => MovablePropType.ShieldBotBlue,
                TeamColor.Red2 => MovablePropType.ShieldBotRed,
                TeamColor.Red3 => MovablePropType.ShieldBotRed,
                _ => throw new ArgumentOutOfRangeException(nameof(teamColor), teamColor, null)
            };
        }

        public static TeamColor GetTeamColorByPlayerIdx(int playerIdx, int numPlayers, TeamsType teamsType)
        {
            if (teamsType == TeamsType.FreeForAll)
                return (TeamColor) playerIdx;
            
            var teamIdx = teamsType.CalcPlayerTeamIdx(numPlayers, playerIdx);
                    
            // e.g. for 4-player game:
            // idx 0 = TeamColor.Blue
            // idx 1 = TeamColor.Blue2
            // idx 2 = TeamColor.Red
            // idx 3 = TeamColor.Red2
            var numPlayersOnFirstTeam = teamsType.NumPlayersOnFirstTeam(numPlayers);
            var whichTeamColorArray = teamIdx switch
            {
                0 => BlueTeamColors,
                1 => RedTeamColors,
                2 => GreenTeamColors,
                _ => throw new ArgumentOutOfRangeException(nameof(teamIdx), teamIdx, 
                    $"GetTeamColorByPlayerIdx: invalid teamIdx. Args: {playerIdx}, {numPlayers}, {teamsType}")
            };
            return whichTeamColorArray[playerIdx % numPlayersOnFirstTeam];
        }
    }
}