﻿using System;
using System.Collections.Generic;

namespace BarnardsStar.Characters
{
    public enum Team
    {
        Human,
        Robot,
        Critter
    }

    public static class TeamExtensions
    {
        private static readonly List<FighterPrototype> HumanMascots = new List<FighterPrototype>()
        {
            FighterPrototype.HumanSniper,
            FighterPrototype.HumanEnforcer,
            FighterPrototype.HumanCommando,
            FighterPrototype.HumanBazookaman
        };
        
        private static readonly List<FighterPrototype> RobotMascots = new List<FighterPrototype>()
        {
            FighterPrototype.RobotOverwatch,
            FighterPrototype.RobotTank,
            FighterPrototype.RobotExterminator,
            FighterPrototype.RobotSummoner
        };
        
        private static readonly List<FighterPrototype> CritterMascots = new List<FighterPrototype>()
        {
            FighterPrototype.CritterGunslinger,
            FighterPrototype.CritterGrump,
            FighterPrototype.CritterBountyHunter,
            FighterPrototype.CritterPsion
        };
        
        public static List<FighterPrototype> GetMascots(this Team team)
        {
            return team switch
            {
                Team.Human => HumanMascots,
                Team.Robot => RobotMascots,
                Team.Critter => CritterMascots,
                _ => throw new ArgumentOutOfRangeException(nameof(team), team, null)
            };
        }
    }
}