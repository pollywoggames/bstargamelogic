using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using System.Runtime.Serialization;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Preview.PreviewOutcomes;
using Newtonsoft.Json;
using Logger = BarnardsStar.Utilities.Logger;
using Vector3 = System.Numerics.Vector3;

namespace BarnardsStar.Maps
{
    [DataContract]
    public class GameMap : IGameMap
    {
        public static readonly Point CoverTilemapOffset = new Point(0, .2f);
        public const float BurrowAnimationTime = 1.2f;

        [DataMember] public MapSettings mapSettings;
        [DataMember] public MapTerrain _terrain;
        [DataMember] public HashSet<Coords> damagedCover;
        [DataMember] public HashSet<Coords> singedGround;

        public MapBounds bounds => _terrain.bounds;
        
        // For dynamically placed teleporters/burrows
        [DataMember] public int nextBurrowChannel = 500;
        [DataMember] public int nextTeleporterChannel = 100;
        
        private List<Teleporter> _teleporters;
        
        [DataMember]
        public List<Teleporter> teleporters {
            get => _teleporters;
            set
            {
                if (value == null)
                {
                    _teleporters = null;
                    _teleportersByCoords = null;
                    _teleporterPairs = null;
                }
                else
                {
                    _teleporters = value;
                    // _teleportersByCoords = value.ToDictionary(tp => tp.coords, tp => tp);
                    
                    _teleportersByCoords = new Dictionary<Coords, Teleporter>();
                    foreach (var tp in _teleporters)
                    {
                        _teleportersByCoords[tp.coords] = tp;
                    }
                    
                    _teleporterPairs = BuildTeleporterPairs(value);
                }
            }
        }

        private static Dictionary<Teleporter, Teleporter> BuildTeleporterPairs(List<Teleporter> teleporters)
        {
            var ret = new Dictionary<Teleporter, Teleporter>();
            foreach (var teleporter in teleporters)
            {
                var other = teleporters.FirstOrDefault(tp =>
                    tp.coords != teleporter.coords && tp.channel == teleporter.channel);
                
                if (other != null)
                {
                    ret[teleporter] = other;
                }
            }
            return ret;
        }

        public int deployableCoverTileCode { get; set; }

        [DataMember] public Game game { get; set; }

        // ability to reference nexi without having a game reference
        public Nexus[] tempNexi;

        [JsonIgnore] public bool hasVirtualActor => virtualActor != null && !virtualActor.IsDestroyed();
        public IMapVirtualActor virtualActor;
        public Dictionary<Coords, Teleporter> _teleportersByCoords;
        public Dictionary<Teleporter, Teleporter> _teleporterPairs;

        private HashSet<Coords> _nexiCoords;

        public HashSet<Coords> nexiCoords
        {
            get
            {
                if (_nexiCoords == null)
                {
                    if (game != null)
                    {
                        _nexiCoords = game.Players
                            .Where(p => !p.OutOfGame && p.nexus != null)
                            .SelectMany(p => p.nexus.AllCoords)
                            .ToSet();
                    }
                    else if (tempNexi != null)
                    {
                        _nexiCoords = tempNexi.SelectMany(nexus => nexus.AllCoords).ToSet();
                    }
                    else
                    {
                        // If the game hasn't been initialized, and we don't have temp nexi, we have to return nothing.
                        // BUT! Don't let it get cached
                        return new HashSet<Coords>();
                    }
                }
                return _nexiCoords;
            }
        }

        public void UpdateNexiCoords()
        {
            _nexiCoords = null;
        }

        public IEnumerable<Coords> allPositionsWithin => _terrain.allPositionsWithin;

        public GameMap() {}

        public GameMap(IMapVirtualActor virtualActor, MapTerrain terrain, HashSet<Coords> damagedCover, HashSet<Coords> singedGround, List<Teleporter> teleporters)
        {
            this.virtualActor = virtualActor;
            this._terrain = terrain;
            this.damagedCover = damagedCover;
            this.singedGround = singedGround;
            this.teleporters = teleporters;
        }

        public GameMap Clone(Game gameClone)
        {
            var newVA = gameClone.contextProvider.ProvideMapVirtualActor();
            var newTerrain = _terrain.Clone();
            
            var newDamagedCover = new HashSet<Coords>();
            newDamagedCover.UnionWith(damagedCover);

            var newSingedGround = new HashSet<Coords>();
            if (singedGround != null)
                newSingedGround.UnionWith(singedGround);

            var newTeleporters = teleporters.Select(t =>
            {
                Player ownerClone = null;
                if (t.Owner != null)
                {
                    ownerClone = gameClone.Players[t.Owner.Idx];
                }
                
                return t.Clone(gameClone, ownerClone);
            }).ToList();

            var newMap = new GameMap(newVA, newTerrain, newDamagedCover, newSingedGround, newTeleporters)
            {
                game = gameClone,
                nextBurrowChannel = nextBurrowChannel,
                nextTeleporterChannel = nextTeleporterChannel,
                mapSettings = mapSettings.Clone(),
            };
            return newMap;
        }

        public Terrain TerrainAt(Coords coords)
        {
            return _terrain.TerrainAt(coords);
        }

        public void SetTerrainAt(Coords coords, Terrain t)
        {
            _terrain.SetTerrainAt(coords, t);
        }

        public TerrainType GetTerrainType(Coords coords, bool countHazards = true)
        {
            if (nexiCoords.Contains(coords))
                return TerrainType.Cover;

            if (countHazards && game.TryGetHazard(coords, out var h) && h.actsAsCover)
                return TerrainType.Cover;
            
            if (!bounds.ContainsCoords(coords))
                return TerrainType.Wall;

            return TerrainAt(coords).terrainType;
        }

        public IEnumerator DamageCover(Coords coords, int damage, Point origin, Fighter triggerer)
        {
            if (GetTerrainType(coords) != TerrainType.Cover)
                yield break;
            
            if (game.TryGetHazard(coords, out var h) && h.targetable)
            {
                yield return (h.OnDamage(triggerer));
                yield break;
            }

            if (game.contextProvider.IsPreviewing())
            {
                game.contextProvider.RegisterPreview(new CoverDamagePreview(game, coords, damage));

                if (triggerer != null && triggerer.Owner.nexus != null &&
                    triggerer.Owner.nexus.AllCoords.Contains(coords))
                {
                    game.contextProvider.RegisterPreview(new NexusSelfDamagePreview(game));
                }
            }
            else
            {
                game.soundCtx.PlayOneShotVaryPitch(Sound.ClawHit);
            }

            if (damagedCover.Contains(coords) || damage >= 2)
            {
                // destroyed
                damagedCover.Remove(coords);

                game.SomethingHappened(new BSEvent(BSEventType.DestroyCover, triggerer));

                var terrainThere = TerrainAt(coords); 
                terrainThere.terrainType = TerrainType.Clear;
                terrainThere.coverTileCode = 0;
                SetTerrainAt(coords, terrainThere);
                
                singedGround ??= new HashSet<Coords>();
                singedGround.Add(coords);
                
                virtualActor.DestroyCover(coords, origin);
            }
            else if (!nexiCoords.Contains(coords))
            {
                // just damaged
                damagedCover.Add(coords);
                virtualActor.DamageCover(coords, origin);
            }
        }

        public bool IsCoverDamaged(Coords coords)
        {
            return GetTerrainType(coords) == TerrainType.Cover && damagedCover.Contains(coords);
        }

        public IEnumerator DamageCoverOrNexus(Fighter actor, int coverDamage, Coords targetCoords)
        {
            if (GetTerrainType(targetCoords) != TerrainType.Cover)
                yield break;
            
            // was the cover we hit actually a nexus?
            bool hitNexus = false;
            foreach (var enemyNexus in actor.Owner.EnemyNexi)
            {
                if (enemyNexus.AllCoords.Contains(targetCoords))
                {
                    yield return enemyNexus.DealDamage(1, actor);
                    hitNexus = true;
                }
            }

            if (!hitNexus)
                yield return game.map.DamageCover(targetCoords, coverDamage, actor.pos, actor);
        }

        public bool IsDamageableTeleporter(Coords c)
        {
            return _teleportersByCoords.TryGetValue(c, out var tp) && tp.type == TeleporterType.Placed;
        }

        public void DamagePlacedTeleporter(Coords c)
        {
            if (_teleportersByCoords.TryGetValue(c, out var tp) && tp.type == TeleporterType.Placed)
            {
                RemoveTeleporter(tp);
            }
        }
        
        public IEnumerator DeployCover(Coords coords, Fighter deployer)
        {
            if (game.contextProvider.IsPreviewing())
                game.contextProvider.RegisterPreview(new DeployCoverPreview(game, coords));
            
            var t = TerrainAt(coords);
            t.terrainType = TerrainType.Cover;
            // make this generic if we have deployable cover other than cones
            t.coverTileCode = deployableCoverTileCode;
            SetTerrainAt(coords, t);

            if (damagedCover.Contains(coords))
                damagedCover.Remove(coords);
            
            virtualActor.DeployCover(coords);
            
            if (game.TryGetHazard(coords, out var h))
            {
                if (h.explosive)
                    yield return h.Explode(deployer);
                    
                game.RemoveHazard(h);
            }
            
            if (_teleportersByCoords.TryGetValue(coords, out var tp) && tp.type != TeleporterType.Fixed)
                RemoveTeleporterAndMaybeBurrowPair(tp);
        }

        public void SetFloorTile(Coords coords, int tileCode)
        {
            var t = TerrainAt(coords);
            t.floorTileCode = tileCode;
            virtualActor.SetTerrain(coords, t);
        }

        public bool IsOdd()
        {
            return mapSettings.IsOdd();
        }

        public bool IsClear(Coords coords, List<Coords> overrideBlockedCoords = null, bool ignoreFighters = false)
        {
            return
                GetTerrainType(coords, !ignoreFighters) == TerrainType.Clear
                && (ignoreFighters || !game.IsFighterAt(coords))
                && !(overrideBlockedCoords != null && overrideBlockedCoords.Contains(coords));
        }

        public MapBounds GetBounds()
        {
            return bounds;
        }

        public bool IsClear(Coords coords, Fighter fighterToIgnore)
        {
            if (GetTerrainType(coords) != TerrainType.Clear)
                return false;

            if (game.TryGetFighter(coords, out var f))
            {
                // If there's a fighter there, then it's only clear if that fighter is fighterToIgnore. 
                return f == fighterToIgnore;
            }

            return true;
        }

        public Coords ClearCoordsBy(Coords coords, List<Coords> overrideBlockedCoords = null, bool ignoreFighters = false)
        {
            return this.FindClearCoordsBy(coords, overrideBlockedCoords, ignoreFighters);
        }

        public IEnumerator ShoveCover(Coords coords, Coords dstCoords, Fighter shover)
        {
            if (GetTerrainType(coords) != TerrainType.Cover)
            {
                Logger.Error("Can't shove cover at coords " + coords);
                yield break;
            }

            var startingPosition = coords.ToPoint() + CoverTilemapOffset;
            var coverProp = game.contextProvider.ProvideProp(MovablePropType.ShovedCover, startingPosition);

            // wait one frame so the coverProp can pick up the right sprites before we change the tiles
            yield return null;
            
            // clear terrain in starting spot
            bool damaged = IsCoverDamaged(coords);
            if (damaged)
            {
                damagedCover.Remove(coords);
                virtualActor.ClearCoverDamageAt(coords);
            }
            var tile = TerrainAt(coords);
            var origCoverTileCode = tile.coverTileCode;
            tile.coverTileCode = 0;
            SetTerrainAt(coords, tile);
            virtualActor.SetTerrain(coords, tile);

            // See if we're going to hit something or not.
            var destTerrainType = GetTerrainType(dstCoords);
            game.TryGetDamageable(dstCoords, out var damageableHit);
            bool bouncingBack = damageableHit != null
                                || destTerrainType == TerrainType.Cover
                                || destTerrainType == TerrainType.Wall;
            if (bouncingBack)
            {
                // push but bounce back
                var dst = dstCoords.ToPoint() + CoverTilemapOffset;
                var delta = dst - startingPosition;
                var halfway = delta * .5f + startingPosition;

                yield return game.animCtx.MoveSmoothly(coverProp, halfway, .1f);

                if (damageableHit != null)
                {
                    game.runCtx.Start(damageableHit.DealDamage(2, shover));
                }
                else if (destTerrainType == TerrainType.Cover)
                {
                    // if we hit cover, damage it
                    yield return DamageCover(dstCoords, 1, startingPosition, shover);
                }
                
                // just destroy instead of animating the move back
                coverProp.Destroy();
                
                // put back cover tile and get rid of prop
                tile.coverTileCode = origCoverTileCode;
                SetTerrainAt(coords, tile);
                virtualActor.SetTerrain(coords, tile);
                if (damaged)
                {
                    damagedCover.Add(coords);
                    virtualActor.SetCoverDamageAt(coords);
                }
                
                // damage self
                yield return DamageCover(coords, 1, dst, shover);
            }
            else if (destTerrainType == TerrainType.Pit)
            {
                // the cover will be destroyed, so make sure to preview it
                if (game.contextProvider.IsPreviewing())
                    game.contextProvider.RegisterPreview(new CoverDamagePreview(game, coords, 2));

                var dst = dstCoords.ToPoint() + CoverTilemapOffset;
                yield return game.animCtx.MoveSmoothly(coverProp, dst, .2f);
                
                // spins & falls down the hole
                float spinAngle = 0;
                var spinAmount = 10f;
                for (var i = 1f; i >= 0; i -= .01f)
                {
                    spinAngle += spinAmount;
                    coverProp.SetRotation(spinAngle);
                    coverProp.SetScale(new Vector3(i, i, i));
                    
                    yield return game.runCtx.WaitSeconds(.01f);
                }
                
                coverProp.Destroy();                
            }
            else if (destTerrainType == TerrainType.Clear)
            {
                if (game.contextProvider.IsPreviewing())
                    game.contextProvider.RegisterPreview(new ShoveCoverPreview(game, coords, dstCoords));

                // shove to new spot
                var dst = dstCoords.ToPoint() + CoverTilemapOffset;
                yield return game.animCtx.MoveSmoothly(coverProp, dst, .2f);

                coverProp.Destroy();

                bool placeNewCover = true;
                if (game.TryGetHazard(dstCoords, out var h))
                {
                    if (h.explosive)
                    {
                        // the explosion will destroy this cover
                        placeNewCover = false;
                        
                        yield return h.Explode(shover);
                    }
                    
                    game.RemoveHazard(h);
                }

                if (game.contextProvider.IsPreviewing())
                {
                    IPreviewAction preview = placeNewCover
                        ? (IPreviewAction) new ShoveCoverPreview(game, coords, dstCoords)
                        : (IPreviewAction) new CoverDamagePreview(game, coords, 2);
                    
                    game.contextProvider.RegisterPreview(preview);
                }

                if (placeNewCover)
                {
                    // create actual cover in the tilemap
                    var newTile = TerrainAt(dstCoords);
                    newTile.coverTileCode = origCoverTileCode;
                    SetTerrainAt(dstCoords, newTile);
                    virtualActor.SetTerrain(dstCoords, newTile);
                    if (damaged)
                    {
                        damagedCover.Add(dstCoords);
                        virtualActor.SetCoverDamageAt(dstCoords);
                    }
                }
            }
            else
            {
                Logger.Error("Unexpected terrain type: " + destTerrainType);
            }
        }

        public (Teleporter, Teleporter) AddBurrows(Fighter actor, Coords c1, Coords c2)
        {
            var owner = actor.Owner;
            
            if (game.contextProvider.IsPreviewing())
            {
                game.contextProvider.RegisterPreview(new CreateBurrowPreview(game, c1, c2));
                game.contextProvider.RegisterPreview(new CreateBurrowPreview(game, c2, c1));
            }
            
            if (!actor.tpChan.HasValue)
            {
                actor.tpChan = nextBurrowChannel;
                nextBurrowChannel++;
            }
            var newChannel = actor.tpChan.Value;
            
            var vActor1 = owner.game.contextProvider.ProvideBurrowVirtualActor(owner);
            var newBurrow1 = new Teleporter(vActor1, c1, newChannel, TeleporterType.Burrow, owner);
            
            var vActor2 = owner.game.contextProvider.ProvideBurrowVirtualActor(owner);
            var newBurrow2 = new Teleporter(vActor2, c2, newChannel, TeleporterType.Burrow, owner);
            
            AddTeleporterPair(newBurrow1, newBurrow2);

            return (newBurrow1, newBurrow2);
        }

        private void AddSingleTeleporter(Teleporter tp)
        {
            teleporters.Add(tp);
            _teleportersByCoords[tp.coords] = tp;
            
            // Check if there's another teleporter with that channel
            var otherTpWithChan = teleporters.FirstOrDefault(
                otherTP => otherTP.coords != tp.coords && otherTP.channel == tp.channel);
            if (otherTpWithChan != null)
            {
                _teleporterPairs[tp] = otherTpWithChan;
                _teleporterPairs[otherTpWithChan] = tp;
            }
        }

        private void AddTeleporterPair(Teleporter tp1, Teleporter tp2)
        {
            teleporters.Add(tp1);
            teleporters.Add(tp2);

            _teleportersByCoords[tp1.coords] = tp1;
            _teleportersByCoords[tp2.coords] = tp2;

            _teleporterPairs[tp1] = tp2;
            _teleporterPairs[tp2] = tp1;
        }

        public void RemoveTeleporterAndMaybeBurrowPair(Teleporter tp)
        {
            if (tp.isBurrow && _teleporterPairs.TryGetValue(tp, out var tpPair))
                RemoveTeleporter(tpPair);
            RemoveTeleporter(tp);
        }

        public void RemoveTeleporter(Teleporter tp)
        {
            teleporters.Remove(tp);
            RemoveTeleporter_Common(tp);
        }

        private void RemoveTeleporterAtIdx(int i)
        {
            var tp = teleporters[i];
            teleporters.RemoveAt(i);
            RemoveTeleporter_Common(tp);
        }

        // Do the actual work of removing the teleporter (everything but remove from `teleporters` list)
        private void RemoveTeleporter_Common(Teleporter tp)
        {
            if (game.contextProvider.IsPreviewing())
                game.contextProvider.RegisterPreview(new DestroyTeleporterPreview(game, tp.coords));

            _teleportersByCoords.Remove(tp.coords);
            _teleporterPairs.Remove(tp);
            if (_teleporterPairs.TryGetValue(tp, out var pair))
            {
                _teleporterPairs.Remove(pair);
            }
            
            if (tp.virtualActor != null && !tp.virtualActor.IsDestroyed())
            {
                tp.virtualActor.AnimateDestroy(game.animCtx);
            }
        }
        
        // Called before placing a new burrow for the player & actor.
        public void RemoveAllBurrowsForPlayer(Player player, int channel)
        {
            for (var i = teleporters.Count - 1; i >= 0; i--)
            {
                var tp = teleporters[i];
                if (tp.isBurrow && tp.Owner == player && tp.channel == channel)
                {
                    RemoveTeleporterAtIdx(i);
                }
            }
        }

        public Teleporter PlaceTeleporter(Fighter placer, Coords coords)
        {
            if (game.contextProvider.IsPreviewing())
            {
                game.contextProvider.RegisterPreview(new CreateTeleporterPreview(game, coords));
            }

            if (!placer.tpChan.HasValue)
            {
                placer.tpChan = nextTeleporterChannel;
                nextTeleporterChannel++;
            }
            var newChan = placer.tpChan.Value;
            
            // Check if there are already 2 teleporters of this channel, and if so, destroy the older one
            if (teleporters.Count(t => t.channel == newChan) >= 2)
            {
                // destroy the older one (first idx)
                var idx = teleporters.FindIndex(t => t.channel == newChan);
                RemoveTeleporterAtIdx(idx);
            }

            var owner = placer.Owner;
            var vActor = owner.game.contextProvider.ProvideTeleporterVirtualActor(newChan);
            var newTP = new Teleporter(vActor, coords, newChan, TeleporterType.Placed, owner);

            AddSingleTeleporter(newTP);
            
            return newTP;
        }
    }
}
