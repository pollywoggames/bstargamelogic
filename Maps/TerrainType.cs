namespace BarnardsStar.Maps
{
    public enum TerrainType
    {
        // No floor, fighters will fall down & die
        Pit,
        
        // Empty floor
        Clear,

        // Impenetrable, full-height wall
        Wall,

        // Half-height wall that fighters can hide behind
        Cover
    }
}