using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public class Path : IEquatable<Path>
    {
        private const float TeleporterMoveCostAdjustment = .01f;
        
        public List<Coords> spaces;
        public Fighter walker; // the fighter that's walking the path

        public Coords Start => spaces[0];
        public Coords End => spaces[^1];

        public Path(Fighter f, Coords newSpace, Path old)
        {
            walker = f;
            spaces = old != null ? new List<Coords>(old.spaces) : new List<Coords>();

            spaces.Add(newSpace);
        }

        public Path(Fighter f, List<Coords> spaces)
        {
            walker = f;
            this.spaces = spaces;
        }

        public bool Equals(Path other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            if (!Equals(walker, other.walker)) return false;
            
            var listsEqual = spaces.All(other.spaces.Contains) && spaces.Count == other.spaces.Count;
            return listsEqual;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Path) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((spaces != null ? spaces.GetHashCode() : 0) * 397) ^ (walker != null ? walker.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            var spacesStr = String.Join(",", spaces);
            return $"Path({walker.id},{spacesStr})";
        }

        public float GetScore()
        {
            // multiply by -1 so a higher score is better
            return -1 * (GetMoveCost() + GetDamage() * 2);
        }

        public int GetDamage()
        {
            var game = walker.game;
            
            var readyAttacks = game.CurrentReadyAttacksAgainstPlayer(walker.Owner);
            int damage = 0;
            foreach (var space in spaces)
            {
                // avoid hazards
                if (game.TryGetHazard(space, out var h) && !walker.Immunities.Contains(h.type))
                {
                    damage += h.PreviewDamage;
                }

                // avoid spaces that ready attack is targeting
                if (readyAttacks.TryGetValue(space, out var readyAttackDmg))
                {
                    damage += readyAttackDmg;
                }
            }


            return damage;
        }

        public float GetMovesLeft()
        {
            return walker.MovesLeft - GetMoveCost();
        }

        private float moveCost = -1;
        private bool slows;
        public float GetMoveCost()
        {
            // moveCost calculated lazily & cached. -1 means it has not been calculated yet.
            if (moveCost < 0)
            {
                bool surfing = walker.Surfing.HasValue;
                moveCost = 0;
                bool alreadySlowed = walker.HasStatusEffect(StatusEffect.Slowed);
                for (var i = 0; i < spaces.Count; i++)
                {
                    var sp = spaces[i];
                    float spaceMovementCost = 1f;
                    bool isLastSpace = i == spaces.Count - 1;

                    if (walker.game.TryGetHazard(sp, out var h))
                    {
                        if (surfing && walker.Surfing.Value == h.type)
                        {
                            spaceMovementCost = .5f;
                        }
                        else if (h.slows && !walker.Immunities.Contains(h.type) && !alreadySlowed
                                 // Don't count slow cost for the last space of the path -- if they're not slowed,
                                 // they can move here for the regular cost, they just won't be able to move further.
                                 && !isLastSpace)
                        {
                            slows = true;
                        }
                    }

                    moveCost += spaceMovementCost;
                }
            }

            if (slows)
                return moveCost + StatusEffectDetails.SlowMovementPenalty;
            else
                return moveCost;
        }
    }
}