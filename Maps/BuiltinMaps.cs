using System;
using System.Collections.Generic;
using System.Linq;

namespace BarnardsStar.Maps
{
    public static class BuiltinMaps
    {
        public const int NumTutorialMaps = 5;
        
        public static readonly List<MapSettings> All = new()
        {
            // anchorage 2p
            new MapSettings("AnchorageStation-0", "Anchorage Station - Standard", 2, MapColor.Grey, MapName.AnchorageStation),
            new MapSettings("AnchorageStation-1", "Anchorage Station - Chasm", 2, MapColor.Grey, MapName.AnchorageStation),
            new MapSettings("AnchorageStation-2", "Anchorage Station - Library 1", 2, MapColor.Grey, MapName.AnchorageStation),
            new MapSettings("AnchorageStation-3", "Anchorage Station - Library 2", 2, MapColor.Grey, MapName.AnchorageStation),
            // new MapSettings("AnchorageStation-4", "Anchorage Station - New Anchor", 2, MapColor.Grey, MapName.AnchorageStation),
            
            // anchorage 4p
            new MapSettings("AnchorageStation4P-0", "Anchorage Station - Auxiliary", 4, MapColor.Grey, MapName.AnchorageStation),
            new MapSettings("AnchorageStation4P-1", "Anchorage Station - Isolate", 4, MapColor.Grey, MapName.AnchorageStation),
            
            // marauder 2p
            new MapSettings("TheIcarus-0", "Marauder - Standard", 2, MapColor.Green, MapName.Marauder),
            new MapSettings("TheIcarus-1", "Marauder - Variation", 2, MapColor.Green, MapName.Marauder),
            new MapSettings("TheIcarus-2", "Marauder - Beltway", 2, MapColor.Green, MapName.Marauder),
            new MapSettings("TheIcarus-3", "Marauder - Island", 2, MapColor.Green, MapName.Marauder),
            
            // marauder 4p
            new MapSettings("Marauder4P-0", "Marauder - Corners", 4, MapColor.Green, MapName.Marauder),
            new MapSettings("Marauder4P-1", "Marauder - Slices", 4, MapColor.Green, MapName.Marauder),
            
            // phoenix 2p
            new MapSettings("TheKestrel-0", "Phoenix - Standard", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("TheKestrel-1", "Phoenix - Construction", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("TheKestrel-2", "Phoenix - Mess Hall", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("TheKestrel-3", "Phoenix - Tetris", 2, MapColor.Grey, MapName.Phoenix),
            // new MapSettings("TheKestrel-4", "Phoenix - Stripes", 2, MapColor.Grey, MapName.TheKestrel),
            // new MapSettings("TheKestrel-5", "Phoenix - Negative", 2, MapColor.Grey, MapName.TheKestrel),
            
            // phoenix 4p
            new MapSettings("Phoenix4P-0", "Phoenix - Transit", 4, MapColor.Grey, MapName.Phoenix),
            new MapSettings("Phoenix4P-1", "Phoenix - Two by Two", 4, MapColor.Grey, MapName.Phoenix),
            
            // 6p
            new MapSettings("Phoenix6P-0", "Hexagon", 6, MapColor.Grey, MapName.Phoenix),
        };

        public static readonly List<MapSettings> _2p = new();
        public static readonly List<MapSettings> _4p = new();
        public static readonly List<MapSettings> _6p = new();

        public static readonly List<MapSettings> TutorialMaps = new()
        {
            new MapSettings("Tutorial-0", "Tutorial-0", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("Tutorial-1", "Tutorial-1", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("Tutorial-2", "Tutorial-2", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("Tutorial-3", "Tutorial-3", 2, MapColor.Grey, MapName.Phoenix),
            new MapSettings("Tutorial-4", "Tutorial-4", 2, MapColor.Grey, MapName.AnchorageStation),
        };

        public static readonly List<MapSettings> BlankMaps = new()
        {
            new MapSettings("Blank-12", "Blank 12 x 12", 2, MapColor.Grey, null),
            new MapSettings("Blank-16", "Blank 16 x 16", 2, MapColor.Grey, null),
            new MapSettings("Blank-Anchorage", "Blank Anchorage", 2, MapColor.Grey, MapName.AnchorageStation),
            new MapSettings("Blank-Marauder", "Blank Marauder", 2, MapColor.Green, MapName.Marauder),
            new MapSettings("Blank-Phoenix", "Blank Phoenix", 2, MapColor.Grey, MapName.Phoenix),
        };

        private static readonly Dictionary<string, MapSettings> reallyAll = new();

        static BuiltinMaps()
        {
            foreach (var m in All)
            {
                m.IsBuiltin = true;
                
                if (m.NumPlayers == 2)
                    m.HideWalls = true;

                if (m.NumPlayers == 2) _2p.Add(m);
                if (m.NumPlayers == 4) _4p.Add(m);
                if (m.NumPlayers == 6) _6p.Add(m);

                reallyAll[m.Id] = m;
            }

            foreach (var m in TutorialMaps)
            {
                m.IsBuiltin = true;
                m.HideWalls = true;
                reallyAll[m.Id] = m;
            }
            
            foreach (var m in BlankMaps)
            {
                m.IsBuiltin = true;
                m.HideWalls = false;
                reallyAll[m.Id] = m;
            }
        }

        public static MapSettings LookupById(string id)
        {
            if (reallyAll.TryGetValue(id, out var m))
                return m;

            return null;
        }

        public static bool IsBuiltinMap(string mapId)
        {
            return reallyAll.ContainsKey(mapId);
        }
        
        public static MapSettings ChooseRandom(int numPlayers)
        {
            var rand = new Random();
            List<MapSettings> mapLayoutsToChooseFrom = numPlayers switch
            {
                (<= 2) => _2p,
                (<= 4) => _4p,
                (<= 6) => _6p,
                _ => throw new ArgumentOutOfRangeException($"Invalid number of players for random map: {numPlayers}")
            };
            return mapLayoutsToChooseFrom[rand.Next(0, mapLayoutsToChooseFrom.Count)];
        }
    }
}