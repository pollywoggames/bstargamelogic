using System;
using System.Collections.Generic;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public static class FloodFillPitTiles
    {
        public static List<Coords> GetPitTiles(IGameMap map)
        {
            var pits = new List<Coords>();
            var visited = new HashSet<Coords>();
            var toVisitStack = new Stack<Coords>();
            
            toVisitStack.Push(Coords.zero);
            // foreach (var c in Coords.AllDirections)
            //     toVisitStack.Push(c);
            // foreach (var c in Coords.TwoAway)
            //     toVisitStack.Push(c);

            var mapBounds = map.GetBounds();
            
            // If the map is not entirely surrounded by walls, we'll hit the failsafe
            int failsafe = 0;
            const int MaxFailsafe = 2_000;
            while (toVisitStack.Count > 0 && failsafe++ < MaxFailsafe)
            {
                var current = toVisitStack.Pop();

                if (map.GetTerrainType(current, false) == TerrainType.Pit)
                    pits.Add(current);

                visited.Add(current);

                foreach (var dir in Coords.OrthogonalDirections)
                {
                    var next = current + dir;
                    if (visited.Contains(next))
                        continue;

                    if (!mapBounds.ContainsCoords(next))
                    {
                        // we escaped! Map not contained by walls
                        return FloodFillPitTiles_NoEnclosingWalls(map, mapBounds);
                    }
                    
                    if (map.GetTerrainType(next, false) == TerrainType.Wall)
                    {
                        visited.Add(next);
                        continue;
                    }
                    
                    toVisitStack.Push(next);
                }
            }

            if (failsafe >= MaxFailsafe)
            {
                // we escaped! Map not contained by walls
                // Logger.Warn("FloodFillPitTiles hit failsafe");
                return FloodFillPitTiles_NoEnclosingWalls(map, mapBounds);
            }

            return pits;
        }

        private static List<Coords> FloodFillPitTiles_NoEnclosingWalls(IGameMap map, MapBounds mapBounds)
        {
            // Use a different algorithm to figure out which are pit tiles because there is
            // no enclosing perimeter of wall tiles.
            var pits = new List<Coords>();
            var alreadyChecked = new Dictionary<Coords, bool>();
            
            // reuse this to avoid excessive GC pressure
            List<Coords> reusableCheckedCoordsList = new(128);
            
            int maxDim = Math.Max(mapBounds.width, mapBounds.height);
            for (int i = 1; i <= maxDim; i++)
            {
                foreach (var coords in Coords.SquareAround(Coords.zero, i, map.IsOdd()))
                {
                    if (map.GetTerrainType(coords, false) != TerrainType.Pit)
                    {
                        // not a pit, don't worry about it
                        continue;
                    }

                    if (!DoesPitEscape(map, mapBounds, coords, alreadyChecked, reusableCheckedCoordsList))
                    {
                        pits.Add(coords);
                    }
                }
            }

            return pits;
        }

        private static bool DoesPitEscape(IGameMap map, MapBounds mapBounds, Coords coords, Dictionary<Coords, bool> alreadyChecked, List<Coords> reusableCheckedCoordsList)
        {
            const int MaxPitSize = 50;
            Coords direction = coords.GetDirectionAwayFromOrigin();
            
            reusableCheckedCoordsList.Clear();

            for (int i = 1; i <= MaxPitSize; i++)
            {
                Coords curr = coords + (direction * i);
                reusableCheckedCoordsList.Add(curr);

                if (alreadyChecked.TryGetValue(curr, out var result))
                {
                    // already checked that this coords escapes, return early
                    UpdateAlreadyCheckedDict(alreadyChecked, reusableCheckedCoordsList, result);
                    return result;
                }

                if (!mapBounds.ContainsCoords(curr))
                {
                    // too far out, haven't found any terrain
                    UpdateAlreadyCheckedDict(alreadyChecked, reusableCheckedCoordsList, true);
                    return true;
                }

                if (map.GetTerrainType(curr, false) != TerrainType.Pit)
                {
                    // found terrain
                    UpdateAlreadyCheckedDict(alreadyChecked, reusableCheckedCoordsList, false);
                    return false;
                }
            }

            UpdateAlreadyCheckedDict(alreadyChecked, reusableCheckedCoordsList, true);
            return true;
        }

        private static void UpdateAlreadyCheckedDict(Dictionary<Coords, bool> alreadyChecked, List<Coords> coords, bool result)
        {
            for (var i = 0; i < coords.Count; i++)
            {
                var c = coords[i];
                alreadyChecked[c] = result;
            }
        }
    }
}