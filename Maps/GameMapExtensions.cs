using System;
using System.Collections.Generic;
using BarnardsStar.Games;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public interface IGameMap
    {
        public bool IsOdd();
        
        public bool IsClear(Coords coords, List<Coords> overrideBlockedCoords = null, bool ignoreFighters = false);

        public MapBounds GetBounds();

        public TerrainType GetTerrainType(Coords coords, bool countHazards);
    }
    
    public static class GameMapExtensions
    {
        public static Coords FindClearCoordsBy(this IGameMap map, Coords coords, List<Coords> overrideBlockedCoords = null, bool ignoreFighters = false)
        {
            if (map.IsClear(coords, overrideBlockedCoords, ignoreFighters))
                return coords;
            
            // Try an expanding circle near the spawn point
            for (int i = 1; i < 10; i++)
            {
                foreach (var coordse in Coords.SquareAround(coords, i, map.IsOdd()))
                {
                    if (map.IsClear(coordse, overrideBlockedCoords, ignoreFighters))
                        return coordse;
                }
            }
            
            throw new Exception("Can't find clear coords by " + coords);
        }
        
        public static List<Coords> DetermineSpawnAreaForPlayer(this IGameMap map, Coords spawnPoint)
        {
            if (spawnPoint == MapPlayerInfo.None) return new List<Coords>();

            var spawnPoints = new List<Coords>() { spawnPoint };
            for (int j = 0; j < 3; j++)
            {
                var spawnCoords = map.FindClearCoordsBy(spawnPoint, spawnPoints, true);
                spawnPoints.Add(spawnCoords);
            }

            return spawnPoints;
        }
    }
}