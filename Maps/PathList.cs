using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public class PathList
    {
        private readonly Game game;
        public readonly Dictionary<Coords, Path> paths;
        private readonly Fighter walker;

        private GameMap map => game.map;

        public PathList(Game game, Fighter walker, Coords? startingCoords = null)
        {
            this.walker = walker;
            this.game = game;

            paths = new Dictionary<Coords, Path>();

            GetMovablePaths(startingCoords);
        }

        public static PathList GetMovableSpaces(Game game, Fighter walker)
        {
            var pl = new PathList(game, walker);
            return pl;
        }

        private void GetMovablePaths(Coords? startingCoords)
        {
            GoNext(startingCoords ?? walker.coords, null);
        }

        private void GoNext(Coords coords, Path currPath)
        {
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var newCoords = coords + dir;

                if (walker.Flier)
                {
                    if (
                        // Can fly over cover
                        map.GetTerrainType(newCoords) == TerrainType.Cover
                        // Can fly over pits
                        || map.GetTerrainType(newCoords) == TerrainType.Pit
                        // Can fly over enemies
                        || (game.TryGetFighter(newCoords, out var f) && f.Owner.Idx != walker.Owner.Idx)
                    ) {
                        newCoords = coords + 2 * dir;
                    }
                }
                else if (walker.Hypertunnel)
                {
                    if (map.GetTerrainType(newCoords) == TerrainType.Cover)
                    {
                        newCoords = coords + 2 * dir;
                    }
                }

                GetMovablePathsRecursive(new Path(walker, newCoords, currPath));
            }
        }

        private bool IsWalkable(Coords coords)
        {
            return map.GetTerrainType(coords) == TerrainType.Clear;
        }

        private void GetMovablePathsRecursive(Path newPath)
        {
            if (newPath.GetMovesLeft() < 0)
            {
                return;
            }

            var coords = newPath.End;

            if (coords == newPath.walker.coords)
            {
                return;
            }

            if (!IsWalkable(coords))
            {
                return;
            }

            var friendlyUnitPresent = false;
            var unit = game.FighterAt(coords);
            if (unit != null)
            {
                if (walker.IsFriendlyTo(unit))
                {
                    friendlyUnitPresent = true;
                }
                else
                {
                    return;
                }
            }

            // If it's a friendly unit space, they can move through the space, but not stop on it.
            if (!friendlyUnitPresent)
            {
                if (paths.TryGetValue(coords, out var otherPath))
                {
                    if (otherPath.GetScore() < newPath.GetScore())
                    {
                        // Only save this square if it's as good or better than the existing one
                        paths[coords] = newPath;
                    }
                }
                else
                {
                    paths.Add(coords, newPath);
                }
            }

            GoNext(coords, newPath);
        }
    }
}