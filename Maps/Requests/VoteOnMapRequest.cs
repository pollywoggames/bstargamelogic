namespace BarnardsStar.Maps.Requests
{
    public class VoteOnMapRequest
    {
        // 1: upvote
        // -1: downvote
        // 0: remove vote
        public int Vote;
    }
}