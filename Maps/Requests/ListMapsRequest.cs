namespace BarnardsStar.Maps.Requests
{
    public class ListMapsRequest
    {
        public MapBrowsingFilters Filters;
    
        // paging
        public int Limit, Offset;
    }
}