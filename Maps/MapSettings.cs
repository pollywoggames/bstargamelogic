using System;

namespace BarnardsStar.Maps
{
    public class MapSettings
    {
        public string Id;
        public string Name;
        
        public int NumPlayers;
        
        public MapColor mapColor;
        
        // re-using MapName enum but only using the first 3 values.
        public MapName? mapBackground;

        // Is it a built-in map? True for maps that are created and live in the Resources directory, & are shipped with
        // the app. False for any user-created maps.
        public bool IsBuiltin;

        public bool HideWalls;

        // true for draft (work-in-progress) maps, false when a map has been published
        public bool IsDraft;
        
        // empty constructor for JSON deserialization 
        public MapSettings() { }
        
        public MapSettings(string Id, string name, int numPlayers, MapColor mapColor, MapName? mapBackground)
        {
            this.Id = Id;
            this.Name = name;
            this.NumPlayers = numPlayers;
            this.mapColor = mapColor;
            this.mapBackground = mapBackground;
        }

        public MapSettings(string name, int numPlayers, MapColor mapColor, MapName? mapBackground)
            : this(Guid.NewGuid().ToString(), name, numPlayers, mapColor, mapBackground)
        { }

        public static MapSettings NewDraftMap()
        {
            return new MapSettings("Untitled Map", 2, MapColor.Grey, MapName.AnchorageStation)
            {
                IsDraft = true
            };
        }

        public override string ToString()
        {
            return $"MapSettings(Id={Id},Name={Name},NumPlayers={NumPlayers},mapColor={mapColor},mapBackground={mapBackground},IsBuiltin={(IsBuiltin ? "true" : "false")})";
        }
        
        public bool IsOdd()
        {
            return mapBackground == MapName.Phoenix;
        }

        public MapSettings Clone()
        {
            return new MapSettings(Id, Name, NumPlayers, mapColor, mapBackground)
            {
                IsBuiltin = IsBuiltin,
                HideWalls = HideWalls
            };
        }

        public bool WorksForNumPlayers(int numPlayers)
        {
            // 0 means any number of players
            if (numPlayers == 0)
                return true;
            
            // If it's an even number of players, it must be that exact number of players.
            // If it's an odd number of players, it can be that number of players or 1 more.
            // e.g. a 3-person game can be played on a 3-person or 4-person map, but a 2-person
            // game can only be played on a 2-person map.
            if (numPlayers % 2 == 0)
                return numPlayers == this.NumPlayers;
            else
                return numPlayers == this.NumPlayers || numPlayers + 1 == this.NumPlayers;
        }
    }
}