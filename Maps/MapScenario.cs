using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public class MapScenario
    {
        // key string is builtin map ID
        private static Dictionary<string, MapScenario> scenarios = new();

        static MapScenario()
        {
            scenarios.Add(
                "Tutorial-0",
                new MovementTutorial
                {
                    hasNexi = false,
                    hasPlayerTwo = false,
                    hideFighterWeapons = true,
                    hideFighterAbilities = true,
                    hideConfirmEndTurnPopup = true,
                    p1alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.HumanEngineer,
                        FighterPrototype.HumanEnforcer,
                        FighterPrototype.HumanCommando,
                        FighterPrototype.HumanBazookaman
                    },
                    winCondition = WinCondition.GetThroughTeleporter
                });

            scenarios.Add(
                "Tutorial-1",
                new AttackTutorial
                {
                    hasNexi = false,
                    hideFighterAbilities = true,
                    hideConfirmEndTurnPopup = true,
                    winCondition = WinCondition.KillAllEnemies,
                    p1alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.HumanSniper,
                        FighterPrototype.HumanEnforcer,
                    },
                    p2alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.CritterMeleeCreep,
                        FighterPrototype.CritterRangedCreep,
                    },
                    playerTwoAIType = AIType.DoNothing,
                    playerTwoGoesFirst = true,
                });

            scenarios.Add(
                "Tutorial-2",
                new AbilityTutorial
                {
                    hasNexi = false,
                    hideConfirmEndTurnPopup = true,
                    winCondition = WinCondition.GetThroughTeleporter,
                    p1alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.CritterBountyHunter,
                    },
                    p2alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.CritterGrump,
                    },
                    playerTwoAIType = AIType.DoNothing,
                    playerTwoGoesFirst = true,
                });
            
            scenarios.Add(
                "Tutorial-3",
                new CoverTutorial
                {
                    hasNexi = false,
                    winCondition = WinCondition.KillAllEnemies,
                    hideFighterAbilities = true,
                    hideConfirmEndTurnPopup = true,
                    p1alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.HumanEnforcer,
                        FighterPrototype.HumanSniper,
                    },
                    p2alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.CritterEngineer,
                    },
                    playerTwoAIType = AIType.DoNothing,
                    playerTwoGoesFirst = true,
                });
            
            scenarios.Add(
                "Tutorial-4",
                new FullGameTutorial
                {
                    hasNexi = true,
                    winCondition = WinCondition.DestroyEnemyNexus,
                    p1alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.HumanSniper,
                        FighterPrototype.HumanBazookaman,
                        FighterPrototype.HumanEnforcer,
                        FighterPrototype.HumanCommando,
                    },
                    p2alreadyChosenFighters = new List<FighterPrototype>
                    {
                        FighterPrototype.CritterGrump,
                        FighterPrototype.CritterGunslinger,
                        FighterPrototype.CritterMeleeCreep,
                        FighterPrototype.CritterRangedCreep,
                    },
                    playerTwoAIType = AIType.Medium,
                    playerTwoGoesFirst = true,
                    playerTwoDoNothingOnFirstTurn = true,
                });
        }
        
        public static MapScenario Lookup(string builtinMapId)
        {
            return scenarios.TryGetValue(builtinMapId, out var ret) ? ret : new MapScenario();
        }
        
        public bool hasNexi = true;
        public bool hasPlayerTwo = true;
        public bool hideFighterWeapons = false;
        public bool hideFighterAbilities = false;
        public bool hideConfirmEndTurnPopup = false;
        public bool playerTwoGoesFirst = false;
        public bool playerTwoDoNothingOnFirstTurn = false;
        public int playerOneNumFighters = 4;
        public int playerTwoNumFighters = 4;
        public List<FighterPrototype> p1alreadyChosenFighters = null;
        public List<FighterPrototype> p2alreadyChosenFighters = null;
        public AIType playerTwoAIType = AIType.DoNothing;
        public WinCondition winCondition = WinCondition.DestroyEnemyNexus;

        public delegate IEnumerator TutorialAction(Game game);
        public delegate IEnumerator FighterTutorialAction(Game game, Fighter fighter);

        public class TutorialPopup
        {
            public GameEventTrigger trigger;
            public TutorialAction action;

            public TutorialPopup(GameEventTrigger trigger, TutorialAction action)
            {
                this.trigger = trigger;
                this.action = action;
            }
        }

        public class TriggeredTutorialPopup
        {
            public Func<Game, bool> trigger;
            public TutorialAction callback;
        }

        public virtual List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>();
        }
        
        public static Coords GetAverageCoordsOfEnemies(Game game)
        {
            var enemyCount = game.Players[1].LiveFighters.Count();
            var enemyPosition = game.Players[1].LiveFighters
                                    .Select(f => f.coords.ToPoint())
                                    .Aggregate((sum, next) => sum + next) / enemyCount;
            return enemyPosition.ToCoords();
        }

        public static Point GetAveragePositionOfEnemies(Game game)
        {
            var enemyCount = game.Players[1].LiveFighters.Count();
            var enemyPosition = game.Players[1].LiveFighters
                                    .Select(f => f.coords.ToPoint())
                                    .Aggregate((sum, next) => sum + next) / enemyCount
                                - Point.up;
            return enemyPosition;
        }
        
        public static Point GetAveragePositionOfFriendlies(Game game)
        {
            var fighterCount = game.Players[0].LiveFighters.Count();
            return game.Players[0].LiveFighters
                    .Select(f => f.coords.ToPoint())
                    .Aggregate((sum, next) => sum + next) / fighterCount
                - Point.up;
        }

        public static void CurrentHelpCallback(Game game, TutorialAction callback)
        {
            game.uiController.SetTutorialHelpButtonCallback(callback);
        }
    }

    public class MovementTutorial : MapScenario
    {
        public override List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>
            {
                new TutorialPopup(GameEventTrigger.OnGameStart, TheseAreYourFighters),
                new TutorialPopup(GameEventTrigger.OnFighterSelect, HowToMove),
                new TutorialPopup(GameEventTrigger.AfterFighterMove, MoveRestOfFighters),
                new TutorialPopup(GameEventTrigger.OnSecondTurnStart, MoveToTeleporter),
            };
        }

        private IEnumerator TheseAreYourFighters(Game game)
        {
            CurrentHelpCallback(game, TheseAreYourFighters);
            
            var points = game.currPlayer.LiveFighters.Select(f => f.coords.ToPoint()).ToList();
            var averagePoint = points.Aggregate((sum, current) => sum + current) / points.Count;

            yield return game.camera.PanTilPointIsCentered(averagePoint);
            game.uiController.ShowTutorialText("These are your fighters. Tap one of them to select it.");
            game.uiController.ShowTutorialArrow(averagePoint - new Point(0, 1f));
        }

        private IEnumerator HowToMove(Game game)
        {
            CurrentHelpCallback(game, HowToMove);
            
            game.uiController.ShowTutorialText("The squares where this fighter can <b>move</b> are now highlighted in <color=green>green</color>. " +
                                               "Tap a green square to select it, then again to move your fighter there.");
            yield break;
        }
        
        private IEnumerator MoveRestOfFighters(Game game)
        {
            CurrentHelpCallback(game, MoveRestOfFighters);
            
            var teleporterPoint = game.map.teleporters[0].coords.ToPoint();
            yield return game.camera.PanTilPointIsCentered(teleporterPoint);
            
            game.uiController.ShowTutorialText("Good. Now move the rest of your fighters towards this teleporter.");
            game.uiController.ShowTutorialArrow(teleporterPoint - new Point(0, 1f));

            game.afterChoiceCallback = new TriggeredTutorialPopup
            {
                trigger = g => g.currPlayer.LiveFighters.All(f => f.MovesLeft < f.MaxMoves),
                callback = TapEndTurnButton
            };
            
            game.uiController.SetTutorialOverlayTapCallback(PanBackToFighters);
        }

        private IEnumerator PanBackToFighters(Game game)
        {
            var point = GetAveragePositionOfFriendlies(game);
            yield return game.camera.PanTilPointIsCentered(point);
        }
        
        private IEnumerator TapEndTurnButton(Game game)
        {
            CurrentHelpCallback(game, TapEndTurnButton);
            
            game.uiController.ShowTutorialText("Tap the \"end turn\" button in the lower right corner to refill your fighters' moves.");
            game.uiController.SetEndTurnButtonFlashing(true);
            yield break;
        }
        
        private IEnumerator MoveToTeleporter(Game game)
        {
            CurrentHelpCallback(game, MoveToTeleporter);
            
            var teleporterPoint = game.map.teleporters[0].coords.ToPoint();
            
            yield return game.camera.PanTilPointIsCentered(teleporterPoint);
            
            game.uiController.ShowTutorialText("To finish this stage, move all your fighters onto this teleporter.");
            game.uiController.ShowTutorialArrow(teleporterPoint - new Point(0, 1f));
        }
    }

    public class AttackTutorial : MapScenario
    {
        public override List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>
            {
                new TutorialPopup(GameEventTrigger.OnSecondTurnStart, TheseAreYourEnemies),
                new TutorialPopup(GameEventTrigger.OnFighterSelect, TapWeaponButton),
                new TutorialPopup(GameEventTrigger.AfterFighterAttack, NoteHowAttackingEndedTurn),
            };
        }

        private IEnumerator TheseAreYourEnemies(Game game)
        {
            CurrentHelpCallback(game, TheseAreYourEnemies);
            
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("These are your enemies. To attack them, select one of your fighters.");
            game.uiController.ShowTutorialArrow(enemyPosition);
        }

        private IEnumerator TapWeaponButton(Game game)
        {
            CurrentHelpCallback(game, TapWeaponButton);
            
            game.uiController.ShowTutorialText("Tap the weapon button next to your fighter's portrait in the lower left corner of the screen to select it.");
            game.uiController.SetAbilityButtonFlashing(WhichAbilitySelected.Weapon);
            game.onWeaponSelectCallback = TapEnemiesToAttack;
            yield break;
        }
        
        private IEnumerator TapEnemiesToAttack(Game game)
        {
            game.uiController.ShowTutorialText("The squares where this fighter can <b>attack</b> are now highlighted in <color=red>red</color>. " +
                                               "Tap a red square to select it, then again to confirm the attack.");
            yield break;
        }

        private IEnumerator NoteHowAttackingEndedTurn(Game game)
        {
            CurrentHelpCallback(game, NoteHowAttackingEndedTurn);
            
            game.onKillCallback = NoticeFightersActionsRefilledOnKill;
            
            game.uiController.ShowTutorialText("Great! Notice how attacking used up all that fighter's moves.");
            game.uiController.SetTutorialOverlayTapCallback(UseOtherFighterToAttack);
            yield break;
        }
        
        private IEnumerator UseOtherFighterToAttack(Game game)
        {
            var fighterWhoHasntAttacked = game.currPlayer.LiveFighters.First(f => f.ActionsLeft > 0).pos - new Point(0, .7f);
            
            yield return game.camera.PanTilPointIsCentered(fighterWhoHasntAttacked);
            
            game.uiController.ShowTutorialText("Now use your other fighter to attack an enemy.");
            game.uiController.ShowTutorialArrow(fighterWhoHasntAttacked);

            game.afterFighterAttackedCallback = EndTurnToGetActions;
        }

        private IEnumerator EndTurnToGetActions(Game game)
        {
            game.uiController.ShowTutorialText("Tap the \"end turn\" button to get more actions.");
            game.uiController.SetEndTurnButtonFlashing(true);
            game.nextTurnStartCallback = KeepAttackingUntilBothEnemiesAreDead;
            yield break;
        }

        private IEnumerator KeepAttackingUntilBothEnemiesAreDead(Game game)
        {
            CurrentHelpCallback(game, KeepAttackingUntilBothEnemiesAreDead);
            
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("Keep attacking the enemies until they are both dead!");
            game.uiController.ShowTutorialArrow(enemyPosition);
        }

        private IEnumerator NoticeFightersActionsRefilledOnKill(Game game, Fighter killer)
        {
            yield return game.camera.PanTilPointIsCentered(killer.pos - Point.up);
            game.uiController.ShowTutorialText("You killed an enemy -- good job!\nNotice how the fighter that got the kill gets its moves and actions refilled.");
            game.uiController.ShowTutorialArrow(killer.pos - Point.up);
        }
    }

    public class AbilityTutorial : MapScenario
    {
        public override List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>
            {
                new TutorialPopup(GameEventTrigger.OnSecondTurnStart, TheseAreYourEnemies),
            };
        }
        
        private IEnumerator TheseAreYourEnemies(Game game)
        {
            CurrentHelpCallback(game, TheseAreYourEnemies);
            
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("This is your enemy. In this stage, you'll be using an ability to attack them.");
            game.uiController.ShowTutorialArrow(enemyPosition);
            
            game.uiController.SetTutorialOverlayTapCallback(SelectYourFighter);
        }

        private IEnumerator SelectYourFighter(Game game)
        {
            CurrentHelpCallback(game, SelectYourFighter);
            
            var fighterPosition = GetAveragePositionOfFriendlies(game);
            
            yield return game.camera.PanTilPointIsCentered(fighterPosition);
            
            game.uiController.ShowTutorialText("To begin, select your fighter.");
            game.uiController.ShowTutorialArrow(fighterPosition);

            game.fighterSelectCallback = SelectHookAbility;
        }

        private IEnumerator SelectHookAbility(Game game)
        {
            game.uiController.ShowTutorialText("Now select the hook ability on the bottom of the screen.");
            game.uiController.SetAbilityButtonFlashing(WhichAbilitySelected.Ability1);
            game.onAbilitySelectCallback = TapEnemiesToAttack;
            yield break;
        }
        
        private IEnumerator TapEnemiesToAttack(Game game)
        {
            game.uiController.ShowTutorialText("The squares you can target with this <b>ability</b> are now highlighted in <color=red>red</color>.");
            
            game.uiController.SetTutorialOverlayTapCallback(TapEnemiesToAttack2);
            
            yield break;
        }
        
        private IEnumerator TapEnemiesToAttack2(Game game)
        {
            var enemyPosition = GetAveragePositionOfEnemies(game);
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            game.uiController.ShowTutorialArrow(enemyPosition);
            
            game.uiController.ShowTutorialText("Select this fighter to use your ability on him.");
            
            game.onKillCallback = EnemiesDownHoleIsInstaKill;
        }

        private IEnumerator EnemiesDownHoleIsInstaKill(Game game, Fighter killer)
        {
            game.uiController.ShowTutorialText("Notice how when a fighter falls down a pit, they instantly die!");
            game.uiController.SetTutorialOverlayTapCallback(AbilityNowOnCooldown);
            yield break;
        }
        
        private IEnumerator AbilityNowOnCooldown(Game game)
        {
            CurrentHelpCallback(game, AbilityNowOnCooldown);
            
            game.uiController.ShowTutorialText("Your ability is now on cooldown. End your turn twice to wait for it to be off cooldown.");
            game.uiController.SetEndTurnButtonFlashing(true);
            game.nextTurnStartCallback = CheckAbilityOffCooldown;
            yield break;
        }

        private IEnumerator CheckAbilityOffCooldown(Game game)
        {
            var yourFighter = game.Players[0].Fighters[0];

            if (yourFighter.GetAbility(BSAbility.Hook).OnCooldown)
            {
                // keep waiting, call this again next turn
                game.uiController.SetEndTurnButtonFlashing(true);
                game.nextTurnStartCallback = CheckAbilityOffCooldown;
                yield break;
            }
            
            // it's off cooldown
            game.uiController.SetEndTurnButtonFlashing(false);
            var teleporterPoint = game.map.teleporters[0].coords.ToPoint();
            yield return game.camera.PanTilPointIsCentered(teleporterPoint);
            game.uiController.ShowTutorialArrow(teleporterPoint - new Point(0, 1f));
            game.uiController.ShowTutorialText("Now you'll use that same ability to get across the pit to this teleporter.");
            game.uiController.SetTutorialOverlayTapCallback(FirstMoveHere);
        }

        private IEnumerator FirstMoveHere(Game game)
        {
            CurrentHelpCallback(game, FirstMoveHere);
            
            var dest = new Coords(-3, 1).ToPoint() - new Point(0, .75f);
            yield return game.camera.PanTilPointIsCentered(dest);
            game.map.SetFloorTile(new Coords(-3, 1), 1455);
            game.uiController.ShowTutorialArrow(dest);
            game.uiController.ShowTutorialText("First, move your fighter here.");
            game.uiController.SetTutorialOverlayTapCallback(UseHookAbilityToCrossGap);
        }

        private IEnumerator UseHookAbilityToCrossGap(Game game)
        {
            var dest = new Coords(1, 1).ToPoint() - new Point(0, .75f);
            yield return game.camera.PanTilPointIsCentered(dest);
            game.uiController.ShowTutorialArrow(dest);
            game.uiController.ShowTutorialText("Then, use the same <b>hook</b> ability on this square to pull yourself over the gap!");

            game.afterChoiceCallback = new TriggeredTutorialPopup
            {
                trigger = g => g.Players[0].LiveFighters.First().coords.x >= 0,
                callback = GetToTeleporter
            };
        }
        
        private IEnumerator GetToTeleporter(Game game)
        {
            CurrentHelpCallback(game, GetToTeleporter);
            
            var teleporterPoint = game.map.teleporters[0].coords.ToPoint();
            
            yield return game.camera.PanTilPointIsCentered(teleporterPoint);
            
            game.uiController.ShowTutorialText("To finish this stage, end your turn, then move your fighter to this teleporter.");
            game.uiController.ShowTutorialArrow(teleporterPoint - new Point(0, 1f));            
        }
    }

    public class CoverTutorial : MapScenario
    {
        public override List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>
            {
                new TutorialPopup(GameEventTrigger.OnSecondTurnStart, TheseAreYourEnemies),
            };
        }
        
        private IEnumerator TheseAreYourEnemies(Game game)
        {
            CurrentHelpCallback(game, TheseAreYourEnemies);
            
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("This enemy is protected because they're standing behind cover.");
            game.uiController.ShowTutorialArrow(enemyPosition);
            
            game.uiController.SetTutorialOverlayTapCallback(SelectYourFighter);
        }
        
        private IEnumerator SelectYourFighter(Game game)
        {
            var fighterPosition = GetAveragePositionOfFriendlies(game);
            
            yield return game.camera.PanTilPointIsCentered(fighterPosition);
            
            game.uiController.ShowTutorialText("Select your fighter, then select their weapon.");
            game.uiController.ShowTutorialArrow(fighterPosition);

            game.fighterSelectCallback = SelectWeapon;
        }

        private IEnumerator SelectWeapon(Game game)
        {
            game.uiController.SetAbilityButtonFlashing(WhichAbilitySelected.Weapon);
            game.onWeaponSelectCallback = TryTargetEnemy;
            yield break;
        }

        private IEnumerator TryTargetEnemy(Game game)
        {
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("Note how you cannot target this enemy -- this is because they're standing behind cover.");
            game.uiController.ShowTutorialArrow(enemyPosition);
            
            game.uiController.SetTutorialOverlayTapCallback(DestroyCover);
        }
        
        private IEnumerator DestroyCover(Game game)
        {
            CurrentHelpCallback(game, DestroyCover);
            
            var coverPos = GetAveragePositionOfEnemies(game) + Point.left;
            yield return game.camera.PanTilPointIsCentered(coverPos);
            game.uiController.ShowTutorialArrow(coverPos);
            game.uiController.ShowTutorialText("Destroy this cover to be able to attack the enemy. Cover takes two hits to destroy, so shoot it with both your fighters' attacks.");

            var coverCoord = GetAverageCoordsOfEnemies(game) + Coords.left;

            game.afterChoiceCallback = new TriggeredTutorialPopup
            {
                trigger = g => g.map.GetTerrainType(coverCoord) != TerrainType.Cover,
                callback = NowAttackEnemy
            };
        }

        private IEnumerator NowAttackEnemy(Game game)
        {
            CurrentHelpCallback(game, NowAttackEnemy);
            
            var enemyPosition = GetAveragePositionOfEnemies(game);
            
            yield return game.camera.PanTilPointIsCentered(enemyPosition);
            
            game.uiController.ShowTutorialText("Great job -- the enemy is now exposed!");
            game.uiController.ShowTutorialArrow(enemyPosition);
            
            game.uiController.SetTutorialOverlayTapCallback(KillEnemy);
        }

        private IEnumerator KillEnemy(Game game)
        {
            game.uiController.ShowTutorialText("End your turn to get more actions, then kill the enemy fighter to proceed.");
            game.uiController.SetEndTurnButtonFlashing(true);
            yield break;
        }
    }

    public class FullGameTutorial : MapScenario
    {
        public override List<TutorialPopup> GetTutorialPopups()
        {
            return new List<TutorialPopup>
            {
                new TutorialPopup(GameEventTrigger.OnSecondTurnStart, FullGame),
                new TutorialPopup(GameEventTrigger.AfterFighterRespawned, YourFighterJustRespawned),
                new TutorialPopup(GameEventTrigger.OnDamageNexusDirectly, DealtDamageToBase),
            };
        }

        private IEnumerator FullGame(Game game)
        {
            game.onKillCallback = KilledEnemy;
            
            game.uiController.SetTutorialHelpButtonCallback(FullGame);
            game.uiController.ShowTutorialText("In this tutorial, you'll play a full game of Barnard's Star.");
            game.uiController.SetTutorialOverlayTapCallback(ThisIsYourBase);
            yield break;
        }
        
        private IEnumerator ThisIsYourBase(Game game)
        {
            var yourNexusPosition = (game.Players[0].nexus.CenterCoords + Coords.down).ToPoint();
            yield return game.camera.PanTilPointIsCentered(yourNexusPosition);
            game.uiController.ShowTutorialArrow(yourNexusPosition);
            game.uiController.ShowTutorialText("This is your base. When your fighters die, they will respawn near it.");
            
            game.uiController.SetTutorialOverlayTapCallback(ThisIsEnemyBase);
        }
        
        private IEnumerator ThisIsEnemyBase(Game game)
        {
            var enemyNexusPosition = (game.Players[1].nexus.CenterCoords + Coords.down).ToPoint();
            yield return game.camera.PanTilPointIsCentered(enemyNexusPosition);
            game.uiController.ShowTutorialArrow(enemyNexusPosition);
            game.uiController.ShowTutorialText("Your objective is to destroy your enemy's base. Attack it with your fighters until it explodes!");
            
            game.uiController.SetTutorialOverlayTapCallback(PanBackToFighters);
        }
        
        private IEnumerator PanBackToFighters(Game game)
        {
            var friendlyPosition = GetAveragePositionOfFriendlies(game);
            yield return game.camera.PanTilPointIsCentered(friendlyPosition);
            game.uiController.ShowTutorialArrow(friendlyPosition);
            
            game.uiController.ShowTutorialText("Remember, you can select your (or enemy) fighters and hold down their ability buttons to see what they do.");
            
            game.uiController.SetTutorialOverlayTapCallback(GoForthAndAttack);
        }
        
        private IEnumerator GoForthAndAttack(Game game)
        {
            game.uiController.ShowTutorialText("Now use what you've learned to attack the enemies and destroy their base!");
            yield break;
        }

        private IEnumerator YourFighterJustRespawned(Game game)
        {
            game.uiController.SetTutorialHelpButtonCallback(YourFighterJustRespawned);

            var spawnPoint = game.lastFighterWhoRespawned != null ?
                (game.lastFighterWhoRespawned.coords + Coords.down).ToPoint() :
                (game.Players[0].SpawnPoint + Coords.down).ToPoint();
            
            yield return game.camera.PanTilPointIsCentered(spawnPoint);
            game.uiController.ShowTutorialArrow(spawnPoint);
            game.uiController.ShowTutorialText("Your fighter just died and respawned! Since this was the first time they died, they respawned on the very next turn.");
            game.uiController.SetTutorialOverlayTapCallback(NextTimeFighterDies);
        }

        private IEnumerator NextTimeFighterDies(Game game)
        {
            game.uiController.ShowTutorialText("The next time this fighter dies, they'll wait one turn before respawning.");
            game.uiController.SetTutorialOverlayTapCallback(ThreeOrMoreDeaths);
            yield break;
        }
        
        private IEnumerator ThreeOrMoreDeaths(Game game)
        {
            game.uiController.ShowTutorialText("When a fighter dies for the third or more time in a game, they'll have to wait 2 turns before respawning.");
            yield break;
        }

        private IEnumerator KilledEnemy(Game game, Fighter killer)
        {
            game.uiController.ShowTutorialText("You killed an enemy fighter -- good job!");
            game.uiController.SetTutorialOverlayTapCallback(KilledEnemy2);
            yield break;
        }

        private IEnumerator KilledEnemy2(Game game)
        {
            game.uiController.SetTutorialHelpButtonCallback(KilledEnemy2);
            game.uiController.ShowTutorialText("The enemy fighter will respawn next turn.\n\nNote that their base took 1 damage when they died.");
            yield break;
        }

        private IEnumerator DealtDamageToBase(Game game)
        {
            game.uiController.SetTutorialHelpButtonCallback(DealtDamageToBase);
            var enemyNexusPosition = (game.Players[1].nexus.CenterCoords + Coords.down).ToPoint();
            yield return game.camera.PanTilPointIsCentered(enemyNexusPosition);
            game.uiController.ShowTutorialArrow(enemyNexusPosition);
            game.uiController.ShowTutorialText("You dealt damage to the enemy base. Good job!");
            game.uiController.SetTutorialOverlayTapCallback(DealtDamageToBase2);
        }
        
        private IEnumerator DealtDamageToBase2(Game game)
        {
            game.uiController.ShowTutorialText("Note that no matter how much damage an attack does, it will only deal 1 damage to the base.");
            yield break;
        }
    }
}