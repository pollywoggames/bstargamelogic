using System;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Primitives;
using Newtonsoft.Json;

namespace BarnardsStar.Maps
{
    public class Teleporter : IEquatable<Teleporter>
    {
        public Coords coords;
        public int channel;
        
        public bool isBurrow => type == TeleporterType.Burrow;
        public TeleporterType type;
        
        // Owner is null for teleporters that are part of the map
        public Player Owner;
        
        public bool OnCooldown;

        [JsonIgnore] public bool hasVirtualActor => virtualActor != null && !virtualActor.IsDestroyed();
        [JsonIgnore] public ITeleporterVirtualActor virtualActor;
        
        public Teleporter(ITeleporterVirtualActor virtualActor, Coords coords, int channel, TeleporterType type, Player owner)
        {
            this.virtualActor = virtualActor;
            this.coords = coords;
            this.channel = channel;
            this.type = type;
            this.Owner = owner;
            this.virtualActor?.Init(this);
        }

        public void SetTeleporting(bool teleporting)
        {
            if (hasVirtualActor) virtualActor.SetTeleporting(teleporting);
        }

        public void SetOnCooldown(bool onCooldown)
        {
            this.OnCooldown = onCooldown;
            
            if (hasVirtualActor) virtualActor.SetOnCooldown(this.OnCooldown);
        }

        public Teleporter Clone(Game gameClone, Player ownerClone)
        {
            var clone = (Teleporter)MemberwiseClone();
            clone.virtualActor = gameClone.contextProvider.ProvideTeleporterVirtualActor(channel);
            clone.virtualActor.Init(clone);
            clone.Owner = ownerClone;
            return clone;
        }


        public bool Equals(Teleporter other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return coords.Equals(other.coords) && channel == other.channel;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Teleporter)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (coords.GetHashCode() * 397) ^ channel;
            }
        }
    }

    public enum TeleporterType
    {
        // Teleporters that are part of the map
        Fixed,
        
        // Teleporters placed by an ability
        Placed,
        
        // Burrows created by the burrow ability
        Burrow,
    }
}