using System.Collections.Generic;

namespace BarnardsStar.Maps.Responses
{
    public class ListMapsResponse
    {
        public List<CompressedMapDetails> Maps;
    }
}