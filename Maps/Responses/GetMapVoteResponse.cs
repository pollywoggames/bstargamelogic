namespace BarnardsStar.Maps.Responses
{
    public class GetMapVoteResponse
    {
        public string MapId;
        public int CurrentPlayerVote;
    }
}