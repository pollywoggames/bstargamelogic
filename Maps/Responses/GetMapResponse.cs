using BarnardsStar.Utilities;

namespace BarnardsStar.Maps.Responses
{
    public class GetMapResponse
    {
        public string Id, Name;
        public bool IsDraft;
        public MapMetadata Meta;

        // How the current player has voted on this map.
        // 0: hasn't voted
        // 1: upvoted
        // -1: downvoted
        public int CurrentPlayerVote;
    
        public byte[] CompressedProto;

        public MapDetails ToMapDetails()
        {
            var decompressed = CompressionUtils.DecompressToString(CompressedProto);
            var mapProto = JsonSerializer.Deserialize<MapPrototype>(decompressed);
            
            return new MapDetails()
            {
                Id = Id,
                Name = Name,
                IsBuiltin = false,
                IsDraft = IsDraft,
                Meta = Meta,
                Proto = mapProto
            };
        }
    }
}