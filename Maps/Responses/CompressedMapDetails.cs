namespace BarnardsStar.Maps.Responses
{
    public class CompressedMapDetails
    {
        public string Id, Name;
        public bool IsDraft;
        public MapMetadata Meta;
        public byte[] CompressedProto;
    }
}