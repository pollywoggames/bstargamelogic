using System;

#if UNITY_2020_3_OR_NEWER
using System.Collections.Generic;
using BarnardsStar.Logic.UI.Menus.Choosers;
#endif

namespace BarnardsStar.Maps
{
    // The color scheme a particular map uses
    [Serializable]
    public enum MapColor
    {
        Grey,
        Green,
        Blue
    }

    public static class MapColorExtensions
    {
        public static int ToMapWallTileCode(this MapColor mapColor)
        {
            return mapColor switch
            {
                // Note the numbers correspond to tile codes in scifitactical/Assets/Resources/Tiles/TileList.json
                MapColor.Grey => 1094,
                MapColor.Green => 1095,
                MapColor.Blue => 2178,
                _ => throw new ArgumentOutOfRangeException(nameof(mapColor), mapColor, null)
            };
        }
 
#if UNITY_2020_3_OR_NEWER
        public static List<EnumChooserChoice> GetAllChoices()
        {
            return new List<EnumChooserChoice>
            {
                new("Gray", MapColor.Grey, "bbbbbb"),
                new("Green", MapColor.Green, "83b37d"),
                new("Blue", MapColor.Blue, "7ab0c4")
            };
        }
#endif
    }
}