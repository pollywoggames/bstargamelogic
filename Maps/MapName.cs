using System;

#if UNITY_2020_3_OR_NEWER
using System.Collections.Generic;
using BarnardsStar.Logic.UI.Menus.Choosers;
#endif

namespace BarnardsStar.Maps
{
    [Serializable]
    public enum MapName
    {
        AnchorageStation,
        Marauder, // aka The Icarus
        Phoenix, // aka The Kestrel
    }

    public static class MapNameExtensions
    {
#if UNITY_2020_3_OR_NEWER
        public static List<EnumChooserChoice> GetAllChoices()
        {
            return new List<EnumChooserChoice>
            {
                new("None", null),
                new("Anchorage\nStation", MapName.AnchorageStation),
                new("Marauder", MapName.Marauder),
                new("Phoenix", MapName.Phoenix),
            };
        }
#endif
    }
}