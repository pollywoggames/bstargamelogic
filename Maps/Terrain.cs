using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

// disabling this lint because I think it's less readable
// ReSharper disable UseIndexFromEndExpression

namespace BarnardsStar.Maps
{
    [JsonConverter(typeof(TerrainJsonConverter))]
    public struct Terrain
    {
        public TerrainType terrainType;
        public int _floorTileCode, _coverTileCode;

        // there can be a floor & underdecor tile code for every terrain type except pit
        public int floorTileCode
        {
            get => terrainType == TerrainType.Pit ? 0 : _floorTileCode;
            set => _floorTileCode = value;
        }

        // there can only be a cover tile if its terrain type is cover 
        public int coverTileCode
        {
            get => terrainType == TerrainType.Cover ? _coverTileCode : 0;
            set
            {
                _coverTileCode = value;
                if (value == 0 && terrainType == TerrainType.Cover)
                    terrainType = TerrainType.Clear;
                if (value != 0 && terrainType == TerrainType.Clear)
                    terrainType = TerrainType.Cover;
            }
        }

        public Terrain(TerrainType terrainType) : this()
        {
            this.terrainType = terrainType;

            _floorTileCode = 0;
            _coverTileCode = 0;
        }

        public static List<List<Terrain>> ArrayFromBounds(MapBounds bounds)
        {
            var ret = new List<List<Terrain>>();
            for (int i = 0; i < bounds.width * 2 + 1; i++)
            {
                var elem = new List<Terrain>();
                for (int j = 0; j < bounds.height * 2 + 1; j++)
                {
                    elem.Add(new Terrain());
                }
                ret.Add(elem);
            }
            return ret;
        }

        public bool Equals(Terrain other)
        {
            return terrainType == other.terrainType
                   && _floorTileCode == other._floorTileCode
                   && _coverTileCode == other._coverTileCode;
        }

        public override bool Equals(object obj)
        {
            return obj is Terrain other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)terrainType;
                hashCode = (hashCode * 397) ^ _floorTileCode;
                hashCode = (hashCode * 397) ^ _coverTileCode;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return Serialize();
        }
        
        // ***************************************************
        // ***   Stuff for serialization/deserialization   ***
        // ***************************************************

        public string Serialize()
        {
            if (terrainType == TerrainType.Pit)
                return "";
            
            StringBuilder sb = new StringBuilder();
            sb.Append((int)terrainType)
                .Append(':')
                .Append(_floorTileCode)
                .Append(':')
                .Append(_coverTileCode);

            var str = sb.ToString();

            // Chop off as many ":0" 's on the end as possible.
            // First, scan backwards from the end of the string, looking for ":0" 's
            int chop = 0;
            while (chop < 2
                   && str[str.Length - (1 + 2 * chop)] == '0'
                   && str[str.Length - (2 + 2 * chop)] == ':')
                chop++;
            // Then chop them off
            if (chop > 0)
                str = str.Substring(0, str.Length - chop * 2);

            return str;
        }

        public static Terrain Deserialize(string strValue)
        {
            if (string.IsNullOrEmpty(strValue))
                return new Terrain() { terrainType = TerrainType.Pit };

            var split = strValue.Split(':');

            var terrainType = (TerrainType)int.Parse(split[0]);
            var floorTileCode = split.Length > 1 ? int.Parse(split[1]) : 0;
            var coverTileCode = split.Length > 2 ? int.Parse(split[2]) : 0;

            return new Terrain
            {
                terrainType = terrainType,
                _floorTileCode = floorTileCode,
                _coverTileCode = coverTileCode,
            };
        }
    }
}