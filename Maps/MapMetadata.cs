using System;
using BarnardsStar.Attributes;

#if NETCOREAPP
using MongoDB.Bson.Serialization.Attributes;
#endif

namespace BarnardsStar.Maps
{
    /// <summary>
    /// Metadata about a user-created map.
    /// </summary>
    public class MapMetadata
    {
        // player ID of the player who created this map
        public string CreatorId;
        
        // name of the player who created this map.
        // Note: BsonIgnore'd bc it's looked up dynamically in case the player changes their name
        [BsonIgnore]
        public string CreatorName;
        
        // This is the final upload date for published maps, or the last updated date for draft maps
        public DateTime CreationDate;
        
        public int Upvotes, Downvotes;

        public int GetScore()
        {
            return Upvotes - Downvotes;
        }
    }
}