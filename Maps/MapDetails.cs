using BarnardsStar.Utilities;
using Newtonsoft.Json;
using JsonSerializer = BarnardsStar.Utilities.JsonSerializer;

namespace BarnardsStar.Maps
{
    /// <summary>
    /// All the details of a map, needed for ChooseMapDialog
    /// </summary>
    public class MapDetails
    {
        public string Id, Name;
        public bool IsBuiltin, IsDraft;
        public MapMetadata Meta;
        public MapPrototype Proto;

        [JsonIgnore]
        public MapSettings mapSettings => Proto.GameMap.mapSettings;

        public byte[] GetCompressedProto()
        {
            return CompressionUtils.Compress(JsonSerializer.Serialize(Proto));
        }
    }
}