using System;
using Newtonsoft.Json;

namespace BarnardsStar.Maps
{
    public class TerrainJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Terrain terrain = (Terrain) value;
            writer.WriteValue(terrain.Serialize());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string strValue = null;
            if (reader.TokenType == JsonToken.String && reader.Value != null)
                strValue = (string) reader.Value;

            return Terrain.Deserialize(strValue);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Terrain);
        }
     }
}