using System;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public struct SingleMove : IEquatable<SingleMove>
    {
        public Coords coords;
        public bool isTeleport;
        public bool disableScore;

        public SingleMove(Coords coords, bool isTeleport, bool disableScore = false)
        {
            this.coords = coords;
            this.isTeleport = isTeleport;
            this.disableScore = disableScore;
        }

        public bool Equals(SingleMove other)
        {
            return coords.Equals(other.coords) && isTeleport == other.isTeleport && disableScore == other.disableScore;
        }

        public override bool Equals(object obj)
        {
            return obj is SingleMove other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(coords, isTeleport, disableScore);
        }
        
        public static bool operator ==(SingleMove a, SingleMove b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(SingleMove a, SingleMove b)
        {
            return !(a == b);
        }
    }
}