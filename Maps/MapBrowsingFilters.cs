using BarnardsStar.Games;
using Newtonsoft.Json;

namespace BarnardsStar.Maps
{
    public class MapBrowsingFilters
    {
        // Search for a map by name
        public string Name;

        // Only show maps from this player 
        public string PlayerId;
        
        public int? NumPlayers;
        // Local-only option for disabling changing the number of players filter -- this is for
        // when ChooseMapDialog gets called from BetterHostGamePane where they've already chosen
        // how many players will be in the game.
        [JsonIgnore]
        public bool DisableChangingNumPlayers;

        public TeamsType? teamsType;

        public MapBrowsingSort Sort;
        public bool SortReverse;

        public bool ShowDrafts;

        // Local-only options for ChooseMapDialog_FilterPopup state.
        // null - can't show blank maps (ie. it's being shown for choosing a map for a game, not 
        // for the map editor)
        [JsonIgnore]
        public bool? ShowBlankMaps;
        
        // Local-only option for ChooseMapDialog_FilterPopup state.
        [JsonIgnore]
        public bool ShowBuiltinMaps = true;
    }

    public class MapSortTuple
    {
        public MapBrowsingSort Sort;
        public bool Reverse;
    }

    public enum MapBrowsingSort
    {
        // upvotes
        Popularity,
        // default newest first
        CreationDate,
        // default A-Z (by name)
        Alphabetical,
    }
}