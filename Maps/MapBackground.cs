using System;
using System.Collections.Generic;

namespace BarnardsStar.Maps
{
    [Serializable]
    public enum MapBackground
    {
        NovaBlue,
        NovaRed,
        NovaYellow,
        PlanetBlue,
        PlanetRed,
        PlanetYellow
    }

    public static class MapBackgrounds
    {
        public static List<MapBackground> All()
        {
            return new List<MapBackground>
            {
                MapBackground.NovaBlue,
                MapBackground.NovaRed,
                MapBackground.NovaYellow,
                MapBackground.PlanetBlue,
                MapBackground.PlanetRed,
                MapBackground.PlanetYellow
            };
        }
    }
}