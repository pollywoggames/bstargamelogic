using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Games;
using BarnardsStar.Primitives;
using Newtonsoft.Json;

namespace BarnardsStar.Maps
{
    public class MapPrototype : IGameMap
    {
        [JsonIgnore]
        public string Id => GameMap.mapSettings.Id;
        
        // Everything you need to create a map instance server-side
        public GameMap GameMap { get; set; }
        
        public List<MapPlayerInfo> PlayerInfos { get; set; }

        public int NumPlayers => PlayerInfos?.Count ?? 0;
        
        public bool IsOdd()
        {
            return GameMap.mapSettings.IsOdd();
        }

        public bool IsClear(Coords coords, List<Coords> overrideBlockedCoords = null, bool ignoreFighters = false)
        {
            if (overrideBlockedCoords != null && overrideBlockedCoords.Contains(coords))
                return false;
            
            // check collisions with nexi
            if (PlayerInfos.Any(pi => (coords - pi.NexusCenterPoint).magnitude <= 1.5f))
            {
                return false;
            }

            return GameMap.IsClear(coords, overrideBlockedCoords, ignoreFighters);
        }

        public MapBounds GetBounds()
        {
            return GameMap.bounds;
        }

        public TerrainType GetTerrainType(Coords coords, bool countHazards)
        {
            return GameMap.GetTerrainType(coords, countHazards);
        }
        
        // Check whether a map (in the map editor) is valid for upload.
        // Returns a list of error messages.
        public List<string> CheckValidForUpload()
        {
            List<string> errorMessages = new();

            string tp = CheckValidTeleporters();
            if (!string.IsNullOrEmpty(tp)) errorMessages.Add(tp);
            
            var nex = CheckValidNexiAndSpawnPoints();
            errorMessages.AddRange(nex);
            
            return errorMessages;
        }

        private string CheckValidTeleporters()
        {
            // Key: tp channel. Value: how many tp's of that channel.
            var teleporterCountsByChannel = new Dictionary<int, int>();

            foreach (var tp in GameMap.teleporters)
            {
                if (teleporterCountsByChannel.TryGetValue(tp.channel, out var count))
                    count++;
                else
                    count = 1;

                teleporterCountsByChannel[tp.channel] = count;
            }
            
            // make sure each channel that is present has exactly 2
            foreach (var (_, count) in teleporterCountsByChannel)
            {
                if (count != 2)
                    return "Teleporter(s) are missing pairs";
            }

            return null;
        }

        private List<string> CheckValidNexiAndSpawnPoints()
        {
            List<string> errMessages = new();
            for (int i = 0; i < GameMap.mapSettings.NumPlayers; i++)
            {
                var playerInfo = PlayerInfos[i];
                if (playerInfo.NexusCenterPoint == MapPlayerInfo.None)
                {
                    errMessages.Add($"Missing base for player {i + 1}");
                }
                
                if (playerInfo.SpawnPoint == MapPlayerInfo.None)
                {
                    errMessages.Add($"Missing spawn point for player {i + 1}");
                }
            }
            return errMessages;
        }
    }
}