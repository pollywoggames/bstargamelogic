using System;
using BarnardsStar.Primitives;

namespace BarnardsStar.Maps
{
    public struct MapBounds : IEquatable<MapBounds>
    {
        public int width, height;

        public MapBounds(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public bool ContainsCoords(Coords coords)
        {
            return coords.x >= -width && coords.x <= width && coords.y >= -height && coords.y <= height;
        }

        public int MaxDimension()
        {
            return Math.Max(width, height);
        }

        public override string ToString()
        {
            return $"MapBounds({width}x{height})";
        }

        public string Serialize()
        {
            return $"{width}x{height}";
        }

        public static MapBounds Deserialize(string strValue)
        {
            var split = strValue.Split('x');
            return new MapBounds(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]));
        }
        
        #region equality

        public bool Equals(MapBounds other)
        {
            return width == other.width && height == other.height;
        }

        public override bool Equals(object obj)
        {
            return obj is MapBounds other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (width * 397) ^ height;
            }
        }
        
        public static bool operator ==(MapBounds a, MapBounds b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(MapBounds a, MapBounds b)
        {
            return !(a == b);
        }
        
        #endregion
    }
}
