using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarnardsStar.Primitives;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace BarnardsStar.Maps
{
    [JsonConverter(typeof(MapTerrainJsonConverter))]
    public class MapTerrain
    {
        public MapBounds bounds;
        public List<List<Terrain>> terrain;

        public MapTerrain(MapBounds bounds, List<List<Terrain>> terrain)
        {
            this.bounds = bounds;
            this.terrain = terrain;
        }

        public IEnumerable<Coords> allPositionsWithin
        {
            get
            {
                for (int x = 0; x < terrain.Count; x++)
                    for (int y = 0; y < terrain[x].Count; y++)
                        yield return new Coords(x - bounds.width, y - bounds.height);
            }
        }
        
        public Terrain TerrainAt(Coords coords)
        {
            return terrain[coords.x + bounds.width][coords.y + bounds.height];
        }
        
        // mainly for unit tests on the server
        public Terrain TerrainAtRaw(int x, int y)
        {
            return terrain[x][y];
        }

        public void SetTerrainAt(Coords coords, Terrain t)
        {
            terrain[coords.x + bounds.width][coords.y + bounds.height] = t;
        }

        public MapTerrain Clone()
        {
            var newTerrain = Terrain.ArrayFromBounds(bounds);
            
            for (int x = 0; x < terrain.Count; x++)
                for (int y = 0; y < terrain[x].Count; y++)
                    newTerrain[x][y] = terrain[x][y];

            // bounds will be copied by value since it's a struct
            return new MapTerrain(bounds, newTerrain);
        }

        public string Serialize()
        {
            var serializedTerrain = new List<string>(terrain.Count);
            for (int i = 0; i < terrain.Count; i++)
            {
                var row = terrain[i];
                serializedTerrain.Add(PackTerrainRow(row));
            }

            var terrainPart = string.Join("#", serializedTerrain);

            return bounds.Serialize() + "^" + terrainPart;
        }

        public static MapTerrain Deserialize(string strValue)
        {
            var parts = strValue.Split('^');
            var bounds = MapBounds.Deserialize(parts[0]);
            
            var rows = parts[1].Split('#');
            
            var terrain = new List<List<Terrain>>(rows.Length);
            for (int i = 0; i < rows.Length; i++)
            {
                var row = UnpackTerrainRow(rows[i]).Select(Terrain.Deserialize).ToList();
                terrain.Add(row);
            }

            return new MapTerrain(bounds, terrain);
        }
        
        private static string PackTerrainRow(List<Terrain> terrains)
        {
            StringBuilder sb = new StringBuilder(capacity: terrains.Count * 24);

            bool first = true;
            foreach (var terrain in terrains)
            {
                // join with | in between each one
                if (first) first = false;
                else sb.Append('|');

                sb.Append(terrain.Serialize());
            }

            return sb.ToString();
        }
        
        public static string[] UnpackTerrainRow(string row)
        {
            return row.Split('|');
        }
    }
}