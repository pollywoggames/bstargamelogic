using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using BarnardsStar.Abilities;
using BarnardsStar.Abilities.ActiveAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.InMemory;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using Newtonsoft.Json;

namespace BarnardsStar.Hazards
{
    public class Hazard
    {
        public const int ElectricityDamage = 1, FireDamage = 2, AcidDamage = 2;

        public static readonly List<Sound> WaterSounds = new List<Sound>
        {
            Sound.WaterDrip1,
            Sound.WaterDrip2,
            Sound.WaterDrip3,
            Sound.WaterDrip4
        };
        
        public static readonly List<Sound> OilSounds = new List<Sound>
        {
            Sound.Oil1,
            Sound.Oil2,
            Sound.Oil3,
            Sound.Oil4,
        };

        public HazardType type { get; set; }
        
        // Is replaced by fire when it is shot or fire is placed next to it
        public bool flammable { get; set; }
        
        // Is replaced by steam (smoke) when it is shot or fire is placed next to it
        public bool steamable { get; set; }
        
        // Explodes upon getting shot or stepped on (land mine)
        public bool explosive { get; set; }
        
        // Conducts electricity
        public bool conductor {get; set;}
        
        // Goes away after one turn
        public bool dissapates {get; set;}
        
        // It'll be consumed by acid being present next to it
        public bool acidifies {get; set;}
        
        // Acts as cover - can hide behind it/damage it, instead of stepping on it
        public bool actsAsCover { get; set; }
        
        // Whether targeters should target it
        public bool targetable { get; set; }
        
        // Spreads acid upon getting damaged
        public bool spreadsAcid { get; set; }
        
        // How much damage it deals to fighters walking on it
        public int damage { get; set; }
        
        // Whether this hazard can be moved, e.g. by Forklift ability
        public bool moveable { get; set; }
        
        // Whether this hazard slows the fighter it touches (or fighters walking on it)
        public bool slows { get; set; }
        
        // This is now unused but might result in serialization bugs for existing games
        // that have the field. So we leave it in for now.
        public Fighter placer { get; set; }
        
        public Player _owner { get; set; }

        [JsonIgnore]
        public Player owner
        {
            get
            {
                if (_owner == null && placer != null)
                {
                    _owner = placer.Owner;
                }

                return _owner;
            }

            set => _owner = value;
        }

        [JsonIgnore] public bool hasVirtualActor => virtualActor != null && !virtualActor.IsDestroyed();
        [JsonIgnore] public IHazardVirtualActor virtualActor;

        public Sound? sound { get; set; }
        public List<Sound> sounds;
        public Sound? damageSound { get; set; }

        private Coords _coords;
        public Coords coords
        {
            get
            {
                if (hasVirtualActor) _coords = virtualActor.GetCoords();
                return _coords;
            }
            set
            {
                _coords = value;
                virtualActor?.SetCoords(value);
            }
        }

        [JsonIgnore]
        public Game game => owner.game;

        [JsonIgnore]
        public int PreviewDamage => damage;

        public Hazard(Fighter placer, HazardType type, IHazardVirtualActor virtualActor)
        {
            this.type = type;
            this._owner = placer?.Owner;
            this.virtualActor = virtualActor;

            acidifies = false;
            actsAsCover = false;
            conductor = false;
            dissapates = false;
            explosive = false;
            flammable = false;
            spreadsAcid = false;
            steamable = false;
            targetable = false;
            moveable = false;
            slows = false;
            damage = 0;
            switch (type)
            {
                case HazardType.Water:
                    conductor = true;
                    acidifies = true;
                    steamable = true;
                    targetable = true;
                    sounds = WaterSounds;
                    break;
                case HazardType.Oil:
                    flammable = true;
                    acidifies = true;
                    targetable = true;
                    slows = true;
                    sounds = OilSounds;
                    break;
                case HazardType.Fire:
                    damage = FireDamage;
                    dissapates = true;
                    sound = Sound.FlamethrowerFire;
                    break;
                case HazardType.Acid:
                    damage = AcidDamage;
                    dissapates = true;
                    conductor = true;
                    targetable = true;
                    steamable = true;
                    sound = Sound.AcidBubble;
                    damageSound = Sound.AcidBurn;
                    break;
                case HazardType.SmokeScreen:
                    dissapates = true;
                    break;
                case HazardType.LandMine:
                    explosive = true;
                    targetable = true;
                    moveable = true;
                    damage = LandMine.LandMineDamage;
                    break;
                case HazardType.AcidBubble:
                    spreadsAcid = true;
                    actsAsCover = true;
                    targetable = true;
                    moveable = true;
                    break;
                case HazardType.Electricity:
                    sound = Sound.ElectricSparks;
                    damageSound = Sound.ElectricDischarge;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            virtualActor?.Init(this);
        }
        
        public Hazard Clone()
        {
            var clone = (Hazard) MemberwiseClone();
            
            clone.virtualActor = new InMemoryHazardVirtualActor(coords);
            
            return clone;            
        }

        // returns true if the hazard should be removed
        public IEnumerator UpdateForTurn()
        {
            if (game.TryGetFighter(coords, out var f))
            {
                yield return TryDamageFighter(f, null);
            }
        }
        
        public bool CanDamageFighter(Fighter f)
        {
            return !f.Immunities.Contains(type) && (explosive || damage > 0);
        }

        public IEnumerator TryDamageFighter(Fighter f, Fighter origin)
        {
            if (f.coords != coords || damage <= 0 || f.Immunities.Contains(type))
                yield break;

            var actualDamage = Math.Min(damage, f.CurrHealth);
            if (type == HazardType.Acid)
                game.SomethingHappened(new BSEvent(BSEventType.DealAcidDamage, owner) { amount = actualDamage });
            
            virtualActor.DoDamageEffect();
            if (damageSound.HasValue)
                game.soundCtx.PlayOneShotVaryPitch(sound.Value);
            yield return f.DealDamage(damage, origin);
        }

        public static bool CanPlaceHazardAt(Game game, HazardType type, Coords coords)
        {
            // This used to have semi-complex rules about which hazard types could replace which other hazard
            // types. Hazards replace other hazards infrequently enough that it was hard to get an intuition about
            // these rules naturally, by just playing the game. So I decided to simplify it quite a bit by allowing
            // any hazard to replace any other hazard.
            return game.map.GetTerrainType(coords) == TerrainType.Clear;
        }

        public static IEnumerator PlaceHazardAt(Fighter fighter, HazardType type, Coords coords, bool alreadyRecursing = false)
        {
            Game game = fighter.game;
            Player owner = fighter.Owner;
            
            if (!CanPlaceHazardAt(game, type, coords))
                yield break;

            // Ensure that the same type of hazard can't get placed on the same coords during the same action
            // (causes 2x damage in certain scenarios otherwise)
            if (game.HazardsPlacedThisAction.TryGetValue(coords, out var ht) && ht == type)
                yield break;
            game.HazardsPlacedThisAction[coords] = type;
            
            if (game.TryGetHazard(coords, out var alreadyThere))
            {
                if (alreadyThere.acidifies && type == HazardType.Acid)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.ConsumeHazardWithAcid, fighter));
                }
                
                if (alreadyThere.explosive)
                    yield return alreadyThere.Explode(fighter);
            }

            var newHazard = new Hazard(fighter, type, game.contextProvider.ProvideHazardVirtualActor(type, coords));
            yield return owner.game.AddHazard(coords, newHazard, fighter);

            if (!alreadyRecursing)
            {
                if (type == HazardType.Fire)
                    yield return CheckIgniteFireAround(fighter, coords);
                else if (type == HazardType.Acid)
                    yield return CheckSpreadAcidAround(fighter, coords);
            }
        }

        public static IEnumerator Shock(Fighter originFighter, Coords coords, int extraDamage = 0)
        {
            yield return ShockRecursive(originFighter, null, coords, new HashSet<Coords>(), extraDamage);
        }

        private static IEnumerator ShockRecursive(Fighter originFighter, Coords? origin, Coords coords, HashSet<Coords> visited, int extraDamage)
        {
            Game game = originFighter.game;
            
            // Ensure that the same type of hazard can't get placed on the same coords during the same action
            // (causes 2x damage in certain scenarios otherwise)
            if (game.CoordsElectrifiedThisAction.Contains(coords))
                yield break;
            game.CoordsElectrifiedThisAction.Add(coords);

            if (visited.Contains(coords))
                yield break;
            visited.Add(coords);
            
            var f = ShockableFighterAt(game, coords);
            if (f != null)
            {
                originFighter.game.soundCtx.PlayOneShotVaryPitch(Sound.ElectricDischarge);

                f.AddStatusEffect(StatusEffect.Electrified, 1, originFighter);

                // remove shield if it's there
                int damage = f.ShieldUp ? f.CurrShield : 1;
                damage += extraDamage;
                
                yield return f.DealDamage(damage, originFighter);
                
                if (f.IsDead)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.KillFighterViaElectrocution, originFighter));
                }
            }
            else
            {
                // add a shock vfx if there's not one already for a fighter
                originFighter.game.soundCtx.PlayOneShotVaryPitch(Sound.ElectricSparks);
                game.animCtx.PlayVFX(VFXType.ShockSquareEffect, coords.ToPoint());
            }

            var propagate = ShockableHazardAt(game, coords);
            if (!propagate)
                yield break;
            
            if (game.contextProvider.IsPreviewing())
                game.contextProvider.RegisterPreview(new HazardPreview(game, coords, HazardType.Electricity));
            
            if (origin.HasValue)
                game.animCtx.PlayElectricArcEffect(origin.Value.ToPoint(), coords.ToPoint(), VFXColor.Blue, .65f);
                
            game.animCtx.Camera().Shake(.2f, CameraConstants.DefaultShakeIntensity / 2f);

            var coroutines =
                (
                    from dir in Coords.OrthogonalDirections
                    select coords + dir into dest
                    where ShockableHazardAt(game, dest)
                    select ShockRecursive(originFighter, coords, dest, visited, extraDamage)
                ).ToList();

            yield return game.runCtx.WaitSeconds(.2f);
            yield return game.runCtx.All(coroutines);
        }
        
        public static Fighter ShockableFighterAt(Game game, Coords coords)
        {
            return game.TryGetFighter(coords, out var f) ? f : null;
        }

        public static bool ShockableHazardAt(Game game, Coords coords)
        {
            return game.TryGetHazard(coords, out var hazard)
                   && hazard.conductor;
        }

        public static IEnumerator CheckIgniteFireAround(Fighter fighter, Coords coords)
        {
            Player player = fighter.Owner;
            
            var visited = new HashSet<Coords>();
            var burned = new HashSet<Coords>();
            
            var coroutines = new List<IEnumerator>();
            foreach (var dir in Coords.OrthogonalDirections)
                coroutines.Add(CheckIgniteFireRecursive(fighter, coords + dir, visited, burned));
            
            yield return player.runCtx.WaitSeconds(.2f);
            yield return player.runCtx.All(coroutines);
            
            fighter.game.SomethingHappened(new BSEvent(BSEventType.BurnOilTile, fighter) { amount = burned.Count });
        }
        
        private static IEnumerator CheckIgniteFireRecursive(Fighter fighter, Coords coords, HashSet<Coords> visited, HashSet<Coords> burned)
        {
            Game game = fighter.game;
            
            if (visited.Contains(coords))
                yield break;
            visited.Add(coords);

            if (game.TryGetHazard(coords, out var hazard) && (hazard.flammable || hazard.steamable))
            {
                if (hazard.flammable)
                {
                    burned.Add(coords);
                    game.animCtx.Camera().Shake(.2f, CameraConstants.DefaultShakeIntensity / 2f);
                    
                    var coroutines = new List<IEnumerator>();
                    coroutines.Add(PlaceHazardAt(fighter, HazardType.Fire, coords, true));
                    foreach (var dir in Coords.OrthogonalDirections)
                        coroutines.Add(CoroutineUtils.DelayThen(game.runCtx, .2f,
                            CheckIgniteFireRecursive(fighter, coords + dir, visited, burned)));

                    yield return game.runCtx.All(coroutines);
                }
                else if (hazard.steamable)
                {
                    yield return PlaceHazardAt(fighter, HazardType.SmokeScreen, coords, true);
                }
            }
        }
        
        public static IEnumerator CheckSpreadAcidAround(Fighter fighter, Coords coords)
        {
            var visited = new HashSet<Coords>();

            var waitGroup = new WaitGroup(fighter.runCtx);
            foreach (var dir in Coords.OrthogonalDirections)
                waitGroup.Start(CheckSpreadAcidAroundRecursive(fighter, coords + dir, visited));

            yield return fighter.runCtx.WaitSeconds(.2f);
            yield return waitGroup.Wait();
        }
        
        private static IEnumerator CheckSpreadAcidAroundRecursive(Fighter fighter, Coords coords, HashSet<Coords> visited)
        {
            Game game = fighter.game;
            
            if (visited.Contains(coords))
                yield break;
            visited.Add(coords);

            if (game.TryGetHazard(coords, out var hazard) && hazard.acidifies)
            {
                game.animCtx.Camera().Shake(.2f, CameraConstants.DefaultShakeIntensity / 2f);

                var coroutines = new List<IEnumerator>();
                coroutines.Add(PlaceHazardAt(fighter, HazardType.Acid, coords, true));
                foreach (var dir in Coords.OrthogonalDirections)
                    coroutines.Add(CoroutineUtils.DelayThen(game.runCtx, .2f,
                        CheckSpreadAcidAroundRecursive(fighter, coords + dir, visited)));

                yield return game.runCtx.All(coroutines);
            }
        }

        public IEnumerator OnDamage(Fighter triggerer)
        {
            if (type == HazardType.Oil)
            {
                triggerer.game.RegisterAchievementActionOnSubmit(AchievementAction.BurnOilTile);
            }
            
            if (flammable)
            {
                yield return PlaceHazardAt(triggerer, HazardType.Fire, coords);
            }
            else if (steamable)
            {
                yield return PlaceHazardAt(triggerer, HazardType.SmokeScreen, coords);
            }
            else if (explosive)
            {
                yield return Explode(triggerer);
            }
            else if (spreadsAcid)
            {
                game.soundCtx.PlayOneShotVaryPitch(Sound.WaterDrip4);
                yield return SpreadAcid(triggerer);
            }
        }

        public IEnumerator Explode(Fighter triggererer)
        {
            game.RemoveHazard(this);
            yield return Bazooka.DoExplosion(game, triggererer, coords, damage);
        }

        public IEnumerator SpreadAcid(Fighter triggerer)
        {
            // Remove original hazard (acid bubble)
            game.RemoveHazard(this);
            
            // Replace it with acid
            yield return PlaceHazardAt(triggerer, HazardType.Acid, coords);
        }

        public static IEnumerator PlaceAcidWithProjectile(Fighter actor, Coords origin, Coords coords)
        {
            yield return PlaceWithProjectile(HazardType.Acid, MovablePropType.AcidProjectile, actor, origin, coords);
        }
        
        public static IEnumerator PlaceWithProjectile(HazardType hazardType, MovablePropType propType, Fighter actor, Coords origin, Coords coords)
        {
            var contextProvider = actor.game.contextProvider;
            var animCtx = actor.game.animCtx;
            
            var projectile = contextProvider.ProvideProp(propType, origin.ToPoint());
            var dst = coords.ToPoint();
            var distance = Point.Distance(origin.ToPoint(), dst);
            
            yield return animCtx.MoveParabolic(projectile, dst, (int) (6 + 5 * distance), .3f);
            projectile.Destroy();

            yield return PlaceHazardAt(actor, hazardType, coords);
        }

        // Similar to GameMap.ShoveCover - changes here should probably be reflected there too
        public static IEnumerator ShoveHazard(Game game, Hazard hazard, Coords srcCoords, Coords dstCoords, Fighter pusher)
        {
            // sanity check
            if (!game.TryGetHazard(srcCoords, out var h) || h != hazard)
            {
                Logger.Error($"Hazard passed in is different from hazard at coords: {srcCoords}");
                yield break;
            }
            
            if (game.contextProvider.IsPreviewing())
                game.contextProvider.RegisterPreview(new ShoveCoverPreview(game, srcCoords, dstCoords));

            var offset = hazard.virtualActor.GetOffset();
            var startingPosition = srcCoords.ToPoint() + offset;
            
            // See if we're going to hit something or not.
            game.TryGetDamageable(dstCoords, out var damageableHit);
            
            game.TryGetHazard(dstCoords, out var hazardHit);
            
            var destTerrainType = game.map.GetTerrainType(dstCoords);

            bool bouncingBack = damageableHit != null
                                || destTerrainType == TerrainType.Cover
                                || destTerrainType == TerrainType.Wall
                                || (hazardHit != null &&
                                    (hazardHit.moveable || hazardHit.explosive || hazardHit.actsAsCover));
            if (bouncingBack)
            {
                // push but bounch back
                var dst = dstCoords.ToPoint() + offset;
                var delta = dst - startingPosition;
                var halfway = delta * .5f + startingPosition;

                yield return game.animCtx.MoveSmoothly(hazard.virtualActor, halfway, .1f);

                var wg = new WaitGroup(game.runCtx);

                if (damageableHit != null)
                {
                    wg.Start(damageableHit.DealDamage(2, pusher));
                }
                else if (destTerrainType == TerrainType.Cover)
                {
                    // if we hit cover, damage it
                    wg.Start(game.map.DamageCover(dstCoords, 1, startingPosition, pusher));
                }
                else if (hazardHit != null)
                {
                    // Hit a hazard, damage it
                    wg.Start(hazardHit.OnDamage(pusher));
                }

                // just pop back instead of animating
                hazard.virtualActor.SetPosition(startingPosition);
                
                // damage self
                wg.Start(hazard.OnDamage(pusher));
                
                yield return wg.Wait();
            }
            else if (destTerrainType == TerrainType.Pit)
            {
                // push down a hole
                var dst = dstCoords.ToPoint() + offset;
                yield return game.animCtx.MoveSmoothly(hazard.virtualActor, dst, .2f);
                
                // spins & falls down the hole
                float spinAngle = 0;
                var spinAmount = 10f;
                for (var i = .75f; i >= 0; i -= .01f)
                {
                    spinAngle += spinAmount;
                    hazard.virtualActor.SetRotation(spinAngle);
                    hazard.virtualActor.SetScale(new Vector3(i, i, i));
                    
                    yield return game.runCtx.WaitSeconds(.01f);
                }

                game.RemoveHazard(hazard);
                game.Hazards.Remove(srcCoords);
            }
            else if (destTerrainType == TerrainType.Clear)
            {
                // shove to new spot
                var dst = dstCoords.ToPoint() + offset;
                yield return game.animCtx.MoveSmoothly(hazard.virtualActor, dst, .2f);

                if (hazardHit != null)
                {
                    // If it's non-null at this point, that means it's not explosive, moveable, or actsAsCover
                    // so we can just remove it.
                    game.RemoveHazard(hazardHit);
                }

                if (game.contextProvider.IsPreviewing())
                    game.contextProvider.RegisterPreview(new ShoveCoverPreview(game, srcCoords, dstCoords));

                hazard.coords = dstCoords;
                
                // move hazard in the game's Hazards dict
                game.Hazards.Remove(srcCoords);
                game.Hazards[dstCoords] = hazard;
            }
            else
            {
                Logger.Error("Unexpected terrain type: " + destTerrainType);
            }
            game.RefreshObjects();
        }
    }
}
