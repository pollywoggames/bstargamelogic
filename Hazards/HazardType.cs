using System;

namespace BarnardsStar.Hazards
{
    public enum HazardType
    {
        Acid,
        Oil,
        Water,
        Fire,
        LandMine,
        SmokeScreen,
        AcidBubble,

        // Not really a hazard like the other ones are,
        // but it has enough similarity in interactions
        // that we include it in the list
        Electricity
    }

    public static class HazardTypeExtensions
    {
        public static string GetImmunityDescription(this HazardType hazardType)
        {
            return hazardType switch
            {
                HazardType.Acid => "damage from Acid",
                HazardType.Oil => "getting slowed by Oil",
                HazardType.Fire => "damage from Fire",
                _ => nameof(hazardType)
            };
        }
    }
}