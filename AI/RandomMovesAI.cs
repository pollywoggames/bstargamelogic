using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Utilities;

namespace BarnardsStar.AI
{
    public class RandomMovesAI : AIManager
    {
        public RandomMovesAI(Game game, Randy random) : base(game, random)
        { }

        protected override PotentialAction Choose(Fighter f, List<PotentialAction> choices, PathfindingMap pathfindingMap)
        {
            return random.RandomInList(choices);
        }
    }
}