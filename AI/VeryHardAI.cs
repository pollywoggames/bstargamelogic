using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarnardsStar.Games;
using BarnardsStar.Utilities;

#if UNITY_2020_3_OR_NEWER
using Cysharp.Threading.Tasks;

// for some UIController calls
using BarnardsStar.Logic.UI.InGame;

#else
using UniTask = System.Threading.Tasks.Task;
#endif

namespace BarnardsStar.AI
{
    public class VeryHardAI : AIManager
    {
        public VeryHardAI(Game game, Randy random) : base(game, random)
        { }

        protected override IEnumerator TryTakeTurn(List<string> fighterIds)
        {
            if (fighterIds != null)
            {
                Logger.Warn("VeryHardAI was provided a fighter turn order, but will ignore it in favor of choosing the best possible move order");
            }

            List<string> fighterIdsHaventGone = game.currPlayer.LiveFighters.Select(f => f.id).ToList();
            while (fighterIdsHaventGone.Count > 0)
            {
                // get list of all possible moves for all fighters that have a move/action
                TreeSearchNode chosen = null;

                // use in-memory clone bc we're on the thread pool
                var baseGameState = game.CloneInMemory();

                var chosenMoveTask = TaskUtils.RunOnThreadPool(() =>
                    ChooseVeryHardAIMove(
                        baseGameState,
                        fighterIdsHaventGone,
                        result => { chosen = result; }));

#if UNITY_2020_3_OR_NEWER
                UIController.Instance.ShowAIThinking();
#endif
                
                yield return TaskUtils.AwaitTaskFromCoroutine(chosenMoveTask, runCtx, 15f, "Hit AI thinking timeout");

#if UNITY_2020_3_OR_NEWER
                UIController.Instance.HideAIThinking();
#endif

                if (chosen == null)
                {
                    yield break;
                }

                var fighter = game.FighterById(chosen.selectedFighterId);
                int fighterKillsBefore = fighter.numKills;
                
                foreach (var c in chosen.choices)
                {
                    yield return game.SubmitChoice(c.choice, err =>
                    {
                        Logger.Error("Error submitting choice: " + err + "\n" + chosen.GetDescription());
                    });
                    yield return runCtx.WaitSeconds(.35f);
                }

                bool gotKill = fighter.numKills > fighterKillsBefore;
                if (!gotKill)
                    fighterIdsHaventGone.Remove(chosen.selectedFighterId);
            }
        }

        private async UniTask ChooseVeryHardAIMove(Game baseGameState, List<string> fighterIdsToInclude, Action<TreeSearchNode> callback)
        {
            // should already be on the thread pool, but just in case ...
            await TaskUtils.SwitchToThreadPool();
            
            TreeSearchNode baseNode = TreeSearchNode.BaseNode(baseGameState);
            baseNode.SimulateAndExpandChildren(fighterIdsToInclude);

            var sortedChildren = baseNode.SortChildren(-10);
            
            const int childrenToReSimulate = 15;
            Logger.Info($"Re-simulating {childrenToReSimulate} children with score > -10");
            
            int resimCount = 0;
            foreach (var childEntry in sortedChildren)
            {
                foreach (var child in childEntry.Value)
                {
                    float scoreBefore = child.bestAvgScore;
                    child.SimulateAndExpandChildren(fighterIdsToInclude);
                    Logger.Info($"child now has {child.children?.Count} children, previous best score: {scoreBefore}, new best score: {child.bestAvgScore}");
                    
                    // now that we figured out the best score, we can discard the children to avoid running out of memory
                    child.TrimChildren();

                    resimCount++;
                    if (resimCount >= childrenToReSimulate)
                        break;
                }
                
                if (resimCount >= childrenToReSimulate)
                    break;
            }
            
            sortedChildren = baseNode.SortChildren();
            
            Logger.Info($"After resorting there are now {sortedChildren.Count}");
            
            if (sortedChildren.Count == 0)
            {
                Logger.Warn("No good AI moves found, passing turn");
                callback(null);
                return;
            }

            StringBuilder sb = new StringBuilder(2048);

            sb.Append("Potential actions\n\n");
            
            int count = 0;
            foreach (var key in sortedChildren.Keys)
            {
                var nodesWithScore = sortedChildren[key];
                foreach (var node in nodesWithScore)
                {
                    node.GetDescription(sb);
                    
                    count++;
                    if (count >= 5)
                        break;
                }
                
                if (count >= 5)
                    break;
            }
            Logger.Info(sb.ToString());

            var topScore = sortedChildren.Keys[0];
            var best = random.RandomInList(sortedChildren[topScore]);
            
            Logger.Info("Chosen: " + best.GetDescription());
            
            callback(best);
        }
    }
}