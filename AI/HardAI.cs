using BarnardsStar.Games;
using BarnardsStar.Utilities;

namespace BarnardsStar.AI
{
    public class HardAI : AIManager
    {
        public HardAI(Game game, Randy random) : base(game, random)
        { }
        
        // No overrides, the default is already hard mode
    }
}