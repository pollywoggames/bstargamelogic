using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarnardsStar.Abstract.Providers.InMemory;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Utilities;

namespace BarnardsStar.AI
{
    public class TreeSearchNode
    {
        public Game preGameState;
        public string selectedFighterId;
        public List<SimulatedChoice> choices;

        public bool isBaseNode { get; private set; }
        
        // May be null until simulated
        public Game postGameState;
        public List<PreviewOutcome> previewOutcomes;
        
        // Score from just the preview outcomes in this node
        public float score;
        
        // Average score of this node plus child nodes.
        // Averaged based on the number of fighter actions taken to get to this point.
        // Takes into account kill resets (higher score for kill resets since it's multiple
        // actions but only for the cost of 1.)
        public float bestAvgScore;
        public TreeSearchNode bestAvgScoreChild;

        public TreeSearchNode parent;
        public List<TreeSearchNode> children;

        private TreeSearchNode(Game preGameState, string selectedFighterId, List<SimulatedChoice> choices, TreeSearchNode parent, bool isBaseNode)
        {
            this.isBaseNode = isBaseNode;
            this.preGameState = preGameState;
            this.selectedFighterId = selectedFighterId;
            this.choices = choices;
            this.parent = parent;
        }

        public static TreeSearchNode BaseNode(Game gameState)
        {
            return new TreeSearchNode(
                gameState,
                null,
                null,
                null,
                true);
        }

        public TreeSearchNode Clone()
        {
            var clone = (TreeSearchNode) MemberwiseClone();

            // remake list so it doesn't have the same reference
            clone.choices = new List<SimulatedChoice>();
            if (choices != null)
                clone.choices.AddRange(choices);

            if (postGameState != null)
            {
                clone.preGameState = postGameState.CloneForPreviews();
                // clear postGameState so it can be simulated
                clone.postGameState = null;
            }

            clone.children = null;

            if (previewOutcomes != null)
            {
                clone.previewOutcomes = new List<PreviewOutcome>();
                clone.previewOutcomes.AddRange(previewOutcomes);
            }

            return clone;
        }

        public TreeSearchNode CloneWithChoice(Choice choice)
        {
            var clone = Clone();
            clone.isBaseNode = false;
            clone.choices.Add(new SimulatedChoice(choice, false));
            // clone.Simulate();
            return clone;
        }

        public bool IsEmpty()
        {
            return selectedFighterId == null || choices == null || choices.Count == 0;
        }

        public void SimulateAndExpandChildren(List<string> fighterIdsToInclude)
        {
            Simulate();
            ExpandChildren(fighterIdsToInclude);
            foreach (var child in children)
            {
                child.Simulate();
            }
        }

        public void Simulate()
        {
            if (postGameState != null)
            {
                // Already simulated!
                return;
            }

            if (IsEmpty())
            {
                // Passing the turn, no need to simulate
                return;
            }
            
            // Logger.Info($"Simulating: {string.Join("\n", choices)}\n on {preGameState}");

            previewOutcomes ??= new List<PreviewOutcome>();
            postGameState = preGameState.Clone(new PreviewContextProvider());

            for (var i = 0; i < choices.Count; i++)
            {
                var choice = choices[i];
                if (choice.simulated)
                    continue;

                var ii = i;
                var previewResult = postGameState.PreviewChoice(choice.choice, false, err =>
                {
                    var errMsg = "Error previewing choice: " + err + "\n";

                    errMsg += $"Choice: {choice}\n";

                    if (ii > 0)
                    {
                        errMsg += "Previous choices:\n";
                        for (int j = 0; j < ii; j++)
                        {
                            errMsg += $" {j + 1}. {choices[j]}\n";
                        }
                    }

                    errMsg += "\nPre-" + preGameState;
                    errMsg += "\nPost-" + preGameState;

                    Logger.Error(errMsg);
                });

                postGameState = previewResult.finalGameState;
                previewOutcomes.AddRange(previewResult.outcomes);

                choice.simulated = true;
            }

            score = 0;
            bool gotKill = false;
            foreach (var po in previewOutcomes)
            {
                score += po.GetScore(preGameState.currPlayer, preGameState.pathfindingMap);

                if (po is DamagePreview { killsTarget: true })
                    gotKill = true;
            }

            Backpropagate(gotKill);
        }

        private int CountDepth()
        {
            int depth = 0;
            var currParent = parent;
            while (currParent != null)
            {
                depth++;
                currParent = currParent.parent;
            }
            return depth;
        }

        private void Backpropagate(bool gotKill)
        {
            float depth = CountDepth();
            if (gotKill)
                depth -= .9f;
            
            // avoid divide by zero
            if (depth == 0) depth = .1f;
            
            bestAvgScore = score / depth;
            // Logger.Info($"bestAvgScore: {bestAvgScore} (score: {score}, actionsUsed: {actionsUsed})");

            var currParent = parent;
            while (currParent != null)
            {
                if (currParent.bestAvgScore <= bestAvgScore)
                {
                    currParent.bestAvgScore = bestAvgScore;
                    currParent.bestAvgScoreChild = this;
                    
                    currParent = currParent.parent;
                }
                else
                {
                    break;
                }
            }
        }

        // Expands and simulates children.
        public void ExpandChildren(List<string> fighterIdsToInclude)
        {
            if (children != null)
            {
                // already expanded
                return;
            }
            children = new List<TreeSearchNode>();
            
            var gameState = isBaseNode ?
                preGameState.Clone(new PreviewContextProvider()) :
                postGameState;
            
            foreach (var fighter in gameState.currPlayer.Fighters)
            {
                if (!fighterIdsToInclude.Contains(fighter.id))
                    continue;

                if (fighter.IsDead || (fighter.MovesLeft == 0 && fighter.ActionsLeft == 0))
                    continue;

                // Logger.Info($"base: {isBaseNode} AddChildNodesForFighterMoves for fighter {fighter}\nin {gameState}");
                
                AddChildNodesForFighterMoves(gameState, fighter);
            }
        }

        private void AddChildNodesForFighterMoves(Game gameState, Fighter f)
        {
            var potentialMoves = gameState.GetAllPossibleMoves(f);
                    
            foreach (var move in potentialMoves)
            {
                // Logger.Info("AddChildNodesForFighterMoves potentialMove " + move);
                
                var moveNode = new TreeSearchNode(
                    gameState,
                    f.id,
                    new List<SimulatedChoice>() { new SimulatedChoice(new Choice(f.id, ChoiceType.Movement, move.movesToGetHere), false) },
                    this,
                    false);
                moveNode.Simulate();
                
                children.Add(moveNode);
            
                // now add children for doing attacks & various abilities from that location
                var fighterInPreview = moveNode.postGameState.FighterById(f.id);
                if (fighterInPreview == null || fighterInPreview.ActionsLeft <= 0)
                    continue;
                
                var weaponTargets = fighterInPreview.GetWeapon().GetTargets();

                // Logger.Info($"AddChildNodesForFighterMoves {move}, adding attacks\n{string.Join("\n",weaponTargets.validTargets.Select(t => t.coords))}");

                foreach (var weaponTarget in weaponTargets.validTargets)
                {
                    children.Add(moveNode.CloneWithChoice(
                        new Choice(fighterInPreview, ChoiceType.Attack, weaponTarget.coords)));
                }
                
                // Logger.Info("AddChildNodesForFighterMoves " + move + ", adding abilities");

                foreach (var ability in fighterInPreview.Abilities)
                {
                    if (ability.OnCooldown || ability.IsPassive)
                        continue;

                    var abilityTargets = ability.GetTargets();
                    
                    // Logger.Info($"AddChildNodesForFighterMoves {move}, adding ability {ability.type}\n{string.Join("\n",abilityTargets.validTargets.Select(t => t.coords))}");
                    
                    foreach (var abilityTarget in abilityTargets.validTargets)
                    {
                        children.Add(moveNode.CloneWithChoice(
                            new Choice(fighterInPreview, ChoiceType.Ability, abilityTarget.coords, ability.type)));
                    }
                }
            }
        }
        
        public SortedList<float, List<TreeSearchNode>> SortChildren(float minThreshold = 0)
        {
            var sorted = new SortedList<float, List<TreeSearchNode>>();
            foreach (var child in children)
            {
                float childScore = child.bestAvgScore;
                
                if (childScore <= minThreshold)
                    continue;

                // add it with negative score so it's sorted in descending order
                float scoreKey = -childScore;
                if (sorted.TryGetValue(scoreKey, out var list))
                    list.Add(child);
                else
                    sorted.Add(scoreKey, new List<TreeSearchNode> { child });
            }

            return sorted;
        }

        public void TrimChildren()
        {
            // Clear list of children for garbage collection.
            children = null;
        }

        public string GetDescription(StringBuilder sb = null)
        {
            if (string.IsNullOrEmpty(selectedFighterId))
            {
                return "(empty selectedFighterId)";
            }
            
            Fighter fighter = preGameState.FighterById(selectedFighterId);
            if (fighter == null)
            {
                return $"(fighter {selectedFighterId} not found)";
            }

            sb ??= new StringBuilder(1024);

            sb.Append(
                $"TreeSearchNode ({(isBaseNode ? "base " : "")}score: {score}, bestAvgScore: {bestAvgScore}, children: {(children == null ? "null" : children.Count.ToString())})");

            sb.Append("Choices for ").Append(selectedFighterId).Append(":");
            
            foreach (var simulatedChoice in choices)
            {
                var actionChoice = simulatedChoice.choice;
                
                sb.Append("\n");

                switch (actionChoice.type)
                {
                    case ChoiceType.Movement:
                        sb.Append("  - Move to ");
                        if (actionChoice.moves != null && actionChoice.moves.Count > 0)
                            sb.Append(string.Join(", ", actionChoice.moves));
                        else
                            sb.Append(actionChoice.target);
                        break;

                    case ChoiceType.Attack:
                        sb.AppendFormat("  - Attack ({0}) {1}", fighter.weapon, actionChoice.target);
                        break;

                    case ChoiceType.Ability:
                        sb.AppendFormat("  - Ability ({0}) {1}", actionChoice.selectedAbility.Value, actionChoice.target);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            sb.Append($"\nScores: (total: {score})");

            foreach (var outcomeWithScore in OutcomesWithScores(fighter.Owner, preGameState.pathfindingMap))
            {
                var extra = outcomeWithScore.Item1.GetDescription(fighter.Owner, preGameState.pathfindingMap);
                if (extra != "")
                    extra = $" | {extra}";

                sb.Append(
                    $"\n  + {outcomeWithScore.Item2:N2} -> {outcomeWithScore.Item1.GetType().Name}{extra}");
            }

            if (bestAvgScoreChild != null)
            {
                sb.Append($"And here's bestAvgScoreChild with score {bestAvgScore}:\n")
                    .Append(bestAvgScoreChild.GetDescription());
            }

            sb.Append("\n\n");
            
            // sb.Append("Pre-game ").Append(preGameState.ToString());
            //
            // if (postGameState != null)
            // {
            //     sb.Append("\nPost-game ").Append(postGameState.ToString());
            // }
            //
            // sb.Append("\n\n");

            return sb.ToString();
        }
        
        // for debug purposes
        private List<(PreviewOutcome, float)> OutcomesWithScores(Player owner, PathfindingMap pathfindingMap)
        {
            var ret = new List<(PreviewOutcome, float)>();
            
            foreach (var po in previewOutcomes)
            {
                ret.Add((po, po.GetScore(owner, pathfindingMap)));
            }

            return ret;
        }
    }

    public class SimulatedChoice
    {
        public Choice choice;
        public bool simulated;

        public SimulatedChoice(Choice choice, bool simulated)
        {
            this.choice = choice;
            this.simulated = simulated;
        }

        public override string ToString()
        {
            return $"SimulatedChoice(simulated:{simulated},{choice.ToString()})";
        }
    }
}