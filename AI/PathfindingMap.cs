using System.Collections.Generic;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.AI
{
    public class PathfindingMap
    {
        private const int SentinelMax = 9999;
        
        private int[,] map;
        private MapBounds bounds;
        private Game game;
        private Player player;

        private int arrayWidth => bounds.width * 2 + 1;
        private int arrayHeight => bounds.height * 2 + 1;
        
        public PathfindingMap(Game game, Player currPlayer)
        {
            this.bounds = game.map.bounds;
            this.game = game;
            this.player = currPlayer;
            
            map = new int[arrayWidth, arrayHeight];
            // Set all values in map to large value initially
            for (int x = 0; x < arrayWidth; x++)
                for (int y = 0; y < arrayHeight; y++)
                    map[x, y] = SentinelMax;
            
            CalculateForGameState();
        }

        public bool IsInBounds(Coords coords)
        {
            var xIdx = coords.x + bounds.width;
            var yIdx = coords.y + bounds.height;
            
            return xIdx >= 0 && xIdx < map.GetLength(0)
                && yIdx >= 0 && yIdx < map.GetLength(1);
        }

        public int Get(Coords coords)
        {
            return map[coords.x + bounds.width, coords.y + bounds.height];
        }

        private void Set(Coords coords, int newValue)
        {
            map[coords.x + bounds.width, coords.y + bounds.height] = newValue;
        }

        private void CalculateForGameState()
        {
            // For the nexus and each enemy, do a flood-fill type algorithm
            // where we start at their location and then walk in every direction,
            // recording the distance from themselves.
            //
            // To be clear, the "goal" in this case is moving towards the enemy
            // nexus or enemy fighters. So those locations have distance 0.
            
            HashSet<Coords> visited = new HashSet<Coords>();
            
            Queue<Coords> queue = new Queue<Coords>(1024);
            Queue<Coords> nextLoopQueue = new Queue<Coords>(1024);
            // Enqueue all starting coords -- all enemy fighters & nexi
            foreach (var enemy in game.EnemiesOf(player))
            {
                if (enemy.nexus != null)
                {
                    foreach (var nexusCoords in enemy.nexus.AllCoords)
                        queue.Enqueue(nexusCoords);
                }
                
                foreach (var fighter in enemy.LiveFighters)
                    queue.Enqueue(fighter.coords);
            }

            int currDistance = 0;
            
            while (queue.Count > 0)
            {
                // for each item in the queue:
                //  - mark visited
                //  - set distance at that location to currDistance 
                //  - check all adjacent spaces and enqueue the valid ones
                while (queue.Count > 0)
                {
                    var currCoords = queue.Dequeue();
                    visited.Add(currCoords);

                    var existingDistance = Get(currCoords);
                    // stop and don't enqueue more from this location if it already
                    // has a distance that is better than the current one
                    if (existingDistance <= currDistance)
                        continue;
                    
                    Set(currCoords, currDistance);

                    foreach (var dir in Coords.OrthogonalDirections)
                    {
                        var nextCoords = currCoords + dir;
                        
                        if (!visited.Contains(nextCoords) && game.map.GetTerrainType(nextCoords) == TerrainType.Clear)
                            nextLoopQueue.Enqueue(nextCoords);
                    }

                    // also check if we should go through teleporter
                    if (game.map._teleportersByCoords.TryGetValue(currCoords, out var teleporter)
                        && !(teleporter.isBurrow && teleporter.OnCooldown)
                        && game.map._teleporterPairs.TryGetValue(teleporter, out var teleporterPair))
                    {
                        // Visit teleporter pair coords as part of this loop (same distance)
                        queue.Enqueue(teleporterPair.coords);
                    }
                }

                currDistance++;
                
                // put all items from nextLoopQueue into queue
                while (nextLoopQueue.Count > 0)
                    queue.Enqueue(nextLoopQueue.Dequeue());
            }
            
            // Now go through and change all instances of SentinelMax to the max value (plus a few)
            // (so the super large sentinel value doesn't throw things off if incorrect)
            currDistance += 2;
            for (int x = 0; x < arrayWidth; x++)
                for (int y = 0; y < arrayHeight; y++)
                    if (map[x, y] == SentinelMax)
                        map[x, y] = currDistance;
        }
    }
}