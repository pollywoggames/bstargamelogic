using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Games;
using BarnardsStar.Utilities;

namespace BarnardsStar.AI
{
    public class MediumAI : AIManager
    {
        public MediumAI(Game game, Randy random) : base(game, random)
        { }

        protected override PotentialAction ChooseFromSortedActions(SortedList<float, List<PotentialAction>> sorted, List<PotentialAction> allChoices)
        {
            // Flatten sorted list
            List<PotentialAction> flatSortedList = sorted.SelectMany(entry => entry.Value).ToList();
            
            if (random.Next(0, 10) <= 6)
            {
                // medium-high chance to choose one of the top 10 moves
                return flatSortedList[random.Next(0, Math.Min(10, flatSortedList.Count))];
            }
            else
            {
                // choose a random move
                return random.RandomInList(allChoices);
            }
        }
    }
}