using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Games;
using BarnardsStar.Utilities;

namespace BarnardsStar.AI
{
    public class DoNothingAI : AIManager
    {
        public DoNothingAI(Game game, Randy random) : base(game, random)
        { }

        protected override IEnumerator TryTakeTurn(List<string> fighterIds)
        {
            yield break;
        }
    }
}