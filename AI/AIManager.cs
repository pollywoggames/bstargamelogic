using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using Logger = BarnardsStar.Utilities.Logger;

#if UNITY_2020_3_OR_NEWER
using Cysharp.Threading.Tasks;
#else
using UniTask = System.Threading.Tasks.Task;
#endif

namespace BarnardsStar.AI
{
    public abstract class AIManager
    {
        protected Game game;
        
        protected Randy random;
        
        public IRunContext runCtx => game.runCtx;
        public IUIController uiController => game.uiController;
        public Player player => game.currPlayer;

        protected AIManager(Game game, Randy random)
        {
            this.game = game;
            this.random = random;
        }

        public virtual void ChooseFighters()
        {
            game.ChooseRandomTeamForPlayer(player);
        }

        public IEnumerator TakeTurn(List<string> fighterIds = null)
        {
            yield return runCtx.Run(TryTakeTurn(fighterIds));
            yield return game.EndTurn();
        }

        protected virtual IEnumerator TryTakeTurn(List<string> fighterIds)
        {
            yield return runCtx.WaitSeconds(.3f);

            List<Fighter> fighters;
            if (fighterIds == null)
                fighters = ListUtils.ShuffleList(player.LiveFighters.ToList(), random);
            else
                fighters = fighterIds.Select(id => player.Fighters.First(f => f.id == id)).ToList();
            
            foreach (var f in fighters)
            {
                if (f.Asleep || f.IsDead)
                    continue;

                // If they kill someone, it will reset their moves and actions back to max
                // Then they can take another turn
                int failsafe = 0;
                bool continueOneMoreTime = false;
                while (continueOneMoreTime || (f.ActionsLeft == f.MaxActions && Math.Abs(f.MovesLeft - f.MaxMoves) < .1f))
                {
                    if (continueOneMoreTime)
                        continueOneMoreTime = false;
                    
                    if (f.IsDead)
                        break;

                    // obnoxious way to have a value returned from a coroutine
                    bool didPassTurn = false;
                    
                    yield return runCtx.Run(TakeFighterTurn(f, () => didPassTurn = true));

                    if (didPassTurn)
                        break;

                    if (f.ActionsLeft == 0 && f.MovesLeft > 0)
                        continueOneMoreTime = true;

                    // Break after 1 move if the infinite actions cheat is enabled, otherwise they'll never stop
                    if (game.CheatEnabled(Cheat.InfiniteActions))
                        break;

                    // if there's an error taking their turn, they'll just keep looping through this forever unless we break
                    failsafe++;
                    if (failsafe > 7)
                        break;
                    
                    if (game.gameEnded)
                        yield break;
                }

                yield return runCtx.WaitSeconds(.35f);
            }
        }

        protected IEnumerator TakeFighterTurn(Fighter f, Action passTurnCallback)
        {
            if (f == null || f.virtualActor == null || f.virtualActor.IsDestroyed())
                yield break;

            // Asynchronously choose a move from all possible options
            PotentialAction chosen = null;
            
            // use in-memory clone bc we're on the thread pool
            var gameClone = game.CloneInMemory();
            
            var chosenMoveTask = TaskUtils.RunOnThreadPool(() => ChooseAIMove(gameClone, f, result =>
            {
                chosen = result;
            }));
            
            yield return TaskUtils.AwaitTaskFromCoroutine(chosenMoveTask, runCtx, 10f,
                $"Hit AI thinking timeout for {f.id} @ {f.coords}");

            if (chosen == null)
            {
                passTurnCallback?.Invoke();
                yield break;
            }
            
            yield return runCtx.Run(uiController.SelectUnit(f));
            yield return runCtx.WaitSeconds(.1f);

            foreach (var c in chosen.choices)
            {
                yield return game.SubmitChoice(c);

                yield return runCtx.WaitSeconds(.35f);
            }
            
            game.RefreshObjects();
        }

        // Choose the AI move on the thread pool for better performance on the main thread,
        // and so that we don't block rendering & updating if it goes for a long time
        protected async UniTask ChooseAIMove(Game gameClone, Fighter f, Action<PotentialAction> callback)
        {
            try
            {
                f = gameClone.FighterById(f.id);

                // should already be on the thread pool, but just in case ...
                await TaskUtils.SwitchToThreadPool();

                var pathfindingMap = new PathfindingMap(gameClone, gameClone.currPlayer);
                
                var potentialMoves = gameClone.GetAllPossibleMoves(f);

                // add possibility of fighter standing still
                potentialMoves.Add(new PotentialMove(new List<Coords>(), 0));

                var potentialActions = new List<PotentialAction>();
                foreach (var move in potentialMoves)
                {
                    var movePotentialAction = new PotentialAction(gameClone);
                    foreach (var moveToGetThere in move.movesToGetHere)
                        movePotentialAction.AddChoiceInPlace(new Choice(f.id, ChoiceType.Movement, moveToGetThere));
                    
                    potentialActions.Add(movePotentialAction);

                    // now check doing attacks & various abilities from that location
                    var fighterInPreview = movePotentialAction.currGameState.FighterById(f.id);
                    if (fighterInPreview == null || fighterInPreview.ActionsLeft <= 0)
                        continue;

                    var weaponTargets = fighterInPreview.GetWeapon().GetTargets();
                    foreach (var weaponTarget in weaponTargets.validTargets)
                    {
                        potentialActions.Add(movePotentialAction.CloneWithChoice(new Choice(fighterInPreview,
                            ChoiceType.Attack, weaponTarget.coords)));
                    }

                    foreach (var ability in fighterInPreview.Abilities)
                    {
                        if (ability.OnCooldown || ability.IsPassive)
                            continue;

                        var abilityTargets = ability.GetTargets();
                        foreach (var abilityTarget in abilityTargets.validTargets)
                        {
                            potentialActions.Add(movePotentialAction.CloneWithChoice(new Choice(fighterInPreview,
                                ChoiceType.Ability, abilityTarget.coords, ability.type)));
                        }
                    }
                }

                // We have a list of every possible move + action combination the fighter could possibly make.
                // Now we just need to calculate scores & decide which action to take, and then take that action.
                var chosen = Choose(f, potentialActions, pathfindingMap);

                // for debugging so we can see what the score is of whatever was chosen
                // Logger.Info("chosen move score " + chosen.GetScore(player));

                callback?.Invoke(chosen);
            }
            catch (Exception e)
            {
                Logger.Error($"Error in ChooseAIMove for fighter id '{f.id}' \n{e}");
            }
        }

        protected virtual PotentialAction Choose(Fighter f, List<PotentialAction> choices, PathfindingMap pathfindingMap)
        {
            var sorted = SortPotentialActions(choices, pathfindingMap, true);

            if (sorted.Count == 0)
            {
                // No good moves, just pass turn
                Logger.Info("No good moves, passing turn");
                return null;
                
                // // try again but keep the ones with score <= 0
                // sorted = SortPotentialActions(choices, pathfindingMap, false);
            }
            
#if UNITY_EDITOR
            LogTopActions(f, sorted, pathfindingMap);
#endif
            
            var chosen = ChooseFromSortedActions(sorted, choices);
            
#if UNITY_EDITOR
            var sb = new StringBuilder();
            LogActionDetails(f, pathfindingMap, chosen, sb);
            Logger.Info("Chosen: " + sb.ToString());
#endif
            
            return chosen;
        }

        private SortedList<float, List<PotentialAction>> SortPotentialActions(List<PotentialAction> choices, PathfindingMap pathfindingMap, bool filterNegative)
        {
            var sorted = new SortedList<float, List<PotentialAction>>();
            foreach (var c in choices)
            {
                float score = c.GetScore(player, pathfindingMap);
                
                if (filterNegative && score <= 0)
                    continue;

                // add it with negative score so it's sorted in descending order
                float scoreKey = -score;
                if (sorted.TryGetValue(scoreKey, out var list))
                    list.Add(c);
                else
                    sorted.Add(scoreKey, new List<PotentialAction> {c});
            }

            return sorted;
        }

        protected virtual PotentialAction ChooseFromSortedActions(SortedList<float, List<PotentialAction>> sorted, List<PotentialAction> allChoices)
        {
            // Choose from actions with the top score, and a few points less than that
            
            var topScore = sorted.Keys[0];

            var potentials = new List<PotentialAction>();
            for (int i = 0; i < sorted.Count; i++)
            {
                var currScore = sorted.Keys[i];
                if (-topScore - -currScore > 3f)
                    break;

                potentials.AddRange(sorted.Values[i]);
            }
            
            return random.RandomInList(potentials);
        }

        private void LogTopActions(Fighter f, SortedList<float, List<PotentialAction>> sorted, PathfindingMap pathfindingMap)
        {
            var sb = new StringBuilder(1024);

            sb.Append("Potential Actions for " + f.id + "\n");
            
            var topScore = sorted.Keys[0];
            int count = 1;
            foreach (var actionWithScore in SortedActionsInOrder(sorted))
            {
                var score = actionWithScore.Item1;
                var action = actionWithScore.Item2;
                
                // if (-topScore - -score > 3f)
                //     break;
                
                sb.Append($"{count}. ");
                count++;
                if (count >= 10)
                    break;

                LogActionDetails(f, pathfindingMap, action, sb);
            }
            
            Logger.Info(sb.ToString());
        }

        private static void LogActionDetails(Fighter f, PathfindingMap pathfindingMap, PotentialAction action, StringBuilder sb)
        {
            sb.Append("Choices:");
            
            foreach (var actionChoice in action.choices)
            {
                sb.Append("\n");

                switch (actionChoice.type)
                {
                    case ChoiceType.Movement:
                        sb.Append("  - Move to ");
                        if (actionChoice.moves != null && actionChoice.moves.Count > 0)
                            sb.Append(string.Join(", ", actionChoice.moves));
                        else
                            sb.Append(actionChoice.target);
                        break;

                    case ChoiceType.Attack:
                        sb.AppendFormat("  - Attack ({0}) {1}", f.weapon, actionChoice.target);
                        break;

                    case ChoiceType.Ability:
                        sb.AppendFormat("  - Ability ({0}) {1}", actionChoice.selectedAbility.Value, actionChoice.target);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            sb.Append($"\nScores: (total: {action.GetScore(f.Owner, pathfindingMap)})");

            foreach (var outcomeWithScore in action.OutcomesWithScores(f.Owner, pathfindingMap))
            {
                var extra = outcomeWithScore.Item1.GetDescription(f.Owner, pathfindingMap);
                if (extra != "")
                    extra = $" | {extra}";
                
                sb.Append($"\n  + {outcomeWithScore.Item2:N2} -> {outcomeWithScore.Item1.GetType().Name}{extra}");
            }

            sb.Append("\n\n");
        }

        private IEnumerable<(float, PotentialAction)> SortedActionsInOrder(SortedList<float, List<PotentialAction>> sorted)
        {
            // "SelectMany" is the LINQ version of "flatMap"
            return sorted.SelectMany(entry => entry.Value.Select(action => (entry.Key, action)));
        }
    }
}
