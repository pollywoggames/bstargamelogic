# Setting up the submodule

Update it the first time:

`git submodule update --init`

Use the local .gitconfig file, already configured so your submodule will auto-pull and auto-push
with the parent repo: (the ../ at the begining is to get out of the .git dir)

`git config --local include.path ../Assets/BStarGameLogic/.gitconfig`

Make sure to checkout master before you commit changes in the submodule (otherwise it'll stay in detached HEAD)

`cd Assets/BStarGameLogic`
`git checkout master`

# Code Structure

### Virtual Actors
- These classes provide the "physical" layer the game logic runs on.
- Persistence, timing, and animation can be implemented or mocked out here.
- The Unity client persists/propagates changes in the game state into the active Unity scene.
- The server side client uses a purely in-memory implementation that mocks out animation & timing for instantaneous resolution of player moves.
- The Unity client also uses the in-memory implementation for certain things, for example predicting the effects of a certain move in order to preview it.

### Contexts
- Manager classes that determine how everything else is used.
- For example, the details of a game being played is a context.
- Game state can be cloned and switched from an animated Unity context to an unanimated, in-memory context in order to instantly calculate game state for previews & move verification.

### Primitives
- Basic data structures that stand in for Unity primitives like Vector3.
- Used so this library doesn't have to rely on anything specific to Unity.
