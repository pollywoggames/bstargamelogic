﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BarnardsStar.Utilities
{
    /// <summary>
    /// A slightly better Random class.
    /// </summary>
    public class Randy
    {
        private Random random;
        private int seed;

        public Randy()
        {
            random = new Random();
        }

        public Randy(int seed)
        {
            this.seed = seed;
            random = new Random(seed);
        }

        public int GetSeed()
        {
            return seed;
        }

        public int Next()
        {
            return random.Next();
        }

        public int Next(int min, int max)
        {
            // min: inclusive, max: exclusive
            return random.Next(min, max);
        }

        public float Next(float min, float max)
        {
            return (float)(random.NextDouble() * (max - min)) + min;
        }

        public double NextDouble()
        {
            return random.NextDouble();
        }

        public T RandomInList<T>(List<T> list)
        {
            return list[Next(0, list.Count)];
        }
        
        public T RandomInListAndRemove<T>(List<T> list)
        {
            var randomIdx = Next(0, list.Count);
            var chosen = list[randomIdx];
            list.RemoveAt(randomIdx);
            return chosen;
        }
        
        public T RandomInArray<T>(T[] list)
        {
            return list[Next(0, list.Length)];
        }
    }
}
