using System;
using System.Collections.Generic;
using System.Text;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;

namespace BarnardsStar.Utilities
{
    public static class TextUtils
    {
        public static string Colorize(string text, TextColor color)
        {
            if (color == TextColor.Default)
                return text;
            
            string colorStr = color switch
            {
                TextColor.Action => "32bfff", // light blue
                TextColor.Notify => "ffcE04", // yellow
                TextColor.Disabled => "aaaaaa", // gray
                TextColor.Alert => "ff5a5a", // red
                TextColor.Win => "8888ff", // light blue
                TextColor.Loss => "ff4466", // light red
                _ => throw new ArgumentOutOfRangeException(nameof(color), color, null)
            };

            return $"<color=#{colorStr}>{text}</color>";
        }

        public static string Colorize(string text, TeamColor teamColor)
        {
            return $"<color=#{teamColor.GetBrightTextColor()}>{text}</color>";
        }
        
        // 0 => "A", 1 => "B", etc.
        public static string IndexToTeamLetter(int index)
        {
            char c = (char) ('A' + index);
            return c.ToString();
        }

        /// <summary>
        /// Joins a list of values with commas & spaces. The last element is joined with " and ".
        /// For example, a list of ["One", "Two", "Three"] when passed into this function will
        /// return: "One, Two and Three".
        /// </summary>
        /// <param name="values">List of values to join.</param>
        /// <returns>The joined values.</returns>
        public static string JoinWithAnd(List<string> values)
        {
            switch (values.Count)
            {
                case 0:
                    return "";
                case 1:
                    return values[0];
                case 2:
                    return values[0] + " and " + values[1];
            }

            StringBuilder sb = new StringBuilder();
            bool first = true;
            for (int i = 0; i < values.Count + 1; i++)
            {
                if (first)
                    first = false;
                else
                    sb.Append(", ");

                sb.Append(values[i]);
            }
            
            // append last value
            sb.Append(" and ")
                .Append(values[values.Count - 1]);

            return sb.ToString();
        }

        public static string GetAbilityDamageStr(IAbility ability)
        {
            var baseDamage = ability.GetBaseDamage();
            var damage = ability.GetDamage();

            if (baseDamage == damage)
            {
                return damage.ToString();
            }
            else
            {
                // it's upgraded
                return $"<b>*{damage}*</b>";
            }
        }
        
        public static string GetAbilityRangeStr(IAbility ability)
        {
            var baseRange = ability.GetBaseRange();
            var range = ability.GetRange();

            if (baseRange == range)
            {
                return range.ToString();
            }
            else
            {
                // it's upgraded
                return $"<b>*{range}*</b>";
            }
        }
        
        public static string GetAbilityMiscStr(IAbility ability)
        {
            var baseMisc = ability.GetBaseMisc();
            var misc = ability.GetMisc();

            if (baseMisc == misc)
            {
                return misc.ToString();
            }
            else
            {
                // it's upgraded
                return $"<b>*{misc}*</b>";
            }
        }

        public static string CapitalizeFirstLetter(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            
            var capitalFirstLetter = str.Substring(0, 1).ToUpperInvariant();
            return capitalFirstLetter + str.Substring(1);
        }
    }

    public enum TextColor
    {
        Default, // white
        Action, // light blue
        Notify, // yellow
        Disabled, // gray
        Alert, // red
        
        Win, // soft blue
        Loss, // soft red
    }
}