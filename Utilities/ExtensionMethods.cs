using System.Collections.Generic;
using System.Linq;

namespace BarnardsStar.Utilities
{
    public static class ExtensionMethods
    {
        public static HashSet<T> ToSet<T>(this IEnumerable<T> enumerable)
        {
            var set = new HashSet<T>();
            foreach (var obj in enumerable)
            {
                set.Add(obj);
            }

            return set;
        }

        public static bool AsBool(this bool? optionalBool, bool _default)
        {
            return optionalBool ?? _default;
        }

        public static void SetIndex<T>(this List<T> list, int index, T value)
        {
            while (list.Count < index)
            {
                list.Add(default);
            }

            list[index] = value;
        }
    }
}