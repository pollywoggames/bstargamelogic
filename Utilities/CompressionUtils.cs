using System.IO;
using System.IO.Compression;
using System.Text;

namespace BarnardsStar.Utilities
{
    public static class CompressionUtils
    {
        public static byte[] Compress(string data)
        {
            return Compress(Encoding.UTF8.GetBytes(data));
        }
        
        public static byte[] Compress(byte[] data)
        {
            using var compressedStream = new MemoryStream();
            using var zipStream = new GZipStream(compressedStream, CompressionLevel.Optimal);
            
            zipStream.Write(data, 0, data.Length);
            zipStream.Close();
            return compressedStream.ToArray();
        }

        public static string DecompressToString(byte[] data)
        {
            return Encoding.UTF8.GetString(Decompress(data));
        }
        
        public static byte[] Decompress(byte[] data)
        {
            using var compressedStream = new MemoryStream(data);
            using var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress);
            using var resultStream = new MemoryStream();
            
            zipStream.CopyTo(resultStream);
            zipStream.Close();
            return resultStream.ToArray();
        }

    }
}