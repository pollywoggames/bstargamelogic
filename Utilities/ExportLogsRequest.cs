using System;
using System.Collections.Generic;

namespace BarnardsStar.Utilities
{
    public class ExportLogsRequest
    {
        // These may be null, since export logs requests don't need to be authenticated
        public string Username;
        public string UserId;
        public string DeviceId;
        public string Platform;
        public string GameVersion;
        public string Message;
        
        public List<LogEntry> logEntries;

        public ExportLogsRequest(List<LogEntry> logEntries)
        {
            this.logEntries = logEntries;
        }
    }

    /// <summary>
    /// Copy of the UnityEngine.LogType enum
    /// </summary>
    public enum LogType
    {
        Error,
        Assert,
        Warning,
        Log,
        Exception,
    }

    public class LogEntry
    {
        public LogType LogType;
        public string Message;
        public string StackTrace;
        public DateTime Timestamp;

        public LogEntry(LogType logType, string message, string stackTrace, DateTime timestamp)
        {
            LogType = logType;
            Message = message;
            StackTrace = stackTrace;
            Timestamp = timestamp;
        }
    }
}