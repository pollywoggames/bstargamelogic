using System;

#if UNITY_2020_3_OR_NEWER
using UnityEngine;
#endif

namespace BarnardsStar.Utilities
{
    public interface ILogger
    {
        void InfoLog(object log);
        void WarningLog(object log);
        void ErrorLog(object log);
    }

    public class Logger : ILogger
    {
        private static ILogger _log = new Logger();

        public void InfoLog(object log)
        {
#if UNITY_2020_3_OR_NEWER
            Debug.Log(log);
#else
            Console.WriteLine(log);
#endif
        }

        public void WarningLog(object log)
        {
#if UNITY_2020_3_OR_NEWER
            Debug.LogWarning(log);
#else
            Console.Error.WriteLine(log);
#endif
        }

        public void ErrorLog(object log)
        {
#if UNITY_2020_3_OR_NEWER
            Debug.LogError(log);
#else
            Console.Error.WriteLine(log);
#endif
        }

        public static void Info(object log)
        {
            _log.InfoLog(log);
        }

        public static void Warn(object log)
        {
            _log.WarningLog(log);
        }

        public static void Error(object log)
        {
            _log.ErrorLog(log);
        }

        public static void SetLogger(ILogger logger)
        {
            _log = logger;
        }
    }
}