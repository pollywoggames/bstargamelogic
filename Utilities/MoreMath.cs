using System;

namespace BarnardsStar.Utilities
{
    public static class MoreMath
    {
        public const float PI = (float) Math.PI;
        public const float Deg2Rad = PI * 2f / 360f;
        public const float Rad2Deg = 360f / (PI * 2f);

        public static float Lerp(float a, float b, float f)
        {
            return a * (1f - f) + b * f;
        }

        public static float Clamp(float a, float min, float max)
        {
            if (a <= min)
                return min;

            if (a >= max)
                return max;

            return a;
        }
    }
}