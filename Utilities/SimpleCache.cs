using System.Collections.Generic;

namespace BarnardsStar.Utilities
{
    public class SimpleCache<K, V>
    {
        public delegate V GetValueFunction(K key);

        private readonly Dictionary<K, V> _cache;

        public SimpleCache()
        {
            _cache = new Dictionary<K, V>();
        }

        public void Clear()
        {
            _cache.Clear();
        }

        public void ClearKey(K key)
        {
            if (_cache.ContainsKey(key))
                _cache.Remove(key);
        }

        public V Get(K key, GetValueFunction getter)
        {
            if (_cache.TryGetValue(key, out var cached))
            {
                return cached;
            }

            var gotten = getter(key);
            lock (_cache)
            {
                if (!_cache.ContainsKey(key))
                    _cache.Add(key, gotten);
            }
            return gotten;
        }
    }
}