using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Contexts.InMemory;
using BarnardsStar.Abstract.Contexts.Interfaces;

namespace BarnardsStar.Utilities
{
    public static class CoroutineUtils
    {
        public static IEnumerator DelayThen(IRunContext runContext, float delay, IEnumerator then)
        {
            yield return runContext.WaitSeconds(delay);
            yield return then;
        }

        public static IEnumerator DoNothing()
        {
            yield break;
        }
    }

    public class WaitGroup
    {
        private List<ICoroutineWrapper> coroutines;
        private IRunContext runContext;

        public WaitGroup(IRunContext runContext)
        {
            this.runContext = runContext;
            coroutines = new List<ICoroutineWrapper>();
        }

        public void Start(IEnumerator coroutine)
        {
            if (runContext.IsUnity())
            {
                var wrapper = new UnityCoroutineWrapper(coroutine);
                coroutines.Add(wrapper);
                runContext.Start(wrapper.Run(runContext));
            }
            else
            {
                var wrapper = new InstantCoroutineWrapper(coroutine);
                coroutines.Add(wrapper);
                runContext.Start(wrapper);
            }
        }

        public IEnumerator Wait()
        {
            if (runContext.IsUnity())
                yield return WaitUnity();
            else
                yield return WaitInMemory();
        }

        private IEnumerator WaitUnity()
        {
            // For Unity we just wait for the coroutines to finish
            foreach (var coroutine in coroutines)
            {
                while (!coroutine.IsFinished())
                    yield return null;
            }
        }

        private IEnumerator WaitInMemory()
        {
            // Cast the coroutines once instead of each frame to make it a bit faster
            var castCoroutines = coroutines.Select(c => (InstantCoroutineWrapper)c).ToList();
            
            // In-memory coroutines must be manually run each frame
            while (coroutines.Any(c => !c.IsFinished()))
            {
                // Advance each coroutine 1 frame
                foreach (var coroutine in castCoroutines)
                {
                    if (!coroutine.IsFinished())
                        coroutine.ExecuteFrame(InstantRunContext.DELTA_TIME);
                }

                // Wait a frame in the parent run context
                yield return null;
            }
        }
    }
}