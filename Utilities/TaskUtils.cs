
using System;
using System.Collections;
using System.Threading;
using BarnardsStar.Abstract.Contexts.Interfaces;
#if UNITY_2020_3_OR_NEWER
using Cysharp.Threading.Tasks;
#else
using UniTask = System.Threading.Tasks.Task;
#endif

namespace BarnardsStar.Utilities
{
    public static class TaskUtils
    {
        public static bool IsPending(this UniTask task)
        {
#if UNITY_2020_3_OR_NEWER
            return task.Status == UniTaskStatus.Pending;
#else
            return !task.IsCompleted && !task.IsFaulted && !task.IsCanceled;
#endif
        }

        public static async UniTask SwitchToThreadPool()
        {
            // no need to switch to thread pool on the server since everything runs in a thread pool by default
#if UNITY_2020_3_OR_NEWER
            await UniTask.SwitchToThreadPool();
#endif
        }

        public static UniTask RunOnThreadPool(Func<UniTask> task)
        {
#if UNITY_2020_3_OR_NEWER
            return UniTask.RunOnThreadPool(task);
#else
            // no need to switch to thread pool on the server since everything runs in a thread pool by default
            return task();
#endif
        }

        public static IEnumerator AwaitTaskFromCoroutine(UniTask task, IRunContext runCtx, float timeout, string timeoutMessage)
        {
            yield return runCtx.WaitOneFrame();

            float waited = 0;
            while (task.IsPending() && waited < timeout)
            {
                yield return runCtx.WaitOneFrame();
                waited += runCtx.GetDeltaTime();
            }

            if (waited >= timeout)
            {
                Logger.Error(timeoutMessage);
            }
        }

        public static void PrintCurrThreadInfo(string prefix = "")
        {
            var currThread = Thread.CurrentThread;
            Logger.Info($"{prefix} Current thread info // id: {currThread.ManagedThreadId}, name: {currThread.Name}," +
                $" priority: {currThread.Priority}, isAlive: {currThread.IsAlive}, isBackground: {currThread.IsBackground}, isThreadPool: {currThread.IsThreadPoolThread}");
        }
    }
}