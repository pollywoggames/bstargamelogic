using System;

// Copies of Unity-specific or Server-specific attributes on fields that need to be available for
// both Unity and server.
namespace BarnardsStar.Attributes
{
#if UNITY_2020_3_OR_NEWER

    // This section is for attributes that exist on the server side, but not on Unity.
    // So we make dummy versions of them for Unity.

    public class BsonIgnoreAttribute : Attribute
    { }
    
#else
    
    // This section is for attributes that exist on Unity, but not on the server side.
    // So we make dummy versions of them for the server.

    // iOS Native Plugin requires a Unity serializable type for custom UserNotification data.
    public class SerializeFieldAttribute : Attribute
    { }
    
#endif
}