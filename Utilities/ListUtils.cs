using System;
using System.Collections.Generic;
using System.Linq;

namespace BarnardsStar.Utilities
{
    public static class ListUtils
    {
        public static bool CheckEquivalent<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            return a.All(b.Contains) && a.Count() == b.Count();
        }
        
        public static List<T> ShuffleList<T>(List<T> list, Randy random)
        {
            for (var i = 0; i < list.Count - 1; i++)
                Swap(list, i, random.Next(i, list.Count));

            return list;
        }

        public static void Swap<T>(IList<T> list, int i, int j)
        {
            (list[i], list[j]) = (list[j], list[i]);
        }
        
        public static List<T> RepeatedDefault<T>(int count)
        {
            return Repeated(default(T), count);
        }

        public static List<T> Repeated<T>(T value, int count)
        {
            List<T> ret = new List<T>(count);
            ret.AddRange(Enumerable.Repeat(value, count));
            return ret;
        }

        public static bool ListsEqual<T>(List<T> a, List<T> b)
        {
            if (a == null)
                return b == null;

            if (a.Count != b.Count)
                return false;

            for (int i = 0; i < a.Count; i++)
            {
                if (!Equals(a[i], b[i]))
                    return false;
            }

            return true;
        }
        
        public static string ListToString<T>(List<T> a)
        {
            if (a == null)
                return "null";

            if (a.Count == 0)
                return "[]";

            return "[" + string.Join(", ", a) + "]";
        }
        
        public static string ListToString<T>(List<T> a, Func<T, string> stringer)
        {
            if (a == null)
                return "null";

            if (a.Count == 0)
                return "[]";

            return "[" + string.Join(", ", a.Select(stringer)) + "]";
        }
    }
}