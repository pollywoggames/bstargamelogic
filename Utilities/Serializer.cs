﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BarnardsStar.Utilities
{
    public class JsonSerializer
    {
        public static JsonSerializerSettings newSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                PreserveReferencesHandling = PreserveReferencesHandling.All,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            };
        }

        private static JsonSerializerSettings _settings = null;
        public static JsonSerializerSettings serializerSettings => _settings ??= newSerializerSettings();

        public static string Serialize(object o)
        {
            return JsonConvert.SerializeObject(o, serializerSettings);
        }
        
        public static string SerializeSimple(object o)
        {
            return JsonConvert.SerializeObject(o);
        }

        public static T Deserialize<T>(string str)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(str, serializerSettings);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return default;
            }
        }
    }
}