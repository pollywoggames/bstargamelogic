using System.Collections;
using System.Collections.Generic;

namespace BarnardsStar.Utilities
{
    public class CountingDictionary<K> : IEnumerable<KeyValuePair<K, int>>
    {
        private Dictionary<K, int> dict;

        public CountingDictionary()
        {
            dict = new Dictionary<K, int>();
        }

        public void Add(K key, int val)
        {
            if (dict.TryGetValue(key, out var currVal))
            {
                dict[key] = currVal + val;
            }
            else
            {
                dict.Add(key, val);
            }
        }

        public bool TryGetValue(K key, out int val)
        {
            return dict.TryGetValue(key, out val);
        }

        public IEnumerator<KeyValuePair<K, int>> GetEnumerator()
        {
            return dict.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) dict).GetEnumerator();
        }

        public void Clear()
        {
            dict.Clear();
        }
    }
}