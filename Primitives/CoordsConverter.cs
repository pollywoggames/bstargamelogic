﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace BarnardsStar.Primitives
{
    public class CoordsConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string val = (string)value;
            int open = val.IndexOf("(");
            int mid = val.IndexOf(",");
            int end = val.IndexOf(")");
            string x_val = val.Substring(open + 1, mid - open - 1).Trim();
            string y_val = val.Substring(mid + 1, end - mid - 1).Trim();
            int x = int.Parse(x_val);
            int y = int.Parse(y_val);
            return new Coords(x, y);
        }
    }
}
