using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using BarnardsStar.Utilities;
using Newtonsoft.Json;
using SVector2 = System.Numerics.Vector2;

namespace BarnardsStar.Primitives
{
    [TypeConverter(typeof(CoordsConverter))]
    // A location in a 2d grid.
    public struct Coords : IEquatable<Coords>
    {
        public static readonly Coords
            zero = new Coords(0, 0),
            one = new Coords(1, 1),
            up = new Coords(0, 1),
            down = new Coords(0, -1),
            left = new Coords(-1, 0),
            right = new Coords(1, 0),
            upLeft = up + left,
            upRight = up + right,
            downLeft = down + left,
            downRight = down + right;

        public static readonly Coords[] OrthogonalDirections =
        {
            up, down, left, right
        };
        
        public static readonly Coords[] DiagonalDirections =
        {
            upLeft, upRight, downLeft, downRight
        };

        public static readonly Coords[] AllDirections =
        {
            up, down, left, right, upLeft, upRight, downLeft, downRight
        };
        
        public static readonly Coords[] TwoAway =
        {
            up * 2, upRight, right * 2, downRight, down * 2, downLeft, left * 2, upLeft
        };

        public readonly int x;
        public readonly int y;

        [JsonIgnore]
        public float magnitude => (float) Math.Sqrt(x * x + y * y);

        [JsonIgnore]
        public Coords normalized
        {
            get
            {
                var newX = 0;
                var newY = 0;

                if (x > 0)
                {
                    newX = 1;
                }
                else if (x < 0)
                {
                    newX = -1;
                }

                if (y > 0)
                {
                    newY = 1;
                }
                else if (y < 0)
                {
                    newY = -1;
                }

                return new Coords(newX, newY);
            }
        }

        public Coords(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Equals(Coords other)
        {
            return x == other.x && y == other.y;
        }

        public static Coords operator +(Coords a, Coords b)
        {
            return new Coords(a.x + b.x, a.y + b.y);
        }

        public static Coords operator -(Coords a, Coords b)
        {
            return new Coords(a.x - b.x, a.y - b.y);
        }
        
        public static Coords operator -(Coords a)
        {
            return new Coords(-a.x, -a.y);
        }

        public static Coords operator *(Coords c, int i)
        {
            return new Coords(c.x * i, c.y * i);
        }

        public static Coords operator *(int i, Coords c)
        {
            return new Coords(c.x * i, c.y * i);
        }

        public static bool operator ==(Coords a, Coords b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(Coords a, Coords b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return $"Coords({x},{y})";
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Coords c)
            {
                return Equals(c);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return x * 379 + y;
        }

        [Pure]
        public Point ToPoint()
        {
            return Point.FromCoords(this);
        }

        [Pure]
        public SVector2 NormalizedFloat()
        {
            var mag = magnitude;
            
            // avoid divide by zero error
            if (mag == 0)
                return SVector2.Zero;
            
            return new SVector2(x / mag, y / mag);
        }

        [Pure]
        public SVector2 ToVector2()
        {
            return new SVector2(x, y);
        }

        public Coords GetDirectionAwayFromOrigin()
        {
            bool xBiggerThanY = Math.Abs(x) >= Math.Abs(y);

            // check which quadrant it's in
            if (x >= 0 && y >= 0)
                return xBiggerThanY ? right : up;
            if (x < 0 && y >= 0)
                return xBiggerThanY ? left : up;
            if (x < 0 && y < 0)
                return xBiggerThanY ? left : down;
            else
                return xBiggerThanY ? right : down;
        }

        public static Coords FromPoint(Point point)
        {
            var x = (int) Math.Floor(point.x);
            var y = (int) Math.Floor(point.y);
            return new Coords(x, y);
        }

        public static float Distance(Coords a, Coords b)
        {
            var xs = a.x - b.x;
            var ys = a.y - b.y;
            return (float) Math.Sqrt(xs * xs + ys * ys);
        }
        
        public static float DistanceSquared(Coords a, Coords b)
        {
            var xs = a.x - b.x;
            var ys = a.y - b.y;
            return xs * xs + ys * ys;
        }

        public static float DotProduct(Coords a, Coords b)
        {
            // see: https://www.mathsisfun.com/algebra/vectors-dot-product.html
            return a.x * b.x + a.y * b.y;
        }

        public static Coords Perpendicular(Coords c)
        {
            if (c == up || c == down)
                return right;
            
            if (c == right || c == left)
                return up;
            
            Logger.Warn($"Called Coords.Perpendicular on invalid Coords: {c}. Returning 0,1");
            return up;
        }

        private static readonly Coords[] squareDirectionsUpCW = {right, down, left, up};
        private static readonly Coords[] squareDirectionsUpCCW = {left, down, right, up};
        private static readonly Coords[] squareDirectionsDownCW = {left, up, right, down};
        private static readonly Coords[] squareDirectionsDownCCW = {right, up, left, down};

        public static IEnumerable<Coords> SquareAround(Coords center, int radius, bool isOdd)
        {
            if (radius <= 0)
                return new List<Coords>() { center };
            
            return SquareAroundInDirection(center, radius, GetSquareDirectionFor(center, isOdd));
        }

        private static SquareDirection GetSquareDirectionFor(Coords coords, bool isOdd)
        {
            return isOdd ? OddSquareDirection(coords) : EvenSquareDirection(coords);
        }

        private static SquareDirection EvenSquareDirection(Coords coords)
        {
            if (coords.x >= 0)
            {
                if (coords.y >= 0)
                    // Top right quadrant - go down & left first 
                    return SquareDirection.DownCW;
                else
                    // Bottom right quadrant - go up & left first
                    return SquareDirection.UpCCW;
            }
            else
            {
                if (coords.y >= 0)
                    // Top left quadrant - go down & right first
                    return SquareDirection.DownCCW;
                else
                    // Bottom left quadrant - go up & right first
                    return SquareDirection.UpCW;
            }
        }
        
        private static SquareDirection OddSquareDirection(Coords coords)
        {
            if (coords.x > 0)
            {
                if (coords.y >= 0)
                    // Top right quadrant - go down & left first 
                    return SquareDirection.DownCW;
                else
                    // Bottom right quadrant - go up & left first
                    return SquareDirection.UpCCW;
            }
            else
            {
                if (coords.y > 0)
                {
                    // Top left quadrant - go down & right first
                    return SquareDirection.DownCCW;
                }
                else
                {
                    // special case for isOdd: x == 0
                    if (coords.x == 0)
                    {
                        return SquareDirection.UpCCW;
                    }
                    else
                    {
                        // Bottom left quadrant - go up & right first
                        return SquareDirection.UpCW;
                    }
                }
            }
        }

        private static IEnumerable<Coords> SquareAroundInDirection(Coords center, int radius, SquareDirection squareDirection)
        {
            (Coords startDirection, Coords[] squareDirections) = squareDirection switch
            {
                SquareDirection.UpCW => (upLeft, squareDirectionsUpCW),
                SquareDirection.UpCCW => (upRight, squareDirectionsUpCCW),
                SquareDirection.DownCW => (downRight, squareDirectionsDownCW),
                SquareDirection.DownCCW => (downLeft, squareDirectionsDownCCW),
                _ => throw new ArgumentOutOfRangeException(nameof(squareDirection), squareDirection, null)
            };
            return SquareAroundInternal(center, startDirection, squareDirections, radius);
        }
        
        private static IEnumerable<Coords> SquareAroundInternal(Coords center, Coords startDirection, Coords[] squareDirections, int radius)
        {
            var curr = center + (radius * startDirection);
            foreach (var direction in squareDirections)
            {
                for (int i = 0; i < radius * 2; i++)
                {
                    curr += direction;
                    yield return curr;
                }
            }
        }
    }
    
    public enum SquareDirection
    {
        UpCW, UpCCW, DownCW, DownCCW
    }
}
