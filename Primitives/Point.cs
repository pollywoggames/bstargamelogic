using System;
using System.Diagnostics.Contracts;
using Newtonsoft.Json;

namespace BarnardsStar.Primitives
{
    /// <summary>A location in 3d space.</summary>
    public struct Point
    {
        public static readonly Point
            zero = new Point(0, 0),
            one = new Point(1, 1, 1),
            up = new Point(0, 1), 
            down = new Point(0, -1), 
            left = new Point(-1, 0), 
            right = new Point(1, 0), 
            upLeft = up + left, 
            upRight = up + right, 
            downLeft = down + left, 
            downRight = down + right;

        public float x, y, z;

        [JsonIgnore]
        public float magnitude => (float) Math.Sqrt(x * x + y * y + z * z);

        public Point(float x, float y, float z = 0)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        [JsonIgnore]
        public Point normalized
        {
            get
            {
                float num = magnitude;
                return (double) num > 9.999999747378752E-06 ? this / num : Point.zero;
            }
        }

        public static float Distance(Point a, Point b)
        {
            var xs = a.x - b.x;
            var ys = a.y - b.y;
            var zs = a.z - b.z;
            return (float) Math.Sqrt(xs * xs + ys * ys + zs * zs);
        }
        
        public static bool operator ==(Point a, Point b)
        {
            return Math.Abs(a.x - b.x) < .005f
                && Math.Abs(a.y - b.y) < .005f
                && Math.Abs(a.z - b.z) < .005f;
        }

        public static bool operator !=(Point a, Point b)
        {
            return !(a == b);
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Point operator -(Point a, Point b)
        {
            return new Point(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        
        public static Point operator -(Point a)
        {
            return new Point(-a.x, -a.y, -a.z);
        }
        
        public static Point operator *(Point a, float f)
        {
            return new Point(a.x * f, a.y * f, a.z * f);
        }

        public static Point operator *(float f, Point a)
        {
            return new Point(a.x * f, a.y * f, a.z * f);
        }

        public static Point operator /(Point a, float f)
        {
            return new Point(a.x / f, a.y / f, a.z / f);
        }

        public override string ToString()
        {
            return $"Point({x},{y},{z})";
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Point p)
            {
                return Equals(p);
            }

            return false;
        }

        private bool Equals(Point other)
        {
            return Math.Abs(x - other.x) < .01f && Math.Abs(y - other.y) < .01f && Math.Abs(z - other.z) < .01f;
        }

        public override int GetHashCode()
        {
            return (int) (x * 537901 + y * 915 + z);
        }

        public Coords ToCoords()
        {
            return Coords.FromPoint(this);
        }

        public static Point FromCoords(Coords coords)
        {
            var x = coords.x + .5f;
            var y = coords.y + .5f;

            return new Point(x, y);
        }
    }
}