// ReSharper disable RedundantNameQualifier
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.InMemory;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using Newtonsoft.Json;

namespace BarnardsStar.Players
{
    public class Nexus : IDamageable
    {
        public Coords CenterCoords { get; set; }
        public List<Coords> AllCoords { get; set; }

        public int MaxHealth { get; set; }
        public int CurrHealth { get; set; }

        public bool Invulnerable;

        public bool IsShip, FacingLeft;

        [JsonIgnore] public bool hasVirtualActor => virtualActor != null && !virtualActor.IsDestroyed();
        [JsonIgnore] public INexusVirtualActor virtualActor;

        [JsonIgnore] public Action OnHealthChange;
        public Player owner { get; set; }

        [JsonIgnore] public Game game => owner.game;

        // Ensure that a nexus is only damaged 1x per event. So e.g. a shotgun which fires multiple shots will
        // only damage the nexus once.
        // This is JsonIgnore because it's always false between turns, only gets set to true in the middle of 
        // a turn's execution/replay.
        [JsonIgnore] public bool alreadyDamagedDuringThisAction;

        private bool alreadyExploding;

        private IRunContext runCtx => game.runCtx;
        private IContextProvider contextProvider => game.contextProvider;
        private ISoundContext soundCtx => game.soundCtx;

        public Nexus() { }

        public Nexus(INexusVirtualActor virtualActor, int health, bool isShip, bool facingLeft)
        {
            this.virtualActor = virtualActor;

            MaxHealth = health;
            CurrHealth = health;

            this.IsShip = isShip;
            this.FacingLeft = facingLeft;

            if (this.virtualActor != null)
            {
                SetCenterCoords(this.virtualActor.GetCenterCoords());
                this.virtualActor.Init(this);
            }
        }

        public Nexus Clone()
        {
            var clone = (Nexus) MemberwiseClone();
            
            clone.virtualActor = new InMemoryNexusVirtualActor(CenterCoords);
            clone.OnHealthChange = null;

            return clone;
        }

        public Player GetOwner()
        {
            return owner;
        }

        public IEnumerator DealDamage(int damage, Fighter origin)
        {
            yield return DealDamage(damage, origin, true);
        }

        public IEnumerator DealDamage(
            int damage,
            Fighter origin,
            bool damagedNexusDirectly,
            bool force = false)
        {
            if (!force)
            {
                if (Invulnerable
                    || CurrHealth <= 0
                    || damage <= 0
                    || (damagedNexusDirectly && alreadyDamagedDuringThisAction))
                {
                    yield break;
                }
                
                // Ensure nexus can only take 1 damage at a time
                if (damage > 1) damage = 1;
            
                // You can only damage the nexus *directly* 1x per action
                if (damagedNexusDirectly)
                    alreadyDamagedDuringThisAction = true;
            }
            
            if (contextProvider.IsPreviewing())
                contextProvider.RegisterPreview(new DamagePreview(game, damage, this));

            int h = CurrHealth - damage;
            
            if (h < 0)
            {
                h = 0;
            }
            else if (h > MaxHealth)
            {
                h = MaxHealth;
            }
            
            CurrHealth = h;
            OnHealthChange?.Invoke();
            virtualActor.PlayDamagedEffect();
            soundCtx.PlayOneShot(Sound.NexusDamage);

            if (CurrHealth <= 0)
            {
                // check if the game is now over
                var gameOver_winningTeamIdx = game.GameShouldEnd();
                
                // If the game is going to end, set the nexi of the winning team invulnerable so that they don't die
                // as a result of further consequences of the current action (e.g. a Beast w/ 1 health attacks enemy
                // base, and your own base also only has 1 health).
                //
                // Really this should probably be a draw if that occurs but for now the game doesn't support draws.
                if (gameOver_winningTeamIdx.HasValue)
                {
                    foreach (var p in game.Players.Where(p => p.TeamIdx == gameOver_winningTeamIdx.Value))
                    {
                        if (p.nexus != null)
                            p.nexus.Invulnerable = true;
                    }
                }
                
                if (!gameOver_winningTeamIdx.HasValue && origin != null && origin.Owner.IsEnemyOf(owner))
                    game.RefillKillerActionsMoves(origin);
                
                owner.OutOfGame = true;
                
                yield return runCtx.Run(ExplodeCoroutine(gameOver_winningTeamIdx.HasValue, !force));
                
#if UNITY_2020_3_OR_NEWER
                // Special case - on client only, if it's not TV and the local player's nexus has been destroyed,
                // we're most likely watching it in order to dismiss it.
                if (contextProvider.GetContextType() == ContextType.Unity // filter out previewing/in-memory
                    && owner.IsLocalControlledPlayer()
                    && !game.localPlayerSurrendered
                    && !BarnardsStar.Logic.GameManager.IsTV
                    && game.isCurrTurnReplay
                    && game.gameMode.IsOnline())
                {
                    BarnardsStar.Logic.GameManager.Instance.ShowMultiplayerPromptsAtEndOfReplay = true;
                    game.networkCtx.AckGameOver(game);
                }
#endif

                // update map's nexi coords since this nexus no longer exists (instead of cover,
                // it'll be "clear" terrain type)
                game.map.UpdateNexiCoords();
            }

            if (damagedNexusDirectly && game.OnDamageNexusDirectlyCallback != null)
            {
                yield return game.OnDamageNexusDirectlyCallback(game);
                game.OnDamageNexusDirectlyCallback = null;
            }
        }

        public int GetEffectiveHP()
        {
            return CurrHealth;
        }

        private IEnumerator ExplodeCoroutine(bool endOfGame, bool confirm)
        {
            // prevent multiple explosions
            if (alreadyExploding)
                yield break;
            alreadyExploding = true;
            
            var wg = new WaitGroup(runCtx);
            wg.Start(owner.AllFightersDie());

            yield return runCtx.WaitSeconds(.5f);
            yield return endOfGame ? virtualActor.PlayBigExplosion() : virtualActor.PlayLittleExplosion();
            
            yield return wg.Wait();

            if (endOfGame)
            {
                if (confirm && owner.IsTurn && !owner.IsAI)
                {
                    yield return game.camera.PanTilPointIsCentered(CenterCoords.ToPoint());
                    yield return runCtx.WaitSeconds(.5f);

                    // If you destroyed your own nexus on your turn, confirm that that's what you wanted to do
                    // before actually finishing the move (and give a chance to undo)
                    game.uiController.ShowSelfDestructConfirmDialog(() => game.GameOver());
                }
                else
                {
                    // wait a little longer
                    yield return runCtx.WaitSeconds(1.5f);
                    game.GameOver();
                }
            }
            else if (owner.IsTurn && owner.IsLocalControlledPlayer())
            {
                // If it's the owner's turn and they've destroyed their own nexus and it's NOT the end of the game,
                // we still need to submit the turn.
                // But first we should give them a chance to undo.
                game.uiController.ShowSelfDestructConfirmDialog(() => runCtx.Start(game.EndTurn()));
            }
        }

        public IEnumerable<Coords> GetTargetableGridCoords()
        {
            return AllCoords;
        }

        public void SetCenterCoords(Coords centerCoords)
        {
            CenterCoords = centerCoords;

            AllCoords = new List<Coords>(9) { CenterCoords };
            foreach (var direction in Coords.AllDirections)
            {
                AllCoords.Add(CenterCoords + direction);
            }
        }

        public bool IsDead()
        {
            return CurrHealth <= 0;
        }
    }
}
