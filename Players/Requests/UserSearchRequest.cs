namespace BarnardsStar.Players.Requests
{
    /// <summary>
    /// Request for searching for another user by name.
    /// </summary>
    public class UserSearchRequest
    {
        public string searchStr;
    }
}