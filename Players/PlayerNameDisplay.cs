using System;
using BarnardsAscot.bstargamelogic.Auth;
using BarnardsStar.Characters;

namespace BarnardsStar.Players
{
    // Everything you need on the frontend for displaying a player's name w/ details
    public class PlayerNameDisplay
    {
        public string Username;
        public string UserId;
        public PlayerIcon Icon;
        // For sorting - in friends list, we sort by most recently active
        public DateTime lastActivity;

        // Limit length of username
        public string GetShortenedUsername()
        {
            var username = Username;
            if (username.Length > 16)
                username = username.Substring(0, 14) + "...";
            return username;
        }
    }

    public class PlayerIcon
    {
        // Players can choose an icon to be displayed next to their name, like a profile picture.
        // It can either be a fighter (in the team color of their choice) or one of the badges they've unlocked.
        // One of these will be null.
        public FighterIcon FighterIcon;
        public BStarBadge? BadgeIcon;

        protected bool Equals(PlayerIcon other)
        {
            return Equals(FighterIcon, other.FighterIcon) && BadgeIcon == other.BadgeIcon;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PlayerIcon)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((FighterIcon != null ? FighterIcon.GetHashCode() : 0) * 397) ^ BadgeIcon.GetHashCode();
            }
        }

        public override string ToString()
        {
            return $"PlayerIcon({(BadgeIcon.HasValue ? BadgeIcon.Value.ToString() : FighterIcon.ToString())})";
        }

        // Always-unlocked team colors/fighter prototypes to randomly assign to players
        private static readonly TeamColor[] randomTeamColors = { TeamColor.Blue, TeamColor.Red };
        private static readonly FighterPrototype[] randomFighters =
        {
            FighterPrototype.HumanEnforcer,
            FighterPrototype.HumanSniper,
            FighterPrototype.HumanCommando,
            FighterPrototype.HumanBazookaman,
            FighterPrototype.RobotTank,
            FighterPrototype.RobotOverwatch,
            FighterPrototype.RobotExterminator,
            FighterPrototype.RobotSummoner,
            FighterPrototype.CritterGrump,
            FighterPrototype.CritterGunslinger,
            FighterPrototype.CritterBountyHunter,
            FighterPrototype.CritterPsion,
        };
        private static Random random = new Random();
        public static PlayerIcon GetRandomIcon()
        {
            var randomTeamColor = randomTeamColors[random.Next(randomTeamColors.Length)];
            var randomFighter = randomFighters[random.Next(randomFighters.Length)];
            return new PlayerIcon { FighterIcon = new FighterIcon(randomTeamColor, randomFighter) };
        }
    }

    public class FighterIcon
    {
        public TeamColor teamColor;
        public FighterPrototype fighterPrototype;

        public FighterIcon(TeamColor teamColor, FighterPrototype fighterPrototype)
        {
            this.teamColor = teamColor;
            this.fighterPrototype = fighterPrototype;
        }

        public override string ToString()
        {
            return $"FighterIcon({teamColor},{fighterPrototype})";
        }

        protected bool Equals(FighterIcon other)
        {
            return teamColor == other.teamColor && fighterPrototype == other.fighterPrototype;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FighterIcon)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)teamColor * 397) ^ (int)fighterPrototype;
            }
        }
    }
}