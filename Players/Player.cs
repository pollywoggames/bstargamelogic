using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Primitives;
using System.Runtime.Serialization;
using BarnardsStar.AI;
using BarnardsStar.Upgrades;
using BarnardsStar.Utilities;
using Newtonsoft.Json;

#if UNITY_2020_3_OR_NEWER
using BarnardsStar.Logic;
using BarnardsStar.Logic.Networking;
#endif

namespace BarnardsStar.Players
{
    [DataContract]
    public class Player : IEquatable<Player>
    {
        [DataMember] public List<Fighter> Fighters;

        [DataMember] public Game game;

        [DataMember] public bool HasChosenTeam;
        [DataMember] public ChosenTeam chosenTeam;

        [DataMember] public int TeamIdx;
        [DataMember] public int Idx;
        [DataMember] public Nexus nexus;
        [DataMember] public Coords SpawnPoint;
        [DataMember] public Team team;
        [DataMember] public TeamColor teamColor;
        [DataMember] public int numFightersToChoose;

        [DataMember] public string UserId;
        [DataMember] public string Username;
        [DataMember] public string EloRating;

        [DataMember] public bool OutOfGame;
        [DataMember] public bool DidSurrender;

        [DataMember] public int NextFighterId;

        [DataMember] public bool IsAI;
        [JsonIgnore] public bool IsHuman => !IsAI; 
        
        [DataMember] public AIType? aiType;
        private AIManager _aiManager;
        public AIManager aiManager
        {
            get
            {
                if (_aiManager == null && aiType.HasValue)
                {
                    _aiManager = aiType.Value switch
                    {
                        AIType.DoNothing => new DoNothingAI(game, game.randy),
                        AIType.RandomMoves => new RandomMovesAI(game, game.randy),
                        AIType.Medium => new MediumAI(game, game.randy),
                        AIType.Hard => new HardAI(game, game.randy),
                        AIType.VeryHard => new VeryHardAI(game, game.randy),
                        _ => throw new ArgumentException("Can't initialize AI manager for AI type: " + aiType.Value)
                    };
                }

                return _aiManager;
            }

            set => _aiManager = value;
        }

        public IEnumerable<Fighter> LiveFighters => Fighters?.Where(f => !f.IsDead);

        public bool IsTurn => game?.currPlayerTurn == Idx;

        public IContextProvider contextProvider => game.contextProvider;
        public IAnimationContext animCtx => game.animCtx;
        public IRunContext runCtx => game.runCtx;

        public List<Player> Enemies => game.EnemiesOf(this);
        public IEnumerable<Nexus> EnemyNexi => Enemies.Select(p => p.nexus).Where(n => n != null);

        public bool HasAnyMovesRemaining => LiveFighters.Any(f => f.ActionsLeft > 0 || f.MovesLeft > 0);

        [JsonIgnore] public PerPlayerGameSettings _backupPlayerSettings;
        [JsonIgnore] public PerPlayerGameSettings playerSettings
        {
            get
            {
                var gamePlayerSettings = game?.gameSettings?.PlayerSettings[Idx];
                return gamePlayerSettings ?? _backupPlayerSettings;
            }
        }

        // Is this a player that we have local control over?
        // ie. can we submit moves for them. Must be:
        //   a) not a TV game, AND
        //   b) a local player, OR
        //   c) an online player whose user ID is the same as the currently logged-in user.
        // Note: loggedInPlayerId is passed in as an argument because on the client, it'll be
        // the currently logged in user from UserInfo.Instance.UserId, whereas on the server
        // it'll be the userID from the current request.
        public bool IsLocalControlledPlayer(string loggedInPlayerId = null)
        {
#if UNITY_2020_3_OR_NEWER
            bool isTV = GameManager.IsTV;
            loggedInPlayerId ??= UserInfo.Instance.UserId;
#else
            const bool isTV = false;
#endif

            return !isTV && !IsAI && (game.gameMode.IsLocal() || UserId == loggedInPlayerId);
        }

        public Player() { }

        public Player(int idx, int teamIdx, bool isAI, Nexus nexus, TeamColor teamColor, Coords? spawnPoint, int numFightersToChoose)
        {
            Idx = idx;
            TeamIdx = teamIdx;
            this.IsAI = isAI;
            this.teamColor = teamColor;

            if (spawnPoint.HasValue)
                SpawnPoint = spawnPoint.Value;

            if (numFightersToChoose > 0)
            {
                this.numFightersToChoose = numFightersToChoose;
                HasChosenTeam = false;
            }
            else
            {
                HasChosenTeam = true;
            }

            if (nexus != null)
            {
                this.nexus = nexus;
                this.nexus.owner = this;
            }

            Fighters = new List<Fighter>();
        }

        public override string ToString()
        {
            return $"Player(idx:{Idx},color:{teamColor},playerId:{UserId},isAI:{IsAI},aiType:{aiType})";
        }

        public IEnumerator ChooseTeamAndSpawn(ChosenTeam _chosenTeam, bool shouldCallStarTurnAgain = true)
        {
            ChooseTeam(_chosenTeam);

            yield return animCtx.AnimateFightersSpawn(Fighters, false);

            if (shouldCallStarTurnAgain)
            {
                yield return runCtx.Run(game.StartTurn());
            }
        }

        public void ChooseTeam(ChosenTeam _chosenTeam)
        {
            HasChosenTeam = true;
            this.chosenTeam = _chosenTeam;

            team = _chosenTeam.team;

            foreach (var fighterPrototype in _chosenTeam.fighterChoices)
            {
                var nextCoords = ClearCoordsBySpawnPoint();
                var va = contextProvider.ProvideFighterVirtualActor(nextCoords.ToPoint());
                AddFighter(fighterPrototype, va);
            }

            nexus?.virtualActor.RefreshSprite(team, teamColor);
        }

        public Coords ClearCoordsBySpawnPoint(List<Coords> overrideBlockedCoords = null)
        {
            return game.map.ClearCoordsBy(SpawnPoint, overrideBlockedCoords);
        }

        public Fighter AddFighter(FighterPrototype prototype, IFighterVirtualActor virtualActor, bool initForGame = true)
        {
            var fighter = new Fighter(this, prototype, virtualActor, initForGame: initForGame);

            
            if (playerSettings != null && playerSettings.fighterUpgrades != null &&
                playerSettings.fighterUpgrades.TryGetValue(prototype, out var upgrades))
            {
                fighter.Upgrades = upgrades;
                fighter.ReapplyUpgrades();
            }
            
            Fighters.Add(fighter);
            game?.RefreshObjects();
            return fighter;
        }

        public Player Clone()
        {
            return (Player) MemberwiseClone();
        }
        
        public static bool operator==(Player a, Player b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Player a, Player b)
        {
            return !(a == b);
        }

        public bool Equals(Player other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Idx == other.Idx;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Player) obj);
        }

        public override int GetHashCode()
        {
            return Idx;
        }

        public bool IsFriendlyTo(Player player)
        {
            // same player
            return this == player
                   // same team
                   || this.TeamIdx == player.TeamIdx;
        }

        public bool IsEnemyOf(Player player)
        {
            return !IsFriendlyTo(player);
        }
        
        public IEnumerator AllFightersDie()
        {
            WaitGroup wg = new WaitGroup(runCtx);
            
            yield return runCtx.WaitSeconds(.25f);

            // Make a copy rather than operating on LiveFighters directly, since
            // if there are any minions, they'll be removed from the collection,
            // resulting in InvalidOperationException due to the collection being
            // modified while looping over it.
            var fighters = LiveFighters.ToList();
            foreach (var fighter in fighters)
            {
                // Deal massive damage, not just CurrHealth, in case they have a shield or other status effects
                wg.Start(fighter.DealDamage(999, null));
                yield return runCtx.WaitSeconds(.5f);
            }

            yield return wg.Wait();
        }

        public bool NexusIsDead()
        {
            return nexus == null || nexus.CurrHealth <= 0;
        }
    }

    public enum AIType
    {
        DoNothing = 0,
        RandomMoves = 1,
        Medium = 2,
        Hard = 3,
        VeryHard = 4,
    }

    public static class AITypeExtensions
    {
        public static string Name(this AIType aiType)
        {
            return aiType switch
            {
                AIType.DoNothing => "Trivial",
                AIType.RandomMoves => "Easy",
                AIType.Medium => "Medium",
                AIType.Hard => "Hard",
                AIType.VeryHard => "Very Hard",
                _ => throw new ArgumentOutOfRangeException(nameof(aiType), aiType, null)
            };
        }
    }
}