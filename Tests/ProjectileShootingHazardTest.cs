// using System.Collections;
// using System.Collections.Generic;
// using Assets.bstargamelogic.Tests;
// using BarnardsStar.Abstract.Providers.InMemory;
// using BarnardsStar.Abstract.Providers.Interfaces;
// using BarnardsStar.Characters;
// using BarnardsStar.Games;
// using BarnardsStar.Maps;
// using BarnardsStar.Players;
// using BarnardsStar.Primitives;
// using NUnit.Framework;
//
// public class ProjectileShootingHazardTest
// {
//     [Test]
//     public void ProjectileShootingHazardTestSimplePasses()
//     {
//         IContextProvider contextProvider = new InMemoryContextProvider();
//
//         MapLayout mapLayout = MapLayout.Lookup(MapName.Testing, 0);
//         MapPrototype mapPrototype = TestUtils.I.LoadMap(mapLayout);
//
//         var p0nexus = new Nexus(contextProvider.ProvideNexusVirtualActor(new Coords(-10, -10)));
//         var p0 = new Player(0, 0, PlayerType.Player, p0nexus, TeamColor.Blue,
//             mapPrototype.SpawnInfo.BluePlayerSpawnPoint, 4);
//         
//         var p1nexus = new Nexus(contextProvider.ProvideNexusVirtualActor(new Coords(-10, -10)));
//         var p1 = new Player(1, 1, PlayerType.Player, p1nexus, TeamColor.Red,
//             mapPrototype.SpawnInfo.RedPlayerSpawnPoint, 4);
//         
//         Game game = new Game(
//             contextProvider,
//             mapPrototype.GameMap,
//             new List<Player> { p0, p1 },
//             0,
//             WinCondition.DestroyEnemyNexus
//         );
//         
//         // still working on this
//     }
// }
