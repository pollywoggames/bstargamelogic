// using BarnardsStar.Logic.Utilities;
// using BarnardsStar.Maps;
//
// namespace Assets.bstargamelogic.Tests
// {
//     public interface TestUtility
//     {
//         MapPrototype LoadMap(MapLayout mapLayout);
//     }
//     
//     public class TestUtils : TestUtility
//     {
//         private static TestUtility _instance;
//         public static TestUtility I => _instance ??= new TestUtils();
//
// #if UNITY_2020_1_OR_NEWER
//         public MapPrototype LoadMap(MapLayout mapLayout)
//         {
//             return MapUtils.LoadMap(mapLayout);
//         }
// #else
//         public MapPrototype LoadMap(MapLayout mapLayout)
//         {
//             
//         }
// #endif
//     }
// }