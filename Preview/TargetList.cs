﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Preview
{
    public class TargetList : IEquatable<TargetList>
    {
        // Coordinates that can't be hit because they're behind cover but we still want to highlight them
        public List<Coords> coveredCoords;
        public List<Target> validTargets;

        private Dictionary<Coords, Target> targetsByCoords;
        
        public int Count => validTargets.Count;

        public TargetList()
        {
            validTargets = new List<Target>();
            coveredCoords = new List<Coords>();
            
            targetsByCoords = new Dictionary<Coords, Target>();
        }

        public void Add(Target t)
        {
            validTargets.Add(t);
            if (!targetsByCoords.ContainsKey(t.coords))
                targetsByCoords.Add(t.coords, t);
        }

        public void AddRange(IEnumerable<Target> targets)
        {
            foreach (var t in targets)
                Add(t);
        }

        public void AddCoveredCoords(Coords v)
        {
            coveredCoords.Add(v);
        }

        public Target? GetByCoords(Coords c)
        {
            if (targetsByCoords.TryGetValue(c, out var t))
                return t;
            
            return null;
        }

        public bool HasTargetedOrCovered(Coords c)
        {
            return validTargets.Any(t => t.coords == c)
                   || coveredCoords.Any(covered => covered == c);
        }

        public bool Equals(TargetList other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ListUtils.CheckEquivalent(coveredCoords, other.coveredCoords) && ListUtils.CheckEquivalent(validTargets, other.validTargets);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TargetList) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((coveredCoords != null ? coveredCoords.GetHashCode() : 0) * 397) ^ (validTargets != null ? validTargets.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return $"TargetList({String.Join(",",validTargets)},covered={String.Join(",",coveredCoords)})";
        }
    }
}