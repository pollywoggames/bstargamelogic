using BarnardsStar.Abilities;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class StatusEffectPreview : PreviewOutcome, IPreviewAction 
    {
        public Fighter f;
        public StatusEffect type;
        public int strength;

        public StatusEffectPreview(Fighter f, StatusEffect type, int strength) : base(null)
        {
            this.f = f;
            this.type = type;
            this.strength = strength;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            var score = type switch
            {
                StatusEffect.Stunned => DamageMultiplier * 5,
                StatusEffect.Electrified => DamageMultiplier * 5,
                StatusEffect.Slowed => DamageMultiplier * 3,
                StatusEffect.CantBeStunned => -5,
                _ => 0
            };

            if (type.Details().PreviewScoreDependsOnStrength)
                score *= strength;

            if (f.Owner.IsFriendlyTo(owner))
            {
                score = -.7f * score;
            }

            return score;
        }

        public override string GetDescription(Player owner, PathfindingMap pathfindingMap)
        {
            return $"{f.id} gets {strength} {type}";
        }
    }
}

