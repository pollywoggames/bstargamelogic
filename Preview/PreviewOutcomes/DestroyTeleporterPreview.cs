using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class DestroyTeleporterPreview : PreviewOutcome, IPreviewAction
    {
        public Coords coords;
        
        public DestroyTeleporterPreview(Game game, Coords coords) : base(game)
        {
            this.coords = coords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            if (game.map._teleportersByCoords.TryGetValue(coords, out var tp) && tp.Owner != null)
            {
                return tp.Owner.IsFriendlyTo(owner) ? -5 : 5;
            }

            return 0;
        }
    }
}