using System;
using System.Linq;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class HazardPreview : PreviewOutcome, IPreviewAction 
    {
        public HazardType type;
        public Coords coords;

        public HazardPreview(Game game, Coords coords, HazardType type) : base(game)
        {
            this.type = type;
            this.coords = coords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            float score = 0;
            
            float value = type switch
            {
                HazardType.Acid => Hazard.AcidDamage,
                HazardType.Oil => Hazard.FireDamage * .4f,
                HazardType.Water => 1f,
                HazardType.Fire => Hazard.FireDamage,
                HazardType.LandMine => 4f, // can't be placed directly on enemies, so higher score for placing it adjacent 
                HazardType.SmokeScreen => -2f, // it's a good thing to place near friendlies 
                HazardType.AcidBubble => 1f,
                HazardType.Electricity => Hazard.ElectricityDamage, 
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, "can't find damageMultiplier for hazard type")
            };

            value *= DamagePreview.DamageMultiplier / 4f;

            if (game.TryGetFighter(coords, out var foundFighter))
            {
                score += ScoreForFighter(owner, foundFighter, value);
            }
            if (score != 0) return score;

            value *= .75f;
            
            // check adjacent squares for less value
            for (var i = 0; i < Coords.OrthogonalDirections.Length; i++)
            {
                var dir = Coords.OrthogonalDirections[i];
                if (game.TryGetFighter(coords + dir, out foundFighter))
                    score += ScoreForFighter(owner, foundFighter, value);
            }
            if (score != 0) return score;

            value *= .75f;
            
            // check squares 2 away for even less value
            for (var i = 0; i < Coords.TwoAway.Length; i++)
            {
                var dir = Coords.TwoAway[i];
                if (game.TryGetFighter(coords + dir, out foundFighter))
                    score += ScoreForFighter(owner, foundFighter, value);
            }
            return score;
        }

        private float ScoreForFighter(Player owner, Fighter foundFighter, float value)
        {
            // try to put hazard under enemies, not friendlies
            return foundFighter.Owner.IsFriendlyTo(owner) ? -value * .75f : value;
        }

        public override string ToString()
        {
            return $"HazardPreview({type},{coords})";
        }
    }
}

