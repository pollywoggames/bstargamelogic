using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class DeployCoverPreview : PreviewOutcome, IPreviewAction
    {
        public Coords coords;
        
        public DeployCoverPreview(Game game, Coords coords) : base(game)
        {
            this.coords = coords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // try to place cover next to friendlies
            var ret = 0;
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var adjCoords = coords + dir;
                if (game.TryGetFighter(adjCoords, out var adj))
                    ret += adj.Owner.IsFriendlyTo(owner) ? -2 : 2;
            }
            
            return ret;
        }
    }
}

