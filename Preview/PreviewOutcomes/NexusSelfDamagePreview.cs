using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    /// <summary>
    /// Used to prevent easy/medium AI from attacking their own nexus. i.e. randomly shooting over it
    /// or attacking it with a melee attack.
    ///
    /// Of course, it doesn't actually damage their own nexus. But it looks weird. So we want to prevent it anyway. 
    /// </summary>
    public class NexusSelfDamagePreview : PreviewOutcome, IPreviewAction
    {
        public NexusSelfDamagePreview(Game game) : base(game)
        { }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // Note: if we start looking more than a turn ahead, this will be problematic
            return -1;
        }
    }
}