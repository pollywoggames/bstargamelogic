using System;
using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class CoverDamagePreview : PreviewOutcome, IPreviewAction
    {
        private const int DistanceDiffThreshold = 5;
        
        public Coords coords;
        public int amount;

        public CoverDamagePreview(Game game, Coords coords, int amount) : base(game)
        {
            this.coords = coords;
            this.amount = amount;
        }

        public override string ToString()
        {
            return $"CoverDamage({amount}@{coords})";
        }
        
        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // try to destroy/damage cover next to enemies
            float ret = 0;
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var adjCoords = coords + dir;
                if (game.TryGetFighter(adjCoords, out var adj))
                    ret += amount * 5 * (adj.Owner.IsFriendlyTo(owner) ? -1 : 1);
            }
            
            // Try to destroy/damage cover that has a large pathfindingMap diff
            int verticalDiff = GetPathfindingMapDiff(pathfindingMap, coords + Coords.up, coords + Coords.down);
            int horizontalDiff = GetPathfindingMapDiff(pathfindingMap, coords + Coords.left, coords + Coords.right);
            int maxDiff = Math.Max(verticalDiff, horizontalDiff);
            if (maxDiff >= DistanceDiffThreshold)
                ret += 15;

            // If we already have some points, then increase those points for destroying
            // (rather than damaging) the cover.
            if (ret > 0 && amount >= 2 || game.map.IsCoverDamaged(coords))
                ret *= 2;
            
            // cap maximum cover damage score
            if (ret > 20f)
                ret = 20f;
            
            // Don't damage cover for no reason
            if (ret == 0)
                ret = -5;

            return ret;
        }

        private int GetPathfindingMapDiff(PathfindingMap pathfindingMap, Coords coords1, Coords coords2)
        {
            if (!pathfindingMap.IsInBounds(coords1) || !pathfindingMap.IsInBounds(coords2))
                return 0;

            // Only give bonus for destroying cover that is clear on both sides
            if (game.map.GetTerrainType(coords1) != TerrainType.Clear ||
                game.map.GetTerrainType(coords2) != TerrainType.Clear)
                return 0;

            int score1 = Math.Abs(pathfindingMap.Get(coords1));
            int score2 = Math.Abs(pathfindingMap.Get(coords2));

            return Math.Abs(score1 - score2);
        }

        public override string GetDescription(Player owner, PathfindingMap pathfindingMap)
        {
            var ret = "";
            
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var adjCoords = coords + dir;
                if (game.TryGetFighter(adjCoords, out var adj))
                {
                    var goodOrBad = adj.Owner.IsFriendlyTo(owner);
                    ret += $" ({goodOrBad} adjacent to fighter {adj.id} @ {adjCoords})";
                }
            }
            
            int verticalDiff = GetPathfindingMapDiff(pathfindingMap, coords + Coords.up, coords + Coords.down);
            int horizontalDiff = GetPathfindingMapDiff(pathfindingMap, coords + Coords.left, coords + Coords.right);

            if (verticalDiff >= DistanceDiffThreshold)
                ret +=
                    $" (vertical diff is {verticalDiff}, {pathfindingMap.Get(coords + Coords.up)} @ {coords + Coords.up})" +
                    $" and {pathfindingMap.Get(coords + Coords.down)} @ {coords + Coords.down})";
            if (horizontalDiff >= DistanceDiffThreshold)
                ret +=
                    $" (horizontal diff is {horizontalDiff}, {pathfindingMap.Get(coords + Coords.right)} @ {coords + Coords.right})" +
                    $" and {pathfindingMap.Get(coords + Coords.left)} @ {coords + Coords.left})";
            
            
            // If we already have some points, then increase those points for destroying
            // (rather than damaging) the cover.
            if (amount >= 2 || game.map.IsCoverDamaged(coords))
                ret += " (destroys cover)";
            
            return ret;
        }
    }
}

