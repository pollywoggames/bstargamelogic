using System;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class SummonMinionPreview : PreviewOutcome
    {
        public Coords coords;
        public FighterPrototype fighterPrototype;
        
        public SummonMinionPreview(Game game, Coords coords, FighterPrototype fighterPrototype) : base(game)
        {
            this.coords = coords;
            this.fighterPrototype = fighterPrototype;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            float ret = 20;
            
            // extra points for being closer to enemy nexus
            var enemyNexus = game.EnemiesOf(owner)[0].nexus;
            if (enemyNexus != null)
            {
                var enemyNexusCoords = game.EnemiesOf(owner)[0].nexus.CenterCoords;
                if (Coords.Distance(coords, enemyNexusCoords) <= 10)
                    ret += 5;
                if (Coords.Distance(coords, enemyNexusCoords) <= 5)
                    ret += 5;
            }

            // extra points for being next to cover
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var currCoords = coords + dir;
                if (game.map.GetTerrainType(currCoords) == TerrainType.Cover) ret += 3;
            }
            
            // less points for spawning on a hazard where they'll take damage
            var minionImmunities = AllFighters.Instance.All[fighterPrototype].immunities;
            if (owner.game.TryGetHazard(coords, out var h) && !minionImmunities.Contains(h.type))
            {
                ret -= h.PreviewDamage * 2.5f;
            }

            return ret;
        }
    }
}
