﻿using BarnardsStar.Abilities;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class CooldownPreview : PreviewOutcome, IPreviewAction
    {
        private Fighter fighter;
        private BSAbility ability;

        public CooldownPreview(Game game, Fighter fighter, BSAbility ability) : base(game)
        {
            this.fighter = fighter;
            this.ability = ability;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            return fighter.Owner.IsFriendlyTo(owner) ? -4f : 4f;
        }
    }
}