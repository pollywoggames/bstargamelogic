using System;
using System.Linq;
using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class CreateBurrowPreview : PreviewOutcome, IPreviewAction
    {
        public Coords coords;
        public Coords otherBurrowCoords;
        
        public CreateBurrowPreview(Game game, Coords coords, Coords otherBurrowCoords) : base(game)
        {
            this.coords = coords;
            this.otherBurrowCoords = otherBurrowCoords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // try to place burrow next to other fighters (friendly or enemy)
            float nextToFighters = Coords.OrthogonalDirections
                .Select(dir => coords + dir)
                .Count(adjCoords => game.IsFighterAt(adjCoords));

            float farFromOtherBurrow = (otherBurrowCoords - coords).magnitude;

            return (int) Math.Round(nextToFighters / 2f + farFromOtherBurrow / 3f);
        }
    }
}