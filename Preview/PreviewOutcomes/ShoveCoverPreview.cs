using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class ShoveCoverPreview : PreviewOutcome, IPreviewAction
    {
        public Coords srcCoords, dstCoords;
        
        public ShoveCoverPreview(Game game, Coords src, Coords dst) : base(game)
        {
            srcCoords = src;
            dstCoords = dst;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            return 2;
        }
    }
}