using System;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Utilities;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class DamagePreview : PreviewOutcome, IPreviewAction, IEquatable<DamagePreview>
    {
        public int amount;
        public IDamageable target;
        public bool ignoreShield;
        public bool killsTarget;

        public DamagePreview(Game game, int amount, IDamageable target, bool killsTarget = false, bool ignoreShield = false) : base(game)
        {
            this.amount = amount;
            this.target = target;
            this.killsTarget = killsTarget;
            this.ignoreShield = ignoreShield;
        }

        public bool Equals(DamagePreview other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return amount == other.amount && Equals(target, other.target);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DamagePreview) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (amount * 397) ^ (target != null ? target.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return $"DamagePreview({target},amount={amount})";
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            try
            {
                if (amount == 0)
                {
                    return 0;
                }

                var isEnemy = target != null && target.GetOwner().IsEnemyOf(owner);

                var multiplier = DamageMultiplier;
                if (target is Nexus)
                {
                    multiplier = NexusDamageMultiplier;
                }

                var damageScore = multiplier * (isEnemy ? amount : amount / 2);

                if (killsTarget) damageScore += KillBonus;

                if (!isEnemy)
                    damageScore = -damageScore;

                return damageScore;
            }
            catch (Exception e)
            {
                Logger.Warn(e);
                return 1;
            }
        }

        public override string GetDescription(Player owner, PathfindingMap pathfindingMap)
        {
            string damageableName = target switch
            {
                Fighter damageableFighter => damageableFighter.id,
                Nexus damageableNexus => $"Player {damageableNexus.owner.Idx + 1}'s nexus",
                _ => "??"
            };

            var ret = $"{damageableName} takes {amount} damage";

            if (killsTarget)
                ret += " (kills target)";

            return ret;
        }
    }
}