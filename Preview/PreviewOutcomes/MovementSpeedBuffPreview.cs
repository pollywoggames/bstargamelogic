using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class MovementSpeedBuffPreview : PreviewOutcome, IPreviewAction
    {
        public Fighter f;
        public float amount;

        public MovementSpeedBuffPreview(Fighter f, float amount) : base(f.Owner.game)
        {
            this.f = f;
            this.amount = amount;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            float score = 5f * amount;

            // negative half score for buffing enemies
            if (owner.IsEnemyOf(f.Owner))
                score = -score / 2f;
            
            return score;
        }
    }
}