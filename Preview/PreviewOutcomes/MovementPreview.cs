using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using SVector2 = System.Numerics.Vector2;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class MovementPreview : PreviewOutcome, IEquatable<MovementPreview>
    {
        // TODO make this dynamic based on the enemy fighter's longest-range skill, and targeting pattern
        public const float CloseThreshold = 3f;

        public Fighter fighter;
        public Coords startingCoords;
        public List<SingleMove> moves;

        public Coords lastCoords { get; }
        
        public MovementPreview(Game game, Fighter f, Coords startingCoords, List<SingleMove> p) : base(game)
        {
            fighter = f;
            moves = p;

            this.startingCoords = startingCoords;

            lastCoords = this.startingCoords;
            foreach (var move in moves)
            {
                if (move.disableScore)
                    break;

                lastCoords = move.coords;
            }
        }
        
        public bool Equals(MovementPreview other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(fighter, other.fighter) && Equals(moves, other.moves);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MovementPreview) obj);
        }

        public static bool operator ==(MovementPreview a, MovementPreview b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(MovementPreview a, MovementPreview b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((fighter != null ? fighter.GetHashCode() : 0) * 397) ^ (moves != null ? moves.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return $"MovementPreview({fighter.id},{lastCoords}";
        }

        // for reference as new CalculateScore implementation is improved 
        private float CalculateScoreFallback(Player owner)
        {
            Logger.Warn($"MovementPreview.CalculateScore called without pathfinding map, falling back to old implementation. fighter={fighter.id}");

            // if we're moving an enemy fighter, static score of 1
            if (fighter.Owner.IsEnemyOf(owner))
                return 1;
            
            float ret = 0;
            
            // if there's an enemy close by our nexus, defend it is 1st priority
            if (owner.nexus != null && enemiesCloseToCoords(owner, owner.nexus.CenterCoords))
            {
                ret += MoveTowardsPoint(owner.nexus.CenterCoords);
            }
            else
            {
                // attack enemy instead
                var enemies = game.EnemiesOf(owner);

                foreach (var enemy in enemies)
                {
                    // if we're already close to any enemies, attack them
                    Fighter closestEnemyFighter = null;
                    float closestEnemyDist = float.MaxValue;
                    foreach (var enemyFighter in enemy.LiveFighters)
                    {
                        var dist = Coords.Distance(enemyFighter.coords, fighter.coords);
                        if (dist < closestEnemyDist)
                        {
                            closestEnemyFighter = enemyFighter;
                            closestEnemyDist = dist;
                        }
                    }

                    // if an enemy is closer than CloseThreshold, we prioritize attacking them
                    if (closestEnemyFighter != null && closestEnemyDist < CloseThreshold)
                    {
                        ret += MoveTowardsPoint(closestEnemyFighter.coords);
                    }
                    // else, move closer to our target (enemy nexus, or just enemies if there is no nexus)
                    else if (enemy.nexus != null)
                    {
                        ret += MoveTowardsPoint(enemy.nexus.CenterCoords);
                    }
                    // last, prioritize moving towards enemy fighters outside CloseThreshold (if there are any)
                    else if (closestEnemyFighter != null)
                    {
                        ret += MoveTowardsPoint(closestEnemyFighter.coords);
                    }
                }
            }

            ret += prioritizeCover(owner);

            return ret;
        }

         private int MoveTowardsPoint(Coords nexusCoords)
         {
             var currDist = Coords.Distance(fighter.coords, nexusCoords);
             var newDist = Coords.Distance(lastCoords, nexusCoords);
             return (int) Math.Round(currDist - newDist);
         }

         private bool enemiesCloseToCoords(Player owner, Coords coords)
         {
             var close = false;
             foreach (var enemy in game.EnemiesOf(owner).SelectMany(p => p.LiveFighters))
             {
                 var dist = Coords.Distance(enemy.coords, coords);
                 if (dist < CloseThreshold) close = true;
             }

             return close;
         }

         private float prioritizeCover(Player owner)
         {
             float ret = 0;
             
             // stick by cover if enemies are in the vicinity
             if (enemiesCloseToCoords(owner, lastCoords))
             {
                 foreach (var dir in Coords.OrthogonalDirections)
                 {
                     var currCoords = lastCoords + dir;
                     // get points for being next to cover
                     if (game.map.GetTerrainType(currCoords) == TerrainType.Cover)
                         ret += 5;
                 }
             }

             return ret;
         }
         
         // Prioritize standing next to cover, and use dot product towards enemies to prioritize
         // putting the cover in between the fighter and enemies.
         private float prioritizeCoverV2(Player owner)
         {
             float ret = 0;

             const float closeThresholdSquared = 8f * 8f;
             
             var closeEnemiesNormVectors =
                 game.EnemiesOf(owner)
                     .SelectMany(p => p.LiveFighters)
                     .Where(f => Coords.DistanceSquared(f.coords, lastCoords) <= closeThresholdSquared)
                     .Select(f => (f.coords - lastCoords).NormalizedFloat())
                     .ToList();

             if (closeEnemiesNormVectors.Count == 0)
                 // No close enemies, don't worry about cover
                 return 0;

             float addingForCover = 0;
             foreach (var dir in Coords.OrthogonalDirections)
             {
                 var currCoords = lastCoords + dir;
                 // are we next to cover in this direction?
                 if (game.map.GetTerrainType(currCoords) == TerrainType.Cover)
                 {
                     foreach (var closeEnemyNormVector in closeEnemiesNormVectors)
                     {
                         var dotProduct = DotProduct(dir.ToVector2(), closeEnemyNormVector);
                         // We clamp to the range [0,1] so that we add points for putting cover
                         // in between us and enemies, but we don't take away points for having cover
                         // behind us.
                         addingForCover += MoreMath.Clamp(dotProduct, 0, 1f) * 5f;
                     }
                 }
             }
             addingForCover = Math.Min(addingForCover, 10f);
             ret += addingForCover;

             return ret;
         }
         
         private string prioritizeCoverV2Description(Player owner)
         {
             const float closeThresholdSquared = 8f * 8f;
             
             var closeEnemiesNormVectors =
                 game.EnemiesOf(owner)
                     .SelectMany(p => p.LiveFighters)
                     .Where(f => Coords.DistanceSquared(f.coords, lastCoords) <= closeThresholdSquared)
                     .Select(f => (f.coords - lastCoords).NormalizedFloat())
                     .ToList();

             if (closeEnemiesNormVectors.Count == 0)
                 // No close enemies, don't worry about cover
                 return "";

             List<string> things = new List<string>();
             foreach (var dir in Coords.OrthogonalDirections)
             {
                 var currCoords = lastCoords + dir;
                 // are we next to cover in this direction?
                 if (game.map.GetTerrainType(currCoords) == TerrainType.Cover)
                 {
                     foreach (var closeEnemyNormVector in closeEnemiesNormVectors)
                     {
                         var dotProduct = DotProduct(dir.ToVector2(), closeEnemyNormVector);
                         // We clamp to the range [0,1] so that we add points for putting cover
                         // in between us and enemies, but we don't take away points for having cover
                         // behind us.
                         var adding = MoreMath.Clamp(dotProduct, 0, 1f) * 5f;
                         if (adding > 0)
                            things.Add($"Cover in direction {dir.x},{dir.y} worth {adding}");
                     }
                 }
             }

             if (things.Count > 0)
                 return string.Join(" <> ", things);

             return "";
         }
         
         public static float DotProduct(SVector2 a, SVector2 b)
         {
             // see: https://www.mathsisfun.com/algebra/vectors-dot-product.html
             return a.X * b.X + a.Y * b.Y;
         }

         protected override float CalculateScore(Player owner, PathfindingMap pfmap)
         {
             if (startingCoords == lastCoords)
                 return 0;
             
             if (pfmap == null)
                 return CalculateScoreFallback(owner);
             
             var startingDist = pfmap.Get(startingCoords);
             
             var endingDist = pfmap.Get(lastCoords);
             // max score at 6 distance from enemy/nexus
             if (endingDist <= 6) endingDist = 6;

             float score = (startingDist - endingDist) * 2;
             score = MoreMath.Clamp(score, -20f, 20f);

             score += prioritizeCoverV2(owner);

             if (fighter.Owner.IsEnemyOf(owner))
                 score = -score;
             
             return score;
         }

         public override string GetDescription(Player owner, PathfindingMap pathfindingMap)
         {
             return $"{fighter.id} moves " +
                    $"from {startingCoords.x},{startingCoords.y} (dist: {pathfindingMap.Get(startingCoords)}) " +
                    $"to {lastCoords.x},{lastCoords.y} (dist: {pathfindingMap.Get(lastCoords)})"
                    + prioritizeCoverV2Description(owner);
         }
    }
}