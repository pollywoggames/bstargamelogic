using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class CreateTeleporterPreview : PreviewOutcome, IPreviewAction
    {
        public Coords coords;
        
        public CreateTeleporterPreview(Game game, Coords coords) : base(game)
        {
            this.coords = coords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // TODO
            return 0;
        }
    }
}