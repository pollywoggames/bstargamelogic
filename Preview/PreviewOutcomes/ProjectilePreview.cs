using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class ProjectilePreview : PreviewOutcome, IPreviewAction, IEquatable<ProjectilePreview>
    {
        public List<Point> points;
        public bool hitAnyone;

        public ProjectilePreview(Game game, List<Point> points, bool hitAnyone) : base(game)
        {
            this.points = points;
            this.hitAnyone = hitAnyone;
        }

        public override string ToString()
        {
            return $"ProjectilePreview({String.Join(",", points.Select(p => p.ToCoords()))})";
        }

        public bool Equals(ProjectilePreview other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(points, other.points);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProjectilePreview) obj);
        }

        public override int GetHashCode()
        {
            return (points != null ? points.GetHashCode() : 0);
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // if (!hitAnyone)
            //     return -10;
            
            return 0;
        }
    }
}