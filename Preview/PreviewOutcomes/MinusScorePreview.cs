using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class MinusScorePreview : PreviewOutcome, IPreviewAction 
    {
        public string desc;
        public float score;

        public MinusScorePreview(Game game, string desc, float score) : base(game)
        {
            this.desc = desc;
            this.score = score;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            // make sure it's actually negative
            if (score > 0) score = -score;

            return score;
        }
    }
}