using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class GainShieldPreview : PreviewOutcome, IPreviewAction
    {
        public Fighter f;
        public int amount;
        public bool shouldCountScore;

        public GainShieldPreview(Fighter f, int amount, bool shouldCountScore = true) : base(f.Owner.game)
        {
            this.f = f;
            this.amount = amount;
            this.shouldCountScore = shouldCountScore;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            if (!shouldCountScore)
                return 0;
            
            float score = DamagePreview.DamageMultiplier * amount;

            // negative score for giving shields to enemies
            // (and make it smaller to compensate for other benefits such as stunning)
            if (owner.IsEnemyOf(f.Owner))
                score = -score / 2f;
            
            return score;
        }
    }
}