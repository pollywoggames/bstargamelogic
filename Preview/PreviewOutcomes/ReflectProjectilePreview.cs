using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class ReflectProjectilePreview : PreviewOutcome, IPreviewAction
    {
        public Coords reflectorCoords, shooterCoords;

        public ReflectProjectilePreview(Game game, Coords reflectorCoords, Coords shooterCoords) : base(game)
        {
            this.reflectorCoords = reflectorCoords;
            this.shooterCoords = shooterCoords;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            if (game.TryGetFighter(reflectorCoords, out var reflector))
            {
                return reflector.Owner.IsFriendlyTo(owner) ? -2 : 2;
            }

            return 0;
        }
    }
}

