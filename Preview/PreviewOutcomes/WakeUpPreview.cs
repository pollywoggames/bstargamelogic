﻿using BarnardsStar.AI;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public class WakeUpPreview : PreviewOutcome, IPreviewAction
    {
        public Fighter self;

        public WakeUpPreview(Fighter self) : base(self.game)
        {
            this.self = self;
        }

        protected override float CalculateScore(Player owner, PathfindingMap pathfindingMap)
        {
            return 0;
        }
    }
}