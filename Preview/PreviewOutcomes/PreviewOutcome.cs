﻿using BarnardsStar.AI;
using BarnardsStar.Games;
using BarnardsStar.Players;

namespace BarnardsStar.Preview.PreviewOutcomes
{
    public abstract class PreviewOutcome
    {
        public delegate int ScoreOverride(Player owner);

        protected const float NexusDamageMultiplier = 25;
        public const float DamageMultiplier = 10;
        public const float KillBonus = 50;

        private float? _score;
        
        public ScoreOverride scoreOverride;
        public Game game;

        protected PreviewOutcome(Game game)
        {
            this.game = game;
        }

        public virtual string GetDescription(Player owner, PathfindingMap pathfindingMap)
        {
            return "";
        }

        protected abstract float CalculateScore(Player owner, PathfindingMap pathfindingMap);
        
        public float GetScore(Player owner, PathfindingMap pathfindingMap)
        {
            // if (_score == null)
            // {
                if (scoreOverride != null)
                {
                    _score = scoreOverride(owner);
                }
                else
                {
                    var score = CalculateScore(owner, pathfindingMap);
                    _score = score;
                }
            // }
        
            return _score.Value;
        }
    }
}