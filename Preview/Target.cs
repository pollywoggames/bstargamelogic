﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Preview
{
    public struct Target : IEquatable<Target>
    {
        public Coords coords;
        public HashSet<Target> Linked;

        public Target(Coords coords, List<Target> toLink = null)
        {
            this.coords = coords;

            Linked = new HashSet<Target>();

            toLink?.ForEach(Link);
        }

        public void Link(Target tc)
        {
            Linked.Add(tc);
            tc.Linked.Add(this);
        }

        public List<Target> GetAllLinked()
        {
            var ret = new List<Target>(Linked.Count + 1) {this};
            ret.AddRange(Linked);
            return ret;
        }

        // private List<DamagePreview> GetDamages()
        // {
        //     return previews.Select(p => p.GetDamage()).Where(dp => dp != null).ToList();
        // }

        public bool HasReadyAttackTargets()
        {
            return false;
            // var damagePreviews = GetDamages();
            // if (damagePreviews == null || damagePreviews.Count == 0)
            // {
            //     return false;
            // }
            //
            // var hasNonNexusTargets = false;
            // damagePreviews.ForEach(t =>
            // {
            //     if (!(t.target is Nexus))
            //     {
            //         hasNonNexusTargets = true;
            //     }
            // });
            // return hasNonNexusTargets;
        }

        public bool HasTargets()
        {
            return false;
        }

        public bool Equals(Target other)
        {
            return coords.Equals(other.coords) && ListUtils.CheckEquivalent(Linked, other.Linked);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Target) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (coords.GetHashCode() * 397) ^ (Linked != null ? Linked.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            var linkedStr = "";
            if (Linked.Count > 0)
            {
                var thisCoords = this.coords;
                linkedStr = "," + string.Join(",", Linked.Where(l => l.coords != thisCoords).Select(l => l.coords.ToString()));
            }

            return $"Target({coords}{linkedStr})";
        }
    }
}