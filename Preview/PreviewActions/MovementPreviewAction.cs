﻿using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Preview.PreviewActions
{
    public class MovementPreviewAction : IPreviewAction
    {
        public Fighter self;
        public SingleMove move;
        public bool forceAdd;

        public MovementPreviewAction(Fighter self, SingleMove move, bool forceAdd = false)
        {
            this.self = self;
            this.move = move;
            this.forceAdd = forceAdd;
        }
    }
}