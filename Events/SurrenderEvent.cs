using System.Collections;
using BarnardsStar.Games;
using BarnardsStar.Players;

namespace BarnardsStar.Events
{
    public class SurrenderEvent : AbstractEvent
    {
        private int? overridePlayerIdx;

        public SurrenderEvent(Game game, int? overridePlayerIdx) : base(game, null)
        {
            this.overridePlayerIdx = overridePlayerIdx;
        }

        protected override IEnumerator Execute()
        {
            Player whichPlayer = game.currPlayer;
            if (overridePlayerIdx.HasValue)
            {
                whichPlayer = game.Players[overridePlayerIdx.Value];
            }
            
            if (whichPlayer.nexus != null)
            {
                var nexus = whichPlayer.nexus;
                yield return runCtx.Run(nexus.DealDamage(nexus.CurrHealth, null, true, true));
            }
        }
    }
}