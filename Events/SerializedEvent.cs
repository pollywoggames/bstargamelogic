﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
//
// namespace BStarGameLogic.Events
// {
//     public abstract class SerializedEvent
//     {
//         public string type;
//
//         public abstract string ToJson();
//
//         public static List<SerializedEvent> FromJson(string json)
//         {
//             var eventStrs = json.Split('\n');
//             var ret = new List<SerializedEvent>();
//             foreach (var s in eventStrs)
//             {
//                 if (s == null || s.Length == 0 || s.Trim().Length == 0)
//                     continue;
//
//                 if (s.Contains("type\":\"move"))
//                     ret.Add(JsonUtility.FromJson<SerializedMove>(s));
//                 else if (s.Contains("type\":\"atk"))
//                     ret.Add(JsonUtility.FromJson<SerializedAttack>(s));
//                 else if (s.Contains("type\":\"ablt"))
//                     ret.Add(JsonUtility.FromJson<SerializedAbilityUse>(s));
//                 else if (s.Contains("type\":\"spawn"))
//                     ret.Add(JsonUtility.FromJson<SerializedFighterSpawn>(s));
//                 else if (s.Contains("type\":\"respawn"))
//                     ret.Add(JsonUtility.FromJson<SerializedFighterRespawn>(s));
//                 else
//                     Debug.LogError("can't deserialize event: " + s);
//             }
//
//             return ret;
//         }
//
//         public static AbstractEvent Deserialize(SerializedEvent evt)
//         {
//             if (evt is SerializedMove move)
//             {
//                 var f = BattleManager.Instance.FightersById[move.fighterId];
//                 return new MovementEvent(f, new Path(f, move.spaces));
//             }
//
//             if (evt is SerializedAttack atk)
//             {
//                 var f = BattleManager.Instance.FightersById[atk.fighterId];
//                 return new AttackEvent(f, TargetPreview.New(new TargetOrigin(f), atk.target, f.WeaponObj.Preview));
//             }
//
//             if (evt is SerializedAbilityUse ablt)
//             {
//                 var f = BattleManager.Instance.FightersById[ablt.fighterId];
//                 var ability = f.bsFighter.Abilities.Find(ab => ab.GetName() == ablt.ability);
//
//                 var targetCoords = TargetPreview.New(new TargetOrigin(f), ablt.target, ability.Preview);
//                 if (ablt.linked != null && ablt.linked.Count > 0)
//                     foreach (var coords in ablt.linked)
//                         targetCoords.Link(TargetPreview.New(new TargetOrigin(f), coords, ability.Preview));
//
//                 return new AbilityEvent(f, ability, targetCoords);
//             }
//
//             if (evt is SerializedFighterSpawn spawn)
//                 // TODO rework this to use FighterPrototype instead of name
//                 return new SpawnFightersEvent(spawn.chosenFighters.Select(fighterName =>
//                 {
//                     foreach (var bp in Characters.Instance.All.Values)
//                         if (bp.Name == fighterName)
//                             return bp;
//
//                     Debug.LogError("Can't find fighter blueprint with name " + fighterName);
//                     return null;
//                 }).ToList());
//             if (evt is SerializedFighterRespawn respawn) return new RespawnFightersEvent(respawn.respawning);
//
//             Debug.LogError("Can't deserialize event object " + evt.GetType());
//             return null;
//         }
//     }
//
//     [Serializable]
//     public class SerializedMove : SerializedEvent
//     {
//         public string fighterId;
//         public List<Vector2Int> spaces;
//
//         public override string ToJson()
//         {
//             type = "move";
//             return JsonUtility.ToJson(this);
//         }
//     }
//
//     [Serializable]
//     public class SerializedAttack : SerializedEvent
//     {
//         public string fighterId;
//         public Vector2Int target;
//
//         public override string ToJson()
//         {
//             type = "atk";
//             return JsonUtility.ToJson(this);
//         }
//     }
//
//     [Serializable]
//     public class SerializedAbilityUse : SerializedEvent
//     {
//         public string ability;
//         public string fighterId;
//         public List<Vector2Int> linked;
//         public Vector2Int target;
//
//         public override string ToJson()
//         {
//             type = "ablt";
//             return JsonUtility.ToJson(this);
//         }
//     }
//
//     [Serializable]
//     public class SerializedFighterSpawn : SerializedEvent
//     {
//         public List<string> chosenFighters;
//
//         public override string ToJson()
//         {
//             type = "spawn";
//             return JsonUtility.ToJson(this);
//         }
//     }
//
//     [Serializable]
//     public class SerializedFighterRespawn : SerializedEvent
//     {
//         public List<FighterRespawnInfo> respawning;
//
//         public override string ToJson()
//         {
//             type = "respawn";
//             return JsonUtility.ToJson(this);
//         }
//     }
// }

