﻿// using System.Collections;
// using System.Collections.Generic;
// using System.Linq;
//
// namespace BStarGameLogic.Events
// {
//     public class SpawnFightersEvent : AbstractEvent
//     {
//         public List<FighterBlueprint> chosenFighters;
//
//         public SpawnFightersEvent(List<FighterBlueprint> fighterBlueprints)
//         {
//             chosenFighters = fighterBlueprints;
//         }
//
//         public override IEnumerator Execute()
//         {
//             var player = PlayersManager.currPlayer;
//             var thisPlayerSpawnPoints = player.spawnPoints;
//
//             if (chosenFighters.Count != thisPlayerSpawnPoints.Count)
//                 Debug.LogError("mismatch between number of spawnpoints (" + thisPlayerSpawnPoints.Count +
//                                ") and number of specified characters (" + chosenFighters.Count + ")");
//
//             var spawnPointsToRemoveFromList = new List<FighterSpawnPoint>();
//             var spawnPointLocations = new List<Vector3>();
//             for (var charIdx = 0; charIdx < chosenFighters.Count; charIdx++)
//             {
//                 var blueprint = chosenFighters[charIdx];
//                 if (blueprint == null)
//                 {
//                     Debug.LogError("Can't instantiate null FighterBlueprint");
//                     continue;
//                 }
//
//                 var spawnPoint = thisPlayerSpawnPoints[charIdx];
//                 spawnPointLocations.Add(spawnPoint.transform.position);
//
//                 if (spawnPoint.DebugFighterToSpawn != null && spawnPoint.DebugFighterToSpawn != "")
//                     foreach (var bp in Characters.Instance.All.Values)
//                         if (spawnPoint.DebugFighterToSpawn == bp.team + " " + bp.Name)
//                         {
//                             blueprint = bp;
//                             break;
//                         }
//
//                 if (spawnPoint.gameObject.activeInHierarchy)
//                 {
//                     GameInitializer.CreateFighter(blueprint, spawnPoint.teamColor, player.idx, spawnPoint.GridCoords,
//                         spawnPoint.FacingLeft);
//                     player.respawnInfo.Add(new FighterRespawnInfo(blueprint.proto, spawnPoint.transform.position,
//                         spawnPoint.FacingLeft));
//                 }
//
//                 Object.Destroy(spawnPoint.gameObject, .5f);
//                 spawnPointsToRemoveFromList.Add(spawnPoint);
//             }
//
//             foreach (var sp in spawnPointsToRemoveFromList) thisPlayerSpawnPoints.Remove(sp);
//
//             BattleManager.Instance.RefreshObjects();
//
//             player.ownedFighters.ForEach(f => f.gameObject.SetActive(false));
//
//             yield return CameraController.Instance.PanTilPointsVisible(spawnPointLocations);
//             if (BattleManager.Instance.DebugCinematicMode)
//                 yield return new WaitForSeconds(.5f);
//
//             player.ownedFighters.ForEach(f => f.gameObject.SetActive(true));
//             yield return AnimationUtils.AnimateFightersSpawn(player.ownedFighters);
//
//             if (BattleManager.Instance.DebugCinematicMode)
//                 yield return new WaitForSeconds(.5f);
//         }
//
//         public override SerializedEvent Serialize()
//         {
//             var ret = new SerializedFighterSpawn();
//             ret.chosenFighters = chosenFighters.Select(bp => bp.Name).ToList();
//             return ret;
//         }
//     }
// }

