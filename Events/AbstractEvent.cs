﻿using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;

namespace BarnardsStar.Events
{
    public abstract class AbstractEvent
    {
        public Game game;
        protected Fighter self;

        protected GameMap map => game.map;
        protected IFighterVirtualActor virtualActor => self.virtualActor;
        protected IAnimationContext animCtx => game.animCtx;
        protected ICameraController camera => animCtx.Camera();
        protected IRunContext runCtx => game.runCtx;
        protected ISoundContext soundCtx => game.soundCtx;

        protected AbstractEvent(Game game, Fighter self)
        {
            this.game = game;
            this.self = self;
        }

        // public abstract SerializedEvent Serialize();
        protected abstract IEnumerator Execute();

        public IEnumerator Process()
        {
            if (animCtx.IsReplayMode() && self != null)
            {
                yield return camera.PanTilPointVisible(self.pos);
            }

            if (self != null)
                yield return self.AnimateCoroutine(Execute());
            else
                yield return game.runCtx.Run(Execute());
        }
    }
}