﻿using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Events
{
    public class AttackEvent : AbstractEvent
    {
        public Target target;

        public AttackEvent(Game g, Fighter f, Target t) : base(g, f)
        {
            self = f;
            target = t;
        }

        protected override IEnumerator Execute()
        {
            if (self.IsDead)
                yield break;

            yield return camera.PanTilPointsVisible(new List<Coords>
            {
                self.coords,
                target.coords
            });

            self.virtualActor.Face(target.coords.ToPoint());

            yield return runCtx.WaitSeconds(.2f);

            self.SetActionsLeft(self.ActionsLeft - 1);
            
            yield return self.AnimateCoroutine(self.GetWeapon().Attack(target));
            
            // If OverrideCoords was set during the execution of the attack, clear it now 
            self.OverrideCoords = null;

            self.OnAttack?.Invoke();
        }
    }
}

