﻿using System;
using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using Logger = BarnardsStar.Utilities.Logger;
using Vector3 = System.Numerics.Vector3;

namespace BarnardsStar.Events
{
    public class MovementEvent : AbstractEvent
    {
        private static readonly Sound[] walkSounds =
        {
            Sound.Walk1,
            Sound.Walk2,
            Sound.Walk3,
            Sound.Walk4,
            Sound.Walk5,
            Sound.Walk6,
        };
        
        private static readonly Sound[] wobbleWalkSounds =
        {
            Sound.WalkSquish1,
            Sound.WalkSquish2,
            Sound.WalkSquish3,
        };
        
        // just for sounds
        private static Randy _random = new Randy();
        
        public Path path;
        
        // Internal state variables for animation
        private bool wobbleLastMovedLeft;
        private IMovable acidSurfingFX;
        private int? continuousSound;
        private int? surfingSound;

        public MovementEvent(Game game, Path path) : base(game, path.walker)
        {
            this.path = path;

            wobbleLastMovedLeft = self.virtualActor.IsFacingLeft();
        }

        protected override IEnumerator Execute()
        {
            if (self.IsDead)
            {
                yield break;
            }

            game.currentlyMovingFighter = self;
            yield return self.AnimateCoroutine(ExecuteMove());
            game.currentlyMovingFighter = null;
        }

        private IEnumerator ExecuteMove()
        {
            yield return camera.PanTilPointsVisible(path.spaces);

            continuousSound = null;
            if (self.MoveType == MovementAnimationType.Smooth)
            {
                var sound = self.Prototype == FighterPrototype.CritterFly ? Sound.FlyBuzzing : Sound.Glide;
                continuousSound = soundCtx.PlayPausable(sound);
            }

            var moveCount = 0;
            foreach (var dstCoords in path.spaces)
            {
                var dst = Point.FromCoords(dstCoords) + Fighter.OffsetOnGrid;
                virtualActor.Face(dst);

                var delta = dstCoords - self.coords;
                bool flying = false;
                if (delta.magnitude > 1.1f)
                {
                    if (self.Hypertunnel)
                    {
                        var middle = self.coords + delta.normalized;
                        virtualActor.HypertunnelAnimation(middle, delta.y != 0);
                    }
                    else if (self.Flier)
                    {
                        // check if we're crossing a gap
                        var inBetween = self.coords + delta.normalized;
                        if (game.map.GetTerrainType(inBetween) == TerrainType.Pit)
                            game.SomethingHappened(new BSEvent(BSEventType.CrossGap, self.Owner));
                        
                        flying = true;
                        soundCtx.PlayOneShot(Sound.WingsFlap);
                        
                        var backwardRotationAngle = self.GetBackwardsRotationAngle() * .5f;
                        // self.virtualActor.SetRotation(backwardRotationAngle);
                        // yield return runCtx.WaitSeconds(.1f);
                        self.virtualActor.SetRotation(-backwardRotationAngle);
                        
                        yield return animCtx.MoveParabolic(virtualActor, dstCoords.ToPoint() + Fighter.OffsetOnGrid, 12, 1.5f);
                        
                        self.virtualActor.SetRotation(0);

                        // MoveParabolic doesn't register a movement preview so we do it here manually
                        if (game.contextProvider.IsPreviewing())
                            game.contextProvider.RegisterPreview(new MovementPreviewAction(self, new SingleMove(dstCoords, false), true));

                        // kick up some dust on landing
                        virtualActor.HopLandingAnimation(dstCoords);
                        // short pause/shake on landing
                        yield return animCtx.ShakeObject(virtualActor, 2, .06f);
                    }
                    else
                    {
                        Logger.Error($"Movement delta was greater than 1 (from {self.coords} to {dstCoords}, magnitude {delta.magnitude}) but " +
                                     $"unit doesn't have hypertunnel and isn't a flier. Maybe OverrideCoords is set incorrectly? self.OverrideCoords: {self.OverrideCoords}");
                    }
                }

                bool startsSurfing = self.Surfing.HasValue
                                    && game.TryGetHazard(self.coords, out var h)
                                    && h.type == self.Surfing.Value;
                
                bool surfingOnDst = self.Surfing.HasValue
                                    && game.TryGetHazard(dstCoords, out var h2)
                                    && h2.type == self.Surfing.Value;

                // Actual movement motion (if not already taken care of)
                if (!flying)
                {
                    if (startsSurfing)
                    {
                        yield return MoveSurfing(dst, surfingOnDst);
                    }
                    else
                    {
                        switch (self.MoveType)
                        {
                            case MovementAnimationType.Smooth:
                                yield return animCtx.MoveSmoothly(self, dst, .25f);
                                break;

                            case MovementAnimationType.Bob:
                                yield return MoveBobbingly(dst);
                                break;

                            case MovementAnimationType.HeavyBob:
                                yield return MoveHeavy(dst);
                                break;

                            case MovementAnimationType.Wobble:
                                yield return MoveWobbly(dst);
                                break;

                            case MovementAnimationType.Roll:
                                yield return MoveRoll(dst, moveCount);
                                break;
                        }
                    }
                }

                var moveCost = surfingOnDst ? .5f : 1f;
                // overrideMax=true here so that if they had more than MaxMoves, it won't limit it to MaxMoves
                self.SetMovesLeft(self.MovesLeft - moveCost, true);

                if (self.IsDead)
                {
                    break;
                }

                if (self.LeavesTrail.HasValue)
                {
                    yield return CheckLeavesTrail(self.coords);
                }

                // Trigger hazards etc. but don't teleport (teleporterChannel = -1)
                // (Only teleport if it's the last move of the path -- we check that later)
                yield return game.OnFighterMoved(self, true);

                // Killed by a ready attack shot
                if (self.IsDead)
                {
                    Cleanup();
                    yield break;
                }
                
                // see if we teleport
                bool isLastSpace = dstCoords == path.spaces[path.spaces.Count - 1];
                if (isLastSpace && map._teleportersByCoords.TryGetValue(dstCoords, out _))
                {
                    self.ResetRotation();
                    
                    // stop the continuous sound while we teleport
                    if (continuousSound.HasValue)
                        soundCtx.Stop(continuousSound.Value);

                    // Check if we teleport - preventHazardDamage to make sure we don't get double-damaged for
                    // stepping on a teleporter that also has a hazard on it
                    yield return game.OnFighterMoved(self, preventHazardDamage: true);
                    
                    // Check again if we were killed by a ready attack shot
                    if (self.IsDead)
                    {
                        Cleanup();
                        yield break;
                    }

                    // start up continuous sound again if applicable
                    if (self.MoveType == MovementAnimationType.Smooth)
                        continuousSound = soundCtx.PlayPausable(Sound.Glide);
                }

                moveCount++;
            }
            
            Cleanup();
            yield return animCtx.BumpObject(self);
        }

        private void Cleanup()
        {
            if (continuousSound.HasValue)
                soundCtx.Stop(continuousSound.Value);
            
            if (surfingSound.HasValue)
                soundCtx.Stop(surfingSound.Value);
            
            if (acidSurfingFX != null)
                acidSurfingFX.Destroy();

            if (self.virtualActor != null && !self.virtualActor.IsDestroyed())
                self.ResetRotation();
        }

        private IEnumerator CheckLeavesTrail(Coords coords)
        {
            if (!self.LeavesTrail.HasValue)
                yield break;
            
            var trailType = self.LeavesTrail.Value;
            if (Hazard.CanPlaceHazardAt(self.game, trailType, coords))
            {
                runCtx.Start(Hazard.PlaceHazardAt(self, trailType, coords));
            }
        }

        private IEnumerator MoveBobbingly(Point dst)
        {
            var delta = dst - self.GetPosition();
            var halfway = self.GetPosition() + .5f * delta;
            halfway.y += .2f;

            yield return animCtx.MoveSmoothly(self, halfway, .1f);
            yield return animCtx.MoveSmoothly(self, dst, .1f);
            soundCtx.PlayOneShot(RandomWalkSound(), _random.Next(0.8f, 1.2f));
        }

        public static Sound RandomWalkSound()
        {
            return walkSounds[_random.Next(0, walkSounds.Length)];
        }

        private IEnumerator MoveHeavy(Point dst)
        {
            yield return animCtx.MoveParabolic(self, dst, 6, .12f);
            soundCtx.PlayOneShot(RandomWalkSound(), _random.Next(0.5f, 0.7f));
            camera.Shake(.1f, .06f);
            yield return runCtx.WaitSeconds(.1f);
        }

        private IEnumerator MoveWobbly(Point dst)
        {
            var delta = dst - self.GetPosition();
            var halfway = self.GetPosition() + .5f * delta;

            var angle = wobbleLastMovedLeft ? -20f : 20f;
            wobbleLastMovedLeft = !wobbleLastMovedLeft;

            soundCtx.PlayOneShot(wobbleWalkSounds[_random.Next(0, wobbleWalkSounds.Length)], _random.Next(0.8f, 1.2f));
            yield return animCtx.MoveSmoothlyWhileRotating(self, halfway, angle, .17f);
            yield return animCtx.MoveSmoothly(self, dst, .17f);
        }

        private IEnumerator MoveRoll(Point dst, int moveCount)
        {
            yield return animCtx.MoveSmoothlyWhileRolling(self, dst, self.virtualActor.IsFacingLeft(), moveCount % 2 == 1, .34f);
        }

        private IEnumerator MoveSurfing(Point dst, bool willContinueSurfing)
        {
            if (!surfingSound.HasValue)
            {
                surfingSound = soundCtx.PlayPausable(Sound.AcidSurfing);
            }
            
            var xOffset = -.3f;
            if (self.virtualActor.IsFacingLeft())
                xOffset *= -1;
            
            var feetOffset = new Point(xOffset, -0.5f);
            
            // tilt back the fighter cuz they're surfing, bro
            self.TipBackwards();
            
            if (acidSurfingFX == null)
            {
                var feetPosition = self.coords.ToPoint() + feetOffset;
                acidSurfingFX = game.contextProvider.ProvideProp(MovablePropType.AcidSurfingFX, feetPosition);
            }
            
            (acidSurfingFX as IPausableVFX)?.Play();

            var scaleX = self.virtualActor.IsFacingLeft() ? -1f : 1f;
            acidSurfingFX.SetScale(new Vector3(scaleX, 1f, 1f));
            
            // The acid surfing thing will follow the fighter's movement
            game.runCtx.Start(animCtx.MoveSmoothly(acidSurfingFX, dst + feetOffset, .15f));
            yield return animCtx.MoveSmoothly(self, dst, .3f);
            
            // Pause the particle system between each square so it doesn't look
            // weird if we get ReadyAttack'ed in the middle of surfing
            (acidSurfingFX as IPausableVFX)?.Pause();

            if (!willContinueSurfing)
            {
                self.ResetRotation();
                acidSurfingFX.Destroy();
                acidSurfingFX = null;
                
                soundCtx.Stop(surfingSound.Value);
                surfingSound = null;
            }
        }
    }
}
