﻿using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Events
{
    public class AbilityEvent : AbstractEvent
    {
        private readonly AbstractAbility ability;
        private readonly Target target;

        public bool putAbilityOnCooldown = true;

        public AbilityEvent(Game g, Fighter f, AbstractAbility ab, Target t) : base(g, f)
        {
            self = f;
            ability = ab;
            target = t;
        }

        protected override IEnumerator Execute()
        {
            if (self.IsDead)
                yield break;

            yield return camera.PanTilPointsVisible(new List<Coords>
            {
                self.coords,
                target.coords
            });

            if (!ability.IsPassive && !ability.IsUntargeted)
                self.virtualActor.Face(target.coords.ToPoint());
            
            if (putAbilityOnCooldown)
            {
                self.SetActionsLeft(self.ActionsLeft - 1);
                
                ability.SetOnCooldown();
            }

            // Hide health bar, looks better when animating if health bar isn't showing
            self.HideHealthBar();

            if (ability.DisableMovementPreviewsForActor())
                self.TempDisableMovementPreviews = true;
            if (ability.DisableMovementPreviewScoresForActor())
                self.TempDisableMovementPreviewScore = true;
            
            yield return self.AnimateCoroutine(ability.Execute(target));

            if (self.TempDisableMovementPreviews)
                self.TempDisableMovementPreviews = false;
            if (self.TempDisableMovementPreviewScore)
                self.TempDisableMovementPreviewScore = false;
            
            // If OverrideCoords was set during the execution of the ability, clear it now 
            self.OverrideCoords = null;

            var extraMovement = ability.GetMovementAfter();
            if (extraMovement > 0 && !self.IsDead)
            {
                self.SetMovesLeft(self.MovesLeft + 1);
            }
        }
    }
}

