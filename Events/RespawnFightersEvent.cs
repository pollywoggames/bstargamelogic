﻿// using System.Collections;
// using System.Collections.Generic;
// using System.Linq;
//
// namespace BStarGameLogic.Events
// {
//     public class RespawnFightersEvent : AbstractEvent
//     {
//         public List<FighterRespawnInfo> fightersToRespawn;
//
//         public RespawnFightersEvent(List<FighterRespawnInfo> f)
//         {
//             fightersToRespawn = f;
//         }
//
//         public override IEnumerator Execute()
//         {
//             yield return CameraController.Instance.PanTilPointsVisible(fightersToRespawn.Select(fri => fri.pos)
//                 .ToList());
//
//             if (BattleManager.Instance.DebugCinematicMode)
//                 yield return new WaitForSeconds(.5f);
//
//             var currPlayer = PlayersManager.currPlayer;
//             var respawned = new List<Fighter>();
//             foreach (var respawning in fightersToRespawn)
//             {
//                 var blueprint = Characters.Instance.All[respawning.proto];
//
//                 respawned.Add(GameInitializer.CreateFighter(blueprint, currPlayer.teamColor, currPlayer.idx,
//                     TilemapUtils.WorldCoordsToGridCoords(respawning.pos), respawning.facingLeft));
//             }
//             
//             HUDController.Instance.InitMiniPortraits();
//
//             yield return AnimationUtils.AnimateFightersSpawn(respawned, false);
//
//             foreach (var f in respawned)
//             {
//                 PlayersManager.Instance.ResetFighterMovesAndActions(f);
//
//                 foreach (var a in f.bsFighter.Abilities)
//                 {
//                     a.CooldownTurnsRemaining = 0;
//                     yield return a.StartOfTurn();
//                 }
//
//                 f.ActionIndicatorParent.SetActive(true);
//             }
//
//             if (BattleManager.Instance.DebugCinematicMode)
//                 yield return new WaitForSeconds(.5f);
//         }
//
//         public override SerializedEvent Serialize()
//         {
//             var ret = new SerializedFighterRespawn();
//             ret.respawning = fightersToRespawn;
//             return ret;
//         }
//     }
// }

