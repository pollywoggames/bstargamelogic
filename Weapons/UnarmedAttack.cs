using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons
{
    public class UnarmedAttack : Weapon
    {
        public const int UnarmedDamage = 2;

        protected int damageToCover = 1;
        protected Sound sound;
        protected bool suppressKnockback = false;
        protected bool damageCover = true;
        protected bool dontCallOnImpactIfHitSomething = false;

        public UnarmedAttack(Fighter actor) : base(actor, 161, UnarmedDamage, 1)
        {
            sound = Sound.UnarmedStrike;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new MeleeWeaponTargeter(actor, false, true);
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override IEnumerator Attack(Target target)
        {
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();
            
            // Make sure the actor stays in their own coords even though they'll be moving into
            // the enemy's square for a sec.
            actor.OverrideCoords = actor.coords;

            var positionBefore = actor.pos;
            var dst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
            var origDelta = dst - actor.pos;
            actor.virtualActor.Face(dst);

            // Rear up backwards: tilt + move backwards slightly
            var backwardRotationAngle = actor.GetBackwardsRotationAngle();
            actor.SetRotation(backwardRotationAngle);
            yield return animCtx.MoveSmoothly(actor, actor.pos - origDelta.normalized * .4f, .13f);

            // Then they tilt forwards and bump into the other guy, moving a little into their square
            actor.SetRotation(-backwardRotationAngle);
            var delta = origDelta * .8f;
            dst = actor.pos + delta;

            yield return animCtx.MoveSmoothly(actor, dst, .07f);

            soundCtx.PlayOneShot(sound);

            WaitGroup consequencesWg = new WaitGroup(game.runCtx);
            bool hitSomething = false;
            var atkables = game.AttackablesFor(actor.Owner);
            if (atkables.TryGetValue(target.coords, out var d))
            {
                hitSomething = true;
                if (suppressKnockback && d is Fighter f)
                    consequencesWg.Start(f.DealDamage(GetDamage(), actor, true));
                else
                    consequencesWg.Start(d.DealDamage(GetDamage(), actor));
            }
            else if (damageCover && game.map.GetTerrainType(target.coords) == TerrainType.Cover)
            {
                hitSomething = true;
                yield return game.map.DamageCover(target.coords, damageToCover, actor.pos, actor);
            }
            else if (game.map._teleportersByCoords.TryGetValue(target.coords, out var tp) && tp.type == TeleporterType.Placed)
            {
                hitSomething = true;
                game.map.RemoveTeleporter(tp);
            }

            bool dontCallOnImpact = dontCallOnImpactIfHitSomething && hitSomething;
            if (!dontCallOnImpact)
                consequencesWg.Start(OnImpact(target));
            
            // wait for hitstop animation
            yield return runCtx.WaitSeconds(.25f);

            // move back to original position & rotation
            actor.SetRotation(0);
            yield return animCtx.MoveSmoothly(actor, positionBefore, .07f);

            actor.virtualActor.BackwardInSortingOrder();

            yield return consequencesWg.Wait();
            
            // move back to original position & rotation again, just to be sure
            // (if they were damaged during it sometimes they get stuck in the middle)
            yield return runCtx.WaitSeconds(.15f);
            actor.SetRotation(0);
            actor.SetPosition(positionBefore);
        }

        public virtual IEnumerator OnImpact(Target target)
        {
            yield break;
        }
    }
}