namespace BarnardsStar.Weapons
{
    public enum BSWeapon
    {
        AcidPunch = 0,
        DiagonalRifle = 1,
        Forklift = 2,
        Machete = 3,
        Pistol = 4,
        PlasmaSword = 5,
        PsiBlast = 6,
        Rifle = 7,
        SavagePeck = 8,
        Shotgun = 9,
        Sidearm = 10, // like pistol but only shoots diagonally
        SmashAttack = 11,
        UnarmedAttack = 12,
        BashSelfDamageAttack = 13,
        DashStrike = 14,
        MachineGun = 16,
        Magnum = 17, // like pistol but shoots orthogonal and digaonal directions
        Revolver = 18, // like magnum but shorter range
    }
}