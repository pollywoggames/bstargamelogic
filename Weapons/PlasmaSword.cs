﻿using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Weapons
{
    public class PlasmaSword : Machete
    {
        public PlasmaSword(Fighter actor) : base(actor)
        {
            (targeter as MeleeWeaponTargeter).includeHazards = true;
            
            spriteIdx = 119;
            sound = Sound.ReflectProjectile;
            
            // destroy cover in 1 hit
            damageToCover = 2;
            
            // don't ignite hazards if we hit a fighter or cover (acid bubble specifically)
            dontCallOnImpactIfHitSomething = true;
        }

        public override IEnumerator OnImpact(Target target)
        {
            if (game.TryGetHazard(target.coords, out var h))
            {
                // ignite oil, change acid/water to steam
                yield return h.OnDamage(actor);
            }
        }
    }
}