using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Projectile
    {
        public delegate IEnumerator ImpactFunction(Coords impactCoords);
        
        public Fighter fighter;
        public bool hitsHalfWalls;
        public bool stopsAtFirstImpact;
        public ImpactFunction impactFunction;
        public int range;
        public Coords targetCoords;
        public IProjectileVirtualActor virtualActor;
        public bool StraightLineOnly;
        public float speed;
        public bool hitAnyone;
        public bool canHitFriendlies;
        
        // Wonky special case handling for an upgraded shotgun bullet at max range
        public int keepGoingTicks = 0;
        private bool collidedButKeepGoing = false;

        // If this projectile was reflected by a fighter, this will be set to the reflecting fighter's coords.
        public Coords? lastReflectedByFighterCoords;

        private Coords lastSpacePassed;
        private WaitGroup consequences;
        
        // for feeding into projectile preview later
        private List<Point> keyPoints;

        private Game game => fighter.game;
        private IRunContext runCtx => fighter.runCtx;

        public Projectile(
            IProjectileVirtualActor virtualActor, 
            Fighter fighter, 
            Coords targetCoords, 
            int range,
            bool hitsHalfWalls = false,
            ImpactFunction impactFunction = null,
            bool stopsAtFirstImpact = true
        ) {
            this.fighter = fighter;
            this.hitsHalfWalls = hitsHalfWalls;
            this.stopsAtFirstImpact = stopsAtFirstImpact;
            this.impactFunction = impactFunction;
            this.range = range;
            this.targetCoords = targetCoords;
            this.virtualActor = virtualActor;

            hitAnyone = false;
            StraightLineOnly = false;
            speed = .25f;
            keyPoints = new List<Point>();
            
            if (!stopsAtFirstImpact && impactFunction == null)
                throw new Exception("Can't have stopsAtFirstImpact=false and impactFunction=null -- not implemented");
        }

        private void SetRotation(Point delta)
        {
            var rotation = MoreMath.Rad2Deg * Math.Atan2(delta.y, delta.x);
            virtualActor.SetRotation((float)rotation);
        }

        public IEnumerator Fly(Point dst)
        {
            consequences = new WaitGroup(runCtx);
            yield return runCtx.Run(FlyLoop(dst));

            // if !stopsAtFirstImpact, we've already called impactFunction
            if (stopsAtFirstImpact)
            {
                if (impactFunction == null)
                {
                    yield return runCtx.Run(virtualActor.PlayExplosion());
                    virtualActor.Destroy();
                }
                else
                {
                    virtualActor.Destroy();
                    yield return impactFunction(lastSpacePassed);
                }
            }
            else
            {
                yield return runCtx.WaitSeconds(.2f);
                virtualActor.Destroy();
            }

            yield return consequences.Wait();
        }

        private IEnumerator FlyLoop(Point dst)
        {
            var src = virtualActor.GetPosition();
            keyPoints.Add(src);
            var delta = dst - src;
            SetRotation(delta);
            delta = speed * delta.normalized;
            
            Coords straightLineDelta = Coords.zero;
            if (StraightLineOnly)
            {
                straightLineDelta = (dst.ToCoords() - src.ToCoords()).normalized;
            }

            ProjectileInternalState state = new ProjectileInternalState()
            {
                src = src,
                delta = delta,
                startingGridCoords = virtualActor.GetPosition().ToCoords(),
                straightLineNextCoords = virtualActor.GetPosition().ToCoords() + straightLineDelta,
                straightLineDelta = straightLineDelta,
                attackables = game.AttackablesFor(fighter.Owner),
                damageOrigin = fighter,
            };
            
            bool keepLooping = true;
            float elapsedTime = 0;
            while (keepLooping)
            {
                yield return null;
                
                elapsedTime += runCtx.GetDeltaTime();
                const float timePerTick = 0.02f;
                while (keepLooping && elapsedTime > timePerTick)
                {
                    var result = FlyLoopTick(state);
                    if (result == FlyLoopTickResult.StopLooping)
                        keepLooping = false;
                    else if (result == FlyLoopTickResult.FreezeFrame)
                    {
                        yield return runCtx.WaitSeconds(.2f);
                        
                        // reset explosion animation so it can play again at the next impact
                        // (only applicable to !stopsAtFirstImpact)
                        virtualActor.ResetExplosion();
                    }

                    elapsedTime -= timePerTick;
                }
            }

            keyPoints.Add(virtualActor.GetPosition());
            if (game.contextProvider.IsPreviewing())
                game.contextProvider.RegisterPreview(new ProjectilePreview(game, keyPoints, hitAnyone));
        }

        private FlyLoopTickResult FlyLoopTick(ProjectileInternalState state)
        {
            // Special handling for an upgraded shotgun bullet at max range
            if (collidedButKeepGoing)
            {
                // change delta to move directly towards middle of square
                var middleOfSquare = virtualActor.GetPosition().ToCoords().ToPoint();
                var delta = middleOfSquare - virtualActor.GetPosition();
                delta = speed * delta.normalized;
                
                if (keepGoingTicks > 0)
                {
                    keepGoingTicks--;
                    virtualActor.SetPosition(virtualActor.GetPosition() + delta);
                    return FlyLoopTickResult.KeepLooping;
                }
                else
                {
                    return FlyLoopTickResult.StopLooping;
                }
            }

            virtualActor.SetPosition(virtualActor.GetPosition() + state.delta);
            Coords projectileCoords = virtualActor.GetPosition().ToCoords();
            
            // Only do calculation on the frame it enters each space
            if (state.spacesPassed.Contains(projectileCoords) || projectileCoords == state.startingGridCoords)
            {
                return FlyLoopTickResult.KeepLooping;
            }

            if (StraightLineOnly)
            {
                if (projectileCoords != state.straightLineNextCoords)
                {
                    state.spacesSkippedForStraightLineChecking++;
                    // Stop iterating if spacesSkippedForStraightLineChecking gets too big
                    return state.spacesSkippedForStraightLineChecking <= 100
                        ? FlyLoopTickResult.KeepLooping
                        : FlyLoopTickResult.StopLooping;
                }
                
                state.straightLineNextCoords += state.straightLineDelta;
            }

            lastSpacePassed = projectileCoords;
            state.spacesPassed.Add(projectileCoords);

            var collides = false;
            var terrainType = game.map.GetTerrainType(projectileCoords);
            bool wasntCover = false;
            if (terrainType == TerrainType.Cover)
            {
                if (hitsHalfWalls || projectileCoords == targetCoords)
                {
                    if (impactFunction == null)
                    {
                        consequences.Start(game.map.DamageCover(projectileCoords, 1, state.src, fighter));
                    }

                    collides = true;
                }
                else
                {
                    state.lastSpacePassedWasCover = true;
                }

                if (game.contextProvider.IsPreviewing() && fighter.Owner.nexus != null && fighter.Owner.nexus.AllCoords.Contains(projectileCoords))
                {
                    game.contextProvider.RegisterPreview(new NexusSelfDamagePreview(game));
                }
            }
            else
            {
                wasntCover = true;
                if (terrainType == TerrainType.Wall)
                    collides = true;
            }
            
            if (canHitFriendlies
                && projectileCoords == targetCoords
                && game.TryGetFighter(projectileCoords, out var hitFighter)
                && hitFighter.Owner.IsFriendlyTo(fighter.Owner))
            {
                collides = true;
            }
            
            // If it's flown off the edge of the map
            if (projectileCoords.magnitude > 100f)
            {
                collides = true;
            }
            
            bool atMaxRange = state.spacesPassed.Count >= range;
            
            // hit a hazard
            if (!collides
                && ((!hitsHalfWalls && projectileCoords == targetCoords) || (hitsHalfWalls && atMaxRange))
                && game.TryGetHazard(projectileCoords, out var h)
                && h.targetable
                && !EnemyFighterThere(projectileCoords))
            {
                collides = true;
                consequences.Start(h.OnDamage(fighter));
            }
            
            // hit a placed teleporter
            if (!collides
                && projectileCoords == targetCoords
                && game.map._teleportersByCoords.TryGetValue(projectileCoords, out var tp)
                && tp.type == TeleporterType.Placed
                && !EnemyFighterThere(projectileCoords))
            {
                collides = true;
                game.map.RemoveTeleporter(tp);
            }

            if (atMaxRange)
            {
                // // Set it back a space for bazooka so it explodes right at max range, not the next space (which is out of range)
                // lastSpacePassed = lastProjectileCoords;
                // projectileCoords = lastProjectileCoords;

                collides = true;
            }

            // Projectile is getting reflected!
            if (state.attackables.TryGetValue(projectileCoords, out var d) && d is Fighter f &&
                f.ReflectProjectiles)
            {
                hitAnyone = true;
                
                keyPoints.Add(virtualActor.GetPosition());
                if (game.contextProvider.IsPreviewing())
                    game.contextProvider.RegisterPreview(new ReflectProjectilePreview(game, projectileCoords, state.startingGridCoords));
                
                f.virtualActor.Face(state.src);

                lastReflectedByFighterCoords = f.coords;
                
                game.soundCtx.PlayOneShot(Sound.ReflectProjectile);
                state.damageOrigin = f;
                f.DidReflectProjectile(state.src);
                // now reset src in the off chance it gets reflected again
                state.src = virtualActor.GetPosition();

                state.delta = -state.delta;
                SetRotation(state.delta);

                // reverse who can get hit by it
                state.attackables = game.AttackablesFor(f.Owner);

                // since we'll be passing through the same spaces again
                state.spacesPassed.Clear();
                state.startingGridCoords = projectileCoords;
                
                // reverse the straight line checker
                if (StraightLineOnly)
                {
                    state.straightLineDelta = -state.straightLineDelta;
                    
                    // 2x since we have to make up for the 1 it already went past where it's supposed to
                    state.straightLineNextCoords += 2 * state.straightLineDelta;
                }

                // freeze frame!
                return FlyLoopTickResult.FreezeFrame;
            }
            
            // Hit the currently moving fighter in the middle of a move
            IDamageable hitDamageable = null;
            if (game.currentlyMovingFighter != null && projectileCoords == game.currentlyMovingFighter.coords && fighter.IsEnemyOf(game.currentlyMovingFighter))
                hitDamageable = game.currentlyMovingFighter;
            
            // Hit another damageable (enemy fighter or nexus)
            else if (state.attackables.TryGetValue(projectileCoords, out var damageable))
            {
                if (damageable is Fighter hitDamageableFighter)
                {
                    // fighter must not be behind cover, and not be dead
                    if (!state.lastSpacePassedWasCover && !hitDamageableFighter.IsDead)
                        hitDamageable = hitDamageableFighter;
                }
                else
                {
                    // it's a nexus, cover doesn't apply
                    hitDamageable = damageable;
                }
            }
            
            if (hitDamageable != null)
            {
                hitAnyone = true;
                
                // Only deal weapon damage if impactFunction is non-null
                if (impactFunction == null)
                    consequences.Start(fighter.GetWeapon().DealDamage(hitDamageable, state.damageOrigin));
                
                collides = true;
            }

            if (collides)
            {
                if (stopsAtFirstImpact)
                {
                    if (atMaxRange && keepGoingTicks > 0)
                    {
                        collidedButKeepGoing = true;
                        keyPoints.Add(virtualActor.GetPosition() - state.delta);
                        return FlyLoopTickResult.KeepLooping;
                    }
                    
                    return FlyLoopTickResult.StopLooping;
                }
                else
                {
                    runCtx.Start(virtualActor.PlayExplosion());

                    // special handling for multiple impacts (if stopsAtFirstImpact is false, impactFunction must not be null)
                    consequences.Start(impactFunction(lastSpacePassed));

                    // if it doesn't stop at first impact, the only way it can stop is max range
                    if (atMaxRange)
                    {
                        return FlyLoopTickResult.StopLooping;
                    }
                    else
                    {
                        // freeze frame for intermediate impact
                        return FlyLoopTickResult.FreezeFrame;
                    }
                }
            }

            if (state.lastSpacePassedWasCover && wasntCover)
                state.lastSpacePassedWasCover = false;

            // continue looping
            return FlyLoopTickResult.KeepLooping;
        }

        private bool EnemyFighterThere(Coords coords)
        {
            return game.TryGetFighter(coords, out var f) && f.IsEnemyOf(fighter);
        }

        private class ProjectileInternalState
        {
            public HashSet<Coords> spacesPassed = new HashSet<Coords>();
            public int spacesSkippedForStraightLineChecking = 0;
            public bool lastSpacePassedWasCover = false;

            public Point src;
            public Point delta;
            public Coords startingGridCoords;
            public Coords straightLineNextCoords;
            public Coords straightLineDelta;
            public Dictionary<Coords, IDamageable> attackables;
            public Fighter damageOrigin;
        }

        private enum FlyLoopTickResult
        {
            KeepLooping,
            StopLooping,
            FreezeFrame
        }
    }
}