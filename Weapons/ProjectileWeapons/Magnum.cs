using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Magnum : Pistol
    {
        public const int MagnumDamage = 2, MagnumRange = 4;

        public Magnum(Fighter actor) : base(actor, 247, MagnumDamage, MagnumRange, DirectionsType.OrthoAndDiagonal)
        {
            directionsType = DirectionsType.OrthoAndDiagonal;
            StraightLineOnly = true;
        }
    }
}