using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Revolver : Pistol
    {
        public const int RevolverDamage = 2, RevolverRange = 2;

        public Revolver(Fighter actor) : base(actor, 250, RevolverDamage, RevolverRange, DirectionsType.OrthoAndDiagonal)
        {
            directionsType = DirectionsType.OrthoAndDiagonal;
            StraightLineOnly = true;
        }
    }
}