using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Sidearm : Pistol
    {
        public const int SidearmDamage = 2, SidearmRange = 3;

        public Sidearm(Fighter actor) : base(actor, 220, SidearmDamage, SidearmRange, DirectionsType.DiagonalOnly)
        {
            directionsType = DirectionsType.DiagonalOnly;
            StraightLineOnly = true;
        }
    }
}