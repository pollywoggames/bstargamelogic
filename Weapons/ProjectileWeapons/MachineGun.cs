using System;
using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class MachineGun : ProjectileWeapon
    {
        public const int MachineGunDamage = 1, MachineGunRange = 4;

        public MachineGun(Fighter actor) : base(actor, 243, MachineGunDamage, MachineGunRange, hitsHalfWalls: true, hitsMaxRange: true)
        {
            sound = Sound.MachineGunShot;
        }

        protected override IEnumerator Shoot(Coords targetCoords, Point _)
        {
            var delta = (targetCoords - actor.coords).normalized;

            var waitGroup = new WaitGroup(runCtx);
            for (int i = 2; i <= 4; i++)
            {
                var thisShot_targetCoords = actor.coords + delta * i;
                // var targetDelta = target.coords.ToPoint() - actor.coords.ToPoint();
                var thisShot_targetDelta = thisShot_targetCoords.ToPoint() - actor.coords.ToPoint();
                waitGroup.Start(OneShot(thisShot_targetCoords, thisShot_targetDelta, i + RangeUpgrade));

                // short delay in between shots
                yield return runCtx.WaitSeconds(.3f);
            }

            yield return waitGroup.Wait();
        }
    }
}