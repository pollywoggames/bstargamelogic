using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Pistol : ProjectileWeapon
    {
        public const int PistolDamage = 2, PistolRange = 4;
        
        // for pistol itself
        public Pistol(Fighter actor) : base(actor, 113, PistolDamage, PistolRange)
        { }
        
        // for subclasses
        protected Pistol(Fighter actor, int spriteIdx, int damage, int range, DirectionsType directionsType) : base(actor, spriteIdx, damage, range, directionsType: directionsType)
        { }
    }
}