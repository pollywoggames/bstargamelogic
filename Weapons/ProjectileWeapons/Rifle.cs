using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Rifle : ProjectileWeapon
    {
        public const int RifleDamage = 3, RifleRange = 6;

        public Rifle(Fighter actor, int damage = RifleDamage, int range = RifleRange, 
            DirectionsType directionsType = DirectionsType.OrthoOnly) : base(actor, 111, damage, range, directionsType: directionsType)
        {
            sound = Sound.RifleGunshot;
        }

        protected override IEnumerator Shoot(Coords targetCoords, Point targetDelta)
        {
            yield return OneShot(targetCoords, targetDelta);
        }
    }
}