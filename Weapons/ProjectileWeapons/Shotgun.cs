using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class Shotgun : ProjectileWeapon
    {
        public const int ShotgunDamage = 1, ShotgunRange = 3;
        public const float ShotgunSpread = 15f;
        public const float ShotgunShots = 3;

        public Shotgun(Fighter actor) : base(actor, 112, ShotgunDamage, ShotgunRange, hitsHalfWalls: true, hitsMaxRange: true)
        {
            UseWideRangeIndicators = true;
            sound = Sound.Shotgun;
        }

        protected override IEnumerator Shoot(Coords targetCoords, Point delta)
        {
            float spread = ShotgunSpread;
            int extraRangeForSideShots = 1;
            if (RangeUpgrade >= 1)
            {
                spread = 8f;
                extraRangeForSideShots = 0;
            }

            var baseAngle = DeltaToAngle(delta);
            var startingAngle = baseAngle - spread;
            var endingAngle = baseAngle + spread;

            var shots = new List<IEnumerator>();
            for (var angle = startingAngle; angle <= endingAngle; angle += spread * 2f / (ShotgunShots - 1))
            {
                var isMiddleShot = Math.Abs(angle - baseAngle) < .1f;
                var range = isMiddleShot ? GetRange() : GetRange() + extraRangeForSideShots;

                int extraTicks = 0;
                if (!isMiddleShot && RangeUpgrade >= 1)
                    extraTicks = 2;
                
                shots.Add(OneShot(targetCoords, AngleToDelta(angle), range, extraTicks));
            }

            yield return runCtx.All(shots);
        }
    }
}