using System;
using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public abstract class ProjectileWeapon : Weapon
    {
        public bool hitsHalfWalls;
        public bool hitsMaxRange;
        public Projectile.ImpactFunction impactFunction;
        
        protected ProjectileType projectileType;
        protected Sound sound;
        protected bool UseMuzzleFlash;
        protected bool StraightLineOnly;
        protected float ProjectileSpeed;
        protected bool canHitFriendlies;
        protected Projectile currProjectile;

        protected Coords? lastReflectedByFighterCoords => currProjectile?.lastReflectedByFighterCoords;

        protected ProjectileWeapon(Fighter actor, int spriteIdx, int damage, int range, bool hitsHalfWalls = false,
            DirectionsType directionsType = DirectionsType.OrthoOnly, bool hitsMaxRange = false)
            : base(actor, spriteIdx, damage, range)
        {
            impactFunction = null;
            UseMuzzleFlash = true;

            sound = Sound.PistolGunshot;
            projectileType = ProjectileType.Regular;
            StraightLineOnly = false;
            ProjectileSpeed = .25f;
            
            this.directionsType = directionsType;
            this.hitsHalfWalls = hitsHalfWalls;
            this.hitsMaxRange = hitsMaxRange;
            RefreshTargeter();
        }

        public override AbilityTargeter GetTargeter()
        {
            return new ProjectileTargeter(actor, Range + RangeUpgrade, hitsHalfWalls, directionsType, hitsMaxRange, true, true);
        }

        public override bool IsProjectileAttack()
        {
            return true;
        }

        public override IEnumerator Attack(Target target)
        {
            // use coords instead of pos so we don't get the Fighter.OffsetOnGrid
            var targetDelta = target.coords.ToPoint() - actor.coords.ToPoint();
            yield return Shoot(target.coords, targetDelta);
            yield return runCtx.WaitSeconds(.5f);
        }

        protected IEnumerator OneShot(Coords targetCoords, Point targetDelta, int rangeOverride = -1, int extraTicksToGo = 0)
        {
            // convert to coords and then back to Point so we don't get the Fighter.OffsetOnGrid
            var src = actor.coords.ToPoint();
            var dst = src + targetDelta;

            if (UseMuzzleFlash && !actor.DisableMuzzleFlash)
                actor.virtualActor.PlayMuzzleFlash();

            camera.Knockback(targetDelta);
            
            var projectile = new Projectile(
                game.contextProvider.ProvideProjectileVirtualActor(projectileType, src),
                actor,
                targetCoords,
                rangeOverride != -1 ? rangeOverride : (Range + RangeUpgrade),
                hitsHalfWalls,
                impactFunction);
            currProjectile = projectile;
            
            projectile.StraightLineOnly = StraightLineOnly;
            projectile.speed = ProjectileSpeed;
            projectile.canHitFriendlies = canHitFriendlies;
            projectile.keepGoingTicks = extraTicksToGo;
            
            soundCtx.PlayOneShot(sound);
            yield return projectile.Fly(dst);
        }

        protected virtual IEnumerator Shoot(Coords targetCoords, Point targetDelta)
        {
            // Single shot
            yield return OneShot(targetCoords, targetDelta);
        }

        public static float DeltaToAngle(Point delta)
        {
            return (float) (MoreMath.Rad2Deg * Math.Atan2(delta.y, delta.x));
        }

        public static Point AngleToDelta(float angle)
        {
            return new Point((float) Math.Cos(MoreMath.Deg2Rad * angle), (float) Math.Sin(MoreMath.Deg2Rad * angle));
        }
    }
}