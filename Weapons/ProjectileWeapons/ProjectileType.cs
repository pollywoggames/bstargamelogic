namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public enum ProjectileType
    {
        Regular,
        Bazooka,
        BusterShot,
        PsiBlast
    }
}