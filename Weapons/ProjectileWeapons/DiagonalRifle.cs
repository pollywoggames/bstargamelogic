using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class DiagonalRifle : Rifle
    {
        public const int DiagonalRifleDamage = 3, DiagonalRifleRange = 4;
        
        public DiagonalRifle(Fighter actor) : base(actor, DiagonalRifleDamage, DiagonalRifleRange, DirectionsType.OrthoAndDiagonal)
        {
            // a variation on the standard rifle that trades a bit of range and damage for the ability to shoot diagonally.
            spriteIdx = 117;
            
            StraightLineOnly = true;
        }
    }
}