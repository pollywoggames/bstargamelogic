using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons.ProjectileWeapons
{
    public class PsiBlast : ProjectileWeapon
    {
        public PsiBlast(Fighter actor) : base(actor, 225, 2, 4)
        {
            UseMuzzleFlash = false;
            projectileType = ProjectileType.PsiBlast;
            ProjectileSpeed = .15f;
            sound = Sound.ElectricDischarge;
        }
    }
}