using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Weapons
{
    public abstract class Weapon : IAbility
    {
        public BSWeapon type;
        public int Damage, Range;
        public DirectionsType directionsType;
        public bool UseWideRangeIndicators;
        public int spriteIdx;

        public int RangeUpgrade { get; private set;}
        public int DamageUpgrade { get; private set;}
        public int MiscUpgrade { get; private set; }
        
        // Weapons can't have a cooldown upgrade
        public int CooldownUpgrade => 0;

        protected Fighter actor;
        protected IFighterVirtualActor virtualActor => actor.virtualActor;
        protected Game game => actor.Owner.game;
        protected IRunContext runCtx => game.runCtx;
        protected IAnimationContext animCtx => game.animCtx;
        protected ISoundContext soundCtx => game.soundCtx;
        protected ICameraController camera => animCtx.Camera();

        protected AbilityTargeter targeter;
        
        private string _cachedTypeName;
        
        protected Weapon(Fighter actor, int spriteIdx, int Damage, int Range)
        {
            this.actor = actor;
            this.spriteIdx = spriteIdx;
            this.Damage = Damage;
            this.Range = Range;

            RefreshTargeter();
        }
        
        public abstract AbilityTargeter GetTargeter();

        protected void RefreshTargeter()
        {
            this.targeter = GetTargeter();
        }

        public int GetSpriteIndex()
        {
            return spriteIdx;
        }

        public string GetTypeName()
        {
            return _cachedTypeName ??= GetType().Name;
        }

        public virtual string GetTargeterTypeName()
        {
            return targeter.GetTypeName();
        }

        public bool Passive()
        {
            return false;
        }

        public string GetCriticalInfo()
        {
            return $"Damage: {TextUtils.GetAbilityDamageStr(this)}\nRange: {TextUtils.GetAbilityRangeStr(this)}";
        }

        public string FormatExtraFields(string rawDescription)
        {
            return targeter.FormatExtraFields(rawDescription);
        }

        public int GetCooldownTurnsRemaining()
        {
            return 0;
        }

        public int GetDamage()
        {
            return Damage + DamageUpgrade;
        }

        public int GetBaseDamage()
        {
            return Damage;
        }

        public int GetRange()
        {
            return Range + RangeUpgrade;
        }

        public int GetBaseRange()
        {
            return Range;
        }

        public virtual int GetMisc()
        {
            return 0;
        }

        public virtual int GetBaseMisc()
        {
            return GetMisc();
        }

        public string GetUnitName()
        {
            return actor.Name;
        }

        public int GetCooldown()
        {
            return 0;
        }

        public virtual bool IsProjectileAttack()
        {
            return false;
        }
        
        public virtual bool CanReceiveDamageUpgrade()
        {
            return true;
        }
        
        public virtual bool CanReceiveRangeUpgrade()
        {
            return true;
        }
        
        public virtual bool CanReceiveCooldownUpgrade()
        {
            return false;
        }
        
        public virtual bool CanReceiveMiscUpgrade()
        {
            return false;
        }

        public virtual void ApplyDamageUpgrade(int amount)
        {
            DamageUpgrade = amount;
        }

        public void ApplyRangeUpgrade(int amount)
        {
            RangeUpgrade = amount;
            RefreshTargeter();
        }

        public void ApplyCooldownUpgrade(int amount)
        {
            Logger.Warn($"ApplyCooldownUpgrade({amount}) to weapon {GetTypeName()} that already didn't have cooldown");
        }

        public virtual void ApplyMiscUpgrade(int amount)
        {
            MiscUpgrade = amount;
        }

        public TargetList GetTargets()
        {
            if (targeter == null)
            {
                Logger.Error("Targeter is null for " + GetType().Name);
                return null;
            }

            return targeter.GetTargets();
        }

        public TargetList GetReadyAttacks()
        {
            if (targeter is not IReadyAttackTargeter readyAttackTargeter)
            {
                Logger.Warn($"Called GetReadyAttacks but targeter is not IReadyAttackTargeter! weapon={type}, targeter={targeter.GetTypeName()}");
                return new TargetList();
            }

            return readyAttackTargeter.GetReadyAttacks();
        }

        public IEnumerator DealDamage(IDamageable target, Fighter origin = null)
        {
            if (origin == null)
                origin = actor;
            
            yield return target.DealDamage(GetDamage(), origin);
        }

        public abstract IEnumerator Attack(Target target);

        public static Weapon New(BSWeapon w, Fighter actor)
        {
            Weapon weapon = w switch
            {
                BSWeapon.AcidPunch => new AcidPunch(actor),
                BSWeapon.DiagonalRifle => new DiagonalRifle(actor),
                BSWeapon.Forklift => new Forklift(actor),
                BSWeapon.Machete => new Machete(actor),
                BSWeapon.Pistol => new Pistol(actor),
                BSWeapon.PlasmaSword => new PlasmaSword(actor),
                BSWeapon.PsiBlast => new PsiBlast(actor),
                BSWeapon.Rifle => new Rifle(actor),
                BSWeapon.SavagePeck => new SavagePeck(actor),
                BSWeapon.Shotgun => new Shotgun(actor),
                BSWeapon.Sidearm => new Sidearm(actor),
                BSWeapon.SmashAttack => new SmashAttack(actor),
                BSWeapon.UnarmedAttack => new UnarmedAttack(actor),
                BSWeapon.BashSelfDamageAttack => new BashSelfDamageAttack(actor),
                BSWeapon.DashStrike => new DashStrike(actor),
                BSWeapon.MachineGun => new MachineGun(actor),
                BSWeapon.Magnum => new Magnum(actor),
                BSWeapon.Revolver => new Revolver(actor),
                _ => throw new Exception("Can't find weapon " + w)
            };
            weapon.type = w;
            return weapon;
        }
    }
}