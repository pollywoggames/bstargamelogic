using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons
{
    public class SmashAttack : Weapon
    {
        public const int SmashRange = 2;
        public const int SmashDamage = 2;

        public static readonly List<Coords> hitArea = new List<Coords>
        {
            Coords.zero,
            Coords.up, Coords.right, Coords.left, Coords.down,
            new Coords(1, 1), new Coords(1, -1), new Coords(-1, 1), new Coords(-1, -1)
        };

        public SmashAttack(Fighter actor) : base(actor, 139, SmashDamage, SmashRange)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new SmashAttackTargeter(actor, 1, SmashRange + RangeUpgrade);
        }

        public override IEnumerator Attack(Target target)
        {
            actor.TempDisableMovementPreviews = true;
            
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();

            var positionBefore = actor.pos;

            var dst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
            actor.virtualActor.Face(dst);
            yield return animCtx.MoveParabolic(actor, dst, 16, 2);
            
            soundCtx.PlayOneShot(Sound.SmashAttack);

            var attackables = game.AttackablesFor(actor.Owner);
            // put them into this first before we deal damage to remove duplicates, so we don't damage the nexus 2x
            var damageablesHit = new HashSet<IDamageable>();
            var wg = new WaitGroup(game.runCtx);
            foreach (var direction in hitArea)
            {
                var hitLocation = target.coords + direction;

                int coverDamage = direction == Coords.zero ? 2 : 1;
                wg.Start(game.map.DamageCover(hitLocation, coverDamage, actor.pos, actor));

                if (attackables.TryGetValue(hitLocation, out var damageable)) damageablesHit.Add(damageable);
                
                if (direction == Coords.zero)
                    game.map.DamagePlacedTeleporter(hitLocation);
            }

            foreach (var d in damageablesHit)
                wg.Start(d.DealDamage(GetDamage(), actor));

            camera.Shake(.4f, CameraConstants.DefaultShakeIntensity);

            yield return runCtx.WaitSeconds(.5f);
            yield return animCtx.MoveSmoothly(actor, positionBefore, .16f);

            yield return wg.Wait();

            actor.virtualActor.BackwardInSortingOrder();
            
            actor.TempDisableMovementPreviews = false;
        }
    }
}