using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons
{
    public class SavagePeck : Machete
    {
        public SavagePeck(Fighter actor) : base(actor)
        {
            spriteIdx = 216;
            sound = Sound.ClawHit;
        }
    }
}