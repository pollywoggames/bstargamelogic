using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;

namespace BarnardsStar.Weapons
{
    public class Forklift : UnarmedAttack
    {
        public Forklift(Fighter actor) : base(actor)
        {
            spriteIdx = 223;
            Damage = 2;
            suppressKnockback = true;
            damageCover = false;
            sound = Sound.ElectricDischarge;
            targeter = new ForkliftTargeter(actor);
        }
        
        public override IEnumerator OnImpact(Target target)
        {
            animCtx.PlayVFX(VFXType.ShockBurstEffect, target.coords.ToPoint(), VFXColor.Purple);
            
            var punchDelta = target.coords - actor.coords;

            if (game.TryGetHazard(target.coords, out var h) && h.moveable)
            {
                animCtx.PlayElectricArcEffect(target.coords.ToPoint(), (target.coords + punchDelta).ToPoint(), VFXColor.Purple, .3f);
                yield return Hazard.ShoveHazard(game, h, target.coords, target.coords + punchDelta, actor);
            }
            else if (game.map.GetTerrainType(target.coords) == TerrainType.Cover && !game.map.nexiCoords.Contains(target.coords))
            {
                animCtx.PlayElectricArcEffect(target.coords.ToPoint(), (target.coords + punchDelta).ToPoint(), VFXColor.Purple, .3f);
                yield return game.map.ShoveCover(target.coords, target.coords + punchDelta, actor);
            }
            else if (game.TryGetFighter(target.coords, out var f))
            {
                animCtx.PlayElectricArcEffect(target.coords.ToPoint(), (target.coords + punchDelta).ToPoint(), VFXColor.Purple, .3f);
                yield return f.GetPushed(punchDelta, actor);
            }
        }
    }
}