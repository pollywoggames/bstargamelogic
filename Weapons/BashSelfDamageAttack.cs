using System.Collections;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Weapons
{
    public class BashSelfDamageAttack : UnarmedAttack
    {
        public BashSelfDamageAttack(Fighter actor) : base(actor)
        {
            spriteIdx = 236;
            Damage = 3;
        }

        public override IEnumerator OnImpact(Target target)
        {
            // deal self 1 damage
            yield return actor.DealDamage(1, null, true);
        }
    }
}