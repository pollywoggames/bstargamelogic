using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;

namespace BarnardsStar.Weapons
{
    public class AcidPunch : UnarmedAttack
    {
        public AcidPunch(Fighter actor) : base(actor)
        {
            spriteIdx = 213;
            Damage = 1;

            targeter = new AcidPunchTargeter(actor, 1, 1);
        }

        public override IEnumerator OnImpact(Target target)
        {
            yield return Hazard.PlaceHazardAt(actor, HazardType.Acid, target.coords);
        }
    }
}