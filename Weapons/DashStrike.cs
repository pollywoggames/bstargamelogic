using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abilities.ActiveAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Weapons
{
    public class DashStrike : Weapon
    {
        public const int DashStrikeDamage = 3;
        public const int DashStrikeRange = 2;

        public DashStrike(Fighter actor) : base(
            actor,
            241,
            DashStrikeDamage,
            DashStrikeRange)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new DashStrikeTargeter(actor, GetRange(), GetDamage());
        }

        public override void ApplyDamageUpgrade(int amount)
        {
            base.ApplyDamageUpgrade(amount);

            RefreshTargeter();
        }

        public override IEnumerator Attack(Target target)
        {
            yield return ChargeAttack.StaticExecute(actor, target, GetDamage(), false, true, true, Sound.DashStrike, Sound.ClawHit);
        }
    }
}