﻿using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Weapons
{
    public class Machete : UnarmedAttack
    {
        public Machete(Fighter actor) : base(actor)
        {
            spriteIdx = 114;
            Damage = 3;
            sound = Sound.Machete;
        }
    }
}