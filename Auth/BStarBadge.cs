using System;
using System.Collections.Generic;

namespace BarnardsAscot.bstargamelogic.Auth
{
    public enum BStarBadge
    {
        // These are serialized by index so DO NOT change the order!
        Dev = 0,
        Beta = 1,
        
        _100GamesPlayed = 4,
        _200GamesPlayed = 5,
        _500GamesPlayed = 6,
        _1000GamesPlayed = 7,
        
        Season1Top10 = 2,
        Season1Player = 3,
        Season1Tourney1st = 8,
        Season1Tourney2nd = 9,
        Season1Tourney3rd = 10,
        
        Season2Top10 = 11,
        Season2Player = 12,
        Season2Tourney1st = 13,
        Season2Tourney2nd = 14,
        Season2Tourney3rd = 15,
        
        Season3Top10 = 16,
        Season3Player = 17,
        
        // add new badges here
    }

    public class BStarBadgeInfo
    {
        public string name;
        public string color;
        public string icon;
        public int sort;
        public List<BStarBadge> supersededBy;

        public static BStarBadgeInfo Lookup(BStarBadge badge)
        {
            var ret = badge switch
            {
                BStarBadge.Dev => new BStarBadgeInfo
                {
                    name = "Dev",
                    color = "#cbde96", // pollywog green
                    icon = "dev-computer",
                    sort = -100,
                },
                BStarBadge.Beta => new BStarBadgeInfo
                {
                    name = "Beta",
                    color = "#3BBDFD", // blue
                    icon = "beta-test-tube",
                    sort = -99,
                },
                BStarBadge._100GamesPlayed => new BStarBadgeInfo
                {
                    name = "100 Games Played",
                    color = "#f5f5f5", // white
                    icon = "100-games-played",
                    sort = 100,
                    supersededBy = new() { BStarBadge._200GamesPlayed, BStarBadge._500GamesPlayed, BStarBadge._1000GamesPlayed },
                },
                BStarBadge._200GamesPlayed => new BStarBadgeInfo
                {
                    name = "200 Games Played",
                    color = "#f5f5f5", // white
                    icon = "200-games-played",
                    sort = 100,
                    supersededBy = new() { BStarBadge._500GamesPlayed, BStarBadge._1000GamesPlayed },
                },
                BStarBadge._500GamesPlayed => new BStarBadgeInfo
                {
                    name = "500 Games Played",
                    color = "#f5f5f5", // white
                    icon = "500-games-played",
                    sort = 100,
                    supersededBy = new() { BStarBadge._1000GamesPlayed },
                },
                BStarBadge._1000GamesPlayed => new BStarBadgeInfo
                {
                    name = "1000 Games Played",
                    color = "#f5f5f5", // white
                    icon = "1000-games-played",
                    sort = 100,
                },
                BStarBadge.Season1Top10 => new BStarBadgeInfo
                {
                    name = "Season 1 Top 10",
                    color = "#fcdf03", // gold
                    icon = "season-1-top-10",
                },
                BStarBadge.Season1Player => new BStarBadgeInfo
                {
                    name = "Season 1",
                    color = "#d8d8d8", // light grey
                    icon = "season-1-player",
                },
                BStarBadge.Season2Top10 => new BStarBadgeInfo
                {
                    name = "Season 2 Top 10",
                    color = "#fcdf03", // gold
                    icon = "season-2-top-10",
                },
                BStarBadge.Season2Player => new BStarBadgeInfo
                {
                    name = "Season 2",
                    color = "#d8d8d8", // light grey
                    icon = "season-2-player",
                },
                BStarBadge.Season1Tourney1st => new BStarBadgeInfo
                {
                    name = "S1 Tourney Winner",
                    color = "#fcdf03", // gold
                    icon = "tourn-1st"
                },
                BStarBadge.Season1Tourney2nd => new BStarBadgeInfo
                {
                    name = "S1 Tourney 2nd",
                    color = "#989898", // silver
                    icon = "tourn-2nd"
                },
                BStarBadge.Season1Tourney3rd => new BStarBadgeInfo
                {
                    name = "S1 Tourney 3rd",
                    color = "#d8752a", // bronze
                    icon = "tourn-3rd"
                },
                BStarBadge.Season2Tourney1st => new BStarBadgeInfo
                {
                    name = "S2 Tourney Winner",
                    color = "#fcdf03", // gold
                    icon = "tourn-1st"
                },
                BStarBadge.Season2Tourney2nd => new BStarBadgeInfo
                {
                    name = "S2 Tourney 2nd",
                    color = "#989898", // silver
                    icon = "tourn-2nd"
                },
                BStarBadge.Season2Tourney3rd => new BStarBadgeInfo
                {
                    name = "S2 Tourney 3rd",
                    color = "#d8752a", // bronze
                    icon = "tourn-3rd"
                },
                BStarBadge.Season3Top10 => new BStarBadgeInfo
                {
                    name = "Season 3 Top 10",
                    color = "#fcdf03", // gold
                    icon = "season-3-top-10",
                },
                BStarBadge.Season3Player => new BStarBadgeInfo
                {
                    name = "Season 3",
                    color = "#d8d8d8", // light grey
                    icon = "season-3-player",
                },
                _ => throw new ArgumentOutOfRangeException(nameof(badge), badge, null)
            };

            ret.sort += (int)badge;

            return ret;
        }
    }
}