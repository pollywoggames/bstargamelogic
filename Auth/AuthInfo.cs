using System;

namespace BarnardsAscot.Auth
{
    public class AuthInfo
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string SteamId { get; set; }
        public string GameCenterPlayerId { get; set; }
        public string GPlayId { get; set; }
        public bool IsAdmin;

        public bool HasTypeSteamId => !string.IsNullOrEmpty(SteamId);
        public bool HasTypeIOS => !string.IsNullOrEmpty(GameCenterPlayerId);
        public bool HasTypeAndroid => !string.IsNullOrEmpty(GPlayId);

        public AuthInfo Copy()
        {
            return (AuthInfo) MemberwiseClone();
        }

        public AuthType GetAuthType()
        {
            if (!string.IsNullOrEmpty(SteamId))
            {
                return AuthType.Steam;
            }

            if (!string.IsNullOrEmpty(GameCenterPlayerId))
            {
                return AuthType.iOS;
            }

            if (!string.IsNullOrEmpty(GPlayId))
            {
                return AuthType.GPlay;
            }

            return AuthType.None;
        }
    }
}