namespace BarnardsAscot.Auth
{
    public class AndroidAuthObject
    {
        public string PlayerId { get; set; }
        public string DisplayName { get; set; }
        public string ServerAuthCode { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(PlayerId) || string.IsNullOrEmpty(DisplayName) ||
                   string.IsNullOrEmpty(ServerAuthCode);
        }
    }
}