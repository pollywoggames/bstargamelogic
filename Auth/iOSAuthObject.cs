using System;

namespace BarnardsAscot.Auth
{
    public class iOSAuthObject
    {
        public string PlayerId { get; set; }
        public string DisplayName { get; set; } 
        public string PublicKeyURL { get; set; }
        public ulong Timestamp { get; set; }
        public byte[] Salt { get; set; }
        public byte[] Signature { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(PlayerId) ||
                   string.IsNullOrEmpty(PublicKeyURL) ||
                   Timestamp == 0 ||
                   Salt.Length == 0 ||
                   Signature.Length == 0;
        }
        
        public override string ToString()
        {
            return $"iOSAuthObject({nameof(PlayerId)}: {PlayerId}, \n" +
                   $"{nameof(DisplayName)}: {DisplayName}, \n" +
                   $"{nameof(PublicKeyURL)}: {PublicKeyURL}, \n" +
                   $"{nameof(Timestamp)}: {Timestamp}, \n" +
                   $"{nameof(Salt)}: {Convert.ToBase64String(Salt)}, \n" +
                   $"{nameof(Signature)}: {Convert.ToBase64String(Signature)})";
        }
    }
}