namespace BarnardsStar.Auth.Responses
{
    public class GetRecentPlayerCountResponse
    {
        public int Count { get; set; }
    }
}