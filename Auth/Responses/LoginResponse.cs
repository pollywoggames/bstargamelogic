﻿using BarnardsAscot.Auth;

namespace BarnardsStar.Auth.Responses
{
    public class LoginResponse
    {
        public EncryptedAppTicket EncryptedAppTicket { get; set; }
        public string iOSAuthToken { get; set; }
        public string GooglePlayAuthToken { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public bool ShouldSetupNotifications { get; set; }
        public string Status { get; set; }
        public string AdminToken { get; set; }
        
        // whether a new player account was just created with the current request
        public bool IsNewPlayer { get; set; }
        
        // whether the current player has ever uploaded a map
        public bool HasPublishedMap { get; set; }
    }
}
