﻿namespace BarnardsStar.Auth.Responses
{
    public class DiscordCountResponse
    {
        public int membersCount;
        public int membersOnline;
    }
}