using System;
using System.Linq;

namespace BarnardsAscot.Auth
{
    // It's unfortunate that this wrapper class has to exist at all
    // Steamworks.NET wraps the C++ API as-is, hence this class for convenience
    public class EncryptedAppTicket
    {
        private byte[] _buffer;
        public byte[] Buffer
        {
            get => _buffer;
            set
            {
                _buffer = value;
                Length = (uint) value.Length;
            }
        }

        public uint Length { get; set; }

        public EncryptedAppTicket()
        {
        }

        public EncryptedAppTicket(byte[] buffer, uint length)
        {
            Buffer = buffer.Take((int) length).ToArray();
        }

        public EncryptedAppTicket(byte[] buffer)
        {
            Buffer = buffer;
        }
        
        public bool IsEmpty()
        {
            return Length == 0;
        }

        public override string ToString()
        {
            return BitConverter.ToString(Buffer);
        }

        public static EncryptedAppTicket FromString(string str)
        {
            int length = (str.Length + 1) /3;
            byte[] arr = new byte[length];
            for (int i = 0; i < length; i++)
                arr[i] = Convert.ToByte(str.Substring(3 * i, 2), 16);
            return new EncryptedAppTicket(arr);
        }
    }
}