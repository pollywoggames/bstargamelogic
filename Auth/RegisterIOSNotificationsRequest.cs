namespace BarnardsAscot.bstargamelogic.Auth
{
    public class RegisterIOSNotificationsRequest
    {
        public bool AllowNotifications { get; set; }
        public string DeviceToken { get; set; }
    }
}