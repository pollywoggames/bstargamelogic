﻿using System.Collections.Generic;
using BarnardsAscot.Auth;
using BarnardsStar.Characters;

namespace BarnardsStar.Auth.Requests
{
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Platform { get; set; }
        public EncryptedAppTicket EncryptedAppTicket { get; set; }
        public iOSAuthObject iOSAuthObject { get; set; }
        public AndroidAuthObject AndroidAuthObject { get; set; }
    }
}
