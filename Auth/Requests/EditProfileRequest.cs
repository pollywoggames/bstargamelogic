using BarnardsStar.Players;

namespace BarnardsStar.Auth.Requests
{
    public class EditProfileRequest
    {
        public string NewUsername;
        public PlayerIcon NewPlayerIcon;
    }
}