namespace BarnardsStar.Auth.Requests
{
    public class RequestSteamNotificationsRequest
    {
        public string SteamId { get; set; }
    }
}