﻿namespace BarnardsStar.Auth.Requests
{
    public class ResetPasswordRequest
    {
        public string Email { get; set; }
    }
}
