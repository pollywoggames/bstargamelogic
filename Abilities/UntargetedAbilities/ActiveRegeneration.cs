﻿using System.Collections;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class ActiveRegeneration : UntargetedAbility
    {
        public const int HealPerTurn = 3;

        public ActiveRegeneration(Fighter actor) : base(actor, 239, 1)
        {
            Damage = HealPerTurn;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            actor.ShowHealthBar();
            actor.SetHealth(actor.CurrHealth + GetDamage());
            soundCtx.PlayOneShot(Sound.Healing);
            animCtx.PlayVFX(VFXType.Regeneration, actor.pos);
            yield return runCtx.WaitSeconds(.65f);
            
            actor.HideHealthBar();

            // if (!game.CheatEnabled(Cheat.FastPassives))
            //     yield return runCtx.WaitSeconds(PassiveAbilityPause);
        }
    }
}
