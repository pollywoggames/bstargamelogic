using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    public class HealTeammatesInOil : UntargetedAbility
    {
        public const int HealAmount = 5;

        public HealTeammatesInOil(Fighter actor) : base(actor, 214, 1)
        {
            Damage = HealAmount;
        }

        public override IEnumerator Execute(Target target)
        {
            soundCtx.PlayOneShot(Sound.Healing);
            
            runCtx.Start(PassiveAbilityEffect(VFXType.Regeneration, null, actor.GetPosition()));
            actor.SetHealth(actor.MaxHealth);
            
            foreach (var teammate in actor.Owner.LiveFighters)
            {
                if (game.TryGetHazard(teammate.coords, out var h) && h.type == HazardType.Oil)
                {
                    // actor already handled above
                    if (teammate.id != actor.id)
                    {
                        runCtx.Start(PassiveAbilityEffect(VFXType.Regeneration, null, teammate.GetPosition()));

                        // gets clamped to max health so we don't worry about it
                        teammate.SetHealth(teammate.CurrHealth + GetDamage());
                    }

                    // The oil gets removed so the just-healed person isn't sitting in such a vulnerable spot
                    game.RemoveHazard(h);
                }
            }
            
            yield break;
        }
    }
}