using System.Collections.Generic;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    public class OilRain : HazardRain
    {
        public OilRain(Fighter actor) : base(actor, 254, 1)
        {
            DisplayRange = 2;
            
            hazardType = HazardType.Oil;
            
            effectDirections = new List<Coords>
            {
                new Coords(0, 0),
                new Coords(0, 1),
                new Coords(0, -1),
                new Coords(1, 0),
                new Coords(-1, 0),
            };

            rainProp = MovablePropType.OilRainFX;
            projectileProp = MovablePropType.OilProjectile;
        }

        public override int GetDamage()
        {
            return Hazard.AcidDamage;
        }
    }
}