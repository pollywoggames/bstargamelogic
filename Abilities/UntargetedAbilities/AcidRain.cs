﻿using System.Collections.Generic;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    public class AcidRain : HazardRain
    {
        public AcidRain(Fighter actor) : base(actor, 206, 1)
        {
            DisplayRange = 2;

            hazardType = HazardType.Acid;
            
            effectDirections = new List<Coords>
            {
                new Coords(0, 0),
                new Coords(0, 1),
                new Coords(0, -1),
                new Coords(1, 0),
                new Coords(1, 1),
                new Coords(1, -1),
                new Coords(-1, 0),
                new Coords(-1, 1),
                new Coords(-1, -1),
                new Coords(0, 2),
                new Coords(0, -2),
                new Coords(2, 0),
                new Coords(-2, 0)
            };

            rainProp = MovablePropType.AcidRainFX;
            projectileProp = MovablePropType.AcidProjectile;
        }

        public override int GetDamage()
        {
            return Hazard.AcidDamage;
        }
    }
}
