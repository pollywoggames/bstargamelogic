using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using System.Numerics;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    public abstract class HazardRain : UntargetedAbility
    {
        protected List<Coords> effectDirections;
        protected HazardType hazardType;
        protected MovablePropType rainProp, projectileProp;
        
        public HazardRain(Fighter actor, int spriteIdx, int cooldown) : base(actor, spriteIdx, cooldown)
        { }
        
        public override IEnumerator Execute(Target target)
        {
            yield return animCtx.MoveRotateSquish(actor,
                actor.pos,
                0,
                new Vector3(1.5f, .67f, 1f), 
                .17f);

            yield return animCtx.MoveRotateSquish(actor,
                actor.pos,
                0,
                new Vector3(.67f, 1.5f, 1f),
                .17f);

            runCtx.Start(animCtx.ShakeObject(actor, 15, .08f));

            var targets = new List<Coords>();
            foreach (var d in effectDirections)
            {
                var loc = actor.coords + d;
                targets.Add(loc);
                if (game.TryGetFighter(loc, out var f)) f.ShowHealthBar();
            }

            targets = ListUtils.ShuffleList(targets, actor.game.randy);

            var rainFx = contextProvider.ProvideProp(rainProp, actor.pos);

            var waitGroup = new WaitGroup(runCtx);
            foreach (var t in targets)
            {
                waitGroup.Start(Hazard.PlaceWithProjectile(hazardType, projectileProp, actor, actor.coords, t));
                yield return runCtx.WaitSeconds(.08f);
            }
            rainFx.Destroy();

            yield return animCtx.MoveRotateSquish(actor,
                actor.pos,
                0,
                Vector3.One,
                .17f);

            yield return waitGroup.Wait();
        }
    }
}