using System.Collections.Generic;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    public class AcidDrizzle : HazardRain
    {
        public AcidDrizzle(Fighter actor) : base(actor, 253, 1)
        {
            DisplayRange = 2;
            
            hazardType = HazardType.Acid;
            
            effectDirections = new List<Coords>
            {
                new Coords(0, 0),
                new Coords(0, 1),
                new Coords(0, -1),
                new Coords(1, 0),
                new Coords(-1, 0),
            };

            rainProp = MovablePropType.AcidRainFX;
            projectileProp = MovablePropType.AcidProjectile;
        }

        public override int GetDamage()
        {
            return Hazard.AcidDamage;
        }
    }
}