using System.Collections;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SelfHaste : UntargetedAbility
    {
        public const int SelfHasteAmount = 2;

        public SelfHaste(Fighter actor) : base(actor, 251, 1)
        {
            Damage = SelfHasteAmount;
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);
            
            animCtx.PlayVFX(VFXType.SpeedBoostVFX, target.coords.ToPoint() + new Point(0, -.3f));
            soundCtx.PlayOneShot(Sound.SpeedBoost);
            actor.SetMovesLeft(actor.MovesLeft + GetDamage(), true);
            
            actor.virtualActor.SetRotation(0);

            // wait a bit for dramatic effect (otherwise it moves on too quick in replay mode)
            yield return runCtx.WaitSeconds(.65f);
        }
    }
}