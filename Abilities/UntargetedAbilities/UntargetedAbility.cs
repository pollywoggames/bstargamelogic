﻿using System;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.Ground;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.UntargetedAbilities
{
    // Abilities that are cast instantly, without requiring the user to input a target
    public abstract class UntargetedAbility : AbstractAbility
    {
        protected UntargetedAbility(Fighter actor, int spriteIdx, int cooldown) : base(actor, spriteIdx, cooldown)
        {
            IsUntargeted = true;
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new SelfTargeter(actor);
        }
    }
}
