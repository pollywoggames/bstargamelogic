﻿using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class KnockoutPunch : AbstractAbility
    {
        public const int KnockoutPunchDamage = 1;

        public KnockoutPunch(Fighter actor) : base(actor, 165, 1)
        {
            DisplayRange = 1;
            Damage = KnockoutPunchDamage;
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new EnemyOnlyTargeter(actor, 1, 1, false);
        }

        public override IEnumerator Execute(Target target)
        {
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();
            
            // Make sure the actor stays in their own coords even though they'll be moving into
            // the enemy's square for a sec.
            actor.OverrideCoords = actor.coords;

            var positionBefore = actor.pos;
            var actorStartingCoords = actor.coords;
            var dst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
            var origDelta = dst - actor.pos;
            actor.virtualActor.Face(dst);

            // Rear up backwards: tilt + move backwards slightly
            var backwardRotationAngle = actor.GetBackwardsRotationAngle();
            actor.SetRotation(backwardRotationAngle);
            yield return animCtx.MoveSmoothly(actor, actor.pos - origDelta.normalized * .4f, .13f);

            // Then they tilt forwards and bump into the other guy, moving a little into their square
            actor.SetRotation(-backwardRotationAngle);
            var delta = origDelta * .8f;
            dst = actor.pos + delta;

            yield return animCtx.MoveSmoothly(actor, dst, .07f);

            soundCtx.PlayOneShot(Sound.UnarmedStrike);
            
            var atkables = game.AttackablesFor(actor.Owner);
            var waitGroup = new WaitGroup(runCtx);
            if (atkables.TryGetValue(target.coords, out var d))
            {
                if (d is Fighter f)
                {
                    yield return f.DealDamage(GetDamage(), actor, true);
                    
                    var punchDelta = target.coords - actorStartingCoords;
                    waitGroup.Start(f.GetPushed(punchDelta, actor));

                    if (!f.IsDead)
                        f.AddStatusEffect(StatusEffect.Stunned, 1, actor);
                }
                else
                {
                    // nexus
                    waitGroup.Start(d.DealDamage(GetDamage(), actor));
                }
            }
            
            // wait for hitstop animation
            yield return runCtx.WaitSeconds(.25f);

            // move back to original position & rotation
            actor.SetRotation(0);
            yield return animCtx.MoveSmoothly(actor, positionBefore, .07f);
            
            actor.virtualActor.BackwardInSortingOrder();
            
            yield return waitGroup.Wait();
        }
    }
}
