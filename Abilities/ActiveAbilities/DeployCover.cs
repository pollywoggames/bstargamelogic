﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class DeployCover : AbstractAbility
    {
        private const int Range = 5;
        
        protected MovablePropType _movablePropType;
        
        public DeployCover(Fighter actor) : base(actor, 252, 1)
        {
            DisplayRange = Range;
            UseWideRangeIndicators = true;
            _movablePropType = MovablePropType.ThrownDeployableCover;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new DeployCoverTargeter(actor, 1, Range + RangeUpgrade);
        }

        public override IEnumerator Execute(Target target)
        {
            var direction = target.coords - actor.coords;
            yield return runCtx.Run(actor.virtualActor.AnimateThrowWindup(direction, 0));
            runCtx.Start(actor.virtualActor.AnimateThrowFollowthrough(direction));

            var coroutines = new List<Tuple<IEnumerator, IMovable, Coords>>();
            foreach (var t in target.GetAllLinked())
            {
                var thrownObj = contextProvider.ProvideProp(_movablePropType, actor.pos);
                var location = t.coords.ToPoint() + GameMap.CoverTilemapOffset;

                var coroutine = animCtx.MoveParabolic(thrownObj, location, 16, 1.5f);
                coroutines.Add(new Tuple<IEnumerator, IMovable, Coords>(coroutine, thrownObj, t.coords));
            }
            soundCtx.PlayOneShot(Sound.Throw);
            yield return runCtx.All(coroutines.Select(c => c.Item1).ToList());
            
            // now that they're all done, do the effects (in parallel)
            var impactCoroutines = new List<IEnumerator>();
            foreach (var c in coroutines)
            {
                var effectObj = c.Item2;
                var coords = c.Item3;
                
                effectObj.Destroy();
                animCtx.PlayVFX(VFXType.DeployCoverLandEffect, coords.ToPoint());
                impactCoroutines.Add(OnImpact(coords));
            }
            soundCtx.PlayOneShot(Sound.Poof);

            // wait for effects to finish
            yield return runCtx.All(impactCoroutines);
        }

        protected virtual IEnumerator OnImpact(Coords coords)
        {
            yield return game.map.DeployCover(coords, actor);
        }
    }
}
