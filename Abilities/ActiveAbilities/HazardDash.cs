using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public abstract class HazardDash : AbstractAbility
    {
        protected HazardType hazardType;
        protected MovablePropType hazardProjectileType;
        protected Sound dashSound;
        
        protected HazardDash(Fighter actor, int spriteIdx, int cooldown) : base(actor, spriteIdx, cooldown)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new DashTargeter(actor, 1, GetRange());
        }

        public override bool DisableMovementPreviewScoresForActor()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();

            var current = actor.coords;
            var delta = target.coords - current;
            var normalized = delta.normalized;
            var dst = target.coords.ToPoint();
            var origDelta = dst - current.ToPoint();

            OnWindUp();

            // Rear up backwards: tilt + move backwards slightly
            var backwardRotationAngle = actor.GetBackwardsRotationAngle();
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return animCtx.MoveSmoothly(actor, actor.pos - origDelta.normalized * .4f, .13f);

            // Now tilt forwards & charge
            actor.virtualActor.SetRotation(-backwardRotationAngle);
            
            soundCtx.PlayOneShot(dashSound);
            var consequences = new WaitGroup(runCtx);
            for (var i = 0; i < delta.magnitude; i++)
            {
                consequences.Start(Hazard.PlaceHazardAt(actor, hazardType, current));
                
                current += normalized;

                bool isLastSquare = i >= delta.magnitude;
                if (!isLastSquare && game.map.GetTerrainType(current) == TerrainType.Pit)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.CrossGap, actor.Owner));
                }

                dst = current.ToPoint() + Fighter.OffsetOnGrid;
                yield return animCtx.MoveSmoothly(actor, dst, .14f);
            }
            
            consequences.Start(OnFinishDashing());

            // splash hazard in all directions
            soundCtx.PlayOneShot(Sound.OilBarrelSplash);
            foreach (var dir in Coords.AllDirections)
            {
                var dstCoords = actor.coords + dir;
                if (!game.TryGetHazard(dstCoords, out var h) || h.type != hazardType)
                {
                    consequences.Start(Hazard.PlaceWithProjectile(hazardType, hazardProjectileType, actor, actor.coords, dstCoords));
                }
            }

            // move back to original rotation
            actor.virtualActor.SetRotation(0);
            actor.virtualActor.BackwardInSortingOrder();

            yield return game.OnFighterMoved(actor);
            
            yield return consequences.Wait();
        }
        
        protected virtual void OnWindUp() { }

        protected abstract IEnumerator OnFinishDashing();
    }
}