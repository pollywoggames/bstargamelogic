using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class OilConeSpray : AbstractAbility
    {
        public const int OilSprayRange = 5;

        public OilConeSpray(Fighter actor) : base(actor, 212, 1)
        {
            DisplayRange = OilSprayRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new OrthogonalDirectionsTargeter(actor, 1, 1);
        }

        public override IEnumerator Execute(Target target)
        {
            var delta = (target.coords - actor.coords).normalized;
            yield return actor.virtualActor.AnimateThrowWindup(delta, 0);
            runCtx.Start(actor.virtualActor.AnimateThrowFollowthrough(delta));

            var range = OilSprayRange + RangeUpgrade;

            var startingCoords = actor.coords + delta;
            var sideDirections = delta.x == 0 ? new[] {Coords.left, Coords.right} : new[] {Coords.up, Coords.down};
            List<IEnumerator> coroutines = new List<IEnumerator>()
            {
                SprayOilInLine(startingCoords, delta, range),
                SprayOilInLine(startingCoords, delta + sideDirections[0], range - 1),
                SprayOilInLine(startingCoords, delta + sideDirections[1], range - 1),
            };
            
            yield return runCtx.All(coroutines);
        }

        private IEnumerator SprayOilInLine(Coords startingCoords, Coords direction, int range)
        {
            var obj = contextProvider.ProvideProp(MovablePropType.OilProjectile, actor.pos);

            var currCoords = startingCoords;
            var gravity = .85f;
            for (var i = 0; i < range;)
            {
                if (game.map.GetTerrainType(currCoords) != TerrainType.Clear)
                    break;

                var targetPosition = currCoords.ToPoint();
                yield return animCtx.MoveParabolic(obj, targetPosition, 10, gravity);

                if (Hazard.CanPlaceHazardAt(actor.game, HazardType.Oil, currCoords))
                {
                    yield return Hazard.PlaceHazardAt(actor, HazardType.Oil, currCoords);
                    i++;
                }
                
                gravity *= .8f;
                if (gravity < .1f)
                    gravity = .1f;

                currCoords += direction;
            }
            
            obj.Destroy();
        }
    }
}