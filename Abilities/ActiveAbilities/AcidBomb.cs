using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class AcidBomb : ThrowSomething
    {
        public const int AcidBombRange = 4;
        
        public AcidBomb(Fighter actor) : base(actor, 228, 1, DirectionsType.OrthoOnly, MovablePropType.AcidProjectile)
        {
            DisplayRange = AcidBombRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new OrthogonalDirectionsTargeter(actor, 1, AcidBombRange + RangeUpgrade);
        }

        public override IEnumerator OnImpact(Target target)
        {
            WaitGroup wg = new WaitGroup(game.runCtx);
            
            // the central place happens immediately
            wg.Start(Hazard.PlaceHazardAt(actor, HazardType.Acid, target.coords));
            
            // all the other places happen in random order
            var targets = new List<Coords>(Coords.OrthogonalDirections);
            targets = ListUtils.ShuffleList(targets, game.randy);
            foreach (var dir in targets)
            {
                wg.Start(Hazard.PlaceAcidWithProjectile(actor, target.coords, target.coords + dir));
                yield return game.runCtx.WaitSeconds(.02f);
            }

            yield return wg.Wait();
        }
    }
}