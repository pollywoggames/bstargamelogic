﻿using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Bazooka : AbstractAbility
    {
        public const int MaxRange = 5, BazookaDamage = 2;

        public Bazooka(Fighter actor) : base(actor, 115, 1)
        {
            DisplayRange = MaxRange;
            Damage = BazookaDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new ProjectileTargeter(
                actor,
                MaxRange + RangeUpgrade,
                true,
                DirectionsType.OrthoOnly,
                true,
                false,
                false);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override bool IsProjectileAttack()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            var src = actor.coords.ToPoint();
            var dst = target.coords.ToPoint();
            camera.Knockback(dst - src);

            actor.virtualActor.PlayMuzzleFlash();
            
            var projectile = new Projectile(
                game.contextProvider.ProvideProjectileVirtualActor(ProjectileType.Bazooka, src),
                actor,
                target.coords,
                MaxRange + RangeUpgrade,
                true,
                Impact);
            
            soundCtx.PlayOneShot(Sound.RifleGunshot);
            yield return projectile.Fly(dst);
        }

        public IEnumerator Impact(Coords impactCoords)
        {
            yield return DoExplosion(game, actor, impactCoords, GetDamage());
        }

        public static IEnumerator DoExplosion(Game game, Fighter triggerer, Coords impactCoords, int _damage)
        {
            game.soundCtx.PlayOneShot(Sound.BazookaExplosion);
            game.camera.Shake(.5f, CameraConstants.DefaultShakeIntensity);

            var damageCenter = impactCoords.ToPoint();
            game.animCtx.PlayVFX(VFXType.BazookaExplosion, damageCenter);

            var damageables = game.AllDamageables();
            // put them into this first before we deal damage to remove duplicates, so we don't damage the nexus 2x
            var damageablesHit = new HashSet<IDamageable>();
            var coordsHit = new List<Coords>(Coords.AllDirections) {Coords.zero};

            bool damagedEnemy = false;

            WaitGroup wg = new WaitGroup(game.runCtx);
            foreach (var direction in coordsHit)
            {
                var coords = impactCoords + direction;
                if (damageables.TryGetValue(coords, out var d))
                {
                    if (d.GetOwner().IsEnemyOf(triggerer.Owner))
                        damagedEnemy = true;
                    
                    damageablesHit.Add(d);
                }

                if (game.TryGetHazard(coords, out var h))
                    wg.Start(h.OnDamage(triggerer));
                
                wg.Start(game.map.DamageCover(coords, 2, damageCenter, triggerer));

                game.map.DamagePlacedTeleporter(coords);
            }
            
            if (!damagedEnemy && game.contextProvider.IsPreviewing())
                // Register negative score for not hitting any enemies (so AI doesn't just use it willy-nilly on cover etc.)
                game.contextProvider.RegisterPreview(new MinusScorePreview(game, "explosion hit no enemy", -10f));

            foreach (var d in damageablesHit)
                wg.Start(d.DealDamage(_damage, triggerer));

            yield return wg.Wait();

            // wait a bit longer -- mainly for replays, to wait for the explosion animation
            yield return game.runCtx.WaitSeconds(.65f);
        }
    }
}
