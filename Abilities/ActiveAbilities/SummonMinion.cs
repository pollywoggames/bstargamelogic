﻿using System.Collections;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SummonMinion : AbstractAbility
    {
        public SummonMinion(Fighter actor) : base(actor, 202, 1)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new EmptySpotTargeter(actor, 1, 1);
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override IEnumerator Execute(Target target)
        {
            if (actor.CurrHealth > 1)
            {
                yield return actor.DealSelfDamage(1);
            }

            // check if dead
            if (actor.CurrHealth <= 0)
                yield break;
            
            soundCtx.PlayOneShot(Sound.SummonMinion);
            
            actor.virtualActor.ForwardInSortingOrder();
            yield return actor.virtualActor.AnimateSpawnMinionWindup(target.coords);

            // Summon & then throw it
            var direction = target.coords - actor.coords;
            yield return runCtx.Run(actor.virtualActor.AnimateThrowWindup(direction, 16));

            var minion = actor.Owner.AddFighter(FighterPrototype.RobotMinion,
                contextProvider.ProvideFighterVirtualActor(actor.coords.ToPoint()));
            minion.SetWasSummoned(true); // so it doesn't respawn

            game.SomethingHappened(new BSEvent(BSEventType.SummonMinions, actor));

            if (actor.Owner.Fighters.Count(f => f.Prototype == FighterPrototype.RobotMinion) >=
                Achievements.ActiveDaemonMinionAchievement)
            {
                game.SomethingHappened(new BSEvent(BSEventType.ExceedActiveDaemonMinionThreshold, actor));
            }

            // make sure it's behind the summoner
            minion.virtualActor.BackwardInSortingOrder();

            runCtx.Start(actor.virtualActor.AnimateThrowFollowthrough(direction));

            yield return animCtx.MoveParabolic(
                minion,
                target.coords.ToPoint() + Fighter.OffsetOnGrid,
                12,
                .5f);

            animCtx.PlayVFX(VFXType.SummonMinionLandEffect, minion.pos);

            // put back minion original sorting order
            minion.virtualActor.ForwardInSortingOrder();

            actor.virtualActor.BackwardInSortingOrder();

            yield return game.OnFighterMoved(minion);
        }
    }
}
