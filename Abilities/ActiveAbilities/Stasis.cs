using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Stasis : AbstractAbility
    {
        public const int StasisRange = 3;
        public const int StasisDamageAmount = 2;
        
        public Stasis(Fighter actor) : base(actor, 232, 1)
        {
            DisplayRange = StasisRange;
            Damage = StasisDamageAmount;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new StasisTargeter(actor, 1, StasisRange + RangeUpgrade, false);
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);
            
            soundCtx.PlayOneShot(Sound.ElectricSparks);

            var singleDelta = (target.coords - actor.coords).normalized;
            var perpendicular = Coords.Perpendicular(singleDelta);

            List<Coords> effectCoords = new List<Coords>()
                {
                    target.coords,
                    target.coords - perpendicular,
                    target.coords + perpendicular
                }
                .Where(c =>
                    game.TryGetFighter(c, out var targetFighter)
                    && targetFighter.IsEnemyOf(actor))
                .ToList();

            // Play the purple lightning effect
            foreach (var coords in effectCoords)
            {
                PlayLightningVFX(coords);
            }
            
            yield return runCtx.WaitSeconds(.3f);

            var waitGroup = new WaitGroup(runCtx);
            
            foreach (var coords in effectCoords)
            {
                waitGroup.Start(DamageAndMaybeStun(coords, coords == target.coords));
            }
            
            actor.virtualActor.SetRotation(0);

            yield return runCtx.WaitSeconds(.65f);
            yield return waitGroup.Wait();
        }

        private void PlayLightningVFX(Coords targetCoords)
        {
            animCtx.PlayElectricArcEffect(actor.pos, targetCoords.ToPoint(), VFXColor.Purple, .5f);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, targetCoords.ToPoint(), VFXColor.Purple);
        }

        private IEnumerator DamageAndMaybeStun(Coords targetCoords, bool stun)
        {
            if (game.TryGetFighter(targetCoords, out var targetFighter) && targetFighter.IsEnemyOf(actor))
            {
                soundCtx.PlayOneShot(Sound.Stasis);

                var coordsBefore = targetFighter.coords;
                yield return targetFighter.DealDamage(GetDamage(), actor, stun);

                if (stun && targetFighter.CurrHealth > 0)
                {
                    targetFighter.AddStatusEffect(StatusEffect.Stunned, 1, actor);
                }

                if (targetFighter.CurrHealth > 0)
                {
                    // solve issue with knockback (suppressAnimation on DealDamage not working correctly I guess?)
                    yield return runCtx.WaitSeconds(.3f);
                    targetFighter.coords = coordsBefore;
                }
            }
        }
    }
}