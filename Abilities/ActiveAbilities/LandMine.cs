using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class LandMine : ThrowSomething
    {
        public const int LandMineRange = 3;
        public const int LandMineDamage = 2;
        
        public LandMine(Fighter actor) : base(actor, 222, 1, DirectionsType.DiagonalOnly, MovablePropType.ThrownLandMine)
        {
            DisplayRange = LandMineRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            var ret = new OrthogonalDirectionsTargeter(actor, 1, LandMineRange + RangeUpgrade)
            {
                directionsType = DirectionsType.DiagonalOnly
            };
            return ret;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator OnImpact(Target target)
        {
            soundCtx.PlayOneShot(Sound.TechBeep);
            
            yield return Hazard.PlaceHazardAt(actor, HazardType.LandMine, target.coords);

            if (game.TryGetHazard(target.coords, out var landMineHazard))
            {
                landMineHazard.damage = LandMineDamage + DamageUpgrade;
            }
            else
            {
                Logger.Warn("Trying to edit land mine hazard damage but can't find just placed land mine hazard");
            }
        }
    }
}