using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Burrow : AbstractAbility
    {
        public const int BurrowRange = 4;

        public Burrow(Fighter actor) : base(actor, 237, 1)
        {
            DisplayRange = BurrowRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new CreateTeleporterTargeter(actor, 1, BurrowRange + RangeUpgrade)
            {
                CantTargetAnythingWhenStandingOnTP = true
            };
        }

        public override int GetMovementAfter()
        {
            return 1;
        }

        public override IEnumerator Execute(Target target)
        {
            // If standing on a teleporter, destroy it
            if (game.map._teleportersByCoords.TryGetValue(actor.coords, out var standingTP))
            {
                if (standingTP.type == TeleporterType.Fixed)
                {
                    Logger.Error("Tried executing Burrow ability, but was standing on top of Fixed teleporter!");
                    yield break;
                }
                game.map.RemoveTeleporterAndMaybeBurrowPair(standingTP);
                yield return runCtx.WaitSeconds(.45f);
            }
            
            // targeting a teleporter, destroy it
            if (game.map._teleportersByCoords.TryGetValue(target.coords, out var targetTP))
            {
                if (targetTP.type == TeleporterType.Fixed)
                {
                    Logger.Error("Tried executing Burrow ability, but was targeting Fixed teleporter!");
                    yield break;
                }
                game.map.RemoveTeleporterAndMaybeBurrowPair(targetTP);
                yield return runCtx.WaitSeconds(.45f);
            }
            
            // Destroy other burrows for this player & actor.
            game.map.RemoveAllBurrowsForPlayer(actor.Owner, actor.tpChan.GetValueOrDefault(-1));
            
            var (burrow1, burrow2) = game.map.AddBurrows(actor, actor.coords, target.coords);
            // hide both burrows at first
            burrow1.virtualActor.SetAlpha(0);
            burrow2.virtualActor.SetAlpha(0);

            var waitGroup = new WaitGroup(runCtx);
            
            waitGroup.Start(animCtx.AnimateBurrow(actor, target.coords));
            waitGroup.Start(fadeInBurrows(burrow1, burrow2));

            yield return waitGroup.Wait();
            
            // check for hazard damage, etc.
            yield return game.OnFighterMoved(actor, false, new List<int> { burrow1.channel });
        }

        private IEnumerator fadeInBurrows(Teleporter burrow1, Teleporter burrow2)
        {
            yield return fadeInBurrow(burrow1);
            yield return fadeInBurrow(burrow2);
        }

        private IEnumerator fadeInBurrow(Teleporter burrow)
        {
            float elapsedTime = 0;
            while (elapsedTime <= GameMap.BurrowAnimationTime)
            {
                float ratio = elapsedTime / GameMap.BurrowAnimationTime;
                if (ratio > 1f)
                    ratio = 1f;
                
                burrow.virtualActor.SetAlpha(ratio);

                yield return null;
                elapsedTime += runCtx.GetDeltaTime();
            }

            burrow.virtualActor.SetAlpha(1f);
        }
    }
}