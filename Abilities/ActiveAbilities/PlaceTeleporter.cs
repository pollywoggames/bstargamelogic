using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class PlaceTeleporter : AbstractAbility
    {
        public PlaceTeleporter(Fighter actor) : base(actor, 242, 1)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new CreateTeleporterTargeter(actor, 1, 1);
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override IEnumerator Execute(Target target)
        {
            // targeting a teleporter, destroy it
            if (game.map._teleportersByCoords.TryGetValue(target.coords, out var targetTP))
            {
                if (targetTP.type == TeleporterType.Fixed)
                {
                    Logger.Error("Tried executing PlaceTeleporter ability, but was targeting Fixed teleporter!");
                    yield break;
                }
                game.map.RemoveTeleporterAndMaybeBurrowPair(targetTP);
                yield return runCtx.WaitSeconds(.45f);
            }
            
            var newTeleporter = game.map.PlaceTeleporter(actor, target.coords);
            soundCtx.PlayOneShot(Sound.MakeTeleporter);
            yield return newTeleporter.virtualActor.DissolveInOut(true);
        }
    }
}