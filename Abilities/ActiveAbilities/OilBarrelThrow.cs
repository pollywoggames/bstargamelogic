using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class OilBarrelThrow : ThrowSomething
    {
        public const int OilBarrelThrowRange = 4;
        
        public OilBarrelThrow(Fighter actor) : base(actor, 203, 1, DirectionsType.OrthoOnly, MovablePropType.ThrownOilBarrel)
        {
            DisplayRange = OilBarrelThrowRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new OrthogonalDirectionsTargeter(actor, 1, OilBarrelThrowRange + RangeUpgrade);
        }

        public override IEnumerator OnImpact(Target target)
        {
            var directions = new List<Coords>();
            directions.AddRange(Coords.AllDirections);
            directions.Add(Coords.zero);
            var coroutines = new List<IEnumerator>();
            foreach (var d in directions)
            {
                var loc = target.coords + d;
                if (game.map.GetTerrainType(loc) != TerrainType.Clear)
                    continue;

                coroutines.Add(Hazard.PlaceHazardAt(actor, HazardType.Oil, loc));
            }

            yield return runCtx.All(coroutines);
        }
    }
}
