using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class WaterGrenade : ThrowSomething
    {
        public const int WaterThrowRange = 4;
        
        public WaterGrenade(Fighter actor) : base(actor, 245, 1, DirectionsType.OrthoOnly, MovablePropType.ThrownWaterGrenade)
        {
            DisplayRange = WaterThrowRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new OrthogonalDirectionsTargeter(actor, 1, WaterThrowRange + RangeUpgrade);
        }

        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override IEnumerator OnImpact(Target target)
        {
            var directions = new List<Coords>();
            directions.AddRange(Coords.OrthogonalDirections);
            directions.Add(Coords.zero);
            
            if (MiscUpgrade > 0)
                directions.AddRange(Coords.TwoAway);

            var waitGroup = new WaitGroup(runCtx);
            foreach (var d in directions)
            {
                var loc = target.coords + d;
                if (game.map.GetTerrainType(loc) != TerrainType.Clear)
                    continue;

                waitGroup.Start(Hazard.PlaceHazardAt(actor, HazardType.Water, loc));
            }

            yield return waitGroup.Wait();
        }
    }
}