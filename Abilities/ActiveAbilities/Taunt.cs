using System.Collections;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Events;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Taunt : AbstractAbility
    {
        public Taunt(Fighter actor) : base(actor, 244, 1, DirectionsType.OrthoAndDiagonal)
        {
            DisplayRange = 6;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new TauntTargeter(actor, 1, 6);
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override IEnumerator Execute(Target target)
        {
            if (!game.TryGetFighter(target.coords, out var targetFighter))
            {
                Logger.Error("Taunt can't find enemy fighter at target coords: " + target.coords);
                yield break;
            }

            var (targetingState, targetCoords) = targetFighter.GetProjectileAttackTargeting(actor.coords);
            if (targetingState == -1)
            {
                Logger.Error("Enemy fighter being taunted doesn't have a projectile attack that targets actor!");
                yield break;
            }

            if (targetFighter.hasVirtualActor)
                targetFighter.virtualActor.Face(actor.pos);

            // Dramatic animation
            animCtx.PlayElectricArcEffect(
                actor.pos,
                target.coords.ToPoint(),
                VFXColor.Purple,
                .3f);
            animCtx.PlayVFX(VFXType.TauntedVFX, targetFighter.pos);
            runCtx.Start(animCtx.ShakeObject(targetFighter.virtualActor, 8, .1f));
            yield return runCtx.WaitSeconds(.6f);
            yield return animCtx.AnimateReadyAttack(targetFighter, actor.coords, false);
            
            if (targetFighter.HasReadyAttacks)
                game.DecrementFighterReadyAttack(targetFighter);

            var actorPrevReflectProj = actor.ReflectProjectiles;
            
            AbstractEvent atkEvent;
            if (targetingState == 0)
            {
                // weapon
                atkEvent = new AttackEvent(game, targetFighter, new Target(targetCoords));
            }
            else
            {
                // ability
                atkEvent = new AbilityEvent(
                    game,
                    targetFighter,
                    targetFighter.Abilities[targetingState - 1],
                    new Target(targetCoords))
                {
                    // Don't put ability on cooldown -- makes it slightly less harsh against Rocketeer
                    putAbilityOnCooldown = false
                };
            }
            
            yield return runCtx.Run(atkEvent.Process());

            // if actor had ReflectProjectiles before the projectile, give it back
            if (actorPrevReflectProj)
            {
                actor.SetReflectProjectiles(true);
                
                // also don't put reflectProjectiles ability on cooldown
                var reflectProjAbility = actor.GetAbility(BSAbility.ReflectProjectiles);
                if (reflectProjAbility.OnCooldown)
                    reflectProjAbility.ResetCooldown();
            }
        }
    }
}