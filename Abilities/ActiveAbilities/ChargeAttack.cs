﻿using System.Collections;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class ChargeAttack : AbstractAbility
    {
        public const int ChargeRange = 4;
        public const int ChargeDamage = 3;

        public ChargeAttack(Fighter actor) : base(actor, 142, 1)
        {
            DisplayRange = ChargeRange;
            Damage = ChargeDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new ChargeTargeter(actor, ChargeRange + RangeUpgrade);
        }

        public override bool DisableMovementPreviewScoresForActor()
        {
            return true;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }
        
        public override IEnumerator Execute(Target target)
        {
            yield return StaticExecute(actor, target, GetDamage(), true, false, false, Sound.Charge, Sound.Machete);
        }

        public static IEnumerator StaticExecute(Fighter actor, Target target, int damage, bool getShield, bool takeEnemyPlaceOnKill, bool destroyPlacedTPs, Sound chargeSound, Sound impactSound)
        {
            var game = actor.game;
            var runCtx = game.runCtx;
            var animCtx = game.animCtx;
            var soundCtx = game.soundCtx;
            
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();

            var actorCoordsBefore = actor.coords; 
            var current = actor.coords;
            var delta = target.coords - current;
            var normalized = delta.normalized;
            var dst = target.coords.ToPoint();
            var origDelta = dst - current.ToPoint();
            var posBefore = actor.pos;

            // Rear up backwards: tilt + move backwards slightly
            var backwardRotationAngle = actor.GetBackwardsRotationAngle();
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return animCtx.MoveSmoothly(actor, actor.pos - origDelta.normalized * .4f, .13f);

            if (getShield)
            {
                // (... and get a shield)
                actor.SetMaxShield(2);
                actor.SetCurrShield(2, ShouldCountShieldPreview(actor, target, game));
                // expire shield next turn
                actor.AddStatusEffect(StatusEffect.LoseShieldWhenCounterExpires, 1, actor);
            }

            // Now tilt forwards & charge
            actor.virtualActor.EnableMovingParticles(delta);
            actor.virtualActor.SetRotation(-backwardRotationAngle);
            var attackables = game.AttackablesFor(actor.Owner);
            IDamageable hitEnemy = null;
            bool hitCover = false;
            bool destroyedCover = false;
            Teleporter hitPlacedTP = null;
            soundCtx.PlayOneShot(chargeSound);
            for (var i = 0; i < delta.magnitude; i++)
            {
                current += normalized;
                
                bool isLastSquare = i >= delta.magnitude;
                if (!isLastSquare && game.map.GetTerrainType(current) == TerrainType.Pit)
                    game.SomethingHappened(new BSEvent(BSEventType.CrossGap, actor.Owner));
                
                if (attackables.TryGetValue(current, out hitEnemy))
                    break;

                var terrainType = game.map.GetTerrainType(current);
                if (terrainType == TerrainType.Cover)
                {
                    hitCover = true;
                    break;
                }

                if (destroyPlacedTPs &&
                    game.map._teleportersByCoords.TryGetValue(current, out var tp) && tp.type == TeleporterType.Placed)
                {
                    hitPlacedTP = tp;
                    break;
                }

                if (terrainType == TerrainType.Wall)
                    break;

                dst = current.ToPoint() + Fighter.OffsetOnGrid;
                yield return animCtx.MoveSmoothly(actor, dst, .14f);

                posBefore = actor.pos;
            }

            actor.virtualActor.DisableMovingParticles();

            WaitGroup consequences = new WaitGroup(runCtx);

            var finalDst = posBefore;
            if (hitEnemy != null || hitCover || hitPlacedTP != null)
            {
                // Move halfway into the last square (the one getting attacked)
                dst = current.ToPoint() + Fighter.OffsetOnGrid;
                var lastDelta = dst - actor.pos;
                var halfway = actor.pos + .4f * lastDelta;
                yield return animCtx.MoveSmoothly(actor, halfway, .07f);

                soundCtx.PlayOneShot(impactSound);
                if (hitEnemy != null)
                    consequences.Start(hitEnemy.DealDamage(damage, actor));
                else if (hitCover)
                {
                    destroyedCover = game.map.IsCoverDamaged(current);
                    consequences.Start(game.map.DamageCover(current, 1, actor.pos, actor));
                }
                else if (hitPlacedTP != null)
                {
                    game.map.RemoveTeleporterAndMaybeBurrowPair(hitPlacedTP);
                }

                if (takeEnemyPlaceOnKill && (hitPlacedTP != null || destroyedCover || hitEnemy is Fighter { IsDead: true }))
                {
                    finalDst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
                    
                    // this is to fix weirdness with the server-side interaction of DashStrike
                    // killing someone with ExplodeIntoAcidOnDeath.
                    actor.OverrideCoords = finalDst.ToCoords();
                    game.RefreshObjects();
                }

                // wait for hitstop animation to finish
                yield return runCtx.WaitSeconds(.25f);
            }
            
            // move back to original position/rotation/sorting order
            yield return animCtx.MoveSmoothly(actor, finalDst, .1f);
            actor.virtualActor.SetRotation(0);
            actor.virtualActor.BackwardInSortingOrder();
            actor.OverrideCoords = null;

            yield return consequences.Wait();
            
            if (actor.coords != actorCoordsBefore)
                yield return game.OnFighterMoved(actor);
        }

        // Some extra calculation to see if we're *probably* going to hit an enemy, before we've actually hit an enemy.
        // If we're not targeting an enemy, we don't want to count the shield preview score. Otherwise the AI will
        // just use the Charge ability willy-nilly without hitting anything.
        private static bool ShouldCountShieldPreview(Fighter actor, Target target, Game game)
        {
            bool shouldCountPreview = false;
            if (game.TryGetFighter(target.coords, out var targetedFighter) && targetedFighter.IsEnemyOf(actor))
                shouldCountPreview = true;
            else if (game.EnemiesOf(actor.Owner).Any(p => p.nexus != null && p.nexus.AllCoords.Contains(target.coords)))
                shouldCountPreview = true;
            return shouldCountPreview;
        }
    }
}
