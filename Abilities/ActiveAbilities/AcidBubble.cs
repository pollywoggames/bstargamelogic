using System.Collections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class AcidBubble : DeployCover
    {
        public AcidBubble(Fighter actor) : base(actor)
        {
            spriteIdx = 226;
            Cooldown = 1;
            _movablePropType = MovablePropType.ThrownAcidBubble;
        }

        protected override IEnumerator OnImpact(Coords coords)
        {
            if (game.map._teleportersByCoords.TryGetValue(coords, out var tp) && tp.type != TeleporterType.Fixed)
                game.map.RemoveTeleporterAndMaybeBurrowPair(tp);
            
            yield return Hazard.PlaceHazardAt(actor, HazardType.AcidBubble, coords);
        }
    }
}