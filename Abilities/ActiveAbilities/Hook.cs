﻿using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Hook : AbstractAbility
    {
        public const int MaxRange = 4;
        
        public Hook(Fighter actor) : base(actor, 208, 1)
        {
            DisplayRange = MaxRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new HookTargeter(actor, 1, MaxRange + RangeUpgrade);
        }

        public override IEnumerator Execute(Target target)
        {
            var gridDelta = (target.coords - actor.coords).normalized;

            // set position half a square in the direction of throw
            var startingPosition = actor.pos + .5f * Point.up;
            var hookObj = contextProvider.ProvideProp(MovablePropType.Hook, startingPosition);

            soundCtx.PlayOneShot(Sound.Throw);
            yield return actor.virtualActor.AnimateHookThrow(actor, hookObj, target.coords);
            yield return runCtx.WaitSeconds(.45f);

            actor.TipBackwards();
            
            soundCtx.PlayOneShot(Sound.Woosh);

            if (!game.TryGetFighter(target.coords, out var hitFighter))
            {
                // we've hit a wall, not a fighter. Zoom forwards 'til we get to the square before the wall
                actor.virtualActor.EnableMovingParticles(gridDelta);
                for (var i = 0; i < DisplayRange; i++)
                {
                    var moveToSquare = actor.coords + gridDelta;
                    if (game.CollideAt(moveToSquare))
                        break;

                    var dst = moveToSquare.ToPoint() + Fighter.OffsetOnGrid;
                    yield return animCtx.MoveSmoothly(actor, dst, .17f);
                }

                actor.virtualActor.DisableMovingParticles();

                actor.ResetRotation();
                hookObj.Destroy();

                yield return game.OnFighterMoved(actor);
            }
            else
            {
                // we've hit a fighter. Drag the fighter back to us

                actor.TipBackwards(); // like reeling them in

                hitFighter.virtualActor.EnableMovingParticles(-gridDelta);

                // tip in direction of pull
                if (hitFighter.virtualActor.IsFacingLeft() && gridDelta.x < 0
                    || !hitFighter.virtualActor.IsFacingLeft() && gridDelta.x > 0)
                    hitFighter.TipForwards();
                else
                    hitFighter.TipBackwards();

                Coords lastCrossedCoords = hitFighter.coords;
                for (var i = 0; i < DisplayRange; i++)
                {
                    var moveToSquare = hitFighter.coords - gridDelta;
                    if (game.CollideAt(moveToSquare))
                        break;

                    if (game.map.GetTerrainType(lastCrossedCoords) == TerrainType.Pit)
                        game.SomethingHappened(new BSEvent(BSEventType.CrossGap, actor.Owner));
                    lastCrossedCoords = hitFighter.coords;
                    
                    var dst = moveToSquare.ToPoint() + Fighter.OffsetOnGrid;
                    runCtx.Start(animCtx.MoveSmoothly(hookObj, dst, .17f));
                    yield return animCtx.MoveSmoothly(hitFighter, dst, .17f);
                }

                hitFighter.virtualActor.DisableMovingParticles();
                hitFighter.ResetRotation();
                
                actor.ResetRotation();
                hookObj.Destroy();
                
                if (game.map._teleportersByCoords.TryGetValue(hitFighter.coords, out var tp) && tp.type != TeleporterType.Burrow)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.PushOrPullFighterIntoTeleporter, actor.Owner));
                }

                yield return game.OnFighterPushed(hitFighter, actor);
            }
        }
    }
}
