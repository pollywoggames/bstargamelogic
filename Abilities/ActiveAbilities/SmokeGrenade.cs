using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SmokeGrenade : HazardGrenade
    {
        public const int SmokeGrenadeRange = 3;
        
        public SmokeGrenade(Fighter actor) : base(actor, 221, 1, DirectionsType.DiagonalOnly, MovablePropType.ThrownSmokeGrenade)
        {
            DisplayRange = SmokeGrenadeRange;

            hazardType = HazardType.SmokeScreen;
        }
    }
}