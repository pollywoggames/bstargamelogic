using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class ElectrolysisGrenade : ThrowSomething
    {
        public const int ElectrolysisGrenadeRange = 3;
        
        public ElectrolysisGrenade(Fighter actor) : base(actor, 211, 1, DirectionsType.DiagonalOnly, MovablePropType.ThrownElectricGrenade)
        {
            DisplayRange = ElectrolysisGrenadeRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            var ret = new OrthogonalDirectionsTargeter(actor, 1, ElectrolysisGrenadeRange + RangeUpgrade)
            {
                directionsType = DirectionsType.DiagonalOnly
            };
            return ret;
        }

        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override IEnumerator OnImpact(Target target)
        {
            if (MiscUpgrade > 0)
            {
                // cross AOE
                var waitGroup = new WaitGroup(runCtx);
                
                // middle spot
                waitGroup.Start(Hazard.PlaceHazardAt(actor, HazardType.Water, target.coords));
                foreach (var dir in Coords.OrthogonalDirections)
                {
                    waitGroup.Start(Hazard.PlaceHazardAt(actor, HazardType.Water, target.coords + dir));
                }

                yield return waitGroup.Wait();
            }
            else
            {
                // just single spot on target
                yield return Hazard.PlaceHazardAt(actor, HazardType.Water, target.coords);
            }

            yield return Hazard.Shock(actor, target.coords);
        }
    }
}