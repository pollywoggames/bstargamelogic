using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class DisplacementCharge : ThrowSomething
    {
        public const int DisplacementChargeRange = 3;

        public DisplacementCharge(Fighter actor) : base(actor, 59, 1, DirectionsType.DiagonalOnly, MovablePropType.DisplacementCharge)
        {
            DisplayRange = DisplacementChargeRange;
        }

        public override AbilityTargeter GetTargeter()
        {
            var ret = new ClearSpotTargeter(actor, 1, DisplacementChargeRange + RangeUpgrade)
            {
                directionsType = DirectionsType.DiagonalOnly
            };
            return ret;
        }

        public override IEnumerator OnImpact(Target target)
        {
            soundCtx.PlayOneShot(Sound.SmallBlast);
            
            var props = new List<IMovable>();
            var wg = new WaitGroup(runCtx);
            
            // Fire vfx in the middle
            props.Add(contextProvider.ProvideProp(MovablePropType.DisplacementChargeFire, target.coords.ToPoint()));
            
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var dst = target.coords + dir;
                var terrain = game.map.GetTerrainType(dst, false);
                if (terrain == TerrainType.Wall)
                    continue;

                var prop = contextProvider.ProvideProp(MovablePropType.DisplacementChargeSmoke, dst.ToPoint());
                props.Add(prop);
                if (dir.x < 0)
                    prop.SetRotation(70);
                if (dir.x > 0)
                    prop.SetRotation(-70);

                // can push fighters only
                if (game.TryGetFighter(dst, out var f))
                {
                    wg.Start(f.GetPushed(dir, actor));
                }
                
                // // Old version for pushing hazards & cover (too OP)
                // else if (game.TryGetHazard(dst, out var h) && h.moveable)
                // {
                //     wg.Start(Hazard.ShoveHazard(game, h, dst, dst + dir, actor));
                // }
                // else if (terrain == TerrainType.Cover)
                // {
                //     wg.Start(game.map.ShoveCover(dst, dst + dir, actor));
                // }
            }

            yield return wg.Wait();
            
            // Make sure fx have stopped playing before stopping
            yield return runCtx.WaitSeconds(.25f);
            props.ForEach(p => p.Destroy());
        }
    }
}