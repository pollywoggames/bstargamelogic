using System;
using System.Collections;
using System.Numerics;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class ShieldBot : AbstractAbility
    {
        public const int ShieldBotRange = 3;
        public const int ShieldAmount = 2;

        public ShieldBot(Fighter actor) : base(actor, 217, 1, DirectionsType.OrthoAndDiagonal)
        {
            DisplayRange = ShieldBotRange;
            Damage = ShieldAmount;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new MedKitTargeter(actor, 1, ShieldBotRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            var mpt = actor.Owner.teamColor.ToShieldBotColor();
            var shieldBot = contextProvider.ProvideProp(mpt, actor.pos);

            var dst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
            var distance = Point.Distance(dst, actor.pos);
            var time = Math.Max(.4f, distance / 2.5f);

            bool startedFacingLeft = (dst - actor.pos).x < 0;
            if (!startedFacingLeft)
                shieldBot.SetScale(new Vector3(-1f, 1f, 1f));
            
            yield return animCtx.MoveSmoothly(shieldBot, dst, time);

            if (game.TryGetFighter(target.coords, out var teammate))
            {
                var shieldAmount = GetDamage();
                if (teammate.MaxShield < shieldAmount)
                    teammate.SetMaxShield(shieldAmount);
                teammate.SetCurrShield(shieldAmount);
                
                soundCtx.PlayOneShot(Sound.ShieldUp);

                runCtx.Start(PassiveAbilityEffect(VFXType.Shield, null, teammate.GetPosition()));
            }
            yield return runCtx.WaitSeconds(.65f);
            
            shieldBot.SetScale(new Vector3(startedFacingLeft ? -1f : 1f, 1f, 1f));
            yield return animCtx.MoveSmoothly(shieldBot, actor.pos, time);
            
            shieldBot.Destroy();
        }
    }
}