using System.Collections;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Transfusion : UntargetedAbility
    {
        public Transfusion(Fighter actor) : base(actor, 219, 1)
        { }

        public override IEnumerator Execute(Target target)
        {
            bool anyoneHealed = false;
            foreach (var dir in Coords.AllDirections)
            {
                if (game.TryGetFighter(actor.coords + dir, out var teammate) && actor.IsFriendlyTo(teammate))
                {
                    soundCtx.PlayOneShot(Sound.Healing);
                    teammate.SetHealth(teammate.MaxHealth);
                    runCtx.Start(PassiveAbilityEffect(VFXType.Regeneration, null, teammate.GetPosition()));
                    anyoneHealed = true;
                }
            }
            
            if (anyoneHealed) yield return runCtx.WaitSeconds(.3f);
        }
    }
}