using System.Collections;
using System.Linq;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class BodySlam : AbstractAbility
    {
        public const int MaxRange = 4;
        public const int BodySlamDamage = 2;
        
        public BodySlam(Fighter actor) : base(actor, 162, 1)
        {
            DisplayRange = MaxRange;
            Damage = BodySlamDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new BodySlamTargeter(actor, MaxRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override bool DisableMovementPreviewScoresForActor()
        {
            return true;
        }
        
        public override IEnumerator Execute(Target target)
        {
            var actorStartingCoords = actor.coords;
            
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();

            var actorCoordsBefore = actor.coords;
            var current = actor.coords;
            var delta = target.coords - current;
            var normalized = delta.normalized;
            var dst = target.coords.ToPoint();
            var origDelta = dst - current.ToPoint();
            var posBefore = actor.pos;

            // Rear up backwards: tilt + move backwards slightly
            var backwardRotationAngle = actor.GetBackwardsRotationAngle();
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return animCtx.MoveSmoothly(actor, actor.pos - origDelta.normalized * .4f, .13f);
            
            // Now tilt forwards & charge
            actor.virtualActor.EnableMovingParticles(delta);
            actor.virtualActor.SetRotation(-backwardRotationAngle);
            Fighter hitEnemy = null;
            bool hitCover = false;
            soundCtx.PlayOneShot(Sound.MonsterCharge);
            
            for (var i = 0; i < delta.magnitude; i++)
            {
                current += normalized;
                
                // Check for crossing gap
                bool isLastSquare = i >= delta.magnitude;
                if (!isLastSquare && game.map.GetTerrainType(current) == TerrainType.Pit)
                    game.SomethingHappened(new BSEvent(BSEventType.CrossGap, actor.Owner));
                
                hitEnemy = EnemyFighterThere(current);
                var terrainType = game.map.GetTerrainType(current);
                hitCover = terrainType == TerrainType.Cover;
                
                if (terrainType == TerrainType.Cover || terrainType == TerrainType.Wall || hitEnemy != null)
                    break;

                dst = current.ToPoint() + Fighter.OffsetOnGrid;
                yield return animCtx.MoveSmoothly(actor, dst, .14f);

                posBefore = actor.pos;
            }

            actor.virtualActor.DisableMovingParticles();

            var wg = new WaitGroup(runCtx);
            if (hitCover || hitEnemy != null)
            {
                // Move halfway into the last square (the one getting attacked)
                dst = current.ToPoint() + Fighter.OffsetOnGrid;
                var lastDelta = dst - actor.pos;
                var halfway = actor.pos + .4f * lastDelta;
                yield return animCtx.MoveSmoothly(actor, halfway, .07f);

                soundCtx.PlayOneShot(Sound.BodySlamHit);

                if (hitEnemy != null)
                {
                    wg.Start(hitEnemy.DealDamage(GetDamage(), actor, true));

                    // Push them back a square
                    var punchDelta = (target.coords - actorStartingCoords).normalized;
                    wg.Start(hitEnemy.GetPushed(punchDelta, actor));
                }
                else
                {
                    // was the cover we hit actually a nexus?
                    bool hitNexus = false;
                    foreach (var enemyNexus in enemyNexi)
                    {
                        if (enemyNexus.AllCoords.Contains(current))
                        {
                            wg.Start(enemyNexus.DealDamage(1, actor));
                            hitNexus = true;
                            break;
                        }
                    }
                    
                    if (!hitNexus)
                        wg.Start(HitCover(target.coords, actorStartingCoords, current.ToPoint()));
                }

                // if we hit something, we deal damage to ourselves
                bool shake = actor.CurrHealth > 1;
                wg.Start(actor.DealDamage(1, hitEnemy, false));
                if (shake && actor.virtualActor != null && !actor.virtualActor.IsDestroyed())
                    wg.Start(animCtx.ShakeObject(actor.virtualActor, 4, .1f));

                // check if we died by taking damage
                if (actor.CurrHealth <= 0)
                {
                    yield return wg.Wait();
                    yield break;
                }

                yield return runCtx.WaitSeconds(.3f);
                // back to previous square
                yield return animCtx.MoveSmoothly(actor, posBefore, .1f);
                actor.virtualActor.SetRotation(0);
                
                yield return wg.Wait();
            }

            // move back to original rotation
            actor.virtualActor.SetRotation(0);
            actor.virtualActor.BackwardInSortingOrder();

            if (actor.coords != actorCoordsBefore)
                yield return game.OnFighterMoved(actor);
        }

        private IEnumerator HitCover(Coords coords, Coords actorStartingCoords, Point originPoint)
        {
            yield return game.map.DamageCover(coords, 1, originPoint, actor);
            
            // make sure it hasn't been destroyed
            if (game.map.TerrainAt(coords).terrainType == TerrainType.Cover)
            {
                var pushDelta = (coords - actorStartingCoords).normalized;
                yield return game.map.ShoveCover(coords, coords + pushDelta, actor);
            }
        }

        private Fighter EnemyFighterThere(Coords coords)
        {
            if (game.TryGetFighter(coords, out var f))
                if (f.IsEnemyOf(actor))
                    return f;
            
            return null;
        }
    }
}