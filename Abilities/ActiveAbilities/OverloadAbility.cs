using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class OverloadAbility : UntargetedAbility
    {
        public static List<Coords> effectDirections = new List<Coords>
        {
            new Coords(0, 0),
            new Coords(0, 1),
            new Coords(0, 2),
            new Coords(0, -1),
            new Coords(0, -2),
            new Coords(1, 0),
            new Coords(2, 0),
            new Coords(1, 1),
            new Coords(1, -1),
            new Coords(-1, 0),
            new Coords(-2, 0),
            new Coords(-1, 1),
            new Coords(-1, -1),
        };
        
        public OverloadAbility(Fighter actor) : base(actor, 231, 1)
        {
            DisplayRange = 2;
        }
        
        public override int GetDamage()
        {
            return Hazard.ElectricityDamage;
        }

        public override string GetCriticalInfo()
        {
            return $"Cooldown: {Cooldown}\nRange: 2";
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            var muzzlePosition = actor.virtualActor.GetMuzzlePosition();
            soundCtx.PlayOneShot(Sound.ElectricSparks);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, muzzlePosition);
            runCtx.Start(animCtx.ShakeObject(actor, 15, .08f));
            
            yield return runCtx.WaitSeconds(.3f);
            
            var targets = new List<Coords>();
            foreach (var d in effectDirections)
            {
                if (d == Coords.zero && MiscUpgrade > 0)
                {
                    // Upgraded version: don't shock self
                    continue;
                }
                
                var loc = actor.coords + d;
                targets.Add(loc);
                if (game.TryGetFighter(loc, out var f)) f.ShowHealthBar();
            }
            
            var waitGroup = new WaitGroup(runCtx);
            foreach (var t in targets)
            {
                var terrainType = game.map.GetTerrainType(t);
                if (terrainType == TerrainType.Clear || terrainType == TerrainType.Cover)
                {
                    animCtx.PlayElectricArcEffect(muzzlePosition, t.ToPoint(), VFXColor.Blue, .65f);
                    waitGroup.Start(Hazard.Shock(actor, t));

                    yield return runCtx.WaitSeconds(.1f);
                }
            }

            yield return waitGroup.Wait();
        }
    }
}