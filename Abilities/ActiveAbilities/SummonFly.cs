using System;
using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SummonFly : AbstractAbility
    {
        public SummonFly(Fighter actor) : base(actor, 227, 1)
        { }

        public override AbilityTargeter GetTargeter()
        {
            return new EmptySpotTargeter(actor, 1, 1);
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override IEnumerator Execute(Target target)
        {
            if (actor.CurrHealth > 1)
            {
                yield return actor.DealSelfDamage(1);
            }

            // check if dead
            if (actor.CurrHealth <= 0)
                yield break;
            
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            var muzzlePosition = actor.pos;
            animCtx.PlayVFX(VFXType.ShockBurstEffect, muzzlePosition, VFXColor.Purple);
            runCtx.Start(animCtx.ShakeObject(actor.virtualActor, 2, .06f));
            
            yield return runCtx.WaitSeconds(.3f);
            
            animCtx.PlayElectricArcEffect(muzzlePosition, target.coords.ToPoint(), VFXColor.Purple, .3f);
            soundCtx.PlayOneShot(Sound.ElectricSparks);
            
            yield return runCtx.WaitSeconds(.3f);

            var minion = actor.Owner.AddFighter(FighterPrototype.CritterFly,
                contextProvider.ProvideFighterVirtualActor(actor.coords.ToPoint()));
            minion.SetWasSummoned(true); // so it doesn't respawn
            // we have to summon it elsewhere and then SetPosition to its final location in order to trigger the SummonMinionPreview (needs refactor)
            minion.SetPosition(target.coords.ToPoint());

            animCtx.PlayVFX(VFXType.SummonMinionLandEffect, minion.pos);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, minion.pos, VFXColor.Purple);
            soundCtx.PlayOneShot(Sound.ElectricDischarge);

            yield return game.OnFighterMoved(minion);
            
            yield return runCtx.WaitSeconds(.3f);
            actor.virtualActor.SetRotation(0);
        }
    }
}