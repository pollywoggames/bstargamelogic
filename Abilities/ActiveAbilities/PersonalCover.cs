using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class PersonalCover : ThrowSomething
    {
        public PersonalCover(Fighter actor) : base(actor, 201, 1,
            DirectionsType.OrthoOnly, MovablePropType.ThrownDeployableCover)
        {
            // Make it a smaller animation than normal
            animationNumSteps = 12;
            animationGravity = 1f;
            sound = Sound.Poof;

            DisplayRange = 1;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new EmptySpotTargeter(actor, 1, 1);
        }

        public override IEnumerator OnImpact(Target target)
        {
            animCtx.PlayVFX(VFXType.DeployCoverLandEffect, target.coords.ToPoint());
            yield return game.map.DeployCover(target.coords, actor);
        }
    }
}