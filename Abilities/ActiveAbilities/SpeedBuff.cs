using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SpeedBuff : AbstractAbility
    {
        public const int SpeedBuffRange = 3;
        public const int SpeedBuffAmount = 2;
        
        public SpeedBuff(Fighter actor) : base(actor, 233, 1, DirectionsType.OrthoAndDiagonal)
        {
            DisplayRange = SpeedBuffRange;
            Damage = SpeedBuffAmount;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new MedKitTargeter(actor, 1, SpeedBuffRange + RangeUpgrade);
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            var muzzlePosition = actor.pos + new Point(actor.IsFacingLeft() ? -.2f : .2f, .5f);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, muzzlePosition, VFXColor.Green);
            runCtx.Start(animCtx.ShakeObject(actor.virtualActor, 2, .06f));
            yield return runCtx.WaitSeconds(.3f);
            
            animCtx.PlayVFX(VFXType.SpeedBoostVFX, target.coords.ToPoint() + new Point(0, -.3f));
            soundCtx.PlayOneShot(Sound.SpeedBoost);
            if (game.TryGetFighter(target.coords, out var teammate) && teammate.MovementDelta() > -99f)
            {
                if (!teammate.Owner.IsTurn)
                {
                    // not their turn, have to set a status effect so they get the buff at the beginning of their turn
                    teammate.AddStatusEffect(StatusEffect.SpeedBuffNextTurn, 1, actor);
                }
                else if (teammate.MovementDelta() > -99f)
                {
                    teammate.SetMovesLeft(teammate.MovesLeft + GetDamage(), true);
                }
            }
            
            actor.virtualActor.SetRotation(0);

            // wait a bit for dramatic effect (otherwise it moves on too quick in replay mode)
            yield return runCtx.WaitSeconds(.65f);
        }
    }
}