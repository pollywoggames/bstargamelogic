﻿using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Electrify : AbstractAbility
    {
        public const int ElectrifyRange = 2;
        
        public Electrify(Fighter actor) : base(actor, 205, 1)
        {
            DisplayRange = ElectrifyRange;
        }

        public override int GetDamage()
        {
            return Hazard.ElectricityDamage + DamageUpgrade;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new ElectrifyTargeter(actor, 1, ElectrifyRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            var muzzlePosition = actor.virtualActor.GetMuzzlePosition();
            soundCtx.PlayOneShot(Sound.ElectricSparks);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, muzzlePosition);
            runCtx.Start(animCtx.ShakeObject(actor.virtualActor, 2, .06f));
            
            yield return runCtx.WaitSeconds(.3f);

            animCtx.PlayElectricArcEffect(muzzlePosition, target.coords.ToPoint(), VFXColor.Blue, .65f);
            yield return Hazard.Shock(actor, target.coords, DamageUpgrade);
            
            actor.virtualActor.SetRotation(0);
        }
    }
}
