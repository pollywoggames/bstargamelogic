using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class AcidDash : HazardDash
    {
        public const int AcidDashRange = 5;

        public AcidDash(Fighter actor) : base(actor, 215, 1)
        {
            DisplayRange = AcidDashRange;

            hazardType = HazardType.Acid;
            hazardProjectileType = MovablePropType.AcidProjectile;
            
            dashSound = Sound.DashStrike;
        }

        protected override IEnumerator OnFinishDashing()
        {
            yield return Hazard.PlaceHazardAt(actor, hazardType, actor.coords);
        }
    }
}