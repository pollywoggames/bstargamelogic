﻿using System;
using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SprayWater : AbstractAbility
    {
        // NOTE RangeUpgrade increases both of these
        public const int TargetingRange = 2;
        public const int SprayWaterRange = 5;
        public const int SprayWaterHalfRange = 3;

        public SprayWater(Fighter actor) : base(actor, 204, 0)
        {
            DisplayRange = TargetingRange + SprayWaterRange - 1;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new OrthogonalDirectionsTargeter(actor, 1, TargetingRange + RangeUpgrade);
        }

        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override void ApplyRangeUpgrade(int amount)
        {
            base.ApplyRangeUpgrade(amount);
            DisplayRange = TargetingRange + SprayWaterRange + (RangeUpgrade * 2) - 1;
        }

        public override IEnumerator Execute(Target target)
        {
            var delta = (target.coords - actor.coords).normalized;
            yield return actor.virtualActor.AnimateThrowWindup(delta, 0);
            runCtx.Start(actor.virtualActor.AnimateThrowFollowthrough(delta));

            var waitGroup = new WaitGroup(runCtx);
            
            waitGroup.Start(SprayWaterCoroutine(target.coords, delta, SprayWaterRange + RangeUpgrade));

            if (MiscUpgrade > 0)
            {
                // make it 3 wide with half the range
                var perpendicular = Coords.Perpendicular(delta);
                var otherPerpendicular = -perpendicular;

                int halfRange = SprayWaterHalfRange + RangeUpgrade;
                waitGroup.Start(SprayWaterCoroutine(target.coords + perpendicular, delta, halfRange));
                waitGroup.Start(SprayWaterCoroutine(target.coords + otherPerpendicular, delta, halfRange));
            }

            yield return waitGroup.Wait();
        }

        private IEnumerator SprayWaterCoroutine(Coords start, Coords delta, int range)
        {
            var obj = contextProvider.ProvideProp(MovablePropType.WaterProjectile, actor.pos);
            var currCoords = start;
            var previousCoords = actor.coords;
            var gravity = .85f;
            for (var i = 0; i < range;)
            {
                if (game.map.GetTerrainType(currCoords) != TerrainType.Clear)
                    break;

                var targetPosition = currCoords.ToPoint();

                // If it's the first one and skipping 2 spaces, increase numsteps/gravity for longer animation
                var numSteps = 7 + (int) Math.Round((currCoords - previousCoords).magnitude) * 3;
                yield return animCtx.MoveParabolic(obj, targetPosition, numSteps, gravity);
            
                if (!game.TryGetHazard(currCoords, out var h) || h.type != HazardType.Water)
                {
                    yield return Hazard.PlaceHazardAt(actor, HazardType.Water, currCoords);
                    i++;
                }
                
                gravity *= .8f;
                if (gravity < .1f)
                    gravity = .1f;

                previousCoords = currCoords;
                currCoords += delta;
            }
            
            obj.Destroy();
        }
    }
}
