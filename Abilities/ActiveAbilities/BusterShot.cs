﻿using System;
using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class BusterShot : AbstractAbility
    {
        public const int BusterShotDamage = 1;
        public const int MaxRange = 4;

        private Point src;
        private WaitGroup consequencesWG;
        private Projectile currProjectile;
        
        public BusterShot(Fighter actor) : base(actor, 94, 1)
        {
            DisplayRange = MaxRange;
            Damage = BusterShotDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new BusterShotTargeter(actor, 1, MaxRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            consequencesWG = new WaitGroup(game.runCtx);
            
            src = actor.coords.ToPoint();
            var dst = target.coords.ToPoint();
            var direction = dst - src;

            camera.Knockback(direction);

            actor.virtualActor.PlayMuzzleFlash();

            currProjectile = new Projectile(
                game.contextProvider.ProvideProjectileVirtualActor(ProjectileType.BusterShot, src),
                actor,
                target.coords,
                MaxRange + RangeUpgrade,
                true,
                Impact,
                false);
            
            soundCtx.PlayOneShot(Sound.RifleGunshot);
            yield return currProjectile.Fly(dst);

            // If there were any impacts to wait for
            yield return consequencesWG.Wait();
        }

        public IEnumerator Impact(Coords impactCoords)
        {
            camera.Shake(.15f, CameraConstants.DefaultShakeIntensity);

            var shooter = currProjectile.lastReflectedByFighterCoords.HasValue
                ? game.FighterAt(currProjectile.lastReflectedByFighterCoords.Value)
                : actor;
            
            if (shooter == null)
                shooter = actor;

            bool playSound = false;
            if (game.TryGetFighter(impactCoords, out var f) && f.IsEnemyOf(shooter))
            {
                playSound = true;
                yield return f.DealDamage(GetDamage(), shooter);
            }
            else
            {
                // was the cover we hit actually a nexus?
                bool hitNexus = false;
                foreach (var enemyNexus in enemyNexi)
                {
                    if (enemyNexus.AllCoords.Contains(impactCoords))
                    {
                        consequencesWG.Start(enemyNexus.DealDamage(1, shooter));
                        playSound = true;
                        hitNexus = true;
                        break;
                    }
                }

                if (!hitNexus && game.map.GetTerrainType(impactCoords) == TerrainType.Cover)
                {
                    playSound = true;
                    consequencesWG.Start(game.map.DamageCover(impactCoords, 2, src, shooter));
                }
            }
            
            if (playSound)
                soundCtx.PlayOneShot(Sound.BusterShotHit, game.randy.Next(0.8f, 1.2f));

            // pause for hitstop
            yield return runCtx.WaitSeconds(.2f);
        }
    }
}
