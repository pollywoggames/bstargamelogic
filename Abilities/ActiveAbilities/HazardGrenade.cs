using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public abstract class HazardGrenade : ThrowSomething
    {
        protected HazardType hazardType;
        
        protected HazardGrenade(Fighter actor, int spriteIdx, int cooldown, DirectionsType directionsType, MovablePropType movablePropType)
            : base(actor, spriteIdx, cooldown, directionsType, movablePropType)
        { }

        public override AbilityTargeter GetTargeter()
        {
            var ret = new OrthogonalDirectionsTargeter(actor, 1, GetRange())
            {
                directionsType = this.DirectionsType
            };
            return ret;
        }
        
        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override IEnumerator OnImpact(Target target)
        {
            // place hazard in cross around target
            WaitGroup wg = new WaitGroup(runCtx);
            wg.Start(Hazard.PlaceHazardAt(actor, hazardType, target.coords));

            yield return runCtx.WaitSeconds(.3f);

            foreach (var dir in Coords.OrthogonalDirections)
                wg.Start(Hazard.PlaceHazardAt(actor, hazardType, target.coords + dir));

            if (MiscUpgrade > 0)
            {
                yield return runCtx.WaitSeconds(.3f);
                
                // Upgraded: place hazard 2 away as well
                foreach (var dir in Coords.TwoAway)
                    wg.Start(Hazard.PlaceHazardAt(actor, hazardType, target.coords + dir));
            }

            yield return wg.Wait();
        }
    }
}