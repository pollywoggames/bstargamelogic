using System.Collections;
using BarnardsStar.Abilities;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Weapons
{
    public class ElectricPunch : AbstractAbility
    {
        private const int ElectricPunchDamage = 1;

        public ElectricPunch(Fighter actor) : base(actor, 235, 1)
        {
            DisplayRange = 1;
            Damage = ElectricPunchDamage;
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new MeleeWeaponTargeter(actor, true, false);
        }

        public override bool DisableMovementPreviewsForActor()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            // Bump sorting order up by 1 since they'll be moving into someone else's square
            actor.virtualActor.ForwardInSortingOrder();

            // Make sure the actor stays in their own coords even though they'll be moving into
            // the enemy's square for a sec.
            actor.OverrideCoords = actor.coords;
            
            var positionBefore = actor.pos;
            var dst = target.coords.ToPoint() + Fighter.OffsetOnGrid;
            var origDelta = dst - actor.pos;
            actor.virtualActor.Face(dst);

            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            soundCtx.PlayOneShot(Sound.ElectricSparks, 1.2f);
            
            var leftHandPos = actor.pos + new Point(actor.IsFacingLeft() ? .4f : -.4f, -.4f);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, leftHandPos);
            var rightHandPos = actor.pos + new Point(actor.IsFacingLeft() ? -.4f : .4f, 0f);
            animCtx.PlayVFX(VFXType.ShockBurstEffect, rightHandPos);
            
            runCtx.Start(animCtx.ShakeObject(actor.virtualActor, 2, .06f));
            yield return runCtx.WaitSeconds(.4f);

            // Then they tilt forwards and bump into the other guy, moving a little into their square
            actor.SetRotation(-backwardRotationAngle);
            var delta = origDelta * .8f;
            dst = actor.pos + delta;

            yield return animCtx.MoveSmoothly(actor, dst, .07f);

            soundCtx.PlayOneShot(Sound.ElectricDischarge);

            var atkables = game.AttackablesFor(actor.Owner);
            if (atkables.TryGetValue(target.coords, out var d))
            {
                // Note: don't apply DamageUpgrade here, it's applied on Shock instead (below)
                yield return d.DealDamage(ElectricPunchDamage, actor);
            }
            var waitGroup = new WaitGroup(runCtx);
            waitGroup.Start(Hazard.Shock(actor, target.coords, DamageUpgrade));

            yield return runCtx.WaitSeconds(.25f);
            
            // move back to original position & rotation
            actor.SetRotation(0);
            yield return animCtx.MoveSmoothly(actor, positionBefore, .07f);

            actor.virtualActor.BackwardInSortingOrder();

            yield return waitGroup.Wait();
            
            // Edge case: the actor can self-electrify and then sometimes end up in another square. Put them 
            // back in their original square
            actor.pos = positionBefore;
            // the electrify effect may have gotten reset as well
            if (actor.HasStatusEffect(StatusEffect.Electrified) && actor.virtualActor != null && !actor.virtualActor.IsDestroyed())
            {
                actor.virtualActor.ClearStatusEffect(StatusEffect.Electrified);
                actor.virtualActor.ShowStatusEffect(StatusEffect.Electrified);
            }
        }
    }
}