using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class MedKit : ThrowSomething
    {
        public const int MedKitRange = 2;
        public const int MedKitHealAmount = 5;

        public MedKit(Fighter actor) : base(actor, 97, 1, DirectionsType.OrthoAndDiagonal, MovablePropType.MedKit)
        {
            DisplayRange = MedKitRange;
            Damage = MedKitHealAmount;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new MedKitTargeter(actor, 1, MedKitRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }
        
        public override IEnumerator OnImpact(Target target)
        {
            if (game.TryGetFighter(target.coords, out var teammate))
            {
                // gets clamped to max health so we don't worry about it here
                soundCtx.PlayOneShot(Sound.Healing);
                teammate.SetHealth(teammate.CurrHealth + GetDamage());

                runCtx.Start(PassiveAbilityEffect(VFXType.Regeneration, null, teammate.GetPosition()));

                yield return runCtx.WaitSeconds(.3f);
            }
        }
    }
}