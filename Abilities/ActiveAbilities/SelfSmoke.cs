using System.Collections;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class SelfSmoke : UntargetedAbility
    {
        public SelfSmoke(Fighter actor) : base(actor, 249, 1)
        { }

        public override IEnumerator Execute(Target target)
        {
            soundCtx.PlayOneShot(Sound.Poof);
            yield return Hazard.PlaceHazardAt(actor, HazardType.SmokeScreen, actor.coords);
            yield return runCtx.WaitSeconds(.65f);
        }
    }
}