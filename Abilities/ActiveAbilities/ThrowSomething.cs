using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public abstract class ThrowSomething : AbstractAbility
    {
        protected MovablePropType _propType;
        
        protected int animationNumSteps = 16;
        protected float animationGravity = 1.5f;
        protected Sound sound = Sound.OilBarrelSplash;

        protected ThrowSomething(Fighter actor, int spriteIdx, int cooldown,
            DirectionsType directionsType, MovablePropType movablePropType) : base(actor, spriteIdx,
            cooldown, directionsType)
        {
            _propType = movablePropType;
        }
        
        public override IEnumerator Execute(Target target)
        {
            var direction = target.coords - actor.coords;
            yield return actor.virtualActor.AnimateThrowWindup(direction, 0);
            runCtx.Start(actor.virtualActor.AnimateThrowFollowthrough(direction));

            var thrownObj = contextProvider.ProvideProp(_propType, actor.pos);
            var dst = target.coords.ToPoint();

            soundCtx.PlayOneShot(Sound.Throw);
            yield return animCtx.MoveParabolic(thrownObj, dst, animationNumSteps, animationGravity);
            thrownObj.Destroy();

            soundCtx.PlayOneShot(sound);
            animCtx.PlayVFX(VFXType.DeployCoverLandEffect, dst);

            yield return OnImpact(target);
        }

        public abstract IEnumerator OnImpact(Target target);
    }
}