using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class GlueGun : AbstractAbility
    {
        public int GlueGunDamage = 2;
        public int MaxRange = 4;
        
        public GlueGun(Fighter actor) : base(actor, 240, 1)
        {
            DirectionsType = DirectionsType.DiagonalOnly;
            DisplayRange = MaxRange;
            Damage = GlueGunDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new GlueGunTargeter(actor, MaxRange + RangeUpgrade, DirectionsType.DiagonalOnly);
        }

        public override bool IsProjectileAttack()
        {
            return true;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            var src = actor.coords.ToPoint();
            var dst = target.coords.ToPoint();
            camera.Knockback(dst - src);

            actor.virtualActor.PlayMuzzleFlash();
            
            var projectile = new Projectile(
                game.contextProvider.ProvideProjectileVirtualActor(ProjectileType.Regular, src),
                actor,
                target.coords,
                MaxRange,
                false,
                Impact);
            
            soundCtx.PlayOneShot(Sound.GlueGunShoot);
            yield return projectile.Fly(dst);
            
            // wait a bit for dramatic effect (otherwise it moves on too quick in replay mode)
            yield return runCtx.WaitSeconds(.5f);
        }

        public IEnumerator Impact(Coords impactCoords)
        {
            if (!game.TryGetFighter(impactCoords, out var hitFighter))
            {
                yield break;
            }

            soundCtx.PlayOneShot(Sound.GlueGunHit);
            animCtx.PlayVFX(VFXType.GlueSpot, hitFighter.pos);

            yield return hitFighter.DealDamage(GetDamage(), actor);

            if (hitFighter.CurrHealth > 0)
            {
                // apply status effect to make them slower
                hitFighter.AddStatusEffect(StatusEffect.Slowed, 1, actor, true);
            }
        }
    }
}