using System.Collections;
using System.Linq;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Shockwave : UntargetedAbility
    {
        public Shockwave(Fighter actor) : base(actor, 248, 1)
        {
            Damage = 1;
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.25f);
            
            soundCtx.PlayOneShot(Sound.ElectricityHum);
            soundCtx.PlayOneShot(Sound.NexusDamage);
            
            animCtx.PlayVFX(VFXType.EarthquakeVFX, actor.pos);
            yield return runCtx.WaitSeconds(.2f);

            var waitGroup = new WaitGroup(runCtx);
            var attackables = game.AttackablesFor(actor.Owner);
            var effectDirList = ListUtils.ShuffleList(Coords.AllDirections.ToList(), new Randy());
            for (var i = 0; i < effectDirList.Count; i++)
            {
                var effectCoords = actor.coords + effectDirList[i];

                if (attackables.TryGetValue(effectCoords, out var damageable))
                {
                    waitGroup.Start(damageable.DealDamage(GetDamage(), actor));
                }

                if (game.map.GetTerrainType(effectCoords) == TerrainType.Cover)
                {
                    soundCtx.PlayOneShotVaryPitch(Sound.ElectricDischarge);
                    waitGroup.Start(game.map.DamageCover(effectCoords, 2, actor.pos, actor));

                    yield return runCtx.WaitSeconds(.05f);
                }
                
                if (game.map._teleportersByCoords.TryGetValue(effectCoords, out var tp) && tp.type != TeleporterType.Fixed)
                    game.map.RemoveTeleporterAndMaybeBurrowPair(tp);
            }

            yield return runCtx.WaitSeconds(.65f);
            
            actor.virtualActor.SetRotation(0);
            
            yield return waitGroup.Wait();
        }
    }
}