using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class OilDash : HazardDash
    {
        public const int OilDashRange = 5;
        
        public OilDash(Fighter actor) : base(actor, 257, 1)
        {
            DisplayRange = OilDashRange;

            hazardType = HazardType.Oil;
            hazardProjectileType = MovablePropType.OilProjectile;
            
            dashSound = Sound.BusterShotHit;
        }

        protected override void OnWindUp()
        {
            soundCtx.PlayOneShot(Sound.BusterShotHit, 1.5f);
        }

        protected override IEnumerator OnFinishDashing()
        {
            // get a shield
            actor.SetMaxShield(2);
            actor.SetCurrShield(2);
            // expire shield next turn
            actor.AddStatusEffect(StatusEffect.LoseShieldWhenCounterExpires, 1, actor);
            
            yield break;
        }
    }
}