using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class FireGrenade : HazardGrenade
    {
        public const int FireGrenadeRange = 3;
        
        public FireGrenade(Fighter actor) : base(actor, 256, 1, DirectionsType.DiagonalOnly, MovablePropType.ThrownFireGrenade)
        {
            DisplayRange = FireGrenadeRange;

            hazardType = HazardType.Fire;
        }
    }
}