using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class TeleportGunAbility : AbstractAbility
    {
        public const int TeleportGunDamage = 2;
        public const int MaxRange = 4;
        public const int TeleportDistance = 3;
        
        private Projectile currProjectile;
        
        public TeleportGunAbility(Fighter actor) : base(actor, 238, 1)
        {
            DisplayRange = MaxRange;
            Damage = TeleportGunDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new TeleportGunTargeter(actor, MaxRange + RangeUpgrade);
        }

        public override bool IsProjectileAttack()
        {
            return true;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }
        
        // Use {misc} in description for teleport distance
        public override int GetMisc()
        {
            return TeleportDistance + RangeUpgrade;
        }

        public override int GetBaseMisc()
        {
            return TeleportDistance;
        }

        public override IEnumerator Execute(Target target)
        {
            var src = actor.coords.ToPoint();
            var dst = target.coords.ToPoint();
            camera.Knockback(dst - src);

            actor.virtualActor.PlayMuzzleFlash();

            currProjectile = new Projectile(
                game.contextProvider.ProvideProjectileVirtualActor(ProjectileType.Regular, src),
                actor,
                target.coords,
                MaxRange + RangeUpgrade,
                false,
                Impact)
            {
                canHitFriendlies = true
            };

            soundCtx.PlayOneShot(Sound.GlueGunShoot);
            yield return currProjectile.Fly(dst);
        }

        public IEnumerator Impact(Coords impactCoords)
        {
            soundCtx.PlayOneShot(Sound.TeleportGunHit);
            
            if (!game.TryGetFighter(impactCoords, out var hitFighter))
            {
                animCtx.PlayVFX(VFXType.TeleportGunHitEffect, impactCoords.ToPoint(), actor.Owner.teamColor.ToVFXColor());
                
                if (game.map.GetTerrainType(impactCoords) == TerrainType.Cover)
                    yield return game.map.DamageCoverOrNexus(actor, 1, impactCoords);
                if (game.TryGetHazard(impactCoords, out var h) && h.targetable)
                    yield return h.OnDamage(actor);
                
                yield break;
            }
            
            // found a fighter to teleport
            animCtx.PlayVFX(VFXType.TeleportGunHitEffect, impactCoords.ToPoint(), hitFighter.Owner.teamColor.ToVFXColor());
            
            if (actor.IsEnemyOf(hitFighter))
                yield return hitFighter.DealDamage(GetDamage(), actor, true);
            
            if (hitFighter.IsDead)
            {
                yield break;
            }

            Coords beginningCoords = currProjectile.lastReflectedByFighterCoords ?? actor.coords;
            var delta = (impactCoords - beginningCoords).normalized;

            bool foundDestination = false;
            Coords teleportTargetCoords = impactCoords;
            // Start at the target coords + delta * teleportDistance, then if that spot is blocked or invalid,
            // move space by space back towards the fighter's original spot until a valid teleport destination is found. 
            for (int i = TeleportDistance + RangeUpgrade; i >= 0; i--)
            {
                teleportTargetCoords = impactCoords + (delta * i);

                if (game.map.IsClear(teleportTargetCoords, hitFighter))
                {
                    foundDestination = true;
                    break;
                }
            }

            if (!foundDestination)
                // No valid destination was found, so just teleport this fighter to their same spot
                teleportTargetCoords = hitFighter.coords;
                
            yield return animCtx.AnimateTeleport(hitFighter, teleportTargetCoords, 0);
            yield return game.OnFighterPushed(hitFighter, actor);
        }
    }
}