using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Kinesis : AbstractAbility
    {
        public const int KinesisRange = 4;
        public const int KinesisDamage = 2;
        
        public Kinesis(Fighter actor) : base(actor, 224, 1)
        {
            DisplayRange = KinesisRange;
            Damage = KinesisDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new KinesisTargeter(actor, 2, KinesisRange + RangeUpgrade);
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            var backwardRotationAngle = actor.virtualActor.IsFacingLeft() ? -20f : 20f;
            actor.virtualActor.SetRotation(backwardRotationAngle);
            yield return runCtx.WaitSeconds(.45f);

            var muzzlePosition = actor.virtualActor.GetMuzzlePosition();
            animCtx.PlayVFX(VFXType.ShockBurstEffect, muzzlePosition, VFXColor.Purple);
            soundCtx.PlayOneShot(Sound.ElectricSparks);
            runCtx.Start(animCtx.ShakeObject(actor.virtualActor, 2, .06f));
            
            yield return runCtx.WaitSeconds(.3f);
            
            animCtx.PlayElectricArcEffect(muzzlePosition, target.coords.ToPoint(), VFXColor.Purple, .3f);
            soundCtx.PlayOneShot(Sound.ElectricityHum);
            
            yield return runCtx.WaitSeconds(.3f);
            
            var gridDelta = (target.coords - actor.coords).normalized;

            if (game.TryGetHazard(target.coords, out var h) && h.moveable)
            {
                // we've hit cover. Pull it forward only 1
                var moveToSquare = target.coords - gridDelta;
                animCtx.PlayElectricArcEffect(target.coords.ToPoint(), moveToSquare.ToPoint(), VFXColor.Purple, .3f);
                yield return Hazard.ShoveHazard(game, h, target.coords, moveToSquare, actor);
            }
            else if (game.TryGetFighter(target.coords, out var hitFighter))
            {
                if (hitFighter.IsEnemyOf(actor))
                {
                    bool willFighterBeKilled = (hitFighter.CurrHealth + hitFighter.CurrShield) <= KinesisDamage;
                    yield return hitFighter.DealDamage(GetDamage(), actor, !willFighterBeKilled);

                    if (hitFighter.IsDead)
                    {
                        actor.virtualActor.SetRotation(0);
                        yield break;
                    }
                }
                
                // pull fighter towards us
                
                hitFighter.virtualActor.EnableMovingParticles(-gridDelta);

                // tip in direction of pull
                if (hitFighter.virtualActor.IsFacingLeft() && gridDelta.x < 0
                    || !hitFighter.virtualActor.IsFacingLeft() && gridDelta.x > 0)
                    hitFighter.TipForwards();
                else
                    hitFighter.TipBackwards();

                Coords lastCrossedCoords = hitFighter.coords;
                for (var i = 0; i < DisplayRange; i++)
                {
                    var moveToSquare = hitFighter.coords - gridDelta;
                    if (game.CollideAt(moveToSquare))
                        break;

                    if (game.map.GetTerrainType(lastCrossedCoords) == TerrainType.Pit)
                        game.SomethingHappened(new BSEvent(BSEventType.CrossGap, actor.Owner));
                    lastCrossedCoords = hitFighter.coords;
                    
                    var dst = moveToSquare.ToPoint() + Fighter.OffsetOnGrid;
                    animCtx.PlayElectricArcEffect(hitFighter.pos, moveToSquare.ToPoint(), VFXColor.Purple, .3f);
                    yield return animCtx.MoveSmoothly(hitFighter, dst, .17f);
                }

                hitFighter.virtualActor.DisableMovingParticles();
                hitFighter.ResetRotation();
                
                if (game.map._teleportersByCoords.TryGetValue(hitFighter.coords, out var tp) && tp.type != TeleporterType.Burrow)
                {
                    game.SomethingHappened(new BSEvent(BSEventType.PushOrPullFighterIntoTeleporter, actor.Owner));
                }
                
                yield return game.OnFighterPushed(hitFighter, actor);
            }
            else
            {
                // we've hit cover. Pull it forward only 1
                var moveToSquare = target.coords - gridDelta;
                if (!game.CollideAt(moveToSquare))
                {
                    animCtx.PlayElectricArcEffect(target.coords.ToPoint(), moveToSquare.ToPoint(), VFXColor.Purple, .3f);
                    yield return game.map.ShoveCover(target.coords, moveToSquare, actor);
                }
            }
            
            yield return runCtx.WaitSeconds(.3f);
            
            actor.virtualActor.SetRotation(0);
        }
    }
}