using System.Collections;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.ActiveAbilities
{
    public class Flamethrower : AbstractAbility
    {
        private const int FlamethrowerRange = 4;

        public Flamethrower(Fighter actor) : base(actor, 116, 0)
        {
            DisplayRange = FlamethrowerRange;
        }

        public override int GetDamage()
        {
            return Hazard.FireDamage;
        }

        public override AbilityTargeter GetTargeter()
        {
            return new FlamethrowerTargeter(actor, 1, FlamethrowerRange + RangeUpgrade);
        }

        public override IEnumerator Execute(Target target)
        {
            // Rear up backwards slightly
            actor.virtualActor.Face(target.coords.ToPoint());
            actor.TipBackwards();
            
            yield return runCtx.WaitSeconds(.25f);

            soundCtx.PlayOneShot(Sound.FlamethrowerIgnition);
            actor.virtualActor.PlayVFX(FighterVFXLocation.Muzzle, FighterVFXType.MuzzleFire);
            yield return runCtx.WaitSeconds(.75f);

            // prep this so it's already running after everything else is done
            actor.virtualActor.SetMuzzleSmokeActive(true);

            // shake self slightly
            runCtx.Start(animCtx.ShakeObject(actor, 2, .06f));

            var delta = (target.coords - actor.coords).normalized;
            var waitGroup = new WaitGroup(runCtx);
            for (var i = 1; i <= FlamethrowerRange; i++)
            {
                var coords = actor.coords + delta * i;

                var terrainType = game.map.GetTerrainType(coords);
                if (terrainType == TerrainType.Wall || terrainType == TerrainType.Cover)
                    break;

                if (Hazard.CanPlaceHazardAt(game, HazardType.Fire, coords))
                {
                    waitGroup.Start(Hazard.PlaceHazardAt(actor, HazardType.Fire, coords));
                    runCtx.Start(animCtx.ShakeObject(actor, 2, .06f));
                    yield return runCtx.WaitSeconds(.2f);
                }
            }
            actor.virtualActor.StopVFX(FighterVFXLocation.Muzzle);

            // reset rotation
            actor.virtualActor.SetRotation(0);

            yield return runCtx.WaitSeconds(.2f);
            actor.virtualActor.SetMuzzleSmokeActive(false);
        }
    }
}