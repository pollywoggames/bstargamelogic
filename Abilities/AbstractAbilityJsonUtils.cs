﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using BarnardsStar.Abilities.ActiveAbilities;
using BarnardsStar.Abilities.PassiveAbilities;
using BarnardsStar.Abilities.UntargetedAbilities;
using BStarJsonSerializer = BarnardsStar.Utilities.JsonSerializer;

namespace BarnardsStar.Abilities {

    // copied from stack overflow https://stackoverflow.com/questions/20995865/deserializing-json-to-abstract-class
    public class AbstractAbilityConverterResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            if (typeof(AbstractAbility).IsAssignableFrom(objectType) && !objectType.IsAbstract)
                return null; // pretend TableSortRuleConvert is not specified (thus avoiding a stack overflow)
            return base.ResolveContractConverter(objectType);
        }
    }

    public class AbstractAbilityConverter : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(AbstractAbility));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var oldContractResolver = serializer.ContractResolver;
            serializer.ContractResolver = new AbstractAbilityConverterResolver();

            AbstractAbility ret = null;
            JObject jo = JObject.Load(reader);
            var abilityType = (BSAbility) jo["type"].Value<int>();
            var t = AbstractAbility.GetAbilityType(abilityType);
            if (t != null)
                ret = (AbstractAbility) jo.ToObject(t, serializer);
            
            serializer.ContractResolver = oldContractResolver;
            
            return ret;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}