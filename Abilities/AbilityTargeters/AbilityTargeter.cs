using System;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.AbilityTargeters
{
    public abstract class AbilityTargeter
    {
        public enum TargetStatus
        {
            None,
            Targeted,
            Covered
        }

        protected Fighter actor;
        protected Game _game => actor.game;
        protected GameMap _map => _game.map;

        private string _cachedTargeterTypeName;

        protected AbilityTargeter(Fighter actor)
        {
            this.actor = actor;
        }

        public virtual string GetTypeName()
        {
            return _cachedTargeterTypeName ??= GetType().Name;
        }
        
        public virtual string FormatExtraFields(string rawDescription)
        {
            return rawDescription;
        }
        
        public abstract TargetList GetTargets();
    }
}