using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class MeleeWeaponTargeter : OrthogonalDirectionsTargeter
    {
        public bool includeHazards;
        public bool includePlacedTPs;

        public MeleeWeaponTargeter(Fighter actor, bool includeHazards, bool includePlacedTPs) : base(actor, 1, 1)
        {
            this.includeHazards = includeHazards;
            this.includePlacedTPs = includePlacedTPs;
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            if (damageableThere(coords, includeHazards, includePlacedTPs))
                return TargetStatus.Targeted;

            if (currTerrainType == TerrainType.Cover)
            {
                // Don't target own nexus
                if (actor.Owner.nexus != null && actor.Owner.nexus.AllCoords.Contains(coords))
                    return TargetStatus.None;

                return TargetStatus.Targeted;
            }

            return TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return false;
        }
    }
}