using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class GlueGunTargeter : ProjectileTargeter
    {
        public GlueGunTargeter(Fighter actor, int maxRange, DirectionsType directionsType) : base(actor, maxRange,
            false, directionsType, false, false, false)
        {
            hitMaxRange = false;
        }

        protected override TargetStatus ShouldAdd(Coords targetCoords, Coords? _)
        {
            // Call base.ShouldAdd() each square so that spacesSinceCover gets updated properly
            var baseShouldAdd = base.ShouldAdd(targetCoords, _);

            // Can only target enemy fighters though
            if (enemyFighterThere(targetCoords))
                return baseShouldAdd;

            return TargetStatus.None;
        }
    }
}