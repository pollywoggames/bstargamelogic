﻿using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class BusterShotTargeter : OrthogonalDirectionsTargeter
    {
        public BusterShotTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            return (enemyFighterThere(coords) || currTerrainType == TerrainType.Cover)
                ? TargetStatus.Targeted : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            var terrainType = currTerrainType;
            return terrainType == TerrainType.Wall;
        }
    }
}