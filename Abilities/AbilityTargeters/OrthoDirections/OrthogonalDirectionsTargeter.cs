using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class OrthogonalDirectionsTargeter : AbilityTargeter, IReadyAttackTargeter
    {
        public DirectionsType directionsType;

        protected Coords currDirection;
        protected bool hitMaxRange;
        protected int minRange, maxRange;
        protected TerrainType currTerrainType;
            
        private Dictionary<Coords, IDamageable> attackables;

        public OrthogonalDirectionsTargeter(Fighter actor, int minRange, int maxRange) : base(actor)
        {
            this.minRange = minRange;
            this.maxRange = maxRange;
        }

        public TargetList GetReadyAttacks()
        {
            return GetTargets(true);
        }

        public override TargetList GetTargets()
        {
            return GetTargets(false);
        }

        private TargetList GetTargets(bool isForReadyAttack)
        {
            IOrthoDirectionsReadyAttackTargeter rat = null;
            if (isForReadyAttack)
            {
                if (this is IOrthoDirectionsReadyAttackTargeter)
                {
                    rat = (IOrthoDirectionsReadyAttackTargeter)this;
                }
                else
                {
                    Logger.Warn($"Called GetTargets with isForReadyAttack=true, but unsupported targeter! targeter={GetType().Name}");
                    isForReadyAttack = false;
                }
            }
            
            var coordsAdded = new TargetList();

            attackables = _game.AttackablesFor(actor.Owner);

            foreach (var dir in GetDirections())
            {
                currDirection = dir;
                InitForDirection();
                Coords? rightBefore = null;
                for (var i = 1; i <= maxRange; i++)
                {
                    var newCoords = actor.coords + new Coords(i * dir.x, i * dir.y);
                    currTerrainType = _map.GetTerrainType(newCoords);

                    TargetStatus status = TargetStatus.None;
                    if (i >= minRange)
                    {
                        if (isForReadyAttack)
                            status = rat.ShouldAdd(newCoords, rightBefore, true);
                        else
                            status = ShouldAdd(newCoords, rightBefore);
                        
                        
                        if (status == TargetStatus.Targeted)
                        {
                            coordsAdded.Add(new Target(newCoords));
                        }
                        else if (status == TargetStatus.Covered)
                        {
                            coordsAdded.AddCoveredCoords(newCoords);
                        }
                        else if (hitMaxRange && i == maxRange)
                        {
                            coordsAdded.Add(new Target(newCoords));
                        }
                    }

                    if (ShouldStop(newCoords, status)) break;

                    rightBefore = newCoords;
                }
            }

            return coordsAdded;
        }

        private Coords[] GetDirections()
        {
            switch (directionsType)
            {
                default:
                case DirectionsType.OrthoOnly:
                    return Coords.OrthogonalDirections;
                case DirectionsType.DiagonalOnly:
                    return Coords.DiagonalDirections;
                case DirectionsType.OrthoAndDiagonal:
                    return Coords.AllDirections;
            }
        }

        protected virtual void InitForDirection()
        { }

        protected virtual TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            return currTerrainType == TerrainType.Clear
                ? TargetStatus.Targeted
                : TargetStatus.None;
        }

        protected virtual bool ShouldStop(Coords coords, TargetStatus status)
        {
            return currTerrainType == TerrainType.Wall;
        }

        protected bool fighterThere(Coords coords)
        {
            return _game.IsFighterAt(coords);
        }

        protected bool enemyFighterThere(Coords coords)
        {
            return _game.TryGetFighter(coords, out var f) && actor.IsEnemyOf(f);
        }
        
        protected bool killableEnemyFighterThere(Coords coords, int damage)
        {
            if (!_game.TryGetFighter(coords, out var f))
                return false;

            if (!actor.IsEnemyOf(f))
                return false;

            return (f.CurrHealth + f.CurrShield) <= damage;
        }

        protected bool friendlyFighterThere(Coords coords)
        {
            return _game.TryGetFighter(coords, out var f) && actor.IsFriendlyTo(f);
        }

        protected bool damageableThere(Coords coords, bool includeHazards, bool includePlacedTPs)
        {
            if (attackables.ContainsKey(coords))
                return true;

            if (includeHazards
                && _game.TryGetHazard(coords, out var h)
                && h.targetable
                && !_game.TryGetFighter(coords, out _))
                return true;

            if (includePlacedTPs && _game.map.IsDamageableTeleporter(coords))
                return true;

            return false;
        }

        protected bool enemyNexusThere(Coords coords)
        {
            return attackables.TryGetValue(coords, out var d) && d is Nexus;
        }

        public override string FormatExtraFields(string description)
        {
            description = FormatDirectionsType(description);
            description = FormatGreaterThanOneRange(description);
            return description;
        }

        private string FormatDirectionsType(string description)
        {
            string replacement;

            switch (directionsType)
            {
                case DirectionsType.DiagonalOnly:
                    replacement = "Diagonal directions only.";
                    break;
                case DirectionsType.OrthoAndDiagonal:
                    replacement = "Can target in both orthogonal and diagonal directions.";
                    break;
                default:
                    replacement = "Orthogonal directions only.";
                    break;
            }

            return Regex.Replace(description, @"{directionsType}", replacement);
        }

        private string FormatGreaterThanOneRange(string description)
        {
            var rangedRegex = @"\{>1range:(.+?)\}";
            
            var match = Regex.Match(description, rangedRegex);
            if (!match.Success)
                return description;

            var replacement = maxRange > 1 ? match.Groups[1].Value : "";
            return Regex.Replace(description, rangedRegex, replacement);
        }
    }

    public enum DirectionsType
    {
        OrthoOnly,
        DiagonalOnly,
        OrthoAndDiagonal,
    }
}