﻿using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class EnemyOnlyTargeter : OrthogonalDirectionsTargeter
    {
        private bool hitsNexus;

        public EnemyOnlyTargeter(Fighter actor, int minRange, int maxRange, bool hitsNexus = true) : base(actor,
            minRange, maxRange)
        {
            this.hitsNexus = hitsNexus;
        }

        protected bool enemyThere(Coords coords)
        {
            return hitsNexus ? damageableThere(coords, false, false) : enemyFighterThere(coords);
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            return enemyThere(coords) ? TargetStatus.Targeted : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return currTerrainType == TerrainType.Wall;
        }
    }
}