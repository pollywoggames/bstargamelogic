using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class TeleportGunTargeter : ProjectileTargeter
    {
        public TeleportGunTargeter(Fighter actor, int maxRange, bool hitsHalfWalls = false, DirectionsType directionsType = DirectionsType.OrthoOnly)
            : base(actor, maxRange, hitsHalfWalls, directionsType, false, true, true)
        { }

        protected override TargetStatus ShouldAdd(Coords targetCoords, Coords? _)
        {
            // Only override if there's a friendly fighter there, in which case we can always hit them.
            if (friendlyFighterThere(targetCoords))
                return TargetStatus.Targeted;
            
            return base.ShouldAdd(targetCoords, _);
        }
    }
}