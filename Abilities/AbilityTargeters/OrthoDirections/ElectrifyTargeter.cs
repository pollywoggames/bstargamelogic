using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class ElectrifyTargeter : OrthogonalDirectionsTargeter
    {
        public ElectrifyTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            var f = Hazard.ShockableFighterAt(actor.game, coords);
            return Hazard.ShockableHazardAt(actor.game, coords) || f != null ? TargetStatus.Targeted : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return currTerrainType == TerrainType.Wall;
        }
    }
}