using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class DashStrikeTargeter : OrthogonalDirectionsTargeter
    {
        private int targetKillableUnitsDamage;

        public DashStrikeTargeter(Fighter actor, int maxRange, int targetKillableUnitsDamage)
            : base(actor, 1, maxRange)
        {
            this.targetKillableUnitsDamage = targetKillableUnitsDamage;
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            // Special case - if the unit can be killed, it doesn't matter if the space beforehand is empty
            if (killableEnemyFighterThere(coords, targetKillableUnitsDamage))
                return TargetStatus.Targeted;
            
            // Another special case - if there's damaged cover, it doesn't matter if the space beforehand is empty
            if (currTerrainType == TerrainType.Cover && _map.IsCoverDamaged(coords))
                return TargetStatus.Targeted;
            
            if (damageableThere(coords, false, true) || currTerrainType == TerrainType.Cover)
            {
                // check the space directly before this one -- that space has to be empty (no friendly fighter there) if we can charge to this space
                if (rightBefore.HasValue && fighterThere(rightBefore.Value))
                    return TargetStatus.None;
                
                return TargetStatus.Targeted;
            }

            return TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return enemyFighterThere(coords)
                || (currTerrainType != TerrainType.Clear && currTerrainType != TerrainType.Pit);
        }
    }
}