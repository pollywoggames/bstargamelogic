using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class SmashAttackTargeter : OrthogonalDirectionsTargeter
    {
        public SmashAttackTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            if (currTerrainType == TerrainType.Pit
                || currTerrainType == TerrainType.Wall
                || _game.TryGetFighter(coords, out var f) && f.IsFriendlyTo(actor))
                return TargetStatus.None;

            return TargetStatus.Targeted;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return currTerrainType != TerrainType.Clear;
        }
    }
}