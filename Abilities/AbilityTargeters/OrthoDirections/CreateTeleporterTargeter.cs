using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class CreateTeleporterTargeter : OrthogonalDirectionsTargeter
    {
        // If true, targeter will return NO valid targets at all when cast while standing on a Fixed teleporter
        // (aside from their own tpChan teleporter).
        // Note: can always target when standing on a Placed or Burrow tp because the ability will destroy them.
        public bool CantTargetAnythingWhenStandingOnTP;
        
        public CreateTeleporterTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        public override TargetList GetTargets()
        {
            if (CantTargetAnythingWhenStandingOnTP
                && _game.map._teleportersByCoords.TryGetValue(actor.coords, out var tp)
                && tp.type == TeleporterType.Fixed
                && tp.channel != actor.tpChan.GetValueOrDefault(-1))
            {
                return new TargetList();
            }
            
            return base.GetTargets();
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            if (currTerrainType != TerrainType.Clear)
                return TargetStatus.None;

            if (_game.map._teleportersByCoords.TryGetValue(coords, out var tp) && tp.type == TeleporterType.Fixed)
                return TargetStatus.None;

            if (fighterThere(coords))
                return TargetStatus.None;

            return TargetStatus.Targeted;
        }
    }
}