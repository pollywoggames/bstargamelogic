using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public interface IOrthoDirectionsReadyAttackTargeter
    {
        AbilityTargeter.TargetStatus ShouldAdd(Coords coords, Coords? rightBefore, bool isForReadyAttack);
    }
}