using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class ForkliftTargeter : MeleeWeaponTargeter
    {
        public ForkliftTargeter(Fighter actor) : base(actor, false, true)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            if (friendlyFighterThere(coords))
                return TargetStatus.Targeted;
            
            return base.ShouldAdd(coords, _);
        }
    }
}