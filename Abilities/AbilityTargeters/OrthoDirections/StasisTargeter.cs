using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class StasisTargeter : EnemyOnlyTargeter
    {
        public StasisTargeter(Fighter actor, int minRange, int maxRange, bool hitsNexus = true)
            : base(actor, minRange, maxRange, hitsNexus)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            var perpendicular = Coords.Perpendicular(currDirection);

            bool anyEnemies = false;
            for (int i = -1; i <= 1; i++)
            {
                var currCoords = coords + perpendicular * i;
                anyEnemies = anyEnemies || enemyThere(currCoords);
            }
            
            return anyEnemies ? TargetStatus.Targeted : TargetStatus.None;
        }
    }
}