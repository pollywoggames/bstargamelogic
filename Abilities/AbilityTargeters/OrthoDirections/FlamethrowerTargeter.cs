﻿using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class FlamethrowerTargeter : OrthogonalDirectionsTargeter
    {
        public FlamethrowerTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            bool badTerrain = currTerrainType != TerrainType.Clear && currTerrainType != TerrainType.Pit;

            bool aiStopAtFirstAdd = actor.Owner.IsAI && ShouldAdd(coords, null) == TargetStatus.Targeted;

            return badTerrain || aiStopAtFirstAdd;
        }
    }
}