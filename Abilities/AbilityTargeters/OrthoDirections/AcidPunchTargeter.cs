using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    /// <summary>
    /// Like OrthogonalDirectionsTargeter but targets cover too.
    /// </summary>
    public class AcidPunchTargeter : OrthogonalDirectionsTargeter
    {
        public AcidPunchTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            return currTerrainType == TerrainType.Clear || currTerrainType == TerrainType.Cover
                ? TargetStatus.Targeted
                : TargetStatus.None;
        }
    }
}