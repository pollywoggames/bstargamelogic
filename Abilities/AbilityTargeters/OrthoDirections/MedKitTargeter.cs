using BarnardsStar.Characters;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class MedKitTargeter : OrthogonalDirectionsTargeter
    {
        public MedKitTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        {
            directionsType = DirectionsType.OrthoAndDiagonal;
        }

        public override TargetList GetTargets()
        {
            var targets = base.GetTargets();
            
            // add self as well
            targets.Add(new Target(actor.coords));

            return targets;
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            if (!_game.TryGetFighter(coords, out var f) || f.IsEnemyOf(actor))
                return TargetStatus.None;

            return TargetStatus.Targeted;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return false;
        }
    }
}