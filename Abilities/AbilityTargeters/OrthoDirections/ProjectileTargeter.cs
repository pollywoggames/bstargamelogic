using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class ProjectileTargeter : OrthogonalDirectionsTargeter, IOrthoDirectionsReadyAttackTargeter
    {
        public bool hitsHalfWalls;
        public bool hitsHazards;
        public bool hitsPlacedTPs;
        
        private int spacesSinceCover;

        public ProjectileTargeter(Fighter actor, int maxRange,
            bool hitsHalfWalls,
            DirectionsType directionsType,
            bool hitsMaxRange,
            bool hitsHazards,
            bool hitsPlacedTPs
            ) : base(actor, 1, maxRange)
        {
            this.hitsHalfWalls = hitsHalfWalls;
            this.directionsType = directionsType;
            this.hitMaxRange = hitsMaxRange;
            this.hitsHazards = hitsHazards;
            this.hitsPlacedTPs = hitsPlacedTPs;
        }

        protected override void InitForDirection()
        {
            spacesSinceCover = 99; // Reset between directions -- only matters when it's 0 or 1
        }
        
        protected override TargetStatus ShouldAdd(Coords targetCoords, Coords? rightBefore)
        {
            return ShouldAdd(targetCoords, rightBefore, false);
        }

        public TargetStatus ShouldAdd(Coords targetCoords, Coords? rightBefore, bool isForReadyAttack)
        {
            // can target unbreakable walls
            if (currTerrainType == TerrainType.Wall)
            {
                // don't let AI target walls pointlessly
                return actor.Owner.IsHuman ? TargetStatus.Targeted : TargetStatus.None;
            }
            
            // can target cover directly
            if (currTerrainType == TerrainType.Cover && !enemyNexusThere(targetCoords))
            {
                spacesSinceCover = 0;
                // They can shoot it directly
                return TargetStatus.Targeted;
            }
            spacesSinceCover++;

            // last space was cover
            if (spacesSinceCover <= 1)
                return TargetStatus.Covered;

            if (hitsHazards && _game.TryGetHazard(targetCoords, out var h))
            {
                // can't target anything in smoke
                if (h.type == HazardType.SmokeScreen)
                    return TargetStatus.Covered;

                if (h.targetable)
                    return TargetStatus.Targeted;
            }

            // If it's for ready-attack checking, at this point we don't care about whether there's
            // a damageable there or not, we just always return targeted.
            if (isForReadyAttack)
                return TargetStatus.Targeted;

            // Regular case (not for ready attack)
            if (damageableThere(targetCoords, hitsHazards, hitsPlacedTPs))
                return TargetStatus.Targeted;
            return TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            if (enemyFighterThere(coords) && status != TargetStatus.Covered)
                return true;

            if (hitsHalfWalls && currTerrainType == TerrainType.Cover)
                return true;

            if (currTerrainType == TerrainType.Wall)
                return true;

            // can't target anything in smoke
            if (_game.TryGetHazard(coords, out var h) && h.type == HazardType.SmokeScreen)
                return true;

            return false;
        }

        public override string GetTypeName()
        {
            if (hitsHalfWalls)
                return base.GetTypeName() + ":hitsHalfWalls";
            
            return base.GetTypeName();
        }
    }
}