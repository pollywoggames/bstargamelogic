using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class ClearSpotTargeter : OrthogonalDirectionsTargeter
    {
        public ClearSpotTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? _)
        {
            return !_game.CollideAt(coords) && currTerrainType == TerrainType.Clear
                ? TargetStatus.Targeted : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return false;
        }
    }
}