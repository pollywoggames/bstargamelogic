using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class TauntTargeter : OrthogonalDirectionsTargeter
    {
        public TauntTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        {
            this.directionsType = DirectionsType.OrthoAndDiagonal;
        }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            if (!_game.TryGetFighter(coords, out var f) || !actor.IsEnemyOf(f))
                return TargetStatus.None;

            var projAttackTargeting = f.GetProjectileAttackTargeting(actor.coords);
            return projAttackTargeting.Item1 == -1 ? TargetStatus.None : TargetStatus.Targeted;
        }
    }
}