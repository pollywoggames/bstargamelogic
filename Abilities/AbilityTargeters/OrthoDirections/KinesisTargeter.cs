using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class KinesisTargeter : OrthogonalDirectionsTargeter
    {
        public KinesisTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            return (_game.TryGetHazard(coords, out var h ) && h.moveable)
                || fighterThere(coords)
                || (currTerrainType == TerrainType.Cover && !_game.map.nexiCoords.Contains(coords))
                ? TargetStatus.Targeted : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return fighterThere(coords) || currTerrainType == TerrainType.Wall || currTerrainType == TerrainType.Cover;
        }
    }
}