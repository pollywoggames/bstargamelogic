using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class DashTargeter : OrthogonalDirectionsTargeter
    {
        public DashTargeter(Fighter actor, int minRange, int maxRange) : base(actor, minRange, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            return currTerrainType == TerrainType.Clear && !fighterThere(coords)
                ? TargetStatus.Targeted
                : TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return currTerrainType != TerrainType.Clear && currTerrainType != TerrainType.Pit;
        }
    }
}