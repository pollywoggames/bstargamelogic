﻿using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.OrthoDirections
{
    public class ChargeTargeter : OrthogonalDirectionsTargeter
    {
        public ChargeTargeter(Fighter actor, int maxRange)
            : base(actor, 1, maxRange)
        { }

        protected override TargetStatus ShouldAdd(Coords coords, Coords? rightBefore)
        {
            if (damageableThere(coords, false, false))
            {
                // check the space directly before this one -- that space has to be empty (no friendly fighter there) if we can charge to this space
                if (rightBefore.HasValue && fighterThere(rightBefore.Value))
                    return TargetStatus.None;
                
                return TargetStatus.Targeted;
            }

            if (currTerrainType == TerrainType.Clear && !fighterThere(coords))
                return TargetStatus.Targeted;

            return TargetStatus.None;
        }

        protected override bool ShouldStop(Coords coords, TargetStatus status)
        {
            return enemyFighterThere(coords)
                || (currTerrainType != TerrainType.Clear && currTerrainType != TerrainType.Pit);
        }
    }
}