﻿using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters
{
    public class DeployCoverTargeter : AbilityTargeter
    {
        private readonly int minRange;
        private readonly int maxRange;

        public DeployCoverTargeter(Fighter actor, int minRange, int maxRange) : base(actor)
        {
            this.minRange = minRange;
            this.maxRange = maxRange;
        }

        public override TargetList GetTargets()
        {
            bool actorIsHuman = actor.Owner.IsHuman;
            
            var finalTargets = new TargetList();
            foreach (var dir in Coords.OrthogonalDirections)
            {
                Coords perpendicular;
                if (dir.x != 0)
                    perpendicular = Coords.up;
                else
                    perpendicular = Coords.right;

                for (var i = 1; i <= maxRange; i++)
                {
                    var newCoords = actor.coords + new Coords(i * dir.x, i * dir.y);

                    if (_map.GetTerrainType(newCoords) == TerrainType.Wall)
                        break;

                    if (i >= minRange)
                    {
                        var potentialTargets = new List<Coords> { newCoords };
                        // Slim down the list of potential targets for AI, to speed up processing
                        if (actorIsHuman)
                        {
                            potentialTargets.Add(newCoords + perpendicular);
                            potentialTargets.Add(newCoords - perpendicular);
                        }

                        var added = new TargetList();
                        foreach (var pt in potentialTargets)
                        {
                            if (_map.GetTerrainType(pt) == TerrainType.Clear && !_game.IsFighterAt(pt))
                            {
                                var tc = new Target(pt, added.validTargets);
                                added.Add(tc);
                            }
                        }

                        finalTargets.AddRange(added.validTargets);
                    }
                }
            }

            return finalTargets;
        }
    }
}