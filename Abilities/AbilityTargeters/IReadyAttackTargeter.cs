using BarnardsStar.Preview;

namespace BarnardsStar.Abilities.AbilityTargeters
{
    public interface IReadyAttackTargeter
    {
        TargetList GetReadyAttacks();
    }
}