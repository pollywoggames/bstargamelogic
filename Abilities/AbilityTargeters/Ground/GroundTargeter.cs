using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Preview;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.Ground
{
    public class GroundTargeter : AbilityTargeter
    {
        private readonly int range;

        public GroundTargeter(Fighter actor, int range) : base(actor)
        {
            this.range = range;
        }

        public override TargetList GetTargets()
        {
            var coordsAdded = new TargetList();
            for (var x = actor.coords.x - range; x <= actor.coords.x + range; x++)
            for (var y = actor.coords.y - range; y <= actor.coords.y + range; y++)
            {
                var loc = new Coords(x, y);

                var dist = Coords.Distance(actor.coords, loc);
                if (dist > range)
                    continue;

                var terrain = _map.GetTerrainType(loc);
                if (terrain == TerrainType.Wall || terrain == TerrainType.Cover)
                    continue;

                if (!CanHitCoords(actor.coords, loc))
                    continue;

                var ds = new Target(loc);
                coordsAdded.Add(ds);
            }

            return coordsAdded;
        }

        protected virtual bool CanHitCoords(Coords actorCoords, Coords loc)
        {
            return true;
        }
    }
}