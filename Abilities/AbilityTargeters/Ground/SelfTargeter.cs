using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abilities.AbilityTargeters.Ground
{
    public class SelfTargeter : GroundTargeter
    {
        public SelfTargeter(Fighter actor) : base(actor, 1)
        { }

        protected override bool CanHitCoords(Coords actorCoords, Coords loc)
        {
            return loc == actorCoords;
        }
    }
}