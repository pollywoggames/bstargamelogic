﻿using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Abilities.AbilityTargeters.OrthoDirections;
using BarnardsStar.Abilities.ActiveAbilities;
using BarnardsStar.Abilities.PassiveAbilities;
using BarnardsStar.Abilities.UntargetedAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Preview;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Utilities;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using BarnardsStar.Players;
using BarnardsStar.Primitives;
using BarnardsStar.Weapons;

namespace BarnardsStar.Abilities
{
    [DataContract]
    [JsonConverter(typeof(AbstractAbilityConverter))]
    public abstract class AbstractAbility : IAbility
    {
        public const float PassiveAbilityPause = .5f;
        
        [DataMember] public Fighter actor { get; set; }

        public AbilityTargeter targeter;

        [DataMember] public int CooldownTurnsRemaining { get; set; }
        [DataMember] public int Cooldown;
        [DataMember] public int BaseCooldown; // i.e. what is the cooldown, not counting any upgrades
        
        [DataMember] public bool IsPassive;
        [DataMember] public bool IsUntargeted;

        private int _range;

        [DataMember] public int DisplayRange
        {
            get => _range;
            
            set
            {
                _range = value;
                RefreshTargeter();
            }
        }
        
        [DataMember] public int Damage;
        [DataMember] public int spriteIdx;
        [DataMember] public BSAbility type;
        [DataMember] public DirectionsType DirectionsType;
        [DataMember] public bool UseWideRangeIndicators;
        [DataMember] public bool HasOnDeathTrigger;

        [DataMember] public int RangeUpgrade { get; private set; }
        [DataMember] public int DamageUpgrade { get; private set; }
        [DataMember] public int CooldownUpgrade { get; private set; }
        [DataMember] public int MiscUpgrade { get; private set; }

        public bool OnCooldown => CooldownTurnsRemaining > 0;

        private string _cachedTypeName;

        public Game game => actor?.game;
        public IContextProvider contextProvider => game.contextProvider;
        public IRunContext runCtx => game.runCtx;
        public IAnimationContext animCtx => game.animCtx;
        public ICameraController camera => animCtx.Camera();
        public ISoundContext soundCtx => game.soundCtx;
        public IEnumerable<Nexus> enemyNexi => actor.Owner.EnemyNexi;

        protected AbstractAbility(Fighter actor, int spriteIdx, int cooldown, DirectionsType directionsType = DirectionsType.OrthoOnly)
        {
            this.actor = actor;
            this.spriteIdx = spriteIdx;
            
            this.Cooldown = cooldown;
            this.BaseCooldown = cooldown;
            
            this.DirectionsType = directionsType;

            CooldownTurnsRemaining = 0;
            IsPassive = false;
            IsUntargeted = false;
            
            RefreshTargeter();
        }

        public int GetSpriteIndex()
        {
            return spriteIdx;
        }

        public string GetTypeName()
        {
            return _cachedTypeName ??= GetType().Name;
        }

        public string GetTargeterTypeName()
        {
            return targeter?.GetTypeName();
        }

        public string FormatExtraFields(string description)
        {
            description = targeter.FormatExtraFields(description);
            return description;
        }

        public bool Passive()
        {
            return IsPassive;
        }

        public int GetCooldownTurnsRemaining()
        {
            return CooldownTurnsRemaining;
        }

        public AbstractAbility Clone(Fighter newOwner)
        {
            var clone = (AbstractAbility) MemberwiseClone();

            clone.actor = newOwner;
            clone.RefreshTargeter();

            return clone;
        }

        public void RefreshTargeter()
        {
            targeter = GetTargeter();
        }

        public abstract AbilityTargeter GetTargeter();
        
        public virtual string GetCriticalInfo()
        {
            string cdStr = (Cooldown == BaseCooldown) ? Cooldown.ToString() : $"<b>*{Cooldown}*</b>";
            
            return $"Cooldown: {cdStr}\nRange: {TextUtils.GetAbilityRangeStr(this)}";
        }

        // --- Stub methods
        public virtual IEnumerator StartOfTurn()
        {
            yield break;
        }

        public virtual IEnumerator EndOfTurn()
        {
            yield break;
        }
        
        public virtual IEnumerator OnDeath(Fighter killer)
        {
            yield break;
        }

        public virtual IEnumerator Execute(Target target)
        {
            yield break;
        }

        public virtual void Initialize()
        { }

        public virtual int GetDamage()
        {
            return Damage + DamageUpgrade;
        }

        public int GetBaseDamage()
        {
            return Damage;
        }

        public int GetRange()
        {
            return DisplayRange + RangeUpgrade;
        }

        public int GetBaseRange()
        {
            return DisplayRange;
        }

        public virtual int GetMisc()
        {
            return 0;
        }

        public virtual int GetBaseMisc()
        {
            return GetMisc();
        }

        public string GetUnitName()
        {
            return actor.Name;
        }

        public int GetCooldown()
        {
            return Cooldown;
        }

        public virtual bool IsProjectileAttack()
        {
            return false;
        }

        public virtual int GetMovementAfter()
        {
            return 0;
        }

        public virtual bool DisableMovementPreviewsForActor()
        {
            return false;
        }
        
        // Indicates that the ability should not be used by AI for movement purposes alone.
        public virtual bool DisableMovementPreviewScoresForActor()
        {
            return false;
        }

        public virtual bool CanReceiveDamageUpgrade()
        {
            return false;
        }
        
        public virtual bool CanReceiveRangeUpgrade()
        {
            return true;
        }
        
        public virtual bool CanReceiveCooldownUpgrade()
        {
            return !IsPassive && BaseCooldown > 0;
        }
        
        public virtual bool CanReceiveMiscUpgrade()
        {
            return false;
        }

        public virtual void ApplyDamageUpgrade(int amount)
        {
            DamageUpgrade = amount;
        }

        public virtual void ApplyRangeUpgrade(int amount)
        {
            RangeUpgrade = amount;
            RefreshTargeter();
        }

        public virtual void ApplyCooldownUpgrade(int amount)
        {
            CooldownUpgrade = amount;
            
            Cooldown -= amount;
            if (Cooldown < 0)
                Cooldown = 0;
        }

        public virtual void ApplyMiscUpgrade(int amount)
        {
            MiscUpgrade = amount;
        }

        // --- end stub methods

        public void SetOnCooldown()
        {
            if (Cooldown == 0 || game.CheatEnabled(Cheat.ZeroCooldowns))
                return;
            
            if (contextProvider.IsPreviewing())
                contextProvider.RegisterPreview(new CooldownPreview(game, actor, type));
            
            CooldownTurnsRemaining = Cooldown;
            if (actor.Owner.IsTurn)
                CooldownTurnsRemaining++;
        }

        // Take an ability off cooldown
        public void ResetCooldown()
        {
            CooldownTurnsRemaining = 0;
        }

        public TargetList GetTargets()
        {
            return targeter.GetTargets();
        }

        public void UpdateCooldown()
        {
            if (CooldownTurnsRemaining > 0)
                CooldownTurnsRemaining--;
        }

        protected IEnumerator PassiveAbilityEffect(VFXType vfxType, Action callback, Point? position = null)
        {
            Point pos = position ?? actor.pos;
            yield return camera.PanTilPointVisible(pos);
            callback?.Invoke();

            animCtx.PlayVFX(vfxType, pos, actor.Owner.teamColor.ToVFXColor());
            
            if (!game.CheatEnabled(Cheat.FastPassives))
                yield return runCtx.WaitSeconds(PassiveAbilityPause);
        }

        public static AbstractAbility New(BSAbility a, Fighter actor)
        {
            var type = GetAbilityType(a);
            if (type == null)
                return null;

            var constructor = type.GetConstructor(new[] {typeof(Fighter)});
            var ability = (AbstractAbility) constructor.Invoke(new object[] {actor});
            ability.type = a;
            ability.actor = actor;
            ability.Initialize();

            return ability;
        }
        
        public static Type GetAbilityType(BSAbility ability)
        {
            switch (ability)
            {
                case BSAbility.AcidBomb:
                    return typeof(AcidBomb);
                case BSAbility.AcidBubble:
                    return typeof(AcidBubble);
                case BSAbility.AcidDash:
                    return typeof(AcidDash);
                case BSAbility.AcidRain:
                    return typeof(AcidRain);
                case BSAbility.Bazooka:
                    return typeof(Bazooka);
                case BSAbility.BusterShot:
                    return typeof(BusterShot);
                case BSAbility.ChargeAttack:
                    return typeof(ChargeAttack);
                case BSAbility.DeployCover:
                    return typeof(DeployCover);
                case BSAbility.Electrify:
                    return typeof(Electrify);
                case BSAbility.ElectrolysisGrenade:
                    return typeof(ElectrolysisGrenade);
                case BSAbility.Flamethrower:
                    return typeof(Flamethrower);
                case BSAbility.HealOnKill:
                    return typeof(HealOnKill);
                case BSAbility.HealTeammatesInOil:
                    return typeof(HealTeammatesInOil);
                case BSAbility.Hook:
                    return typeof(Hook);
                case BSAbility.Hypertunnel:
                    return typeof(Hypertunnel);
                case BSAbility.Kinesis:
                    return typeof(Kinesis);
                case BSAbility.KnockoutPunch:
                    return typeof(KnockoutPunch);
                case BSAbility.LandMine:
                    return typeof(LandMine);
                case BSAbility.MedKit:
                    return typeof(MedKit);
                case BSAbility.MoveAfterAttack:
                    return typeof(MoveAfterAttack);
                case BSAbility.OilBarrelThrow:
                    return typeof(OilBarrelThrow);
                case BSAbility.OilConeSpray:
                    return typeof(OilConeSpray);
                case BSAbility.ReadyAttack:
                    return typeof(ReadyAttack);
                case BSAbility.ReflectProjectiles:
                    return typeof(ReflectProjectiles);
                case BSAbility.ActiveRegeneration:
                    return typeof(ActiveRegeneration);
                case BSAbility.Shield:
                    return typeof(Shield);
                case BSAbility.ShieldBot:
                    return typeof(ShieldBot);
                case BSAbility.SketchySyringe:
                    return typeof(SketchySyringe);
                case BSAbility.SmokeGrenade:
                    return typeof(SmokeGrenade);
                case BSAbility.SprayWater:
                    return typeof(SprayWater);
                case BSAbility.SummonFly:
                    return typeof(SummonFly);
                case BSAbility.SummonMinion:
                    return typeof(SummonMinion);
                case BSAbility.TrailAcid:
                    return typeof(TrailAcid);
                case BSAbility.Transfusion:
                    return typeof(Transfusion);
                case BSAbility.Fly:
                    return typeof(FlyAbility);
                case BSAbility.AcidSurfing:
                    return typeof(AcidSurfing);
                case BSAbility.DisplacementCharge:
                    return typeof(DisplacementCharge);
                case BSAbility.OverloadAbility:
                    return typeof(OverloadAbility);
                case BSAbility.Stasis:
                    return typeof(Stasis);
                case BSAbility.SpeedBuff:
                    return typeof(SpeedBuff);
                case BSAbility.BodySlam:
                    return typeof(BodySlam);
                case BSAbility.ExplodeIntoAcidOnDeath:
                    return typeof(ExplodeIntoAcidOnDeath);
                case BSAbility.ElectricPunch:
                    return typeof(ElectricPunch);
                case BSAbility.Burrow:
                    return typeof(Burrow);
                case BSAbility.GlueGun:
                    return typeof(GlueGun);
                case BSAbility.PlaceTeleporter:
                    return typeof(PlaceTeleporter);
                case BSAbility.Taunt:
                    return typeof(Taunt);
                case BSAbility.WaterGrenade:
                    return typeof(WaterGrenade);
                case BSAbility.TeleportGunAbility:
                    return typeof(TeleportGunAbility);
                case BSAbility.Shockwave:
                    return typeof(Shockwave);
                case BSAbility.SelfSmoke:
                    return typeof(SelfSmoke);
                case BSAbility.SelfHaste:
                    return typeof(SelfHaste);
                case BSAbility.PersonalCover:
                    return typeof(PersonalCover);
                case BSAbility.PassiveRegeneration:
                    return typeof(PassiveRegeneration);
                case BSAbility.OilDash:
                    return typeof(OilDash);
                case BSAbility.FireGrenade:
                    return typeof(FireGrenade);
                case BSAbility.AcidDrizzle:
                    return typeof(AcidDrizzle);
                case BSAbility.OilRain:
                    return typeof(OilRain);
                default:
                    Logger.Error("can't find AbstractAbility for " + ability);
                    return null;
            }
        }
    }
}