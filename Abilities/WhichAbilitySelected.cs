namespace BarnardsStar.Abilities
{
    public enum WhichAbilitySelected
    {
        None,
        Weapon,
        Ability1,
        Ability2
    }
}