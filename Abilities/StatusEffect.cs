﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abilities.ActiveAbilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities
{
    public enum StatusEffect
    {
        Stunned,
        Electrified,
        LoseShieldWhenCounterExpires,
        CantBeStunned,
        LoseShieldWhenCounterExpires_EndOfTurn,
        Slowed,
        SpeedBuffNextTurn,
    }

    public class StatusEffectDetails
    {
        public const float SlowMovementPenalty = 1f;
        
        public readonly StatusEffect StatusEffectType;
        public readonly float MovementDelta;
        public readonly bool TakesAwayActions;
        public readonly bool IsEndOfTurn;
        public readonly bool ExtraIfTheirTurn;
        public readonly bool CanGoAboveOne;
        public readonly bool PreviewScoreDependsOnStrength;
        public readonly VFXType? vfxType;
        public readonly Sound? sound;
        
        private readonly Func<Fighter, bool> _checkCanAdd;
        private readonly Action<Fighter> _tickDownToZeroEffect;

        public StatusEffectDetails(StatusEffect type)
        {
            StatusEffectType = type;

            // defaults
            MovementDelta = 0;
            TakesAwayActions = false;
            ExtraIfTheirTurn = true;
            IsEndOfTurn = true;
            CanGoAboveOne = false;
            _checkCanAdd = null;
            PreviewScoreDependsOnStrength = true;
            vfxType = null;
            sound = null;

            switch (StatusEffectType)
            {
                case StatusEffect.Stunned:
                    MovementDelta = -999f;
                    TakesAwayActions = true;
                    PreviewScoreDependsOnStrength = false;
                    _checkCanAdd = f => !f.HasStatusEffect(StatusEffect.CantBeStunned);
                    _tickDownToZeroEffect = f => f.AddStatusEffect(StatusEffect.CantBeStunned, 1, null);
                    break;
                
                case StatusEffect.Electrified:
                    MovementDelta = -999f;
                    TakesAwayActions = true;
                    PreviewScoreDependsOnStrength = false;
                    _checkCanAdd = f => !f.HasStatusEffect(StatusEffect.CantBeStunned);
                    _tickDownToZeroEffect = f => f.AddStatusEffect(StatusEffect.CantBeStunned, 1, null);
                    break;
                
                case StatusEffect.LoseShieldWhenCounterExpires:
                    IsEndOfTurn = false;
                    _tickDownToZeroEffect = f =>
                    {
                        f.SetCurrShield(0);
                        f.SetMaxShield(0);
                    };
                    break;
                
                // Same as LoseShieldWhenCounterExpires but end of turn
                case StatusEffect.LoseShieldWhenCounterExpires_EndOfTurn:
                    IsEndOfTurn = true;
                    _tickDownToZeroEffect = f =>
                    {
                        f.SetCurrShield(0);
                        f.SetMaxShield(0);
                    };
                    break;
                
                case StatusEffect.CantBeStunned:
                    ExtraIfTheirTurn = false;
                    break;

                case StatusEffect.Slowed:
                    ExtraIfTheirTurn = false;
                    MovementDelta = -SlowMovementPenalty;
                    vfxType = VFXType.GlueSpot;
                    sound = Sound.GlueGunHit;
                    break;
                
                case StatusEffect.SpeedBuffNextTurn:
                    IsEndOfTurn = false;
                    _tickDownToZeroEffect = f =>
                    {
                        f.SetMovesLeft(f.MovesLeft + SpeedBuff.SpeedBuffAmount, true);
                        f.game.soundCtx.PlayOneShot(Sound.SpeedBoost);
                    };
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool CheckCanAdd(Fighter fighter)
        {
            return _checkCanAdd == null || _checkCanAdd(fighter);
        }
        
        public void TickDownToZeroEffect(Fighter fighter)
        {
            _tickDownToZeroEffect?.Invoke(fighter);
        }
    }

    public static class StatusEffectExtensions
    {
        private static Dictionary<StatusEffect, StatusEffectDetails> detailsMap;

        public static StatusEffectDetails Details(this StatusEffect statusEffect)
        {
            if (detailsMap == null)
            {
                detailsMap = new Dictionary<StatusEffect, StatusEffectDetails>();
                
                var values = Enum.GetValues(typeof(StatusEffect)).Cast<StatusEffect>();
                foreach (var se in values)
                {
                    detailsMap[se] = new StatusEffectDetails(se);
                }
            }

            return detailsMap[statusEffect];
        }
    }
}