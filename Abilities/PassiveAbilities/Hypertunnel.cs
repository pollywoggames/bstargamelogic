﻿using System;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class Hypertunnel : PassiveAbility
    {
        public Hypertunnel(Fighter actor) : base(actor, 209)
        { }

        public override void Initialize()
        {
            if (actor != null)
                actor.Hypertunnel = true;
        }
    }
}
