﻿using System;
using BarnardsStar.Abilities.AbilityTargeters;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class PassiveAbility : AbstractAbility
    {
        protected PassiveAbility(Fighter actor, int spriteIdx, int cooldown = 0) : base(actor, spriteIdx, cooldown)
        {
            IsPassive = true;
        }

        public override AbilityTargeter GetTargeter()
        {
            return null;
        }

        public override bool CanReceiveRangeUpgrade()
        {
            return false;
        }

        public override string GetCriticalInfo()
        {
            return "Passive";
        }
    }
}
