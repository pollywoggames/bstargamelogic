using System;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class FlyAbility : PassiveAbility
    {
        public FlyAbility(Fighter actor) : base(actor, 229)
        { }

        public override void Initialize()
        {
            if (actor != null)
                actor.Flier = true;
        }
    }
}