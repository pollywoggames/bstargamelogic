﻿     using BarnardsStar.Characters;
     using BarnardsStar.Hazards;

     namespace BarnardsStar.Abilities.PassiveAbilities
     {
         public class TrailAcid : PassiveAbility
         {
             public TrailAcid(Fighter actor) : base(actor, 207)
             { }

             public override void Initialize()
             {
                 if (actor != null)
                     actor.LeavesTrail = HazardType.Acid;
             }
         }
     }
