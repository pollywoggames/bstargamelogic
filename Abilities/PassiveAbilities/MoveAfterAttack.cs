﻿using System;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class MoveAfterAttack : PassiveAbility
    {
        public MoveAfterAttack(Fighter actor) : base(actor, 142)
        { }

        public override void Initialize()
        {
            if (actor != null)
            {
                actor.MoveAfterAttack = true;
                actor.OnAttack += OnAttackTrigger;
            }
        }

        private void OnAttackTrigger()
        {
            if (actor != null && !actor.IsDead)
            {
                var currMoves = actor.MovesLeft;
                var halfMoves = (int)Math.Round((double)actor.MaxMoves / 2f) + 1;
                
                // Don't reduce amount of moves left if it's more than half (i.e. they just got a kill)
                var newMovesLeft = Math.Max(currMoves, halfMoves);
                actor.SetMovesLeft(newMovesLeft);
            }
        }
    }
}
