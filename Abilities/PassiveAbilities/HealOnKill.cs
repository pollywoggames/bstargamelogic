﻿using System;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class HealOnKill : PassiveAbility
    {
        public const int BaseHealAmount = 2;

        public HealOnKill(Fighter actor) : base(actor, 54)
        {
            Damage = BaseHealAmount;
        }
        
        // Damage upgrade: how much the ability heals
        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }
        
        // Misc upgrade: the ability can overheal, giving a shield up to MiscUpgrade
        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override void Initialize()
        {
            if (actor != null)
                actor.OnKill += OnKillTrigger;
        }

        public void OnKillTrigger()
        {
            if (actor.Owner.IsTurn)
            {
                runCtx.Start(PassiveAbilityEffect(VFXType.Regeneration, null));

                var healAmount = GetDamage();
                
                // can we get a shield?
                if (MiscUpgrade > 0 && actor.CurrHealth + healAmount > actor.MaxHealth)
                {
                    var shieldAmount = actor.CurrHealth + healAmount - actor.MaxHealth;
                    
                    // max overheal shield amount stored in MiscUpgrade
                    if (shieldAmount > MiscUpgrade)
                        shieldAmount = MiscUpgrade;
                    
                    actor.SetMaxShield(shieldAmount);
                    actor.SetCurrShield(actor.CurrShield + shieldAmount);
                }
                
                actor.SetHealth(actor.CurrHealth + healAmount);
            }
        }
    }
}
