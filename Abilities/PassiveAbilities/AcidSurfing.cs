using BarnardsStar.Characters;
using BarnardsStar.Hazards;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class AcidSurfing : PassiveAbility
    {
        public AcidSurfing(Fighter actor) : base(actor, 230)
        { }

        public override void Initialize()
        {
            if (actor != null)
                actor.Surfing = HazardType.Acid;
        }
    }
}