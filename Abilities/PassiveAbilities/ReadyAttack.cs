﻿using System;
using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class ReadyAttack : PassiveAbility
    {
        public ReadyAttack(Fighter actor) : base(actor, 199)
        { }

        public override bool CanReceiveMiscUpgrade()
        {
            return true;
        }

        public override IEnumerator EndOfTurn()
        {
            if (actor.ActionsLeft > 0)
            {
                soundCtx.PlayOneShot(Sound.ReadyGun);
                yield return PassiveAbilityEffect(VFXType.ReadyAttack, () =>
                {
                    try
                    {
                        actor?.SetReadyAttacks(1 + MiscUpgrade);
                        game?.FightersReadyAttack?.Add(actor);
                    }
                    catch (Exception e)
                    {
                        Logger.Warn(e);
                    }
                });
            }
        }
    }
}
