using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class PassiveRegeneration : PassiveAbility
    {
        public const int HealPerTurn = 2;

        public PassiveRegeneration(Fighter actor) : base(actor, 255)
        {
            Damage = HealPerTurn;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override IEnumerator StartOfTurn()
        {
            if (!actor.Owner.IsTurn || actor.CurrHealth >= actor.MaxHealth)
                yield break;
            
            var pos = actor.pos;
            yield return camera.PanTilPointVisible(pos);
            actor.ShowHealthBar();
            
            yield return runCtx.WaitSeconds(.3f);
            actor.SetHealth(actor.CurrHealth + HealPerTurn);
            soundCtx.PlayOneShot(Sound.Healing);
            animCtx.PlayVFX(VFXType.Regeneration, pos);
            yield return runCtx.WaitSeconds(.75f);
            
            actor.HideHealthBar();
        }
    }
}