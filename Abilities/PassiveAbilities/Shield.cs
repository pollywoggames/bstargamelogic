﻿using System;
using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class Shield : PassiveAbility
    {
        public const int ShieldStrength = 2;

        public Shield(Fighter actor) : base(actor, 45, 1)
        {
            Damage = ShieldStrength;
        }

        public override bool CanReceiveDamageUpgrade()
        {
            return true;
        }

        public override int GetDamage()
        {
            return ShieldStrength + DamageUpgrade;
        }

        public override IEnumerator StartOfTurn()
        {
            if (!OnCooldown && !actor.ShieldUp && actor.Owner.IsTurn)
            {
                soundCtx.PlayOneShot(Sound.ShieldUp);
                yield return PassiveAbilityEffect(VFXType.Shield, () =>
                {
                    actor.SetMaxShield(GetDamage());
                    actor.SetCurrShield(GetDamage());
                });
            }
        }
    }
}
