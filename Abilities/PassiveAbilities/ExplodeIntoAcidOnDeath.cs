using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Hazards;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class ExplodeIntoAcidOnDeath : PassiveAbility
    {
        private bool AlreadyExploding;
        
        public ExplodeIntoAcidOnDeath(Fighter actor) : base(actor, 234)
        {
            HasOnDeathTrigger = true;
        }

        public override IEnumerator OnDeath(Fighter killer)
        {
            if (AlreadyExploding)
                yield break;
            AlreadyExploding = true;

            // Killer might be null if e.g. they died by walking through fire
            if (killer == null)
                killer = actor;
            
            soundCtx.PlayOneShot(Sound.ExplodeIntoAcid);
            
            WaitGroup wg = new WaitGroup(game.runCtx);
            
            // the central place happens immediately
            wg.Start(Hazard.PlaceHazardAt(killer, HazardType.Acid, actor.coords));
            
            // all the other places happen in random order
            var targets = new List<Coords>(Coords.AllDirections);
            targets = ListUtils.ShuffleList(targets, game.randy);
            bool everyOther = false;
            foreach (var dir in targets)
            {
                wg.Start(Hazard.PlaceAcidWithProjectile(killer, actor.coords, actor.coords + dir));
                
                if (everyOther)
                    yield return game.runCtx.WaitSeconds(.01f);

                everyOther = !everyOther;
            }

            yield return wg.Wait();

            AlreadyExploding = false;
        }
    }
}