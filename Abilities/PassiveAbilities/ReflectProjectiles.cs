﻿using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;

namespace BarnardsStar.Abilities.PassiveAbilities
{
    public class ReflectProjectiles : PassiveAbility
    {
        public ReflectProjectiles(Fighter actor) : base(actor, 210, 1)
        { }
        
        public override IEnumerator StartOfTurn()
        {
            if (!OnCooldown && !actor.ReflectProjectiles && actor.Owner.IsTurn)
            {
                soundCtx.PlayOneShot(Sound.ReflectReady);
                yield return PassiveAbilityEffect(VFXType.ReflectProjectiles,
                    () => { actor.SetReflectProjectiles(true); });
            }
        }
    }
}
