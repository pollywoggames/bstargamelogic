using BarnardsStar.Weapons;

namespace BarnardsStar.Abilities
{
    public interface IAbility
    {
        int GetSpriteIndex();
        string GetTypeName();
        string GetTargeterTypeName();
        bool Passive();
        string GetCriticalInfo();
        string FormatExtraFields(string rawDescription);

        int GetCooldownTurnsRemaining();
        
        // -- Fields that can be parsed from the description at runtime. --
        
        // {damage}
        int GetDamage();
        int GetBaseDamage();
        
        // {range}
        int GetRange();
        int GetBaseRange();
        
        // {misc} -- used for math on certain things for upgrades (like teleport distance is 3 + miscUpgrade)
        int GetMisc();
        int GetBaseMisc();
        
        // {unit}
        string GetUnitName();

        // {cooldown}
        int GetCooldown();

        // --
        
        // -- Upgrade related fields --
        
        int RangeUpgrade { get; }
        int DamageUpgrade { get; }
        int CooldownUpgrade { get; }
        int MiscUpgrade { get; }
        
        bool CanReceiveDamageUpgrade();
        bool CanReceiveRangeUpgrade();
        bool CanReceiveCooldownUpgrade();
        bool CanReceiveMiscUpgrade();

        void ApplyDamageUpgrade(int amount);
        void ApplyRangeUpgrade(int amount);
        void ApplyCooldownUpgrade(int amount);
        void ApplyMiscUpgrade(int amount);

        // --
    }
}
