using System.Collections.Generic;
using BarnardsStar.Preview.PreviewOutcomes;

namespace BarnardsStar.Games
{
    public class PreviewedGameState
    {
        public List<PreviewOutcome> outcomes;
        public Game finalGameState;

        public PreviewedGameState(List<PreviewOutcome> outcomes, Game finalGameState)
        {
            this.outcomes = outcomes;
            this.finalGameState = finalGameState;
        }
    }
}