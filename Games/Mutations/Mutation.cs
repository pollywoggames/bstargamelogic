using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace BarnardsStar.Games.Mutations
{
    /// <summary>
    /// A specific instance of a MutationPrototype, applied to a specific game's GameSettings.
    /// </summary>
    public class Mutation
    {
        public MutationType type;
        public int value;

        [JsonIgnore] private MutationPrototype _proto;
        [JsonIgnore] public MutationPrototype proto => _proto ??= MutationPrototype.FromType(type);
        
        // Empty constructor for JSON serialization
        public Mutation() { }

        public Mutation(MutationType type, int value)
        {
            this.type = type;
            this.value = value;
        }

        public static Mutation FromProto(MutationPrototype proto)
        {
            return new Mutation(proto.type, proto.GetDefault());
        }

        public override string ToString()
        {
            return $"Mutation(proto={proto},value={value})";
        }

        public static Mutation RandomMutation(Random random, List<Mutation> mutationsToExclude)
        {
            var toExclude = mutationsToExclude.Select(m => m.proto.type).ToList();
            toExclude.AddRange(mutationsToExclude.SelectMany(m => m.proto.GetIncompatible()));
            
            var mutationType = MutationTypes.RandomMutationType(random, toExclude);
            var mutation = FromProto(MutationPrototype.FromType(mutationType));

            // Set a random value, if applicable
            if (mutation.proto.HasOptions())
            {
                var possibleValues = mutation.proto.GetRandomOptions();
                mutation.value = possibleValues[random.Next(possibleValues.Count)];
            }

            return mutation;
        }
    }
}