namespace BarnardsStar.Games.Mutations
{
    public class DisableLifeLinkMutation : BooleanMutation
    {
        public override bool IsChooseable => true;
        public override bool IsRandomable => false;
        
        public DisableLifeLinkMutation()
            : base("Disable Life Link", "Killing a fighter will NOT damage that fighter's base.", MutationType.DisableLifeLink)
        { }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.DisableLifeLink = true;
        }
    }
}