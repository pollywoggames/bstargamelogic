namespace BarnardsStar.Games.Mutations
{
    public class RandomWhoGoesFirstMutation : BooleanMutation
    {
        public RandomWhoGoesFirstMutation()
            : base("Random Who Goes First", "Which player goes first will be set randomly when the game starts.", MutationType.RandomWhoGoesFirst)
        { }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.RandomWhoGoesFirst = true;
        }
    }
}