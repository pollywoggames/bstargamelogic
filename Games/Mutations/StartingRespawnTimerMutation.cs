﻿using System;
using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class StartingRespawnTimerMutation : MutationPrototype
    {
        public override bool IsRandomable => false;

        public StartingRespawnTimerMutation() :
            base("Starting Respawn Timer",
                "Change how many turns it takes units to respawn the first time they die (default 0).",
                MutationType.StartingRespawnTimer)
        {}

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            return new List<int>()
            {
                0, 1, 2, 3, 4
            };
        }

        public override int GetDefault()
        {
            return 0;
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.StartingRespawnTimer = value;
        }
    }
}