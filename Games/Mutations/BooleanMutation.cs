using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public abstract class BooleanMutation : MutationPrototype
    {
        protected BooleanMutation(string name, string description, MutationType type)
            : base(name, description, type)
        { }

        // A boolean mutation is "true" if it's enabled in the game settings, or "false" if it's not.
        // It's not customizable otherwise, so no options.
        public override bool HasOptions() { return false; }
        public override List<int> GetOptions() { return new List<int>(); }
    }
}