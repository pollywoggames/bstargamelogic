using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class MirrorMatchMutation : BooleanMutation
    {
        public MirrorMatchMutation()
            : base("Mirror Match", "All teams will have the same fighters as the first team.", MutationType.MirrorMatch)
        { }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.MirrorMatch = true;
        }

        public override List<MutationType> GetIncompatible()
        {
            return new List<MutationType>() { MutationType.PreChooseTeams };
        }
    }
}