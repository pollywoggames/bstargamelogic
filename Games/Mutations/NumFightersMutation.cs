using System;
using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class NumFightersMutation : MutationPrototype
    {
        public NumFightersMutation()
            : base("Number of Units", "Change the number of units on each player's team.", MutationType.NumFighters)
        { }

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            var ret = new List<int>();
            for (int i = 2; i <= 8; i++)
            {
                ret.Add(i);
            }
            return ret;
        }

        public override int GetDefault()
        {
            return 4;
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.NumFighters = value;
        }

        public override void Prove(GameSettings gameSettings, int value)
        {
            gameSettings.NumFighters = value;
        }

        public override void Unprove(GameSettings gameSettings)
        {
            // set back to default
            gameSettings.NumFighters = 4;
        }
    }
}