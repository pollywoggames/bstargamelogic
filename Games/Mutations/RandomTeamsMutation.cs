using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class RandomTeamsMutation : BooleanMutation
    {
        public RandomTeamsMutation() : base("Random Units", "Each team's units will be chosen randomly.", MutationType.RandomTeams)
        { }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.RandomTeams = true;
        }

        public override List<MutationType> GetIncompatible()
        {
            return new List<MutationType>() { MutationType.PreChooseTeams };
        }
    }
}