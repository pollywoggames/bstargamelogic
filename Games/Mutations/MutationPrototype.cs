using System;
using System.Collections.Generic;
using System.Linq;

namespace BarnardsStar.Games.Mutations
{
    public abstract class MutationPrototype
    {
        public readonly string name, description;
        public readonly MutationType type;

        // Can this mutation be chosen when creating a custom game?
        public virtual bool IsChooseable => true;
        
        // Can this mutation be chosen on the server when it creates a game with random mutations?
        public virtual bool IsRandomable => true;

        protected MutationPrototype(string name, string description, MutationType type)
        {
            this.name = name;
            this.description = description;
            this.type = type;
        }

        public override string ToString()
        {
            return $"MutationPrototype({type})";
        }

        public abstract bool HasOptions();
        public abstract List<int> GetOptions();

        // In case the options we want when this mutation is randomly chosen are different
        // from all the ones that are available.
        public virtual List<int> GetRandomOptions()
        {
            return GetOptions();
        }

        public virtual int GetDefault()
        {
            return 0;
        }

        // Set this mutation's settings on the actual GameSettings object, so we don't have to go through
        // a list of Mutations during actual gameplay to find out what the option is set to.
        // This is called only when the game starts.
        public abstract void Bake(GameSettings gameSettings, int value);

        // Like baking, but happens each time the mutation is edited, while the game is still in lobby phase.
        // (Before you bake, you have to prove the dough.)
        public virtual void Prove(GameSettings gameSettings, int value)
        {
            // no-op by default.
        }
        
        // Edit game settings when mutation is removed from game.
        public virtual void Unprove(GameSettings gameSettings)
        {
            // no-op by default.
        }

        // Return a list of all incompatible other mutations. When a mutation is added to a game, other incompatible
        // mutations cannot be added.
        //
        // Note: you need to add this on both sides. For example, if mutation A is incompatible with mutation B,
        // mutationA.GetIncompatible() should return [mutationB] AND mutationB.GetIncompatible() should return [mutationA].
        public virtual List<MutationType> GetIncompatible()
        {
            return new List<MutationType>();
        }

        public static MutationPrototype FromType(MutationType type)
        {
            return type switch
            {
                MutationType.NexusHealth => new NexusHealthMutation(),
                MutationType.NumFighters => new NumFightersMutation(),
                MutationType.RandomTeams => new RandomTeamsMutation(),
                MutationType.MirrorMatch => new MirrorMatchMutation(),
                MutationType.DisableLifeLink => new DisableLifeLinkMutation(),
                MutationType.RandomWhoGoesFirst => new RandomWhoGoesFirstMutation(),
                MutationType.AIGetsExtraFighters => new AIGetsExtraFightersMutation(),
                MutationType.UpgradePoints => new UpgradePointsMutation(),
                MutationType.StartingRespawnTimer => new StartingRespawnTimerMutation(),
                MutationType.MaxRespawnTimer => new MaxRespawnTimerMutation(),
                MutationType.PreChooseTeams => new PreChooseTeamsMutation(),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }

        private static List<MutationPrototype> _all;
        public static List<MutationPrototype> All => _all ??= GetAll();
        
        private static List<MutationPrototype> GetAll()
        {
            return Enum.GetValues(typeof(MutationType))
                .Cast<MutationType>()
                .Select(FromType)
                .ToList();
        }
    }
    
    public enum MutationType
    {
        // Do not change the value of existing mutation types.
        // When you add to this list, also add to MutationPrototype.FromType()
        
        NexusHealth = 0,
        NumFighters = 1,
        RandomTeams = 2,
        MirrorMatch = 3,
        DisableLifeLink = 4,
        RandomWhoGoesFirst = 5,
        AIGetsExtraFighters = 6,
        UpgradePoints = 7,
        StartingRespawnTimer = 8,
        MaxRespawnTimer = 9,
        PreChooseTeams = 10,
    }

    public static class MutationTypes
    {
        private static readonly List<MutationType> mutationTypes;

        static MutationTypes()
        {
            mutationTypes = new List<MutationType>();
            
            var mutationTypesArray = Enum.GetValues(typeof(MutationType));
            // mutationTypesArray is in a weird type (Array) so we have to convert it
            foreach (var mutationType in mutationTypesArray)
            {
                mutationTypes.Add((MutationType) mutationType);
            }
        }

        public static MutationType RandomMutationType(Random random, List<MutationType> mutationTypesToExclude)
        {
            var mutationTypesToChooseFrom = 
                MutationPrototype.All
                    .Where(mp => mp.IsRandomable && !mutationTypesToExclude.Contains(mp.type))
                    .Select(mp => mp.type)
                    .ToList();

            return mutationTypesToChooseFrom[random.Next(mutationTypesToChooseFrom.Count)];
        }

        public static int GetSortValue(this MutationType mutationType)
        {
            // these go early
            if (mutationType == MutationType.PreChooseTeams)
                return -20;
            if (mutationType == MutationType.RandomWhoGoesFirst)
                return -10;
            
            if (mutationType == MutationType.DisableLifeLink)
                // this one goes last
                return 999;

            return (int) mutationType;
        }
    }
}