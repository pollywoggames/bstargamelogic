﻿using System;
using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class MaxRespawnTimerMutation : MutationPrototype
    {
        public MaxRespawnTimerMutation() :
            base("Max Respawn Timer",
                "Change the maximum amount of turns it takes units to respawn, after dying several times (default 2).",
                MutationType.MaxRespawnTimer)
        {}

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            return new List<int>()
            {
                0, 1, 2, 3, 4, 5
            };
        }

        public override List<int> GetRandomOptions()
        {
            return new List<int>()
            {
                0, 1
            };
        }

        public override int GetDefault()
        {
            return 2;
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.MaxRespawnTimer = value;
        }
    }
}