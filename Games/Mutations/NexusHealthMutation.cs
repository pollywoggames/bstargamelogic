using System;
using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class NexusHealthMutation : MutationPrototype
    {
        public NexusHealthMutation()
            : base("Base Health", "Change the amount of health each base has.", MutationType.NexusHealth)
        { }

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            return new List<int>()
            {
                5, 10, 15, 20, 30, 40
            };
        }

        public override List<int> GetRandomOptions()
        {
            return new List<int>() { 5, 15, 20 };
        }

        public override int GetDefault()
        {
            return 15;
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.NexusHealth = value;
        }
    }
}