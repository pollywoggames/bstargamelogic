using System.Collections.Generic;
using BarnardsStar.Players;

namespace BarnardsStar.Games.Mutations
{
    public class AIGetsExtraFightersMutation : MutationPrototype
    {
        public override bool IsChooseable => false;
        public override bool IsRandomable => false;

        public AIGetsExtraFightersMutation()
            : base("AI Gets Extra Units", "Give AI players extra units on their team", MutationType.AIGetsExtraFighters)
        { }

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            return new List<int>()
            {
                1, 2, 3, 4
            };
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            gameSettings.aiExtraFighters = value;
        }
        
        // TODO if this is ever added back, probably need to add Prove/Unprove to handle AI pre-choosing team.
    }
}