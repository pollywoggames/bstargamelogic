using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class PreChooseTeamsMutation : BooleanMutation
    {
        public PreChooseTeamsMutation() : base(
                "Pre-Choose Units",
                "All players must choose their units before the game can start.",
                MutationType.PreChooseTeams)
        { }

        public override void Bake(GameSettings gameSettings, int value)
        {
            Prove(gameSettings, value);
        }

        public override void Prove(GameSettings gameSettings, int value)
        {
            gameSettings.PreChooseTeams = true;
        }

        public override void Unprove(GameSettings gameSettings)
        {
            gameSettings.PreChooseTeams = false;
            
            // if any players have already pre-chosen their team, remove the choices
            foreach (var playerSettings in gameSettings.PlayerSettings)
            {
                playerSettings.PreChosenTeam = null;
            }
        }

        public override List<MutationType> GetIncompatible()
        {
            return new List<MutationType>()
            {
                MutationType.RandomTeams,
                MutationType.MirrorMatch
            };
        }
    }
}