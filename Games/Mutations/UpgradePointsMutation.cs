using System.Collections.Generic;

namespace BarnardsStar.Games.Mutations
{
    public class UpgradePointsMutation : MutationPrototype
    {
        public override bool IsChooseable => false;
        public override bool IsRandomable => false;
        
        public UpgradePointsMutation()
            : base("Upgrade Points", "All players get upgrade points for their fighters", MutationType.UpgradePoints)
        { }

        public override bool HasOptions()
        {
            return true;
        }

        public override List<int> GetOptions()
        {
            return new List<int>()
            {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
            };
        }

        public override void Bake(GameSettings gameSettings, int value)
        {
            foreach (var playerSettings in gameSettings.PlayerSettings)
                playerSettings.upgradePointsToSpend = value;
        }
    }
}