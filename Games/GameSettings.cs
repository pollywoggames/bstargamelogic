﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Characters;
using BarnardsStar.Games.Mutations;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Upgrades;
using BarnardsStar.Utilities;
using Newtonsoft.Json;
using Logger = BarnardsStar.Utilities.Logger;

#if UNITY_2020_3_OR_NEWER
using BarnardsStar.Logic.UI.Menus.Choosers;
using BarnardsStar.Attributes;
#else
using BarnardsAscot.Util;
using MongoDB.Bson.Serialization.Attributes;
#endif

namespace BarnardsStar.Games
{
    public enum GameMode
    {
        // synonym for roguelike
        Campaign = -2,
        
        // local/custom game, vs. AI or hotseat (or both)
        Local = -1,
        
        // online games
        PublicFriendly = 0,
        PrivateFriendly = 1,
        Ranked1v1 = 2,
    }

    public static class GameModeExtensions
    {
        public static bool IsLocal(this GameMode gameMode)
        {
            return gameMode == GameMode.Campaign || gameMode == GameMode.Local;
        }
        
        public static bool IsOnline(this GameMode gameMode)
        {
            return !gameMode.IsLocal();
        }

        public static bool IsRanked(this GameMode gameMode)
        {
            // eventually we may have more ranked queues
            return gameMode == GameMode.Ranked1v1;
        }
    }

    public enum TeamsType
    {
        FreeForAll = 0,
        Teams_AllVersusFirstPlayer = 1,
        Teams2v2 = 2,
        Teams3v3 = 3,
        Teams2v2v2 = 4,
        
        // eventually there may also be Teams4v4 and Teams2v2v2v2
        // ... maybe even Teams3v3v3?!? 
    }

    public static class TeamsTypeExtensions
    {
        public static int NumberOfTeams(this TeamsType teamsType, int numPlayers)
        {
            return teamsType switch
            {
                TeamsType.FreeForAll => numPlayers,
                TeamsType.Teams_AllVersusFirstPlayer => 2,
                TeamsType.Teams2v2 => 2,
                TeamsType.Teams3v3 => 2,
                TeamsType.Teams2v2v2 => 3,
                _ => throw new ArgumentOutOfRangeException(nameof(teamsType), teamsType, null)
            };
        }
        
        public static int CalcPlayerTeamIdx(this TeamsType teamsType, int numPlayers, int playerIdx)
        {
            double numPlayersPerTeam = Math.Round(((double) numPlayers) / ((double) teamsType.NumberOfTeams(numPlayers)));
            return (int) Math.Floor(((double)playerIdx) / numPlayersPerTeam);
        }

        public static int NumPlayersOnFirstTeam(this TeamsType teamsType, int numPlayersInGame)
        {
            return (int) Math.Round(((double) numPlayersInGame) / ((double) teamsType.NumberOfTeams(numPlayersInGame)));
        }
        
#if UNITY_2020_3_OR_NEWER
        public static List<EnumChooserChoice> AllChoices(int numPlayers)
        {
            var choiceList = new List<EnumChooserChoice>() 
                { new EnumChooserChoice("Free-For-All", TeamsType.FreeForAll) };

            switch (numPlayers)
            {
                case 0:
                    // show all choices
                    choiceList.Add(new EnumChooserChoice("Teams (2v2)", TeamsType.Teams2v2));
                    choiceList.Add(new EnumChooserChoice("Teams (3v3)", TeamsType.Teams3v3));
                    choiceList.Add(new EnumChooserChoice("Teams (2v2v2)", TeamsType.Teams2v2v2));
                    break;
                
                case 2:
                    // For 2-player games, rename from "Free-For-All" to just "1v1"
                    var ffaChoice = choiceList[0];
                    ffaChoice.name = "1v1";
                    choiceList[0] = ffaChoice;
                    break;
                
                case 3:
                    choiceList.Add(new EnumChooserChoice("Teams (2v1)", TeamsType.Teams2v2));
                    break;
                    
                case 4:
                    choiceList.Add(new EnumChooserChoice("Teams (2v2)", TeamsType.Teams2v2));
                    break;
                
                case 5:
                    choiceList.Add(new EnumChooserChoice("Teams (3v2)", TeamsType.Teams3v3));
                    choiceList.Add(new EnumChooserChoice("Teams (2v2v1)", TeamsType.Teams2v2v2));
                    break;
                    
                case 6:
                    choiceList.Add(new EnumChooserChoice("Teams (3v3)", TeamsType.Teams3v3));
                    choiceList.Add(new EnumChooserChoice("Teams (2v2v2)", TeamsType.Teams2v2v2));
                    break;
                
                default:
                    Logger.Warn($"TeamsTypeExtensions.AllChoices: unexpected numPlayers value: {numPlayers}. Returning free-for-all as only option.");
                    break;
            }

            return choiceList;
        }
#endif
    }
    
    public class GameSettings
    {
        #region Common Fields
        // These values are common to all game types

        // the procedurally-generated name of the game
        public string Name { get; set; }
       
        // player index of the player who goes first.
        // Or -1 for random.
        public int whoGoesFirst = 0;

        public bool PreChooseTeams;
        
        public List<PerPlayerGameSettings> PlayerSettings;

        public int NumPlayers;

        public TeamsType teamsType;

        public int StartingRespawnTimer = 0;
        public int MaxRespawnTimer = 2;

        public string GameId { get; set; }

        public int aiExtraFighters;

        public bool RandomMap { get; set; }
        public string MapId, MapName;
        
        public MapBackground MapBackground;

        public GameMode GameMode { get; set; }
        

        #endregion
        
        // ---------------------------------------------------------------
        
        #region Local-Only Fields
        // These values are only relevant to local (custom/pass 'n' play or skirmish vs. AI) games

        // Whether to save the game after each turn. (False for tutorials.)
        [BsonIgnore]
        public bool SaveGame;
        
        #endregion
        
        #region Mutations
        
        // A list of mutation objects, for use while the game settings are still editable
        public List<Mutation> Mutations = new List<Mutation>();

        // Once the game starts, all the mutations are "baked" into the following real settings
        public int NexusHealth = 10;
        public int NumFighters = 4;
        public bool RandomTeams;
        public bool MirrorMatch;
        public bool DisableLifeLink;
        
        #endregion

        // --- methods/properties ---------------------------------------------------
        
        #region methods
        
        [BsonIgnore]
        [JsonIgnore]
        public bool RandomWhoGoesFirst
        {
            get => whoGoesFirst == -1;
            set => whoGoesFirst = (value ? -1 : 0);
        }

        /// <summary>
        /// Calculates the team idx for the given player idx.
        /// 
        /// If teams are enabled, with 2 teams, the first half the players (by index, rounded up) are
        /// on team A, and the second half of the players are on team B.
        /// </summary>
        public int CalcPlayerTeamIdx(int playerIdx)
        {
            return teamsType.CalcPlayerTeamIdx(NumPlayers, playerIdx);
        }

        public bool DoesPlayerNeedToPreChooseTeam(int playerIdx)
        {
            if (!PreChooseTeams || playerIdx == -1)
                return false;
            
            var playerSettings = PlayerSettings?[playerIdx];
            if (playerSettings == null)
                return true;

            return playerSettings.PreChosenTeam == null || playerSettings.PreChosenTeam.Count != NumFighters;
        }

        public bool HasAllPreChosenTeams()
        {
            return PlayerSettings != null &&
                   PlayerSettings.All(p => p?.PreChosenTeam != null && p.PreChosenTeam.Count == NumFighters);
        }
        
        public GameSettings Clone()
        {
            var clone = (GameSettings)MemberwiseClone();
            
            if (PlayerSettings != null)
                clone.PlayerSettings = PlayerSettings.Select(p => p.Clone()).ToList();
            
            return clone;
        }

        public void BakeMutations()
        {
            foreach (var mutation in Mutations)
            {
                mutation.proto.Bake(this, mutation.value);
            }
        }

        protected bool Equals(GameSettings other)
        {
            return whoGoesFirst == other.whoGoesFirst
                && ListUtils.ListsEqual(PlayerSettings, other.PlayerSettings)
                && MapId == other.MapId
                && MapBackground == other.MapBackground
                && NexusHealth == other.NexusHealth
                && SaveGame == other.SaveGame
                && GameId == other.GameId
                && GameMode == other.GameMode
                && RandomMap == other.RandomMap;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GameSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = whoGoesFirst;
                hashCode = (hashCode * 397) ^ (PlayerSettings != null ? PlayerSettings.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MapId != null ? MapId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)MapBackground;
                hashCode = (hashCode * 397) ^ NexusHealth;
                hashCode = (hashCode * 397) ^ SaveGame.GetHashCode();
                hashCode = (hashCode * 397) ^ (GameId != null ? GameId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)GameMode;
                hashCode = (hashCode * 397) ^ RandomMap.GetHashCode();
                return hashCode;
            }
        }
        
        #endregion
    }
    
    [Serializable]
    public class PerPlayerGameSettings
    {
        // If false, this player will be controlled by AI. (whether locally or on the server.)
        public bool IsHuman;
        
        // AI difficulty level, if this player is a CPU
        public AIType aiDifficulty;
        
        public List<FighterPrototype> PreChosenTeam;

        // Map of FighterPrototype to their upgrades.
        public Dictionary<FighterPrototype, List<UpgradeType>> fighterUpgrades;

        public int upgradePointsToSpend;

        public int? maxRespawnTimer;

        public PerPlayerGameSettings Clone()
        {
            var clone = (PerPlayerGameSettings) MemberwiseClone();

            if (PreChosenTeam != null)
                clone.PreChosenTeam = PreChosenTeam.Select(p => p).ToList();

            if (fighterUpgrades != null)
            {
                clone.fighterUpgrades = new Dictionary<FighterPrototype, List<UpgradeType>>();
                foreach (var pair in fighterUpgrades)
                    clone.fighterUpgrades.Add(pair.Key, pair.Value);
            }

            return clone;
        }

        public override string ToString()
        {
            return
                $"PerPlayerGameSettings(IsHuman={IsHuman}," +
                $"aiDifficulty={aiDifficulty}," +
                $"PreChosenTeam={(PreChosenTeam == null ? "null" : string.Join(",", PreChosenTeam))})";
        }
    }
}