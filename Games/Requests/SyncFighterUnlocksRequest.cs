using System.Collections.Generic;
using BarnardsStar.Characters;

namespace BarnardsStar.Games.Requests
{
    public class SyncFighterUnlocksRequest
    {
        // The fighters unlocked on the client that should also be unlocked on the server
        public List<FighterPrototype> UnlockedFighters { get; set; }
    }
}