﻿using System.Collections.Generic;

namespace BarnardsStar.Games.Requests
{
    public class SubmitTurnRequest
    {
        public string gameId;
        public ChosenTeam chosenTeam;
        public List<Choice> turnChoices { get; set; }

        // The client sends what it believes is the right game state. Then the server compares against that game
        // state and alerts if there are any differences.
        public Game expectedGameState;
    }
}