namespace BarnardsStar.Games.Requests
{
    public class PlayerGameLobbyRequest
    {
        public string gameLobbyId;
        public string playerId;
    }
}