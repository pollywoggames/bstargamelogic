using System.Collections.Generic;
using BarnardsStar.Characters;

namespace BarnardsStar.Games.Requests
{
    public class SyncFighterUnlocksResponse
    {
        // The fighters unlocked on the server that should also be unlocked on the client
        public List<FighterPrototype> UnlockedFighters { get; set; }
    }
}