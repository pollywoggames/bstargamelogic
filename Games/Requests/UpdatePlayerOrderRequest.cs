﻿using System.Collections.Generic;

namespace BarnardsStar.Games.Requests
{
    public class UpdatePlayerOrderRequest
    {
        public string gameLobbyId;
        public List<string> playerIds;
    }
}