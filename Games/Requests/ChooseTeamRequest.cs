﻿using System.Collections.Generic;
using BarnardsStar.Characters;

namespace BarnardsStar.Games.Requests
{
    public class ChooseTeamRequest
    {
        public string GameId;
        public List<FighterPrototype> fighters;
    }
}