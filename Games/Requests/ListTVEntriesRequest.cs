﻿using System.Collections.Generic;

#if NETCOREAPP
using BarnardsAscot.Services;
#endif

namespace BarnardsStar.Games.Requests
{
    public class ListTVEntriesRequest
#if NETCOREAPP
        : MongoDbService.ListTVEntriesOpts
#endif
    
    {
        // Null - no filter on this field. True - ranked only. False - unranked only.
        public bool? rankedOnly { get; set; }
        public int? numPlayers { get; set; }
        public bool excludeTimeouts { get; set; }
        public int limit { get; set; }
    }
}