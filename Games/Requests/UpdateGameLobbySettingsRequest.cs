namespace BarnardsStar.Games.Requests
{
    public class UpdateGameLobbySettingsRequest
    {
        public string gameLobbyId;
        public GameSettings gameSettings;
    }
}