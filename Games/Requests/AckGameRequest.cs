namespace BarnardsStar.Games.Requests
{
    public class AckGameRequest
    {
        public string GameId { get; set; }
    }
}