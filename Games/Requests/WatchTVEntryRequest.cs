namespace BarnardsStar.Games.Requests
{
    public class WatchTVEntryRequest
    {
        public string GameId { get; set; }
    }
}