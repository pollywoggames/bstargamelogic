﻿namespace BarnardsStar.Games.Requests
{
    public class JoinGameRequest
    {
        public string GameId { get; set; }
        public string SteamSessionId { get; set; }
    }
}
