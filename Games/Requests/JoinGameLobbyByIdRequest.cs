﻿namespace BarnardsStar.Games.Requests
{
    public class JoinGameLobbyByIdRequest
    {
        public string GameId { get; set; }
    }
}
