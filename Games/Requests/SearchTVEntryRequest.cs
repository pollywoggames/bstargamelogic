namespace BarnardsStar.Games.Requests
{
    public class SearchTVEntryRequest
    {
        public string GameId { get; set; }
    }
}