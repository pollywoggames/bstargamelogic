using System;
using System.Collections.Generic;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Games
{
    public struct Choice : IEquatable<Choice>
    {
        public string unitSelectedId;
        public ChoiceType type;
        public Coords target;
        public List<Coords> moves;
        public BSAbility? selectedAbility;
        public int? overridePlayerIdx;
        
        public Choice(string unitSelectedId, ChoiceType type, Coords target, BSAbility? ability = null)
        {
            this.unitSelectedId = unitSelectedId;
            this.type = type;
            this.target = target;
            this.moves = null;
            selectedAbility = ability;
            
            this.overridePlayerIdx = null;
        }
        
        public Choice(string unitSelectedId, ChoiceType type, List<Coords> moves)
        {
            this.unitSelectedId = unitSelectedId;
            this.type = type;
            this.moves = moves;

            if (this.moves != null && this.moves.Count > 0)
                this.target = this.moves[^1];
            else
                this.target = Coords.zero;
            
            this.overridePlayerIdx = null;
            this.selectedAbility = null;
        }

        public Choice(Fighter unitSelected, ChoiceType type, Coords target, BSAbility? ability = null)
            : this(unitSelected.id, type, target, ability)
        { }

        public Choice(ChoiceType type, int? overridePlayerIdx)
        {
            this.type = type;
            this.overridePlayerIdx = overridePlayerIdx;
            
            this.selectedAbility = null;
            this.target = Coords.zero;
            this.unitSelectedId = "";
            this.moves = null;
        }

        public override string ToString()
        {
            string selectedAbilityStr = type == ChoiceType.Ability && selectedAbility.HasValue
                ? ",selectedAbility:" + selectedAbility.Value.ToString()
                : "";
            
            var movesStr = moves != null && moves.Count > 0 ? $",moves:{string.Join(",", moves)}" : "";

            return $"Choice(unit:{unitSelectedId},type:{type},target:{target}{selectedAbilityStr}{movesStr})";
        }

        public Choice CloneForGame(Game game)
        {
            var clone = (Choice) MemberwiseClone();
            return clone;
        }

        public bool Equals(Choice other)
        {
            return Equals(unitSelectedId, other.unitSelectedId) && type == other.type && target.Equals(other.target) && Equals(overridePlayerIdx, other.overridePlayerIdx);
        }

        public override bool Equals(object obj)
        {
            return obj is Choice other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = unitSelectedId.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) type;
                hashCode = (hashCode * 397) ^ target.GetHashCode();

                int overridePlayerIdxNum = overridePlayerIdx.HasValue ? overridePlayerIdx.Value : 397;
                hashCode = (hashCode * 397) ^ overridePlayerIdxNum.GetHashCode();
                
                return hashCode;
            }
        }
    }

    public enum ChoiceType
    {
        Movement, Attack, Ability, Surrender
    }
}
