﻿using System.Collections.Generic;
using BarnardsStar.Characters;

namespace BarnardsStar.Games
{
    public class ChosenTeam
    {
        public Team team;
        public List<FighterPrototype> fighterChoices = new List<FighterPrototype>();

        public ChosenTeam()
        { }

        public ChosenTeam(Team team, List<FighterPrototype> fighterChoices)
        {
            this.team = team;
            this.fighterChoices = fighterChoices;
        }

        public ChosenTeam CloneWithTeam(Team newTeam)
        {
            return new ChosenTeam(newTeam, fighterChoices);
        }
    }
}