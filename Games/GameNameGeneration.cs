using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Characters;

namespace BarnardsStar.Games
{
    public static class GameNameGeneration
    {
        private static Random random = new Random();

        private static List<string> TopLevel = new List<string>()
        {
            "The {nounOf} of {place}",
            "The {epicAdj} {madeUpNoun}",
            "The {color} {madeUpNoun}",
            "The {place} {something}",
            "The {epicAdj} {thing}",
            "{preposition} {place}",
            "The {groupOfPeople} from {place}",
            "The {groupOfPeople} of {place}",
            "The {fighter} from {place}",
            "The {color} {nounOf}",
            "The {color} {something}",
            "The {fighter} and the {madeUpNoun}",
            "The {thing} of {place}"
        };

        private static List<string> TopLevel_LessLikely = new List<string>()
        {
            "The {epicAdj} & the {epicAdj}",
            "Barnard's {thing}",
            "The {madeUpNoun}",
        };

        // {nounOf}
        private static List<string> NounOf = new List<string>()
        {
            // Battle words
            "Defense",
            "Attack",
            "Infiltration",
            "Battle",
            "Campaign",
            "Clash",
            "Conflict",
            "Skirmish",
            "War",
            "Struggle",
            "Cataclysm",
            "Assault",
            "Onset",
            "Invasion",
            "Strike",
            "Barrage",
            "Bombardment",
            "Onslaught",
            "Blitz",
            "Crusade",
            "Voyage",

            // Story-invoking words
            "Bane",
            "City",
            "Shadow",
            "Angel",
            "Demons",
            "Disaster",
            "Grief",
            "Legion",
            "Force",
            "Army",
            "Fear",
            "Hornets",
            "Storm",
            "Tempest",
            "Contamination",
            "Provocation",
            "Anomaly",
            "Mistake",
            "Blunder",
            "Aberration",
            "Calculation",
            "Truth",
            "Venom",
            
            // Architectural words
            "Fortress",
            "Citadel",
            "Pyramid",
            "Palace",
            "Fortification",
            "Keep",
            "Hold",
            "Arches",
            "Stronghold",
            "Pillars",
            "Spires",
            "Turrets",
            "Coliseum",
            "Colossus",
            "Canyon",
            "Garden",
            "Crypt",
            "Gorge",

            // Slightly sarcastic/funny words
            "Bustle",
            "Hubbub",
            "Joy",
            "Thrill",
            "Affection",
            "Element",
            "Potion",
            "Celebration",
        };

        // {something}
        private static List<string> Something = new List<string>()
        {
            "Effect",
            "Reaction",
            "Fallout",
            "Station",
            "Intrigue",
            "Incident",
            "Project",
            "Apocalypse",
            "Calamity",
            "Emergency",
            "Guard",
            "Panic",
            "Mission",
            "Gateway",
            "Habitat",
            "Enigma",
            "Offensive",
            "Tremor",
            "Dusk",
            "Dawn",
            "Heist",
            "Miasma",
            "Mass",
            "Conflagration",
            
            // place sounding things
            "Temple",
            "Sanctuary",
            "Shrine",
            "Cathedral",
            "Pantheon",
            "Arena",
            "Stadium",
            "Pit",
            
            // Slightly sarcastic words
            "Drama",
            "Hullabaloo",
            "Disturbance",
        };

        // {epicAdj}
        private static List<string> EpicAdjective = new List<string>()
        {
            "Critical",
            "Deadly",
            "Defiant",
            "Deviant",
            "Anomalous",
            "Insurgent",
            "Unstable",
            "Volatile",
            "Blazing",
            "Scorching",
            "Stellar",
            "Galactic",
            "Atomic",
            "Interstellar",
            "Immortal",
            "Lethal",
            "Dangerous",
            "Fatal",
            "Virulent",
            "Mortal",
            "Immortal",
            "Pernicious",
            "Malignant",
            "Benevolent",
            "Humane",
            "Monstrous",
            "Feral",
            "Gargantuan",
            "Small",
            "Medium-sized",
            "Microscopic",
            "Tiny",
            "Petite",
            "Oversized",
            "Raging",
            "Sole",
            "Oily",
            "Smoky",
            "Acidic",
            "Watery",
            "Incendiary",
            "Explosive",
            "Robotic",
            "Uncontrollable",
            "Unknowable",
        };

        // {place}
        private static List<string> Places = new List<string>()
        {
            "Andromeda",
            "the {color} Star",
            "the {color} Ship",
            "the {color} Planet",
            "the {color} Kingdom",
            "the {color} Empire",
            "the {color} City",
            "Anchorage",
            "the Marauder",
            "the Phoenix",
            "Tomorrow",
            "the Future",
            "the Past",
            "the Sun",
            "the Asteroid",
            "the Unknown",
            "the Black Hole",
            "the White Hole", // https://www.space.com/white-holes.html
            "the {color} Wormhole",
            "the Wormhole",
            "Paradise",
            "Utopia",
            "Eden",
            "Zion",
            "Wonderland",

            // planets
            "Mercury",
            "Venus",
            "Earth",
            "Sol",
            "Mars",
            "Jupiter",
            "Saturn",
            "Uranus",
            "Neptune",
            "Pluto",
            
            // stars & stuff
            "Promixus",
            "Alpha Centauri",
            "Betelgeuse",
            "Cassiopeia",
            "Scorpius",
            "Taurus",
            "Lyra",
            "Ursa Major",
            "Draco",
            "Gemini",
            "Orion",
            "Virgo",
            "Barnard's Star",
            "Regulus",
            "Sirius",
            "Arcturus",
            "Vega",
            "Rigel",
            "Altair",
            "Deneb",
            "Phobos",
            "Deimos",
            "Europa",
            "Ganymede",
            "Io",
            "Callisto",
            "Titan",
            "Hyperion",
        };

        // {color}
        // Also just interesting adjectives
        private static List<string> Colors = new List<string>()
        {
            "Red",
            "Blue",
            "Green",
            "Ultraviolet",
            "Infrared",
            "White-hot",
            "Luminous",
            "Amber",
            "Golden",
            "Dark",
            "Spectral",
            "Carbonic",
            "Fiery",
            "Obsidian",
            "Sable",
            "Sunless",
            "Shadow",
            "Cloudy",
            "Murky",
            "Grim",
            "Crude",
            "Wild",
            "Unknown",
            "Crystal",
            "Lucid",
            "Nightmare",
            "Deathly",
            "Ruby",
            "Sapphire",
            "Ethereal",
            "Radiant",
            "Innocent",
            "Pure"
        };

        // first half of made-up combo words
        private static List<string> Prefixes = new List<string>()
        {
            "Star",
            "Sun",
            "Planet",
            "World",
            "Moon",
            "Acid",
            "Robo",
            "Neuro",
            "Micro",
            "Giga",
            "Mega",
            "Electro",
            "Pyro",
            "Aqua",
            "Oxy",
            "Cloud",
            "Cryo",
            "Necro",
            "Hyper",
            "Hydro",
        };
            
        // second half of made-up combo words
        private static List<string> Suffixes = new List<string>()
        {
            "breaker",
            "burst",
            "fall",
            "world",
            "cannon",
            "mancer",
            "scope",
            "sword",
            "-Eater",
            "shock",
            "core",
            "star",
            "gun",
            "-Being",
            "freeze",
            "killer",
            "bounce",
            "jump",
            "drive",
            "device",
            "toxin",
            "foil",
            "train",
            "badge",
            "shield",
            "fire",
            "splosion",
        };
        
        // {groupOfPeople}
        private static List<string> GroupOfPeople = new List<string>()
        {
            "Monsters",
            "Beasts",
            "Brutes",
            "Monstrosity",
            "Fiend",
            "Demons",
            "Angels",
            "Platoon",
            "Squad",
            "Soldiers",
            "Rascals",
            "Explorers",
            "Settlers",
            "Exo-Colonists",
            "Anti-Settlers",
            "Raiders",
            "Soldiers",
            "Pioneers",
            "Pilgrims",
            "Travellers",
            "Adventurers",
            "Migrants",
            "Semi-Tourists",
            "Nomads",
            "Vanguard",
            "Rearguard",
            "Lovers",
            "Gladiators",
            "Contenders",
            "Athletes",
            "Challengers",
            "Trainees",
            "Rivals",
        };
        
        // {preposition}
        private static List<string> Preposition = new List<string>()
        {
            "Out of",
            "Voyage into",
            "Journey Across",
            "Under",
            "Away from",
            "Towards",
            "Against",
            "Approaching",
            "En Route to",
            "Not Quite",
            "Excursion to",
            "Jaunt across",
            "Jog around",
            "Ramble around",
            "The Safari of",
            "Sally forth to",
            "The Death March of",
            "Run around",
        };

        private static List<string> Starters;
        private static Dictionary<string, Func<string>> ResolutionFuncs;
        
        static GameNameGeneration()
        {
            // populate all fighter names
            var fighters = new List<string>();
            fighters.AddRange(AllFighters.Instance.Humans.Select(fb => fb.Name));
            fighters.AddRange(AllFighters.Instance.Robots.Select(fb => fb.Name));
            fighters.AddRange(AllFighters.Instance.Critters.Select(fb => fb.Name));
            // summonable fighters not included in above
            fighters.Add("Minion");
            fighters.Add("Fly");
            // other person-sounding words
            fighters.Add("General");
            fighters.Add("Commander");
            fighters.Add("Captain");
            fighters.Add("Thief");
            fighters.Add("Bandit");

            ResolutionFuncs = new Dictionary<string, Func<string>>
            {
                { "{nounOf}", () => RandomFromList(NounOf) },
                { "{something}", () => RandomFromList(Something) },
                { "{epicAdj}", () => RandomFromList(EpicAdjective) },
                { "{place}", () => RandomFromList(Places) },
                { "{color}", () => RandomFromList(Colors) },
                { "{fighter}", () => RandomFromList(fighters) },
                { "{madeUpNoun}", MadeUpNoun },
                { "{groupOfPeople}", () => RandomFromList(GroupOfPeople) },
                { "{thing}", () =>
                    {
                        var which = random.Next(3);
                        return which switch
                        {
                            0 => RandomFromList(NounOf),
                            1 => RandomFromList(Something),
                            _ => RandomFromList(fighters)
                        };
                    } },
                { "{preposition}", () => RandomFromList(Preposition) },
            };

            Starters = new List<string>();
            // Main top-level list gets added 2x
            Starters.AddRange(TopLevel);
            Starters.AddRange(TopLevel);
            // Less-likely top level gets added 1x
            Starters.AddRange(TopLevel_LessLikely);
        }

        // Procedurally generates a name for a game
        public static string GenerateName()
        {
            var name = RandomFromList(TopLevel);

            int failsafe = 0;
            while (name.Contains("{") && failsafe++ < 100)
            {
                foreach (var entry in ResolutionFuncs)
                {
                    var match = entry.Key;
                    var resolution = entry.Value;
                    
                    // Does it contain this entry?
                    int idx = name.IndexOf(match, StringComparison.Ordinal);
                    if (idx == -1)
                        continue;

                    name = name.ReplaceRange(idx, match.Length, resolution());
                }
            }

            return Cleanup(name);
        }

        // {madeUpNoun}
        private static string MadeUpNoun()
        {
            return RandomFromList(Prefixes) + RandomFromList(Suffixes);
        }

        private static string RandomFromList(List<string> list)
        {
            return list[random.Next(0, list.Count)];
        }

        private static string Cleanup(string name)
        {
            // clean up "The the"
            int doubleTheIndex = name.ToLowerInvariant().IndexOf("the the", StringComparison.Ordinal);
            if (doubleTheIndex != -1)
            {
                name = name.ReplaceRange(doubleTheIndex, "the the".Length, "The");
            }

            return name;
        }
    }

    public static class StringExtensions
    {
        public static string ReplaceRange(this string original, int startIdx, int length, string replacement)
        {
            return original.Substring(0, startIdx) +
                   replacement +
                   original.Substring(startIdx + length, original.Length - startIdx - length);
        }
    }
}