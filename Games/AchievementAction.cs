namespace BarnardsStar.Games
{
    public enum AchievementAction
    {
        // Renaming or removing elements from this enum will cause serialization to fail on the client, so don't do that.

        WinMultiplayerGame,
        CompleteMultiplayerGameHuman,
        CompleteMultiplayerGameRobot,
        CompleteMultiplayerGameCritter,
        KillFighterWithReadyAttack,
        KillFighterViaElectrocution,
        DestroyCover,
        ExceedActiveDaemonMinionThreshold,
        BurnOilTile,
        WinGameWithOneSurvivor,
        KillAllEnemyFightersInOneTurn,
        KillEnemyFighter,
        DealAcidDamage,
        WinGameWithOneNexusHealth,
        SummonMinions,
        DealDamageWithPushPull,
        HealByAnyMethod,
        KillFighterWithPushPullDownPit,
        StunEnemies,
        ConsumeHazardWithAcid,
        CrossGap,
        UseTeleporter,
        PushOrPullFighterIntoTeleporter,
    }
}