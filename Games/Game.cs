using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.InMemory;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.InMemory;
using BarnardsStar.Characters;
using BarnardsStar.Events;
using BarnardsStar.Hazards;
using BarnardsStar.Utilities;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using Logger = BarnardsStar.Utilities.Logger;
using Path = BarnardsStar.Maps.Path;
using System.Runtime.Serialization;
using System.Text;
using BarnardsStar.Abilities;
using BarnardsStar.Abstract.Contexts.InMemory;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.AI;
using BarnardsStar.Preview.PreviewActions;
using Newtonsoft.Json;
using Random = System.Random;

#if UNITY_EDITOR
using BarnardsStar.Logic.Settings;
#endif

namespace BarnardsStar.Games
{
    [DataContract]
    public class Game
    {
        // caches
        private Dictionary<Coords, Fighter> _fighters;
        private Dictionary<string, Fighter> _fightersById;
        private SimpleCache<Player, List<Player>> cachedEnemies;
        // this is a list because it's organized by player ID (each index is a map of ready attack against that player idx)
        private List<Dictionary<Coords, int>> currTurnReadyAttacks;

        public List<Fighter> FightersReadyAttack;

        public IContextProvider contextProvider;
        
        [JsonIgnore] public bool turnEnded;

        [JsonIgnore] public Game preTurnState;
        public List<Choice> turnChoices;

        [DataMember] public string GameId;
        
        [DataMember] public GameSettings gameSettings;
        [DataMember] public GameMode gameMode;
        
        // Only serialized for testing purposes
        [DataMember] public List<Cheat> cheatsEnabled;

        [DataMember]
        public int randomSeed;

        [DataMember] public GameMap map;

        [DataMember] public List<Player> Players;
        [JsonIgnore] public Player LocalControlledPlayer => Players.FirstOrDefault(p => p.IsLocalControlledPlayer());
        
        [DataMember] public Dictionary<Coords, Hazard> Hazards;
        
        // A dictionary to keep track of which hazards were placed where, during the current action only
        // (gets cleared after each action is finished processing). This prevents hazards from double-dipping
        // on the same coords. For example, if there is oil on a square, and Oiler uses flamethrower on the
        // square next to it _and_ that square, it'll do 2x damage. This dictionary corrects that double damage.
        public Dictionary<Coords, HazardType> HazardsPlacedThisAction = new Dictionary<Coords, HazardType>();
        // Same thing but a separate tracker for electricity
        public HashSet<Coords> CoordsElectrifiedThisAction = new HashSet<Coords>();
        
        [DataMember] public int currPlayerTurn { get; set; }

        [DataMember] public int? winnerTeamIdx;
        [DataMember] public WinCondition winCondition;

        [DataMember] public bool isFirstTurn = true;

        [DataMember] public int turnCount;

        public Randy randy;

        // used by ReadyAttack related stuff
        public Fighter currentlyMovingFighter;
        
        [DataMember] public bool didSpawnPreChosenTeams;
        private bool alreadyCalledUIStartTurn;

        public bool isCurrTurnReplay;
        public bool erroredDuringReplay = false;

        // achievementActionOccurrences is cleared when the player's turn starts
        private Dictionary<AchievementAction, int> achievementActionOccurrences = new Dictionary<AchievementAction, int>();
        
        // semiPersistentAchievementActionOccurrences is NOT cleared when the player's turn starts
        // that is, achievement actions registered semipersistently are counted during replays
        private Dictionary<AchievementAction, int> semiPersistentAchievementActionOccurrences = new Dictionary<AchievementAction, int>();

        // callbacks for various events
        public MapScenario.TutorialAction fighterSelectCallback,
            afterFighterMovedCallback,
            afterFighterAttackedCallback,
            nextTurnStartCallback,
            onWeaponSelectCallback,
            onAbilitySelectCallback,
            afterFighterRespawnedCallback,
            OnDamageNexusDirectlyCallback;
        public MapScenario.FighterTutorialAction onKillCallback;
        public bool playerTwoDoNothingOnFirstTurn = false;
        public Fighter lastFighterWhoRespawned;

        public MapScenario.TriggeredTutorialPopup afterChoiceCallback;

        // Flag indicating that the local player has surrendered, AND the surrender request was already
        // sent to the server so we shouldn't submit the turn again.
        [JsonIgnore] public bool localPlayerSurrendered;
        
        // Flag indicating the local player acked the game already (for replays mainly)
        [JsonIgnore] public bool alreadyAcked;
        
        public bool isHydrated = false;

        [JsonIgnore] private PathfindingMap _pathfindingMap;
        [JsonIgnore] public PathfindingMap pathfindingMap => _pathfindingMap ??= new PathfindingMap(this, currPlayer);

        public List<Player> winners
        {
            get
            {
                return !winnerTeamIdx.HasValue ?
                    null :
                    Players.Where(p => p.TeamIdx == winnerTeamIdx.Value).ToList();
            }
        }
        
        public List<int> winnerIndexes
        {
            get
            {
                return !winnerTeamIdx.HasValue ? null :
                    Players.Where(p => p.TeamIdx == winnerTeamIdx.Value)
                        .Select(p => p.Idx)
                        .ToList();
            }
        }

        public List<Player> losers
        {
            get
            {
                return !winnerTeamIdx.HasValue ?
                    null :
                    Players.Where(p => p.TeamIdx != winnerTeamIdx.Value).ToList();
            }
        }

        public bool gameEnded => winnerTeamIdx.HasValue;

        public Player currPlayer => Players[currPlayerTurn];

        public IAnimationContext animCtx => contextProvider.GetAnimationContext();
        public ICameraController camera => animCtx.Camera();
        public IRunContext runCtx => contextProvider.GetRunContext();
        public ISoundContext soundCtx => contextProvider.GetSoundContext();
        public INetworkContext networkCtx => contextProvider.GetNetworkContext();
        public IUIController uiController => contextProvider.GetUIController();
        public IOperationsContext opsCtx => contextProvider.GetOperationsContext();
        public IAchievementTracker achievementTracker => contextProvider.GetAchievementTracker();
        
        public Game() {
            Hazards = new Dictionary<Coords, Hazard>();
        }

        public Game(IContextProvider contextProvider, GameMap map, List<Player> players, int randomSeed, WinCondition winCondition)
        {
            Players = players;
            this.map = map;
            this.contextProvider = contextProvider;
            Hazards = new Dictionary<Coords, Hazard>();
            randy = new Randy(randomSeed);
            this.winCondition = winCondition;
            
            BuildCacheObjects();
            cheatsEnabled = new List<Cheat>();

            this.map.game = this;

            ConnectChildren();
            RefreshObjects();
        }

        public void Hydrate(IContextProvider contextProvider)
        {
            this.contextProvider = contextProvider;

            BuildCacheObjects();
            ConnectChildren();
            SetVirtualActors();

            foreach (var p in Players)
            {
                foreach (var f in p.Fighters)
                {
                    foreach (var a in f.Abilities)
                    {
                        a.Initialize();
                    }
                }
            }
            isHydrated = true;
        }

        public static Game CreateFromSettings(GameMode gameMode, GameSettings settings, MapPrototype mapPrototype, int randomSeed)
        {
            settings.BakeMutations();
            
            var map = mapPrototype.GameMap;
            
            // Make sure that player settings is set to something, set it to defaults if it's not
            settings.PlayerSettings ??= new List<PerPlayerGameSettings>();
            for (int i = settings.PlayerSettings.Count; i < settings.NumPlayers; i++)
            {
                settings.PlayerSettings.Add(new PerPlayerGameSettings()
                {
                    IsHuman = true,
                    PreChosenTeam = null
                });
            }
            
            // make sure PlayerSettings isn't for some reason _longer_ than NumPlayers
            if (settings.PlayerSettings.Count > settings.NumPlayers)
            {
                settings.PlayerSettings = settings.PlayerSettings.Take(settings.NumPlayers).ToList();
            }
            
            // initialize players
            var players = new List<Player>();
            for (int i = 0; i < settings.PlayerSettings.Count; i++)
            {
                var playerSettings = settings.PlayerSettings[i];
                var spawnInfo = mapPrototype.PlayerInfos[i];

                var nexus = new Nexus(null, settings.NexusHealth, spawnInfo.NexusIsShip, spawnInfo.NexusFacingLeft);
                nexus.SetCenterCoords(spawnInfo.NexusCenterPoint);

                int teamIdx = settings.CalcPlayerTeamIdx(i);
                TeamColor teamColor = TeamColorExtensions.GetTeamColorByPlayerIdx(i, settings.NumPlayers, settings.teamsType);

                var numFightersThisPlayer = settings.NumFighters + (playerSettings.IsHuman ? 0 : settings.aiExtraFighters);

                var player = new Player(
                    i,
                    teamIdx,
                    !playerSettings.IsHuman,
                    nexus,
                    teamColor,
                    spawnInfo.SpawnPoint,
                    numFightersThisPlayer)
                {
                    aiType = playerSettings.aiDifficulty
                };
                
                if (settings.PreChooseTeams && playerSettings.PreChosenTeam != null && playerSettings.PreChosenTeam.Count > 0)
                {
                    player.HasChosenTeam = true;
                    var team = AllFighters.Instance.All[playerSettings.PreChosenTeam.First()].team;
                    player.chosenTeam = new ChosenTeam(team, playerSettings.PreChosenTeam);
                }
                
                players.Add(player);
            }

            if (settings.whoGoesFirst == -1)
            {
                var random = new Random();
                settings.whoGoesFirst = random.Next(0, settings.NumPlayers);
            }
            
            var game = new Game
            {
                gameSettings = settings,
                gameMode = gameMode,
                map = map,
                Players = players,
                winCondition = WinCondition.DestroyEnemyNexus,
                randomSeed = randomSeed,
                currPlayerTurn = settings.whoGoesFirst,
                GameId = settings.GameId,
            };
            game.ConnectChildren();
            return game;
        }

        public void ConnectChildren()
        {
            if (Players != null)
            {
                foreach (var p in Players)
                {
                    p.game = this;
                }
            }
            map.game = this;
        }
        
        public MapBackground GetMapBackground()
        {
            if (gameSettings != null)
                return gameSettings.MapBackground;
            
            return MapBackground.NovaBlue;
        }

        public void BuildCacheObjects()
        {
            _fighters = new Dictionary<Coords, Fighter>();
            _fightersById = new Dictionary<string, Fighter>();
            FightersReadyAttack = new List<Fighter>();
            cachedEnemies = new SimpleCache<Player, List<Player>>();
            cheatsEnabled = new List<Cheat>();
        }

        public void SetVirtualActors()
        {
            RefreshObjects();
            foreach (var player in Players)
            {
                foreach (var f in player.Fighters)
                {
                    // Commenting out for now, while making a breaking change even when it's not
                    // a new season. Hopefully commenting this out makes replays not be so broken.
                    // if (AllFighters.Instance.All.TryGetValue(f.Prototype, out var blueprint))
                    // {
                    //     // Set weapon/ability objects correctly based on AllFighters specification.
                    //     // If fighters' weapons/abilities change (due to balance changes) then this
                    //     // will pick up those changes for existing games.
                    //     f.weapon = blueprint.EquippedWeapon;
                    //     f.AbilityTypes = blueprint.abilities.ToList();
                    // }

                    IFighterVirtualActor oldVA = null;
                    if (f.virtualActor != null && !f.virtualActor.IsDestroyed())
                    {
                        // If it already has one, destroy it, but only after we make a new one
                        // (since getting fighter position is dependent upon virtual actor)
                        oldVA = f.virtualActor;
                    }

                    f.virtualActor = contextProvider.ProvideFighterVirtualActor(f.pos);

                    oldVA?.Obliterate();

                    f.virtualActor.Init(f, true);
                    f.InvokeStateChangeCallbacks();
                }

                if (player.nexus != null)
                {
                    if (player.nexus.virtualActor == null
                        || player.nexus.virtualActor.IsDestroyed()
                        || (contextProvider.GetContextType() == ContextType.Unity && !player.nexus.virtualActor.IsReal()))
                    {
                        player.nexus.virtualActor = contextProvider.ProvideNexusVirtualActor(player.nexus.CenterCoords);
                    }

                    player.nexus.virtualActor.Init(player.nexus);
                    player.nexus.virtualActor.RefreshSprite(player.team, player.teamColor);

                    if (player.OutOfGame)
                    {
                        player.nexus.virtualActor.Hide();
                        
                        // instantiate blast mark
                        contextProvider.ProvideProp(
                            MovablePropType.NexusExplodedBlastMark,
                            player.nexus.CenterCoords.ToPoint());
                    }
                }
            }

            foreach (var entry in Hazards)
            {
                var hazard = entry.Value;
                hazard.virtualActor = contextProvider.ProvideHazardVirtualActor(hazard.type, entry.Key);
                hazard.virtualActor.Init(hazard);
                hazard.virtualActor.RefreshSprite();
            }

            foreach (var teleporter in map.teleporters)
            {
                teleporter.virtualActor = teleporter.isBurrow ?
                    contextProvider.ProvideBurrowVirtualActor(teleporter.Owner) :
                    contextProvider.ProvideTeleporterVirtualActor(teleporter.channel);

                teleporter.virtualActor.Init(teleporter);
            }

            map.virtualActor = contextProvider.ProvideMapVirtualActor();
            map.virtualActor.Init(map);
        }

        public void ObliterateVirtualActors()
        {
            RefreshObjects();
            foreach (var player in Players)
            {
                if (player.nexus != null && player.nexus.hasVirtualActor)
                    player.nexus.virtualActor.Obliterate();

                foreach (var f in player.Fighters)
                    f.virtualActor.Obliterate();
            }

            foreach (var hazard in Hazards.Values)
                hazard.virtualActor.Obliterate();
            
            foreach (var teleporter in map.teleporters)
                teleporter.virtualActor.Obliterate();
            
            map.virtualActor.Obliterate();
        }

        public Game CloneForPreviews()
        {
            return Clone(new PreviewContextProvider());
        }

        public Game CloneInMemory()
        {
            return Clone(new InMemoryContextProvider());
        }

        public Game Clone(IContextProvider _contextProvider)
        {
            // Start with a shallow copy of all fields
            var gameClone = (Game) MemberwiseClone();
            
            // switch context provider
            gameClone.contextProvider = _contextProvider;  
            
            // new copy of the pointer members
            gameClone._fighters = new Dictionary<Coords, Fighter>();
            gameClone._fightersById = new Dictionary<string, Fighter>();
            gameClone.cachedEnemies = new SimpleCache<Player, List<Player>>();
            gameClone.HazardsPlacedThisAction = new Dictionary<Coords, HazardType>();
            gameClone.CoordsElectrifiedThisAction = new HashSet<Coords>();
            gameClone.randy = new Randy();
            gameClone.preTurnState = null;
            if (gameSettings != null) gameClone.gameSettings = gameSettings.Clone();
            gameClone.achievementActionOccurrences = new Dictionary<AchievementAction, int>();
            gameClone.semiPersistentAchievementActionOccurrences = new Dictionary<AchievementAction, int>();
            gameClone.lastFighterWhoRespawned = null;
            gameClone.afterChoiceCallback = null;
            gameClone._pathfindingMap = null;

            // copy state of the appropriate pointer members
            gameClone.cheatsEnabled = cheatsEnabled == null ? null : new List<Cheat>(cheatsEnabled);

            // Clear all caches
            gameClone.ClearCaches();
            
            // deep clone players & fighters
            gameClone.Players = Players.Select(p => DeepClonePlayer(gameClone, p)).ToList();
            // now we can populate ready attack list
            gameClone.FightersReadyAttack = new List<Fighter>();
            gameClone.RefreshObjects();
            
            // Clone map state.
            // Note: teleporters need a reference to the cloned players, so this has to
            // happen *after* the players are cloned. 
            gameClone.map = map.Clone(gameClone);
            
            // Clone hazard state
            gameClone.Hazards = CopyHazards(gameClone);
            
            // new list, same choices
            gameClone.turnChoices = new List<Choice>();
            if (turnChoices != null)
            {
                foreach (var choice in turnChoices)
                    gameClone.turnChoices.Add(choice);
            }

            gameClone.achievementActionOccurrences =
                new Dictionary<AchievementAction, int>(achievementActionOccurrences);
            gameClone.semiPersistentAchievementActionOccurrences =
                new Dictionary<AchievementAction, int>(semiPersistentAchievementActionOccurrences);

            gameClone.RefreshObjects();
            
            if (_contextProvider is PreviewContextProvider pcp)
                pcp.InitFighterPositions(gameClone);
            
            return gameClone;
        }

        private Dictionary<Coords, Hazard> CopyHazards(Game newParentGame)
        {
            Dictionary<Coords, Hazard> ret = new Dictionary<Coords, Hazard>();

            // do it one player at a time so we can figure out the right owners
            int i = 0;
            foreach (var p in Players)
            {
                foreach (var h in Hazards.Values)
                {
                    if (h.owner == p)
                    {
                        var clone = h.Clone();
                        clone.owner = newParentGame.Players[clone.owner.Idx];
                        ret.Add(clone.coords, clone);
                    }
                }
                i++;
            }

            return ret;
        }

        public void ClearCaches()
        {
            _fighters.Clear();
            _fightersById.Clear();
            cachedEnemies.Clear();
            currTurnReadyAttacks = null;
        }

        public Player DeepClonePlayer(Game g, Player p)
        {
            var playerClone = p.Clone();
            playerClone.game = g;
            
            playerClone.Fighters = playerClone.Fighters.Select(f => DeepCloneFighter(g, playerClone, f)).ToList();

            if (p.nexus != null)
            {
                playerClone.nexus = p.nexus.Clone();
                playerClone.nexus.owner = playerClone;
            }

            // clear AIManager instance, it'll get rebuilt as needed
            playerClone.aiManager = null;

            return playerClone;
        }

        public Fighter DeepCloneFighter(Game g, Player p, Fighter f)
        {
            var fighterClone = f.Clone();

            fighterClone.Owner = p;
            if (f.virtualActor != null && !f.virtualActor.IsDestroyed())
            {
                fighterClone.virtualActor = InMemoryFighterVirtualActor.CopyFrom(f.virtualActor);
            }
            else
            {
                fighterClone.virtualActor = new InMemoryFighterVirtualActor(f.pos);
            }
            
            fighterClone.ClearCaches();

            return fighterClone;
        }

        public void RefreshObjects()
        {
            _fighters ??= new Dictionary<Coords, Fighter>();
            _fighters.Clear();

            FightersReadyAttack ??= new List<Fighter>();
            FightersReadyAttack.Clear();
            
            foreach (var p in Players)
            {
                foreach (var f in p.LiveFighters)
                {
                    _fighters.TryAdd(f.coords, f);

                    if (f.ReadyAttacks > 0)
                        FightersReadyAttack.Add(f);
                }
            }

            _fightersById ??= new Dictionary<string, Fighter>();
            _fightersById.Clear();
            foreach (var f in _fighters.Values)
            {
                if (!_fightersById.ContainsKey(f.id))
                {
                    _fightersById.Add(f.id, f);
                }
            }
        }

        public Fighter FighterById(string id)
        {
            if (_fightersById.TryGetValue(id, out var f))
                return f;

            return null;
        }

        public Fighter FighterAt(Coords c)
        {
            _fighters.TryGetValue(c, out var f);
            return f;
        }
        
        public bool IsFighterAt(Coords c)
        {
            return _fighters.ContainsKey(c);
        }

        public bool TryGetFighter(Coords c, out Fighter f)
        {
            return _fighters.TryGetValue(c, out f);
        }

        public bool CollideAt(Coords c)
        {
            if (FighterAt(c) != null)
                return true;

            var terrainType = map.GetTerrainType(c);
            return terrainType == TerrainType.Wall || terrainType == TerrainType.Cover;
        }

        public IEnumerator AddHazard(Coords c, Hazard h, Fighter origin)
        {
            if (TryGetHazard(c, out var alreadyThere))
            {
                alreadyThere.virtualActor.Remove();
                Hazards[c] = h;
            }
            else
            {
                Hazards.Add(c, h);
            }

            // deal damage to any fighter standing there (unless it's explosive, in which case the
            // damage is dealt when exploding, not when placing)
            if (h.damage > 0 && !h.explosive && TryGetFighter(c, out var f))
            {
                yield return h.TryDamageFighter(f, origin);
            }
            
            // If the hazard has a slow effect, slow any fighter standing there
            if (h.slows
                && TryGetFighter(c, out var fighterToSlow)
                && !fighterToSlow.HasStatusEffect(StatusEffect.Slowed)
                && !fighterToSlow.Immunities.Contains(h.type))
            {
                fighterToSlow.AddStatusEffect(StatusEffect.Slowed, 1, origin);
            }
            
            if (h.sound.HasValue)
                soundCtx.PlayOneShot(h.sound.Value);

            if (h.sound.HasValue)
                soundCtx.PlayOneShotVaryPitch(h.sound.Value);
            else if (h.sounds != null)
                soundCtx.PlayOneShotVaryPitch(randy.RandomInList(h.sounds));

            if (contextProvider.IsPreviewing())
            {
                contextProvider.RegisterPreview(new HazardPreview(this, c, h.type));
            }

            // Now that it's in the game state, refresh its sprite & the ones around it
            h.virtualActor.RefreshSprite();
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var dst = c + dir;
                if (TryGetHazard(dst, out var hazard))
                    hazard.virtualActor.RefreshSprite();
            }
            
            h.virtualActor.DoStartEffect();
        }
        
        public bool TryGetHazard(Coords c, out Hazard h)
        {
            return Hazards.TryGetValue(c, out h);
        }

        public void RemoveHazard(Hazard h)
        {
            if (h.virtualActor != null && !h.virtualActor.IsDestroyed())
                h.virtualActor.Remove();
            Hazards.Remove(h.coords);
            
            // Refresh the sprites of the ones around this one
            foreach (var dir in Coords.OrthogonalDirections)
            {
                var dst = h.coords + dir;
                if (TryGetHazard(dst, out var hazard))
                {
                    if (hazard.virtualActor != null && !hazard.virtualActor.IsDestroyed())
                        hazard.virtualActor.RefreshSprite();
                }
            }
        }

        public bool TryGetDamageable(Coords atCoords, out IDamageable damageable)
        {
            damageable = null;

            foreach (var p in Players)
            {
                if (p.nexus != null && p.nexus.GetTargetableGridCoords().Contains(atCoords))
                {
                    damageable = p.nexus;
                    return true;
                }
            }

            if (TryGetFighter(atCoords, out var f))
            {
                damageable = f;
                return true;
            }

            return false;
        }

        public List<Player> EnemiesOf(Player player)
        {
            return cachedEnemies.Get(player, key =>
            {
                return Players.Where(p => p.IsEnemyOf(key)).ToList();
            });
        }
        
        public Dictionary<Coords, IDamageable> AllDamageables()
        {
            var ret = new Dictionary<Coords, IDamageable>();
            foreach (var p in Players)
            {
                AddAllDamageables(ret, p);
            }

            return ret;
        }

        public Dictionary<Coords, IDamageable> AttackablesFor(Player p)
        {
            var attackables = new Dictionary<Coords, IDamageable>();
            foreach (var enemy in EnemiesOf(p))
            {
                AddAllDamageables(attackables, enemy);
            }

            return attackables;
        }

        private void AddAllDamageables(Dictionary<Coords, IDamageable> damageables, Player p)
        {
            if (p.nexus != null)
            {
                foreach (var coords in p.nexus.GetTargetableGridCoords())
                {
                    if (!damageables.ContainsKey(coords))
                        damageables.Add(coords, p.nexus);
                }
            }

            foreach (var f in p.LiveFighters)
            {
                if (!damageables.ContainsKey(f.coords))
                    damageables.Add(f.coords, f);
            }
        }

        public IEnumerator Resolve(AbstractEvent evt)
        {
            yield return runCtx.Run(evt.Process());
        }

        public IEnumerator OnFighterPushed(Fighter f, Fighter pusher)
        {
            yield return OnFighterMoved(f, false, null, pusher);
        }

        // prevTpedChans: a list of teleporters this fighter has already teleported through in the current move.
        // Used to prevent cyclical, never-ending teleporter loops.
        public IEnumerator OnFighterMoved(Fighter f, bool middleOfMove = false, List<int> prevTpedChans = null, Fighter pusher = null, bool preventHazardDamage = false)
        {
            RefreshObjects();
            
            // check if we wake up any enemies
            var enemyPlayers = f.Owner.Enemies;
            var enemyFighters = enemyPlayers.SelectMany(p => p.LiveFighters).ToList();
            foreach (var fighter in enemyFighters)
            {
                if (fighter.Asleep && Coords.Distance(f.coords, fighter.coords) <= Fighter.WakeupRadius)
                    fighter.SetAsleep(false);
            }
            
            // preventHazardDamage is a flag so if OnFighterMoved is called twice (for shouldTeleport reasons)
            // we don't damage the fighter twice.
            if (!preventHazardDamage && TryGetHazard(f.coords, out var h) && (h.slows || h.CanDamageFighter(f)))
            {
                if (h.explosive)
                {
                    // If there's a pusher, set that as the triggerer so they can get an extra turn
                    // if this explosion kills the victim. If not, the fighter must've walked into
                    // the hazard themselves, so they are the triggerer. 
                    yield return h.Explode(pusher ?? f);
                }
                else if (h.damage > 0)
                {
                    yield return h.TryDamageFighter(f, pusher);
                }

                if (h.slows
                    && !f.HasStatusEffect(StatusEffect.Slowed)
                    && !f.Immunities.Contains(HazardType.Oil))
                {
                    f.AddStatusEffect(StatusEffect.Slowed, 1, pusher ?? f);
                }
            }
            
            if (map.GetTerrainType(f.coords) == TerrainType.Pit)
            {
                yield return f.FallDownHole(pusher);
            }
            
            yield return CheckReadyAttacks(f);
            
            if (map._teleportersByCoords.TryGetValue(f.coords, out var teleporter))
            {
                if (teleporter.isBurrow)
                    yield return FighterMovedOntoBurrow(f, middleOfMove, prevTpedChans, teleporter, pusher);
                else
                    yield return FighterMovedOntoTeleporter(f, middleOfMove, prevTpedChans, teleporter, pusher);
            }
        }

        private IEnumerator FighterMovedOntoTeleporter(Fighter f, bool middleOfMove, List<int> prevTpedChans, Teleporter teleporter, Fighter pusher)
        {
            // Don't teleport if fighter is in the middle of a move.
            if (middleOfMove)
                yield break;

            // Don't teleport on a channel that the fighter has already teleported on during this move. 
            if (prevTpedChans != null && prevTpedChans.Contains(teleporter.channel))
                yield break;
            // Add current teleporter channel to list to prevent future cyclical tps.
            prevTpedChans ??= new List<int>();
            prevTpedChans.Add(teleporter.channel);
            
            if (map._teleporterPairs.TryGetValue(teleporter, out var tpPair) && tpPair != null)
            {
                SomethingHappened(new BSEvent(BSEventType.UseTeleporter, f.Owner));
                
                // Teleporter has a pair -> regular teleport
                var tpDst = map.ClearCoordsBy(tpPair.coords);
                yield return animCtx.AnimateTeleport(f, tpDst, teleporter.channel);
                yield return OnFighterMoved(f, false, prevTpedChans, pusher);
            }
            else if (winCondition == WinCondition.GetThroughTeleporter)
            {
                // Fake teleporter (no matching one) for tutorial purposes only ... animate fighter
                // teleporting offscreen (actually killing them)

                f.SetMovesLeft(0);
                f.SetActionsLeft(0);

                yield return animCtx.AnimateTeleportOut(f);

                // don't use SetHealth because it triggers the death preview
                f.CurrHealth = 0;

                f.Owner.Fighters.Remove(f);

                // Check if all fighters have gone through teleporter. If so, gg
                if (f.Owner.Fighters.Count == 0)
                {
                    winnerTeamIdx = currPlayer.TeamIdx;
                    contextProvider.GetUIController().GameOver();
                }
            }
        }

        private IEnumerator FighterMovedOntoBurrow(Fighter f, bool middleOfMove, List<int> prevTpedChans, Teleporter burrow, Fighter pusher)
        {
            var burrowPair = map._teleporterPairs[burrow];
            if (burrow.Owner.IsEnemyOf(f.Owner))
            {
                // Stepping onto an enemy player's burrow destroys it and its pair.
                map.RemoveTeleporterAndMaybeBurrowPair(burrow);
                yield break;
            }
            
            // Don't teleport if fighter is in the middle of a move.
            if (middleOfMove)
                yield break;

            // Don't teleport on a channel that the fighter has already teleported on during this move. 
            if (prevTpedChans != null && prevTpedChans.Contains(burrow.channel))
                yield break;
            // Add current teleporter channel to list to prevent future cyclical tps.
            prevTpedChans ??= new List<int>();
            prevTpedChans.Add(burrow.channel);
            
            // Burrow not usable when someone is standing on the other end, or if it's not clear (PE pushed cover onto it)
            if (burrowPair != null && (IsFighterAt(burrowPair.coords) || !map.IsClear(burrowPair.coords)))
                yield break;
            
            if (burrow.OnCooldown)
                yield break;
            
            // it's my own burrow, so teleport to the matching burrow.
            if (burrowPair != null)
            {
                // single teleport movement preview for the whole thing
                if (contextProvider.IsPreviewing())
                    contextProvider.RegisterPreview(new MovementPreviewAction(f, new SingleMove(burrowPair.coords, true)));

                if (f.hasVirtualActor) f.virtualActor.HideAllIndicators();
                f.TempDisableMovementPreviews = true;
                
                // First, burrow to the actual burrow
                if (f.virtualActor != null && !f.virtualActor.IsDestroyed()) f.virtualActor.ForwardInSortingOrder();
                yield return animCtx.AnimateBurrow(f, burrowPair.coords);
                if (f.virtualActor != null && !f.virtualActor.IsDestroyed()) f.virtualActor.BackwardInSortingOrder();
                
                burrow.SetOnCooldown(true);
                burrowPair.SetOnCooldown(true);

                if (f.hasVirtualActor) f.virtualActor.RefreshAllIndicators();
                f.TempDisableMovementPreviews = false;
                
                // Trigger any other on-move effects
                yield return OnFighterMoved(f, false, prevTpedChans, pusher);
            }
            else
            {
                Logger.Error($"Burrow at {burrow.coords} has no pair ... ???");
            }
        }

        // Essentially a "dry run" version of FighterMovedOntoTeleporter/Burrow, used for
        // AI planning.
        public bool FighterWillTeleport(Fighter f, Teleporter t)
        {
            if (!map._teleporterPairs.TryGetValue(t, out var pair))
                // Teleporter/burrow doesn't have a pair, can't teleport
                return false;
        
            if (t.isBurrow)
            {
                if (
                    // can't use enemy burrows
                    t.Owner.IsEnemyOf(f.Owner)
                    // can't use burrows on cooldown
                    || t.OnCooldown
                )
                    return false;
                
                // Burrow not usable when someone is standing on the other end, or if it's not clear (PE pushed cover onto it)
                if (IsFighterAt(pair.coords) || !map.IsClear(pair.coords))
                    return false;
            }
            
            return true;
        }

        public IEnumerator CheckReadyAttacks(Fighter fighterMoved)
        {
            if (CheatEnabled(Cheat.NoReadyAttack))
                yield break;
            
            for (var i = 0; i < FightersReadyAttack.Count; i++)
            {
                var f = FightersReadyAttack[i];
                if (f == null || !f.HasReadyAttacks || f.IsDead)
                {
                    FightersReadyAttack.RemoveAt(i);
                    i--;
                    continue;
                }

                // skip ready attacks against allies
                if (f.IsFriendlyTo(fighterMoved))
                    continue;

                TargetList origAttacks = f.GetWeapon().GetTargets();
                var attack = GetReadyAttack(origAttacks, fighterMoved);
                
                if (!attack.HasValue)
                    continue;

                yield return camera.PanTilPointsVisible(new List<Coords>
                {
                    f.coords,
                    attack.Value.coords
                });
                yield return animCtx.AnimateReadyAttack(f, attack.Value.coords, true);
                
                var atk = new AttackEvent(this, f, attack.Value);
                yield return runCtx.Run(atk.Process());
                 
                if (DecrementFighterReadyAttack(f))
                    i--; // They were just removed from the list we're iterating over, decrement i so we don't skip anyone 
                
                RefreshObjects();
            }
        }

        // Thin the bigger list of attacks to just an attack against the fighter that moved
        private Target? GetReadyAttack(TargetList origAttacks, Fighter fighterMoved)
        {
            foreach (var atk in origAttacks.validTargets)
            {
                if (fighterMoved.coords == atk.coords)
                    return atk;
            }
            return null;
        }

        // Returns whether the fighter was removed from the FightersReadyAttack list
        public bool DecrementFighterReadyAttack(Fighter f)
        {
            f.SetReadyAttacks(f.ReadyAttacks - 1);

            if (!f.HasReadyAttacks)
            {
                int i = FightersReadyAttack.IndexOf(f);
                if (i != -1 && i < FightersReadyAttack.Count)
                {
                    FightersReadyAttack.RemoveAt(i);
                }

                return true;
            }

            return false;
        }
        
        // TODO when we support 3+ player games, this will have to change
        public Dictionary<Coords, int> CurrentReadyAttacksAgainstPlayer(Player p)
        {
            if (currTurnReadyAttacks == null)
                BuildCurrTurnReadyAttacksCache();

            return currTurnReadyAttacks[p.Idx];
        }

        private void BuildCurrTurnReadyAttacksCache()
        {
            currTurnReadyAttacks = new List<Dictionary<Coords, int>>();

            foreach (var player in Players)
            {
                var readyAttacksDict = new Dictionary<Coords, int>();
                currTurnReadyAttacks.Add(readyAttacksDict);

                var readyAttackFightersThisPlayer = FightersReadyAttack.Where(f =>
                    f != null && f.HasReadyAttacks && !f.IsDead && f.Owner.IsEnemyOf(player));
                
                foreach (var f in readyAttackFightersThisPlayer)
                {
                    TargetList attacks = f.GetWeapon().GetReadyAttacks();
                    if (attacks == null)
                        continue;

                    foreach (var atk in attacks.validTargets)
                    {
                        var existingDmg = 0;
                        // If there are multiple fighters covering the same space, add their damages together
                        if (readyAttacksDict.TryGetValue(atk.coords, out var dmg))
                            existingDmg = dmg;
                        
                        readyAttacksDict[atk.coords] = existingDmg + f.GetWeapon().GetDamage();
                    }
                }
            }
        }

        public PathList GetPathList(Fighter f)
        {
            return PathList.GetMovableSpaces(this, f);
        }

        public HashSet<Coords> GetMoves(Fighter f)
        {
            var pathList = GetPathList(f);
            return pathList.paths.Keys.ToSet();
        }

        public Path GetPath(Fighter f, Coords coords)
        {
            var pathList = GetPathList(f);
            return pathList.paths.TryGetValue(coords, out var path) ? path : null;
        }

        public Dictionary<Coords, List<Coords>> GetPossibleMoves(Fighter f)
        {
            return getAllPossibleMovesDict(f)
                .Select(p => (p.Key, p.Value.movesToGetHere))
                .ToDictionary(p => p.Key, p => p.movesToGetHere);
        }

        public Dictionary<Coords, PotentialMove> getAllPossibleMovesDict(Fighter f)
        {
            var possibleMoves = GetAllPossibleMoves(f);
            var byCoords = new Dictionary<Coords, PotentialMove>();

            foreach (var move in possibleMoves)
            {
                var endOfMove = move.movesToGetHere[^1];
                addToPossibleMovesDict(move, endOfMove, byCoords);
                
                if (move.alternateEndingCoords.HasValue)
                    addToPossibleMovesDict(move, move.alternateEndingCoords.Value, byCoords);
            }
            
            return byCoords;
        }

        private void addToPossibleMovesDict(PotentialMove move, Coords endOfMove, Dictionary<Coords, PotentialMove> byCoords)
        {
            if (byCoords.TryGetValue(endOfMove, out var otherMove))
            {
                // There's already a move for this coords. Only use this new one if the cost is better.
                if (move.movementCost < otherMove.movementCost)
                {
                    byCoords[endOfMove] = move;
                }
            }
            else
            {
                byCoords[endOfMove] = move;
            }
        }

        public List<PotentialMove> GetAllPossibleMoves(Fighter f)
        {
            return GetAllPossibleMoves(f.id);
        }

        public List<PotentialMove> GetAllPossibleMoves(string fighterId)
        {
            List<PotentialMove> potentialMoves = new List<PotentialMove>();
            AddAllPossibleMoves(this, fighterId, potentialMoves, null, 0);
            return potentialMoves;
        }

        private static void AddAllPossibleMoves(Game game, string fighterId, List<PotentialMove> potentialMoves, PotentialMove baseMove, int depth)
        {
            if (depth >= 10)
            {
                Logger.Error("AddAllPossibleMoves reached maximum recursive depth, bailing");
                return;
            }
            
            var f = game.FighterById(fighterId);
            var pathList = game.GetPathList(f);
            
            // Logger.Info($"AddAllPossibleMoves {fighterId} @ {f.coords}, found {pathList.paths.Count} moves, baseMove: {baseMove}, depth: {depth}");

            // Convert single moves into multiple -- if there is teleportation involved,
            // the fighter may want to move again after teleporting.
            foreach (var pathPair in pathList.paths)
            {
                var coords = pathPair.Key;
                
                var path = pathPair.Value;

                // Add move
                var potentialMove = baseMove == null ?
                    PotentialMove.FromPath(path) :
                    baseMove.AddPath(path);
                
                potentialMoves.Add(potentialMove);

                if (game.map._teleportersByCoords.TryGetValue(coords, out var tp)
                    && game.FighterWillTeleport(f, tp)
                    && path.GetMoveCost() < f.MovesLeft)
                {
                    // Fighter will teleport, simulate the teleport and add more moves.
                    var gameStatePostMove = game.CloneForPreviews();
                    var previewedGameState = gameStatePostMove.PreviewChoice(new Choice(f, ChoiceType.Movement, coords), false);

                    var fighterPostTeleport = previewedGameState.finalGameState.FighterById(f.id);
                    if (fighterPostTeleport != null)
                    {
                        potentialMove.alternateEndingCoords = fighterPostTeleport.coords;
                        potentialMove.AddSimulatedGameState(previewedGameState);

                        if (!fighterPostTeleport.IsDead && fighterPostTeleport.MovesLeft > 0)
                        {
                            AddAllPossibleMoves(gameStatePostMove, f.id, potentialMoves, potentialMove, depth + 1);
                        }
                    }
                }
            }
        }

        public PreviewedGameState PreviewChoice(Choice choice, bool clone = true, Action<string> errCallback = null)
        {
            Game game = clone ? CloneForPreviews() : this;
            
            var instantRunContext = (InstantRunContext) game.contextProvider.GetRunContext();
            instantRunContext.RunInstantly(game.SubmitChoice(choice, errCallback));
            
            // not sure why this is necessary but it is
            ClearCaches();
            RefreshObjects();
            game.ClearCaches();
            game.RefreshObjects();

            return new PreviewedGameState(game.contextProvider.GetPreviews(this), game);
        }

        public Target? GetTargetAndLinked(Choice choice)
        {
            var unitSelected = FighterById(choice.unitSelectedId);
            TargetList targetList;
            if (choice.type == ChoiceType.Attack)
            {
                targetList = unitSelected.GetWeapon().GetTargets();
            }
            else if (choice.type == ChoiceType.Ability && choice.selectedAbility.HasValue)
            {
                var ability = unitSelected.GetAbility(choice.selectedAbility.Value);
                targetList = ability.GetTargets();
            }
            else
            {
                return null;
            }

            return targetList.GetByCoords(choice.target);
        }

        public IEnumerator SubmitChoice(Choice choice, Action<string> errCallback = null)
        {
            errCallback ??= Logger.Error;
            
            if (gameEnded)
                yield break;

            if (choice.type != ChoiceType.Surrender && String.IsNullOrEmpty(choice.unitSelectedId))
            {
                errCallback("Can't submit choice with null unit selected! " + choice);
                yield break;
            }
            ClearCaches();
            RefreshObjects();
            
            var unitSelected = FighterById(choice.unitSelectedId);
            if (unitSelected == null && choice.type != ChoiceType.Surrender)
            {
                errCallback($"Can't find selected unit {choice.unitSelectedId} for choice {choice}");
                erroredDuringReplay = true;
                yield break;
            }
            
            AbstractEvent evt = null;
            List<Coords> movementList = null;
            
            var target = GetTargetAndLinked(choice);
            if (!target.HasValue && (choice.type == ChoiceType.Ability || choice.type == ChoiceType.Attack))
            {
                errCallback("Invalid target for ability/attack choice: " + choice);
                if (isCurrTurnReplay) erroredDuringReplay = true;
                yield break;
            }

            MapScenario.TutorialAction maybeCallback = null;
            
            switch (choice.type)
            {
                case ChoiceType.Movement:
                    if (choice.moves != null)
                    {
                        // copy list to avoid mutating original
                        movementList = new List<Coords>(choice.moves);
                    }
                    else
                    {
                        movementList = new List<Coords> {choice.target};
                    }

                    maybeCallback = afterFighterMovedCallback;
                    afterFighterMovedCallback = null;
                    
                    break;
                case ChoiceType.Attack:
                    evt = new AttackEvent(
                        this,
                        unitSelected,
                        target.Value
                        );

                    maybeCallback = afterFighterAttackedCallback;
                    afterFighterAttackedCallback = null;
                    
                    break;
                case ChoiceType.Ability:
                    var abObj = unitSelected.Abilities.Find(a => a.type == choice.selectedAbility);
                    if (abObj == null)
                    {
                        throw new Exception($"Selected unit {unitSelected} doesn't have ability {choice.selectedAbility}");
                    }
                    else if (abObj.OnCooldown)
                    {
                        throw new Exception("Can't use ability on cooldown");
                    }
                    
                    evt = new AbilityEvent(
                        this,
                        unitSelected,
                        abObj,
                        target.Value
                    );
                    break;
                case ChoiceType.Surrender:
                    choice.overridePlayerIdx ??= currPlayerTurn;
                    
                    Players[choice.overridePlayerIdx.Value].DidSurrender = true;
                    evt = new SurrenderEvent(this, choice.overridePlayerIdx);

                    // Surrenders can happen out of order - to avoid throwing things off, we need to set currPlayerTurn
                    // to the idx of the surrendering player
                    currPlayerTurn = choice.overridePlayerIdx.Value;
                    
                    break;
            }
            
            turnChoices?.Add(choice);

            while (evt != null || (movementList != null && movementList.Count > 0))
            {
                // Unit may have died during movement, in which case we can bail
                // (unitSelected will be null for surrender events)
                if (unitSelected != null && unitSelected.IsDead)
                    break;
                
                // If movementList has any moves, we'll pop the first one off the list
                // and make that into our movement event.
                if (movementList != null && movementList.Count > 0)
                {
                    var nextMovementDest = movementList[0];
                    movementList.RemoveAt(0);

                    var pathList = GetPathList(unitSelected);
                    if (pathList.paths.TryGetValue(nextMovementDest, out var path))
                    {
                        evt = new MovementEvent(this, path);
                    }
                    else
                    {
                        errCallback($"{unitSelected.id} doesn't have a path to next destination {choice.target}!");
                        if (isCurrTurnReplay) erroredDuringReplay = true;
                        yield break;
                    }
                }
                
                // Actually process the event.
                if (evt != null)
                {
                    yield return runCtx.Run(Resolve(evt));
                    // Set to null so we can maybe break out of the loop, if finished processing
                    evt = null;
                }
            }

            ChoiceFinishedProcessing();

            yield return maybeCallback?.Invoke(this);

            if (afterChoiceCallback != null && afterChoiceCallback.trigger(this))
            {
                var tempCallback = afterChoiceCallback.callback;
                afterChoiceCallback = null;
                yield return tempCallback(this);
            }
        }
        
        public void ChoiceFinishedProcessing()
        {
            HazardsPlacedThisAction.Clear();
            CoordsElectrifiedThisAction.Clear();
            
            foreach (var nexus in Players.Select(p => p.nexus))
            {
                if (nexus != null) nexus.alreadyDamagedDuringThisAction = false;
            }
        }

        public bool IsAnimating()
        {
            return Players.Any(p => p.LiveFighters.Any(f => f.IsAnimating));
        }

        public IEnumerator FighterDied(Fighter f, Fighter killer)
        {
            bool killedEnemy = killer != null && f.Owner.Idx != killer.Owner.Idx;

            SomethingHappened(new BSEvent(BSEventType.KillEnemyFighter, killer));
            
            var opponents = GetMyOpponents();
            if (opponents.Any(p => p.Fighters.Count(fi => !fi.IsDead) == 0))
            {
                SomethingHappened(BSEventType.KillAllEnemyFightersInOneTurn);
            }
            
            RefreshObjects();
            if (FightersReadyAttack.Contains(f))
                FightersReadyAttack.Remove(f);

            if (killer != null && !killer.IsDead)
            {
                // Only refill actions/moves if you killed someone on the enemy team
                if (killedEnemy && killer.Owner.IsTurn)
                {
                    RefillKillerActionsMoves(killer);
                }

                if (onKillCallback != null)
                {
                    runCtx.Start(onKillCallback(this, killer));
                    onKillCallback = null;
                }
            }

            if (!f.WasSummoned && !gameSettings.DisableLifeLink)
            {
                yield return f.Owner.nexus?.DealDamage(1, null, false);
            }

            // Only trigger OnKill if you killed someone on the enemy team
            if (killedEnemy)
            {
                killer.numKills++;
                killer.OnKill?.Invoke();
            }

            if (f.WasSummoned)
            {
                f.Owner.Fighters.Remove(f);
            }
            else
            {
                f.SetRespawnTimer();
            }
            
            CheckGameOver();
        }

        public void RefillKillerActionsMoves(Fighter killer)
        {
            // don't refill actions/moves for a fighter if it's not their turn
            if (!killer.Owner.IsTurn)
                return;
            
            if (!killer.LosesActionNextTurn())
                killer.SetActionsLeft(killer.MaxActions);

            killer.SetMovesLeft(killer.CalcMaxMoves());

            animCtx.PlayVFX(VFXType.ResetOnKill, killer.GetPosition());
        }

        // returns the TeamIdx of the winning team
        public int? GameShouldEnd()
        {
            // Check for the case where only AIs are left
            if (gameMode.IsLocal()
                // Was there was ever a human player in this game? - allows for the case
                // where someone creates a game of just AIs to watch them play against each other
                && Players.Any(p => p.IsHuman)
                // Are the only players left in the game AI players?
                && Players.Where(p => !p.NexusIsDead()).All(p => p.IsAI))
            {
                return Players.First(p => !p.NexusIsDead()).TeamIdx;
            }
            
            // Now check to see if there is only 1 team left
            int? teamFound = null;
            foreach (var player in Players)
            {
                if (player.NexusIsDead())
                    continue;

                if (teamFound.HasValue && player.TeamIdx != teamFound.Value)
                    // game shoudn't end yet
                    return null;

                teamFound = player.TeamIdx;
            }

            // If we got this far, we didn't find more than 1 team
            return teamFound;
        }
        
        public void CheckGameOver()
        {
            if (winCondition == WinCondition.KillAllEnemies)
            {
                foreach (var p in Players.Where(p => !p.LiveFighters.Any()))
                {
                    winnerTeamIdx = p.Enemies[0].TeamIdx;
                    GameOver();
                    return;
                }
            }
        }

        public IEnumerator StartTurn(bool isReplay = false)
        {
            randy = new Randy(randomSeed);
            randomSeed = randy.Next();
            
            isCurrTurnReplay = isReplay;
            
            // just check to make sure, in case we for some reason missed it?
            if (gameEnded)
            {
                GameOver();
                yield break;
            }

            alreadyCalledUIStartTurn = false;

            if (!didSpawnPreChosenTeams && gameSettings.PreChooseTeams && gameSettings.HasAllPreChosenTeams())
                yield return SpawnPreChosenTeams();
            // Set to true even if we didn't do it so we can bypass the check on future turns
            didSpawnPreChosenTeams = true;
            
            if (!currPlayer.HasChosenTeam)
            {
                uiController.ShowEndTurnButton(false);

                yield return camera.PanTilPointVisible(currPlayer.SpawnPoint.ToPoint());

                alreadyCalledUIStartTurn = true;
                yield return uiController.StartTurn(isReplay, true, false);

                yield return runCtx.WaitSeconds(.35f);

                ChooseTeam();

                turnEnded = false;
                
                // we'll call StartTurn again after they've chosen their characters
                yield break;
            }
            
            // Tutorial: player has chosen team but fighters not spawned yet
            if (currPlayer.Fighters.Count == 0)
            {
                yield return currPlayer.ChooseTeamAndSpawn(currPlayer.chosenTeam, false);
            }

            yield return PanToPlayerFighters();
            yield return runCtx.WaitSeconds(.35f);
            
            yield return runCtx.Run(TryStartTurn(isReplay));

            if (contextProvider is PreviewContextProvider pcp)
            {
                // Save initial fighter positions after dead ones have respawned
                pcp.InitFighterPositions(this);
            }

            turnEnded = false;

            if (currPlayer.IsAI)
            {
                var wasFirstTurn = isFirstTurn;
                isFirstTurn = false;
                
                // Have to set isFirstTurn to false *before* calling EndTurn or TakeTurn because they'll eventually
                // circle back around and start the next turn.
                if (wasFirstTurn && playerTwoDoNothingOnFirstTurn)
                    yield return EndTurn();
#if UNITY_EDITOR
                // Local debug utility for choosing AI turn order
                // Note: this will probably break replays of AI games once those are supported
                else if (ClientSettings.Get(ClientSettings.ChooseAITurnOrder))
                    uiController.ShowChooseAITurnOrder(currPlayer);
#endif
                else
                    runCtx.Start(currPlayer.aiManager.TakeTurn());
            }
            else
            {
                // human player
                
                if (nextTurnStartCallback != null && !isFirstTurn)
                {
                    // Use temp variable in case the callback sets nextTurnStartCallback
                    var temp = nextTurnStartCallback;
                    nextTurnStartCallback = null;
                    yield return temp(this);
                }
                isFirstTurn = false;
                
                if (gameMode.IsOnline() && 
                    !isReplay && 
                    achievementTracker.GetPendingUnlocks().Count != 0)
                {
                    yield return uiController.ShowPendingUnlocks();
                }
            }
        }

        private IEnumerator SpawnPreChosenTeams()
        {
            // Ensure every player has chosen their team and that their fighters are init'ed.
            // Also hide all the fighters so we can show them spawning in.
            foreach (var p in Players)
            {
                EnsureHasChosenTeam(p);
                    
                foreach (var f in p.Fighters)
                    if (f.hasVirtualActor)
                        f.virtualActor.Hide();
            }

            if (gameMode == GameMode.Campaign)
            {
                // Special roguelike campaign turn order
                yield return SpawnPreChosenTeams_Campaign();
            }
            else
            {
                // regular game -- mark all fighters as dead but about to respawn
                foreach (var player in Players)
                {
                    foreach (var fighter in player.Fighters)
                    {
                        fighter.CurrHealth = 0;
                        fighter.TurnsTilRespawn = 0;
                    }
                }
                RefreshObjects();
            }
        }
        
        private IEnumerator SpawnPreChosenTeams_Campaign()
        {
            // For campaign we use a special team order -- all enemies first, then player last.
            for (int i = 1; i < Players.Count; i++)
            {
                var player = Players[i];
                yield return AnimateSpawnFightersForCampaign(player.Fighters);

                yield return runCtx.WaitSeconds(.35f);
            }
            
            // Now spawn player fighters
            yield return AnimateSpawnFightersForCampaign(Players[0].Fighters);
            
            yield return runCtx.WaitSeconds(.35f);
        }
        
        private IEnumerator AnimateSpawnFightersForCampaign(List<Fighter> fighters)
        {
            yield return camera.PanTilPointsVisible(fighters.Select(f => f.pos).ToList());
            yield return animCtx.AnimateFightersSpawn(fighters, false);
        }
        
        private void EnsureHasChosenTeam(Player player)
        {
            if (!player.HasChosenTeam || player.Fighters.Count == 0)
            {
                var team = AllFighters.Instance.All[player.playerSettings.PreChosenTeam.First()].team;
                var chosenTeam = new ChosenTeam(team, player.playerSettings.PreChosenTeam);
                player.ChooseTeam(chosenTeam);
            }
        }

        public void DidChooseAITurnOrder(List<string> fighterIds)
        {
            runCtx.Start(currPlayer.aiManager.TakeTurn(fighterIds));
        }

        private void ChooseTeam()
        {
            if (gameSettings.MirrorMatch
                // First player chooses their team normally (or randomly, as the case may be)
                && currPlayerTurn != gameSettings.whoGoesFirst)
            {
                // Choose same team as first player
                var firstPlayer = Players[gameSettings.whoGoesFirst];
                runCtx.Start(currPlayer.ChooseTeamAndSpawn(firstPlayer.chosenTeam));
                return;
            }

            if (gameSettings.RandomTeams)
            {
                ChooseRandomTeamForPlayer(currPlayer);
                return;
            }
            
            if (currPlayer.IsAI)
            {
                currPlayer.aiManager.ChooseFighters();
                return;
            }
            
            if (currPlayer.IsLocalControlledPlayer())
            {
                uiController.ShowChooseTeamMenu(currPlayer);
                return;
            }
            
            // Else, do nothing - it's likely a TV game or replay, turn will continue automatically
        }

        public void ChooseRandomTeamForPlayer(Player player)
        {
            randy = new Randy(randomSeed);
            randomSeed = randy.Next();
            
            var teams = Enum.GetValues(typeof(Team));
            var team = (Team) teams.GetValue(randy.Next(0, teams.Length));
            var possibleFighters = ListUtils.ShuffleList(
                AllFighters.Instance.FightersByTeam(team).Select(f => f.proto).ToList(),
                randy);

            var fighters = new List<FighterPrototype>();
            for (var i = 0; i < player.numFightersToChoose; i++)
                fighters.Add(possibleFighters[i]);
            
            runCtx.Start(player.ChooseTeamAndSpawn(new ChosenTeam(team, fighters)));
        }

        public IEnumerator PanToPlayerFighters()
        {
            var points = currPlayer.LiveFighters.Where(f => !f.Asleep).Select(f => f.coords).ToList();
            yield return camera.PanTilPointsVisible(points);
        }
        
        private IEnumerator TryStartTurn(bool isReplay)
        {
            ClearAchievementActions();
            preTurnState = CloneInMemory();
            turnChoices = new List<Choice>();
            
            bool isSkippableAI = currPlayer.IsAI && currPlayer.aiType == AIType.DoNothing;
            if (!alreadyCalledUIStartTurn && !isSkippableAI)
                yield return contextProvider.GetUIController().StartTurn(isReplay);
            alreadyCalledUIStartTurn = true;

            yield return UpdateHazardsForCurrPlayer();

            // if we have no nexus, don't respawn anyone
            if (currPlayer.nexus != null)
            {
                // remove dead minions so they don't respawn
                currPlayer.Fighters.RemoveAll(f => f.WasSummoned && f.IsDead);
                
                var respawningFighters = new List<Fighter>();
                foreach (var f in currPlayer.Fighters.Where(f => f.IsDead))
                {
                    if (!f.TurnsTilRespawn.HasValue || f.TurnsTilRespawn.Value <= 0)
                    {
                        f.TurnsTilRespawn = null;

                        f.ResetStateForRespawn();
                        respawningFighters.Add(f);
                        lastFighterWhoRespawned = f;

                        // so the next fighter doesn't get spawned right on top of this one
                        RefreshObjects();
                    }
                }
                
                if (respawningFighters.Count > 0)
                {
                    yield return animCtx.AnimateFightersSpawn(respawningFighters, true);
                    uiController.UpdateAfterFighterSpawn();

                    if (afterFighterRespawnedCallback != null)
                    {
                        nextTurnStartCallback = afterFighterRespawnedCallback;
                        afterFighterRespawnedCallback = null;
                    }
                }
            }

            foreach (var p in Players)
            {
                bool IsThisPlayersTurn = p.IsTurn;

                if (IsThisPlayersTurn)
                {
                    // At the beginning of this player's turn, set all their burrows off cooldown
                    var thisPlayerTeleporters = map.teleporters.Where(t => t.Owner == p).ToList();
                    foreach (var tp in thisPlayerTeleporters)
                    {
                        tp.SetOnCooldown(false);
                    }
                }
                
                foreach (var f in p.LiveFighters)
                {
                    // give enemy fighters actions/moves so we can highlight their capabilities properly
                    ResetFighterMovesAndActions(f);

                    if (!IsThisPlayersTurn)
                        continue;
                    
                    f.TickDownStatusEffects(false);

                    foreach (var a in f.Abilities)
                    {
                        try
                        {
                            if (f.ReadyAttack)
                            {
                                f.SetReadyAttacks(0);
                                // Don't bother removing from readyAttackFighterIds because we RefreshObjects below
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e);
                        }
                        
                        yield return a.StartOfTurn();
                    }
                }
            }
            
            uiController.ShowEndTurnButton(!isReplay && currPlayer.IsHuman);

            RefreshObjects();

            yield return PanToPlayerFighters();
        }

        private IEnumerator UpdateHazardsForCurrPlayer()
        {
            var hazardsToRemove = new List<Hazard>();
            var waitGroup = new WaitGroup(runCtx);
            foreach (var hazard in Hazards.Values)
            {
                if (hazard.owner.IsTurn)
                {
                    if (hazard.dissapates)
                        hazardsToRemove.Add(hazard);
                    else
                        waitGroup.Start(hazard.UpdateForTurn());
                }
            }
            hazardsToRemove.ForEach(RemoveHazard);
            yield return waitGroup.Wait();
        }

        private void ResetFighterMovesAndActions(Fighter f)
        {
            int actions = !f.LosesActionNextTurn() && !f.Asleep ? f.MaxActions : 0;
            f.SetActionsLeft(actions);

            f.SetMovesLeft(f.CalcMaxMoves());
        }

        public IEnumerator EndTurn(bool isReplay = false)
        {
            if (turnEnded)
                yield break;
            turnEnded = true;

            yield return runCtx.Run(TryEndTurn(isReplay));
        }

        private IEnumerator TryEndTurn(bool isReplay)
        {
            // this is just for pathing, needs to be cleared at the end of every turn
            currTurnReadyAttacks = null;
            
            turnCount++;

            if (!gameEnded)
            {
                // the currPlayer getter depends on the currPlayerTurn index, so we have to do the stuff
                // that depends on currPlayer before we increment currPlayerTurn
                foreach (var f in currPlayer.LiveFighters)
                {
                    f.TickDownStatusEffects(true);
                    f.HideHealthBar();
                }
                
                // after all the fighters have had their status effects cleared, etc.
                foreach (var f in currPlayer.LiveFighters)
                {
                    foreach (var a in f.Abilities)
                    {
                        a.UpdateCooldown();

                        yield return runCtx.Run(a.EndOfTurn());
                    }
                }

                // Tick down respawn counters
                foreach (var f in currPlayer.Fighters.Where(f => f.IsDead))
                {
                    if (f.TurnsTilRespawn.HasValue && f.TurnsTilRespawn.Value > 0)
                        f.TurnsTilRespawn--;
                }
            }

            int failsafe = 0;
            do
            {
                currPlayerTurn++;
                if (currPlayerTurn >= Players.Count) currPlayerTurn = 0;

                if (currPlayer.OutOfGame)
                {
                    // If the current player is out of the game, we'll skip their turn but we still need
                    // to update their hazards. Otherwise they'll stick around forever.
                    yield return UpdateHazardsForCurrPlayer();
                }
            } while (!gameEnded && currPlayer.OutOfGame && failsafe++ < 100); // Skip players who are out of the game (unless game is over)

            // logically, currPlayerTurn has to advance before we can bail
            if (gameEnded)
                yield break;
            
            // hide fighter action indicators for the team that just finished
            Players.ForEach(p => p.Fighters.ForEach(f => f.ClearActionsAndMovesLeft()));

            if (isReplay)
            {
                // If it's a replay, don't submit the current turn or start the next turn
                yield break;
            }
            
            if (gameMode.IsOnline())
            {
                yield return networkCtx.SubmitTurn(this);
            }
            else
            {
                if (gameSettings is { SaveGame: true })
                {
                    opsCtx.SaveGame(this);
                }
                else if (gameMode == GameMode.Campaign)
                {
                    opsCtx.SaveRoguelikeRun();
                }

                runCtx.Start(StartTurn());
            }
        }

        public void GameOver()
        {
            uiController.ShowEndTurnButton(false);

            winnerTeamIdx ??= Players.Find(p => p.nexus.CurrHealth > 0).TeamIdx;
            
            if (gameMode.IsOnline() && !localPlayerSurrendered && !isCurrTurnReplay)
            {
                runCtx.Start(networkCtx.SubmitTurn(this));

                var me = GetMyPlayer();
                switch (me?.team)
                {
                    case Team.Critter:
                        SomethingHappened(BSEventType.CompleteMultiplayerGameCritter);
                        break;
                    case Team.Human:
                        SomethingHappened(BSEventType.CompleteMultiplayerGameHuman);
                        break;
                    case Team.Robot:
                        SomethingHappened(BSEventType.CompleteMultiplayerGameRobot);
                        break;
                }
                
                SomethingHappened(new BSEvent(BSEventType.WinMultiplayerGame, winners));
            }

            if (gameMode == GameMode.Local)
            {
                contextProvider.GetOperationsContext().DeleteGame(GameId);
            }
            else if (gameMode == GameMode.Campaign)
            {
                contextProvider.GetOperationsContext().FinishedRoguelikeGame();
            }

            contextProvider.GetUIController().GameOver();
        }

        public Player GetMyPlayer()
        {
            return Players.Find(p => p.IsLocalControlledPlayer());
        }
        
        public List<Player> GetMyOpponents()
        {
            return Players.Where(p => !p.IsLocalControlledPlayer()).ToList();
        }

        public void RegisterAchievementAction(AchievementAction action, bool requiresMultiplayer, int num = 1)
        {
            if (isCurrTurnReplay)
                return;

            if (requiresMultiplayer && !gameMode.IsOnline())
                return;
            
            achievementTracker.RegisterAchievementAction(action, num);
        }
        
        public void RegisterSemiPersistentAchievementActionOnSubmit(AchievementAction action, int num = 1)
        {
            if (!isCurrTurnReplay)
            {
                semiPersistentAchievementActionOccurrences.TryGetValue(action, out var currNum);
                semiPersistentAchievementActionOccurrences[action] = currNum + num;
            }
        }

        public void RegisterAchievementActionOnSubmit(AchievementAction action, int num = 1)
        {
            if (!isCurrTurnReplay)
            {
                achievementActionOccurrences.TryGetValue(action, out var currNum);
                achievementActionOccurrences[action] = currNum + num;
            }
        }

        public void ClearAchievementActions()
        {
            achievementActionOccurrences = new Dictionary<AchievementAction, int>();
        }
        
        public void SubmitAchievementActions()
        {
            foreach (var kv in achievementActionOccurrences)
            {
                achievementTracker.RegisterAchievementAction(kv.Key, kv.Value);
            }
        }
        
        public void ClearSemiPersistentAchievementActions()
        {
            semiPersistentAchievementActionOccurrences = new Dictionary<AchievementAction, int>();
        }
        
        public void SubmitSemiPersistentAchievementActions()
        {
            foreach (var kv in semiPersistentAchievementActionOccurrences)
            {
                achievementTracker.RegisterAchievementAction(kv.Key, kv.Value);
            }
        }

        public void SubmitAndClearAchievementActions()
        {
            SubmitAchievementActions();
            SubmitSemiPersistentAchievementActions();
            ClearAchievementActions();
            ClearSemiPersistentAchievementActions();
        }

        public void EnableCheat(Cheat cheat)
        {
            cheatsEnabled ??= new List<Cheat>();
            
            cheatsEnabled.Add(cheat);
        }

        public bool CheatEnabled(Cheat cheat)
        {
            cheatsEnabled ??= new List<Cheat>();
            
            return cheatsEnabled.Contains(cheat);
        }

        public IEnumerator EnqueueTutorialPopups(List<MapScenario.TutorialPopup> tutorialPopups)
        {
            List<MapScenario.TutorialAction> toRunImmediately = new List<MapScenario.TutorialAction>();

            foreach (var popup in tutorialPopups)
            {
                switch (popup.trigger)
                {
                    case GameEventTrigger.OnGameStart:
                        toRunImmediately.Add(popup.action);
                        break;
                    case GameEventTrigger.OnFighterSelect:
                        fighterSelectCallback = popup.action;
                        break;
                    case GameEventTrigger.AfterFighterMove:
                        afterFighterMovedCallback = popup.action;
                        break;
                    case GameEventTrigger.AfterFighterAttack:
                        afterFighterAttackedCallback = popup.action;
                        break;
                    case GameEventTrigger.AfterFighterRespawned:
                        afterFighterRespawnedCallback = popup.action;
                        break;
                    case GameEventTrigger.OnSecondTurnStart:
                        nextTurnStartCallback = popup.action;
                        break;
                    case GameEventTrigger.OnWeaponSelect:
                        onWeaponSelectCallback = popup.action;
                        break;
                    case GameEventTrigger.OnDamageNexusDirectly:
                        OnDamageNexusDirectlyCallback = popup.action;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            foreach (var action in toRunImmediately)
                yield return action(this);
        }

        public void OnFighterSelect()
        {
            if (fighterSelectCallback != null)
            {
                runCtx.Start(fighterSelectCallback(this));
                fighterSelectCallback = null;
            }
        }

        public void LocalPlayerSurrendered()
        {
            localPlayerSurrendered = true;
            
            int? playerOverrideIdx = null;
            for (int i = 0; i < Players.Count; i++)
            {
                var p = Players[i];
                if (p.IsLocalControlledPlayer())
                    playerOverrideIdx = i;
            }
            
            runCtx.Start(LocalPlayerSurrenderedCoroutine(playerOverrideIdx));
        }

        private IEnumerator LocalPlayerSurrenderedCoroutine(int? playerOverrideIdx)
        {
            yield return runCtx.Run(SubmitChoice(new Choice(ChoiceType.Surrender, playerOverrideIdx)));
            uiController.GameOver();
        }

        public void SomethingHappened(BSEventType bsEventType)
        {
            SomethingHappened(new BSEvent(bsEventType));
        }

        // Event system for triggering arbitrary events.
        // Note this is for BSEvents, not for Movement/Ability/etc. -Events.
        // Mainly for achievements, later on will be used for stats.
        public void SomethingHappened(BSEvent bsEvent)
        {
            try
            {
                DispatchEvent(bsEvent);
            }
            catch (Exception e)
            {
                Logger.Error("Error dispatching event " + bsEvent + ":\n " + e);
            }
        }
        
        private void DispatchEvent(BSEvent bsEvent)
        {
            switch (bsEvent.type)
            {
                case BSEventType.BurnOilTile:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementActionOnSubmit(AchievementAction.BurnOilTile, bsEvent.amount);
                    break;
                
                case BSEventType.CompleteMultiplayerGameCritter:
                    RegisterAchievementAction(AchievementAction.CompleteMultiplayerGameCritter, true);
                    break;
                
                case BSEventType.CompleteMultiplayerGameHuman:
                    RegisterAchievementAction(AchievementAction.CompleteMultiplayerGameHuman, true);
                    break;
                
                case BSEventType.CompleteMultiplayerGameRobot:
                    RegisterAchievementAction(AchievementAction.CompleteMultiplayerGameRobot, true);
                    break;
                
                case BSEventType.DealAcidDamage:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterSemiPersistentAchievementActionOnSubmit(AchievementAction.DealAcidDamage, bsEvent.amount);
                    break;
                
                case BSEventType.DealDamageWithPushPull:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.DealDamageWithPushPull, false);
                    break;
                
                case BSEventType.DestroyCover:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementActionOnSubmit(AchievementAction.DestroyCover);
                    break;
                
                case BSEventType.ExceedActiveDaemonMinionThreshold:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementActionOnSubmit(AchievementAction.ExceedActiveDaemonMinionThreshold);
                    break;
                
                case BSEventType.HealByAnyMethod:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.HealByAnyMethod, false, bsEvent.amount);
                    break;
                
                case BSEventType.KillAllEnemyFightersInOneTurn:
                    RegisterSemiPersistentAchievementActionOnSubmit(AchievementAction.KillAllEnemyFightersInOneTurn);
                    break;
                
                case BSEventType.KillEnemyFighter:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterSemiPersistentAchievementActionOnSubmit(AchievementAction.KillEnemyFighter);
                    break;
                
                case BSEventType.KillFighterViaElectrocution:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementActionOnSubmit(AchievementAction.KillFighterViaElectrocution);
                    break;
                
                case BSEventType.SummonMinions:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.SummonMinions, false);
                    break;
                
                case BSEventType.WinMultiplayerGame:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.WinMultiplayerGame, true);
                    break;

                case BSEventType.KillFighterWithPushPullDownPit:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.KillFighterWithPushPullDownPit, false);
                    break;

                case BSEventType.StunnedEnemy:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.StunEnemies, false);
                    break;
                
                case BSEventType.ConsumeHazardWithAcid:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.ConsumeHazardWithAcid, false);
                    break;
                
                case BSEventType.CrossGap:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.CrossGap, false);
                    break;
                
                case BSEventType.UseTeleporter:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.UseTeleporter, false);
                    break;
                
                case BSEventType.PushOrPullFighterIntoTeleporter:
                    if (bsEvent.IsForLocalControlledPlayer())
                        RegisterAchievementAction(AchievementAction.PushOrPullFighterIntoTeleporter, false);
                    break;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(512);

            sb.Append("Game state:");

            foreach (var p in Players)
            {
                var playerName = p.Username;
                if (string.IsNullOrEmpty(playerName))
                {
                    playerName = p.IsAI ? p.aiType.Value.Name() + " AI" : "HumanPlr" + p.Idx;
                }

                sb.Append($"\nPlayer {p.Idx} ({playerName})");

                if (p.nexus != null)
                    sb.Append($" Nexus: {p.nexus.CurrHealth}/{p.nexus.MaxHealth} hp");
                
                sb.Append("\nFighters:");

                // live fighters
                foreach (var f in p.LiveFighters)
                {
                    // basic stats
                    sb.Append("\n + ")
                        .Append(f.id)
                        .Append($"{(f.WasSummoned ? " (minion)" : "")} @ {f.coords.x},{f.coords.y}, {f.CurrHealth}/{f.MaxHealth} hp, {f.MovesLeft}/{f.MaxMoves} mv, {f.ActionsLeft} ac");

                    // abilities on cooldown
                    foreach (var ab in f.Abilities)
                    {
                        if (ab.OnCooldown)
                            sb.Append($", ab {ab.type} cd {ab.CooldownTurnsRemaining}");
                    }
                    
                    // status effects
                    foreach (var se in f.statusEffects)
                    {
                        sb.Append($", {se.Key} {se.Value}");
                    }
                }
                
                // dead fighters
                foreach (var f in p.Fighters?.Where(f => f.IsDead))
                {
                    sb.Append($"\n (- {f.id} dead, respawning in {f.TurnsTilRespawn} turns)");
                }
            }

            sb.Append("\n");

            return sb.ToString();
        }
    }

    public enum Cheat
    {
        FastPassives,
        InfiniteActions,
        ZeroCooldowns,
        NoReadyAttack,
    }
    
    public enum WinCondition
    {
        DestroyEnemyNexus,
        KillAllEnemies,
        GetThroughTeleporter
    }

    public static class WinConditionExtensions
    {
        public static string Name(this WinCondition winCondition)
        {
            return winCondition switch
            {
                WinCondition.DestroyEnemyNexus => "Destroy Enemy Nexus",
                WinCondition.KillAllEnemies => "Kill All enemies",
                WinCondition.GetThroughTeleporter => "Get Through Teleporter",
                _ => throw new ArgumentOutOfRangeException(nameof(winCondition), winCondition, null)
            };
        }
    }

    public enum GameEventTrigger
    {
        OnGameStart,
        OnFighterSelect,
        AfterFighterMove,
        AfterFighterAttack,
        AfterFighterRespawned,
        OnSecondTurnStart,
        OnWeaponSelect,
        OnDamageNexusDirectly,
    }
}
