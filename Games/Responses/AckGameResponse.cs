namespace BarnardsAscot.bstargamelogic.Games.Responses
{
    public class AckGameResponse
    {
        // We want to show the map voting popup after the game was acked, so we expose CurrentPlayerVote here
        public string MapId, MapName;
        public int CurrentPlayerVote;
    }
}
