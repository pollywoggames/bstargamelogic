namespace BarnardsStar.Games.Responses
{
    public class Glicko2Rating
    {
        /// <summary>
        /// For converting between Glicko and Glicko2 scales.
        /// </summary>
        public const double ConversionConstant = 173.7178;
        
        public const double UnratedRating = 1500;
        public const double UnratedDeviation = 350;
        public const double UnratedVolatility = 0.06;
        
        // aka Mu
        public double Rating { get; set; }
        
        // aka Phi
        public double Deviation { get; set; }
        
        // aka Sigma
        public double Volatility { get; set; }

        public int EffectiveRating => (int) (Rating - 2 * Deviation);
        public string RatingStr => $"{EffectiveRating} ({(int) Rating} +/- {(int) Deviation * 2})";

        public Glicko2Rating(double rating, double deviation, double volatility)
        {
            Rating = rating;
            Deviation = deviation;
            Volatility = volatility;
        }

        public static Glicko2Rating Unrated()
        {
            return new Glicko2Rating(UnratedRating, UnratedDeviation, UnratedVolatility);
        }

        public Glicko2Rating Clone()
        {
            return (Glicko2Rating) MemberwiseClone();
        }

        // Mu is the rating converted from glicko to glicko2 scale
        public double GetMu()
        {
            return (Rating - 1500.0) / ConversionConstant;
        }

        public void SetMu(double newMu)
        {
            Rating = newMu * ConversionConstant + 1500.0;
        }

        // Phi is the rating deviation converted from glicko to glicko2 scale
        public double GetPhi()
        {
            return Deviation / ConversionConstant;
        }

        public void SetPhi(double newPhi)
        {
            Deviation = newPhi * ConversionConstant;
        }

        public (double, double) ToGlicko2()
        {
            return (GetMu(), GetPhi());
        }

        public bool IsUnrated()
        {
            return Rating == UnratedRating && Deviation == UnratedDeviation;
        }
    }
}