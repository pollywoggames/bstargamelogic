﻿using System.Collections.Generic;

namespace BarnardsStar.Games.Responses
{
    public class JoinGameResponse
    {
        public List<GameTurn> GameTurns { get; set; }
    }
}
