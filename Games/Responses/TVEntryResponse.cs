using System.Collections.Generic;

namespace BarnardsStar.Games.Responses
{
    public class TVEntryResponse
    {
        public List<GameEntry> WatchGames { get; set; }
    }
}