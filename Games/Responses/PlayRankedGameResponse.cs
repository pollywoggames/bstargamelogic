using BarnardsStar.Games.Responses;
using BarnardsStar.Utilities;

namespace BarnardsStar.Games.Requests
{
    public class PlayRankedGameResponse
    {
        // If the game was started.
        // We send both game and gameEntry in case it's my turn (load the game)
        // or not (just put the game entry in the list).
        public GameEntry gameEntry;
        public string serializedGame;
        
        // If it was not started
        public GameLobby gameLobby;
        
        // Empty constructor for JSON deserialization
        public PlayRankedGameResponse() { }

        private PlayRankedGameResponse(GameEntry gameEntry, Game game, GameLobby gameLobby)
        {
            this.gameEntry = gameEntry;
            if (game != null)
                this.serializedGame = JsonSerializer.Serialize(game);
            
            this.gameLobby = gameLobby;
        }

        public static PlayRankedGameResponse GameStarted(GameEntry gameEntry, Game game)
        {
            return new PlayRankedGameResponse(gameEntry, game, null);
        }
        
        public static PlayRankedGameResponse Waiting(GameLobby gameLobby)
        {
            return new PlayRankedGameResponse(null, null, gameLobby);
        }
    }
}