using System.Collections.Generic;

namespace BarnardsStar.Games.Responses
{
    public class GetUpdatesResponse
    {
        public bool HasUpdate { get; set; }
        public List<GameTurn> GameTurns { get; set; }
    }
}