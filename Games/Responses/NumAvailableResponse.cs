namespace BarnardsStar.Games.Responses
{
    public class NumAvailableResponse
    {
        public int NumAvailable { get; set; }
    }
}