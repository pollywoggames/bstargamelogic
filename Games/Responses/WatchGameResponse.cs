using System.Collections.Generic;

namespace BarnardsStar.Games.Responses
{
    public class WatchGameResponse
    {
        public List<GameTurn> GameTurns { get; set; }
        public int? WinnerTeamIdx { get; set; }
    }
}