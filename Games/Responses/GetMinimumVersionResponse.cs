namespace BarnardsStar.Games.Responses
{
    public class GetMinimumVersionResponse
    {
        public int MinimumVersion { get; set; }
    }
}