﻿namespace BarnardsStar.Games.Responses
{
    public class StartGameResponse
    {
        public bool IsMyTurn { get; set; }
        public string serializedGame { get; set; }
    }
}
