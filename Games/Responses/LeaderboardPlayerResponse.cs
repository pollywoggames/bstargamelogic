﻿using BarnardsStar.Players;

namespace BarnardsAscot.bstargamelogic.Games.Responses
{
    public class LeaderboardPlayerResponse
    {
        public string Username;
        public string UserId;
        public PlayerIcon Icon;
        public string LeaderboardValue;

        public PlayerNameDisplay ToPlayerNameDisplay()
        {
            return new PlayerNameDisplay
            {
                Username = Username,
                UserId = UserId,
                Icon = Icon
            };
        }
    }
}