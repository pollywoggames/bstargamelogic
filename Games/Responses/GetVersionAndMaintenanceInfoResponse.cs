using System;
using Newtonsoft.Json;

namespace BarnardsStar.Games.Responses
{
    public class MaintenanceInfo
    {
        public string MaintenanceType { get; set; }
        public DateTime? PlannedMaintenanceTime { get; set; }
        public DateTime? PlannedMaintenanceEndTime { get; set; }
        
        [JsonIgnore]
        public string PlannedMaintenanceTimeStr => PlannedMaintenanceTime?.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss \"UTC\"");
        [JsonIgnore]
        public string PlannedMaintenanceEndTimeStr => PlannedMaintenanceEndTime?.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss \"UTC\"");
    }
    public class GetVersionAndMaintenanceInfoResponse
    {
        public int MinimumVersion { get; set; }
        public string MinimumVersionStr { get; set; }
        public MaintenanceInfo MaintenanceInfo { get; set; }
    }
}