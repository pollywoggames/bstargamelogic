﻿using System;
using System.Collections.Generic;
using BarnardsAscot.bstargamelogic.Auth;
using BarnardsStar.Characters;
using BarnardsStar.Players;

#if UNITY_2020_3_OR_NEWER
using BarnardsStar.Attributes;
#else
using MongoDB.Bson.Serialization.Attributes;
#endif

namespace BarnardsStar.Games.Responses
{
    public class PlayerProfile
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public int activeGames { get; set; }
        public int totalGames { get; set; }
        public int totalGamesThisSeason { get; set; }
        public int rankedGamesThisSeason { get; set; }
        public int rankedWinsTotal { get; set; }
        public int rankedWinsThisSeason { get; set; }

        // How many games this player has played with each faction
        public int totalGamesHumans { get; set; }
        public int totalGamesRobots { get; set; }
        public int totalGamesCritters { get; set; }
        
        public DateTime lastActivity { get; set; }
        public DateTime lastUpdated { get; set; }

        [BsonIgnore] public List<BStarBadge> badges { get; set; }
        [BsonIgnore] public DateTime accountCreationDate { get; set; }
        [BsonIgnore] public List<GameEntry> recentGames { get; set; }
        [BsonIgnore] public string RatingString;
        [BsonIgnore] public PlayerIcon playerIcon;

        // These fields are in relation to the currently logged-in user -- is the current user friends with this
        // user? If not, have they sent a friend request?
        [BsonIgnore] public bool isFriend;
        [BsonIgnore] public bool friendRequestSent;

        public void StartedGame()
        {
            activeGames++;
            totalGames++;
            totalGamesThisSeason++;
        }

        public void FinishedGame(bool isRanked, bool isWin, Team? team)
        {
            activeGames--;

            if (isRanked)
            {
                rankedGamesThisSeason++;
                
                if (isWin)
                    rankedWinsThisSeason++;
            }

            switch (team)
            {
                case Team.Human:
                    totalGamesHumans++;
                    break;
                case Team.Robot:
                    totalGamesRobots++;
                    break;
                case Team.Critter:
                    totalGamesCritters++;
                    break;
            }
        }

        public PlayerNameDisplay ToPlayerNameDisplay()
        {
            return new PlayerNameDisplay
            {
                UserId = UserId,
                Username = Username,
                Icon = playerIcon,
                lastActivity = lastActivity,
            };
        }
    }
}