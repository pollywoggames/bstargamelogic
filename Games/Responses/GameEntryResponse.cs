﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Utilities;
using Newtonsoft.Json;

#if UNITY_2020_3_OR_NEWER
using BarnardsStar.Logic.Networking;
using BarnardsStar.Logic.Utilities;
#endif

// Disabling some warnings cuz most of these fields are populated via JSON
// ReSharper disable UnassignedField.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace BarnardsStar.Games.Responses {
    public class GameEntry
    {
        public string GameType { get; set; }
        public int GameVersion { get; set; }

        // the procedurally-generated name of the game
        public string Name { get; set; }

        public List<GameEntryPlayer> Players;

        public List<string> WinnerIds { get; set; }

        [JsonIgnore] public bool GameEnded => WinnerIds != null;
        
        public string GameId { get; set; }

        public string PlayerTurnId { get; set; }
        public bool CurrPlayerTimedOut { get; set; }
        
        // Like PlayerTurnId, but only for local games. Index is needed because none of the players in local
        // games have a user ID.
        [JsonIgnore] public int LocalPlayerTurnIdx { get; set; }
        
#if UNITY_2020_3_OR_NEWER
        // Client-only properties that look at who is the currently logged in user
        [JsonIgnore] public bool IsMyTurn => PlayerTurnId == UserInfo.Instance.UserId;
        // Can be null, for local games
        [JsonIgnore] public GameEntryPlayer MyPlayer => Players.FirstOrDefault(p => p.Id == UserInfo.Instance.UserId);
        [JsonIgnore] public List<GameEntryPlayer> OtherPlayers => Players.Where(p => p.Id != UserInfo.Instance.UserId).ToList();
#endif

        [JsonIgnore] public GameEntryPlayer CurrPlayer => Players.FirstOrDefault(p => p.Id == PlayerTurnId);
        
        public bool isTV { get; set; }
        public bool IsLocal { get; set; }

        public DateTime StartDate;
        public DateTime EndDate;
        
        // For determining time left until timeout
        public DateTime LastTurnTakenDate;
        
        public int NumTurns;

        public GameSettings gameSettings;
        public bool IsDraftMap;

#if UNITY_2020_3_OR_NEWER
        public string GetStatusText()
        {
            if (IsLocal)
            {
                return TextUtils.Colorize($"{Utils.TapOrClick()} to continue playing!", TextColor.Action);
            }
            
            if (GameEnded)
            {
                return TextUtils.Colorize($"Game ended! {Utils.TapOrClick()} to view results.", TextColor.Action);
            }

            if (CurrPlayerTimedOut)
            {
                return TextUtils.Colorize(IsMyTurn ? "Timed out! Take your turn now." : "Current player timed out!", TextColor.Alert);
            }

            var myPlayer = MyPlayer;
            if (myPlayer != null)
            {
                if (MyPlayer.Dead)
                {
                    return TextUtils.Colorize($"Eliminated! {Utils.TapOrClick()} to view replay.", TextColor.Action);
                }
                if (MyPlayer.Kicked)
                {
                    return TextUtils.Colorize($"Kicked due to time out! {Utils.TapOrClick()} to dismiss.",
                        TextColor.Action);
                }
            }

            if (IsMyTurn)
            {
                return TextUtils.Colorize("My turn!", TextColor.Action);
            }
            
            return "Waiting for " + CurrPlayer?.Name;
        }
#endif

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            if (ReferenceEquals(this, null)) return false;
            if (ReferenceEquals(obj, null)) return false;
            if (this.GetType() != obj.GetType()) return false;

            var y = (GameEntry) obj;
            return Equals(this.Players, y.Players) &&
                   this.StartDate.Equals(y.StartDate) &&
                   this.LastTurnTakenDate.Equals(y.LastTurnTakenDate) &&
                   this.NumTurns == y.NumTurns &&
                   this.GameType == y.GameType &&
                   this.GameVersion == y.GameVersion &&
                   WinnerIdsEqual(y) &&
                   this.GameId == y.GameId &&
                   this.PlayerTurnId == y.PlayerTurnId &&
                   this.IsLocal == y.IsLocal;
        }

        private bool WinnerIdsEqual(GameEntry other)
        {
            if (this.WinnerIds == null)
                return other.WinnerIds == null;

            if (this.WinnerIds.Count != other.WinnerIds.Count)
                return false;

            for (int i = 0; i < this.WinnerIds.Count; i++)
            {
                if (this.WinnerIds[i] != other.WinnerIds[i])
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Players != null ? Players.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ StartDate.GetHashCode();
                hashCode = (hashCode * 397) ^ LastTurnTakenDate.GetHashCode();
                hashCode = (hashCode * 397) ^ NumTurns;
                hashCode = (hashCode * 397) ^ (GameType != null ? GameType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ GameVersion;
                hashCode = (hashCode * 397) ^ (WinnerIds != null ? WinnerIds.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (GameId != null ? GameId.GetHashCode() : 0);
                
                if (!string.IsNullOrEmpty(PlayerTurnId))
                    hashCode = (hashCode * 397) ^ PlayerTurnId.GetHashCode();
                
                hashCode = (hashCode * 397) ^ IsLocal.GetHashCode();
                return hashCode;
            }
        }
    }

    public class GameEntryResponse
    {
        public List<GameEntry> Games { get; set; }
        public List<GameLobby> lobbies { get; set; }
    } 
    
    public class GameLobby : IGameLobby
    {
        public string GameLobbyId;
        public string GameId;
        public DateTime CreatedAt;
        public bool IsLocal;
        public GameSettings gameSettings;

        public string OwnerId { get; set; }

        public List<GameLobbyPlayer> PlayersAndAccepted;

        public bool IsReadyToStart()
        {
            if (IsLocal)
            {
                // If it's a local game, pre-choosing teams is the only thing that can prevent
                // the game from being ready to start.
                return !gameSettings.PreChooseTeams || gameSettings.HasAllPreChosenTeams();
            }
            
            // if pre choose teams is enabled, all players must have chosen teams 
            if (gameSettings.PreChooseTeams && !gameSettings.HasAllPreChosenTeams())
                return false;

            // Do we have enough players?
            return PlayersAndAccepted.Count == gameSettings.NumPlayers
                   // Have all players accepted?
                   && PlayersAndAccepted.All(a => a.accepted);
        }

        public string GetStatusStr(bool includePlayersWord, bool currentPlayerIsOwner)
        {
            if (IsReadyToStart())
            {
                if (!includePlayersWord) return "Ready to start!";
                
                return currentPlayerIsOwner || gameSettings.GameMode == GameMode.Ranked1v1 ?
                    TextUtils.Colorize("Ready to start!", TextColor.Action) : "Waiting for owner to start";
            }
            
            string playersWord = includePlayersWord ? "players " : "";
            
            int numPlayers = gameSettings.NumPlayers;
            int joined = PlayersAndAccepted.Count;

            if (joined != numPlayers)
                // Waiting for players to join
                return $"{joined} / {numPlayers} {playersWord}joined";
            
            int accepted = PlayersAndAccepted.Count(paa => paa.accepted);
            if (accepted != numPlayers)
                // Waiting for players to accept
                return $"{accepted} / {numPlayers} {playersWord}accepted";
            
            // else, we must be waiting for some players to choose their team
            int chosenTeam = 0;
            for (int i = 0; i < gameSettings.PlayerSettings.Count; i++)
                if (!gameSettings.DoesPlayerNeedToPreChooseTeam(i))
                    chosenTeam++;
            return $"{chosenTeam} / {numPlayers} {playersWord}chosen units";
        }

        public bool PlayerAccepted(string playerId)
        {
            var myPlayerAndAccepted = PlayersAndAccepted.FirstOrDefault(p => p.playerNameDisplay.UserId == playerId);
            return myPlayerAndAccepted != null && myPlayerAndAccepted.accepted;
        }

        public bool HasEmptySlots()
        {
            int numPlayers = gameSettings.NumPlayers;
            int joined = PlayersAndAccepted.Count;

            return joined < numPlayers;
        }

        public int GetPlayerIdx(string playerId)
        {
            return PlayersAndAccepted.FindIndex(paa => paa.playerNameDisplay.UserId == playerId);
        }

        public GameLobbyPlayer GetPlayerById(string playerId)
        {
            return PlayersAndAccepted.FirstOrDefault(paa => paa.playerNameDisplay.UserId == playerId);
        }

#if UNITY_2020_3_OR_NEWER
        public int GetCurrentOnlinePlayerIdx()
        {
            if (!gameSettings.GameMode.IsOnline())
                return -1;
            
            return PlayersAndAccepted.FindIndex(pa => pa.playerNameDisplay?.UserId == UserInfo.Instance.UserId);
        }
#endif

        #region equality members

        protected bool Equals(GameLobby other)
        {
            return GameLobbyId == other.GameLobbyId
                && GameId == other.GameId
                && CreatedAt.Equals(other.CreatedAt)
                && Equals(gameSettings, other.gameSettings)
                && OwnerId == other.OwnerId
                && ListUtils.ListsEqual(PlayersAndAccepted, other.PlayersAndAccepted);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GameLobby)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (GameLobbyId != null ? GameLobbyId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (GameId != null ? GameId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CreatedAt.GetHashCode();
                hashCode = (hashCode * 397) ^ (gameSettings != null ? gameSettings.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (OwnerId != null ? OwnerId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PlayersAndAccepted != null ? PlayersAndAccepted.GetHashCode() : 0);
                return hashCode;
            }
        }
        
        #endregion
    }

    public class GameLobbyPlayer
    {
        public PlayerNameDisplay playerNameDisplay;
        public bool accepted;

        public GameLobbyPlayer(PlayerNameDisplay playerNameDisplay, bool accepted)
        {
            this.playerNameDisplay = playerNameDisplay;
            this.accepted = accepted;
        }
    }

    public class GameEntryPlayer
    {
        public string Id;
        public string Name;
        public Team team;
        public TeamColor TeamColor;
        public PlayerIcon playerIcon;
        public DateTime lastActivity;
        public bool Dead, Kicked, Surrendered;
        
        // These can be null if the fighters haven't been chosen yet
        public List<FighterPrototype?> Fighters;

        // false for me, true for the other guy
        public bool IsOpponent;

        public override string ToString()
        {
            // GameEntryPlayer(jamp;Blue;HumanSniper,HumanEnforcer)
            return $"GameEntryPlayer({Name};{TeamColor};{string.Join(",", Fighters.Select(f => f.HasValue ? f.Value.ToString() : "null"))})";
        }

        public PlayerNameDisplay ToPlayerNameDisplay()
        {
            return new PlayerNameDisplay
            {
                UserId = Id,
                Username = GetUsername(),
                Icon = playerIcon,
                lastActivity = lastActivity,
            };
        }

        public string GetUsername()
        {
            return string.IsNullOrEmpty(Name) ? "???" : Name;
        }
    }

    public interface IGameLobby
    {
        public string OwnerId { get; }
        
        public bool IsReadyToStart();

        // Whether the player with the given ID accepted this lobby or not
        public bool PlayerAccepted(string playerId);
    }

    public static class IGameLobbyExtensions
    {
        public static bool ActionRequired(this IGameLobby lobby, string currentPlayerId)
        {
            if (lobby.IsReadyToStart())
            {
                // If owner can start game, then action is required when the current player is the owner
                return lobby.OwnerId == currentPlayerId;
            }
            
            // else, action is required if current player hasn't accepted
            return !lobby.PlayerAccepted(currentPlayerId);
        }
    }
}
