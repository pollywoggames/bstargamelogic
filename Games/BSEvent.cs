using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Characters;
using BarnardsStar.Players;

namespace BarnardsStar.Games
{
    public enum BSEventType
    {
        BurnOilTile,
        CompleteMultiplayerGameCritter,
        CompleteMultiplayerGameHuman,
        CompleteMultiplayerGameRobot,
        ConsumeHazardWithAcid,
        CrossGap,
        DealAcidDamage,
        DealDamageWithPushPull,
        DestroyCover,
        ExceedActiveDaemonMinionThreshold,
        HealByAnyMethod,
        KillAllEnemyFightersInOneTurn,
        KillEnemyFighter,
        KillFighterViaElectrocution,
        KillFighterWithPushPullDownPit,
        PushOrPullFighterIntoTeleporter,
        SummonMinions,
        StunnedEnemy,
        UseTeleporter,
        WinMultiplayerGame,
    }

    public class BSEvent
    {
        public BSEventType type;
        public Fighter fighter;
        public List<Player> players;
        public int amount;

        public BSEvent(BSEventType type)
        {
            this.type = type;
        }
        
        public BSEvent(BSEventType type, Player player)
        {
            this.type = type;
            
            if (player != null)
                this.players = new List<Player> { player };
        }
        
        public BSEvent(BSEventType type, List<Player> players)
        {
            this.type = type;
            this.players = players;
        }

        public BSEvent(BSEventType type, Fighter fighter)
        {
            this.type = type;
            this.fighter = fighter;
            
            if (fighter != null && fighter.Owner != null)
                players = new List<Player>() { fighter.Owner };
        }

        public override string ToString()
        {
            var playersStr = players == null || players.Count == 0
                ? "[]"
                : string.Join(",", players.Select(player => player == null ? "null" : player.ToString()));

            return $"BSEvent(type={type},amount={amount},players={playersStr},fighter={(fighter == null ? "null" : fighter.id)})";
        }

        public bool IsForLocalControlledPlayer()
        {
            return players != null && players.Any(p => p != null && p.IsLocalControlledPlayer());
        }
    }
}