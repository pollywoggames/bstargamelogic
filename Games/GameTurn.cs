using System.Collections.Generic;

namespace BarnardsStar.Games
{
    public class GameTurn
    {
        public Game preTurnState { get; set; }
        public Game postTurnState { get; set; }
        
        public ChosenTeam chosenTeam;
        public List<Choice> turnChoices { get; set; }
        
        public int turnIdx { get; set; }

        public GameTurn Clone()
        {
            return new GameTurn
            {
                preTurnState = preTurnState?.CloneInMemory(),
                postTurnState = postTurnState?.CloneInMemory(),
                chosenTeam = chosenTeam,
                turnChoices = turnChoices == null ? null : new List<Choice>(turnChoices),
            };
        }

        public void SetGameIdAllOverThePlace(string gameId)
        {
            preTurnState.GameId = gameId;
            preTurnState.gameSettings.GameId = gameId;
            if (postTurnState != null)
            {
                postTurnState.GameId = gameId;
                postTurnState.gameSettings.GameId = gameId;
            }
        }
    }
}