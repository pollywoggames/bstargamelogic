using System.Collections.Generic;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Games
{
    public class PotentialMove
    {
        // What choices need to be taken to get to this spot?
        public List<Coords> movesToGetHere { get; private set; }
        
        // If the game state after this move has been simulated, store it here.
        public PreviewedGameState gameStatePostMove { get; private set; }

        private PreviewedGameState gameStatePreMove;

        // Movement cost to get to this spot.
        public float movementCost { get; private set; }

        // Extra ending coords for this move -- used so that you can click where the unit will show up when
        // going through a teleporter.
        public Coords? alternateEndingCoords;

        public static PotentialMove FromPath(Path path)
        {
            return new PotentialMove(new List<Coords> {path.End}, path.GetMoveCost());
        }

        public PotentialMove(List<Coords> movesToGetHere, float movementCost, PreviewedGameState gameStatePostMove = null)
        {
            this.movesToGetHere = movesToGetHere;
            this.gameStatePostMove = gameStatePostMove;
            this.movementCost = movementCost;
        }

        public PotentialMove AddPath(Path path)
        {
            var clone = Clone();
            
            clone.movesToGetHere.Add(path.End);
            // Add .01f because going through a teleporter should be slightly discouraged compared to getting
            // to the same space, in the same amount of moves, without a teleporter
            clone.movementCost += path.GetMoveCost() + .01f;
            
            // reset game state bc the new game state has not been simulated in this copy
            clone.gameStatePreMove = clone.gameStatePostMove;
            clone.gameStatePostMove = null;
            
            return clone;
        }

        private PotentialMove Clone()
        {
            return new PotentialMove(new List<Coords>(movesToGetHere), movementCost, gameStatePostMove);
        }

        public void AddSimulatedGameState(PreviewedGameState previewedGameState)
        {
            gameStatePostMove = previewedGameState;

            if (gameStatePreMove != null)
            {
                gameStatePostMove.outcomes.AddRange(gameStatePreMove.outcomes);
                gameStatePreMove = null;
            }
        }

        public override string ToString()
        {
            return $"PotMove({string.Join(",", movesToGetHere)})";
        }
    }
}