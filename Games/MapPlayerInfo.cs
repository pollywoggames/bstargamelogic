using BarnardsStar.Primitives;

namespace BarnardsStar.Games
{
    public class MapPlayerInfo
    {
        public static readonly Coords None = new Coords(-9999, -9999);
        
        public Coords SpawnPoint;
        public Coords NexusCenterPoint;
        public bool NexusIsShip;
        public bool NexusFacingLeft;
    }
}