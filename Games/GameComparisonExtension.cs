using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Utilities;

namespace BarnardsStar.Games
{
    public static class GameComparisonExtension
    {
        // returns a list of differences -- if the list is empty or null, the two game states do not differ materially
        public static List<string> DiffersMaterially(this Game thisGame, Game otherGame)
        {
            List<string> differences = new List<string>();

            if (thisGame.Players.Count != otherGame.Players.Count)
                differences.Add($"Player count differs (this: {thisGame.Players.Count}, other: {otherGame.Players.Count})");
            else
                ComparePlayers(thisGame, otherGame, differences);
            
            CompareMaps(thisGame, otherGame, differences);
            CompareHazards(thisGame, otherGame, differences);
            
            return differences;
        }

        private static void ComparePlayers(Game thisGame, Game otherGame, List<string> differences)
        {
            for (var i = 0; i < thisGame.Players.Count; i++)
            {
                var thisPlayer = thisGame.Players[i];
                var otherPlayer = otherGame.Players[i];

                ComparePlayerFields(differences, thisPlayer, otherPlayer, i);

                if (thisPlayer.Fighters.Count != otherPlayer.Fighters.Count)
                    differences.Add($"Player {i} Fighters.Count differs (this: {thisPlayer.Fighters.Count}, other: {otherPlayer.Fighters.Count})");
                else
                    CompareFighters(differences, thisPlayer, otherPlayer, i);
                
                if ((thisPlayer.nexus == null && otherPlayer.nexus != null) || (thisPlayer.nexus != null && otherPlayer.nexus == null)) 
                    differences.Add($"Player {i} nexus null differs (this: {(thisPlayer.nexus == null ? "null" : "non-null")}, other: {(otherPlayer.nexus == null ? "null" : "non-null")})");
                else if (thisPlayer.nexus != null && otherPlayer.nexus != null)
                    CompareNexi(differences, thisPlayer, otherPlayer, i);
            }
        }

        private static void ComparePlayerFields(List<string> differences, Player thisPlayer, Player otherPlayer, int i)
        {
            if (thisPlayer.Idx != otherPlayer.Idx)
                differences.Add($"Player {i} Idx differs (this: {thisPlayer.Idx}, other: {otherPlayer.Idx})");
            if (thisPlayer.TeamIdx != otherPlayer.TeamIdx)
                differences.Add($"Player {i} TeamIdx differs (this: {thisPlayer.TeamIdx}, other: {otherPlayer.TeamIdx})");
            if (thisPlayer.team != otherPlayer.team)
                differences.Add($"Player {i} team differs (this: {thisPlayer.team}, other: {otherPlayer.team})");
            if (thisPlayer.teamColor != otherPlayer.teamColor)
                differences.Add($"Player {i} teamColor differs (this: {thisPlayer.teamColor}, other: {otherPlayer.teamColor})");
            if (thisPlayer.OutOfGame != otherPlayer.OutOfGame)
                differences.Add($"Player {i} OutOfGame differs (this: {thisPlayer.OutOfGame}, other: {otherPlayer.OutOfGame})");
            if (thisPlayer.DidSurrender != otherPlayer.DidSurrender)
                differences.Add($"Player {i} DidSurrender differs (this: {thisPlayer.DidSurrender}, other: {otherPlayer.DidSurrender})");
            if (thisPlayer.aiType != otherPlayer.aiType)
                differences.Add($"Player {i} aiType differs (this: {thisPlayer.aiType}, other: {otherPlayer.aiType})");
        }

        private static void CompareFighters(List<string> differences, Player thisPlayer, Player otherPlayer, int playerIdx)
        {
            for (var i = 0; i < thisPlayer.Fighters.Count; i++)
            {
                var thisFighter = thisPlayer.Fighters[i];
                var otherFighter = otherPlayer.Fighters[i];
                
                if (thisFighter.id != otherFighter.id)
                    differences.Add($"Player {playerIdx}'s Fighter {i} id differs (this: {thisFighter.id}, other: {otherFighter.id})");
                if (thisFighter.Prototype != otherFighter.Prototype)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} Prototype differs (this: {thisFighter.Prototype}, other: {otherFighter.Prototype})");
                if (thisFighter.weapon != otherFighter.weapon)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} weapon differs (this: {thisFighter.weapon}, other: {otherFighter.weapon})");
                if (!ListUtils.ListsEqual(thisFighter.AbilityTypes, otherFighter.AbilityTypes))
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} AbilityTypes differs (this: {ListUtils.ListToString(thisFighter.AbilityTypes)}, other: {ListUtils.ListToString(otherFighter.AbilityTypes)})");
                if (thisFighter.MaxActions != otherFighter.MaxActions)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} MaxActions differs (this: {thisFighter.MaxActions}, other: {otherFighter.MaxActions})");
                if (thisFighter.ActionsLeft != otherFighter.ActionsLeft)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} ActionsLeft differs (this: {thisFighter.ActionsLeft}, other: {otherFighter.ActionsLeft})");
                if (thisFighter.MaxHealth != otherFighter.MaxHealth)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} MaxHealth differs (this: {thisFighter.MaxHealth}, other: {otherFighter.MaxHealth})");
                if (thisFighter.CurrHealth != otherFighter.CurrHealth)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} CurrHealth differs (this: {thisFighter.CurrHealth}, other: {otherFighter.CurrHealth})");
                if (thisFighter.MaxMoves != otherFighter.MaxMoves)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} MaxMoves differs (this: {thisFighter.MaxMoves}, other: {otherFighter.MaxMoves})");
                if (Math.Abs(thisFighter.MovesLeft - otherFighter.MovesLeft) > .05)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} MovesLeft differs (this: {thisFighter.MovesLeft}, other: {otherFighter.MovesLeft})");
                if (thisFighter.MaxShield != otherFighter.MaxShield)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} MaxShield differs (this: {thisFighter.MaxShield}, other: {otherFighter.MaxShield})");
                if (thisFighter.CurrShield != otherFighter.CurrShield)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} CurrShield differs (this: {thisFighter.CurrShield}, other: {otherFighter.CurrShield})");
                if (thisFighter.ReadyAttacks != otherFighter.ReadyAttacks)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} ReadyAttacks differs (this: {thisFighter.ReadyAttacks}, other: {otherFighter.ReadyAttacks})");
                if (thisFighter.TurnsTilRespawn != otherFighter.TurnsTilRespawn)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} TurnsTilRespawn differs (this: {thisFighter.TurnsTilRespawn}, other: {otherFighter.TurnsTilRespawn})");
                if (thisFighter.NextTurnsTilRespawn != otherFighter.NextTurnsTilRespawn)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} NextTurnsTilRespawn differs (this: {thisFighter.NextTurnsTilRespawn}, other: {otherFighter.NextTurnsTilRespawn})");
                if (thisFighter.WasSummoned != otherFighter.WasSummoned)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} WasSummoned differs (this: {thisFighter.WasSummoned}, other: {otherFighter.WasSummoned})");
                if (thisFighter.Asleep != otherFighter.Asleep)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} Asleep differs (this: {thisFighter.Asleep}, other: {otherFighter.Asleep})");
                
                // don't compare coords if we agree on the fighter being dead
                if (thisFighter.CurrHealth > 0 && otherFighter.CurrHealth > 0 && thisFighter.coords != otherFighter.coords)
                    differences.Add($"Player {playerIdx}'s Fighter {i} {thisFighter.id} coords differs (this: {thisFighter.coords}, other: {otherFighter.coords})");

                CompareStatusEffects(differences, playerIdx, thisFighter, otherFighter, i);
            }
        }

        private static void CompareStatusEffects(List<string> differences, int playerIdx, Fighter thisFighter, Fighter otherFighter, int fighterIdx)
        {
            var thisStatusEffects = thisFighter.statusEffects;
            var otherStatusEffects = otherFighter.statusEffects;

            var thisStatusEffectsKeys = thisFighter.statusEffects.Keys.ToList();
            var otherStatusEffectsKeys = otherFighter.statusEffects.Keys.ToList();

            if (thisStatusEffectsKeys.Count != otherStatusEffectsKeys.Count)
                differences.Add($"Player {playerIdx}'s Fighter {fighterIdx} {thisFighter.id} statusEffects differs (this: {DictionaryToString(thisStatusEffects)}, other: {DictionaryToString(otherStatusEffects)})");
            else
            {
                foreach (var se in thisStatusEffects)
                {
                    if (!otherStatusEffects.TryGetValue(se.Key, out var otherValue))
                        // other fighter is missing this status effect completely
                        differences.Add($"Player {playerIdx}'s Fighter {fighterIdx} {thisFighter.id} statusEffects differs (this: {DictionaryToString(thisStatusEffects)}, other: {DictionaryToString(otherStatusEffects)})");
                    else if (se.Value != otherValue)
                        differences.Add($"Player {playerIdx}'s Fighter {fighterIdx} {thisFighter.id} statusEffect value for {se.Key} differs (this: {se.Value}, other: {otherValue})");
                }
            }
        }

        private static string DictionaryToString<K, V>(Dictionary<K, V> statusEffects)
        {
            if (statusEffects == null)
                return "";

            var sb = new StringBuilder(statusEffects.Count * 32);
            sb.Append("[");
            bool first = true;
            foreach (var se in statusEffects)
            {
                if (first) first = false;
                else sb.Append(", ");

                sb.Append(se.Key)
                    .Append(": ")
                    .Append(se.Value);
            }
            sb.Append("]");

            return sb.ToString();
        }

        private static void CompareNexi(List<string> differences, Player thisPlayer, Player otherPlayer, int playerIdx)
        {
            var thisNexus = thisPlayer.nexus;
            var otherNexus = otherPlayer.nexus;
            
            if (thisNexus.CenterCoords != otherNexus.CenterCoords)
                differences.Add($"Player {playerIdx}'s Nexus CenterCoords differs (this: {thisNexus.CenterCoords}, other: {otherNexus.CenterCoords})");
            if (thisNexus.MaxHealth != otherNexus.MaxHealth)
                differences.Add($"Player {playerIdx}'s Nexus MaxHealth differs (this: {thisNexus.MaxHealth}, other: {otherNexus.MaxHealth})");
            if (thisNexus.CurrHealth != otherNexus.CurrHealth)
                differences.Add($"Player {playerIdx}'s Nexus CurrHealth differs (this: {thisNexus.CurrHealth}, other: {otherNexus.CurrHealth})");
        }
        
        private static void CompareMaps(Game thisGame, Game otherGame, List<string> differences)
        {
            var thisMap = thisGame.map;
            var otherMap = otherGame.map;
            
            if (thisMap.mapSettings.Id != otherMap.mapSettings.Id)
                differences.Add($"Map Id differs (this: {thisMap.mapSettings.Id}, other: {otherMap.mapSettings.Id})");
            else if (thisMap.bounds != otherMap.bounds)
                differences.Add($"Map bounds differs (this: {thisMap.bounds}, other: {otherMap.bounds})");
            else
            {
                foreach (var position in thisMap.allPositionsWithin)
                {
                    var thisTerrain = thisMap.GetTerrainType(position);
                    var otherTerrain = otherMap.GetTerrainType(position);
                    if (thisTerrain != otherTerrain)
                        differences.Add($"Map terrain differs @ {position.x},{position.y} (this: {thisTerrain}, other: {otherTerrain})");
                    else if (thisTerrain == TerrainType.Cover && thisMap.IsCoverDamaged(position) != otherMap.IsCoverDamaged(position))
                        differences.Add($"Map cover damaged differs @ {position.x},{position.y} (this: {thisMap.IsCoverDamaged(position)}, other: {otherMap.IsCoverDamaged(position)})");
                }
            }

            var thisTeleporters = thisMap.teleporters;
            var otherTeleporters = otherMap.teleporters;
            if (thisTeleporters.Count != otherTeleporters.Count)
                differences.Add($"Number of teleporters differs (this: {thisTeleporters.Count}, other: {otherTeleporters.Count})");
            else
            {
                for (int i = 0; i < thisTeleporters.Count; i++)
                {
                    var thisTP = thisTeleporters[i];
                    var otherTP = otherTeleporters[i];
                    
                    if (thisTP.channel != otherTP.channel)
                        differences.Add($"TP idx {i} channel differs (this: {thisTP.channel}, other: {otherTP.channel})");
                    if (thisTP.coords != otherTP.coords)
                        differences.Add($"TP idx {i} (channel {thisTP.channel}) coords differs (this: {thisTP.coords}, other: {otherTP.coords})");
                    if (thisTP.type != otherTP.type)
                        differences.Add($"TP idx {i} (channel {thisTP.channel}) type differs (this: {thisTP.type}, other: {otherTP.type})");
                    if (thisTP.Owner != otherTP.Owner)
                        differences.Add($"TP idx {i} (channel {thisTP.channel}) Owner differs (this: {thisTP.Owner.Idx}, other: {otherTP.Owner.Idx})");
                    if (thisTP.OnCooldown != otherTP.OnCooldown)
                        differences.Add($"TP idx {i} (channel {thisTP.channel}) OnCooldown differs (this: {thisTP.OnCooldown}, other: {otherTP.OnCooldown})");
                }
            }
        }

        private static void CompareHazards(Game thisGame, Game otherGame, List<string> differences)
        {
            var thisHazards = thisGame.Hazards;
            var otherHazards = otherGame.Hazards;

            foreach (var thisHazard in thisHazards)
            {
                if (!otherHazards.TryGetValue(thisHazard.Key, out var otherHazardType))
                    differences.Add($"This game has hazard {thisHazard.Value.type} @ {thisHazard.Key} which is missing in other game");
                else if (otherHazardType.type != thisHazard.Value.type)
                    differences.Add($"This game has hazard {thisHazard.Value.type} @ {thisHazard.Key} which in other game is of type {otherHazardType})");
            }
            
            foreach (var otherHazard in otherHazards)
            {
                if (!thisHazards.TryGetValue(otherHazard.Key, out var thisHazardType))
                    differences.Add($"Other game has hazard {otherHazard.Value.type} @ {otherHazard.Key} which is missing in this game");
                else if (thisHazardType.type != otherHazard.Value.type)
                    differences.Add($"Other game has hazard {otherHazard.Value.type} @ {otherHazard.Key} which in this game is of type {thisHazardType})");
            }
        }
    }
}