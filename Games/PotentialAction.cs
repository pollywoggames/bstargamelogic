using System;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.AI;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewOutcomes;

namespace BarnardsStar.Games
{
    public class PotentialAction
    {
        public List<Choice> choices;
        public List<PreviewOutcome> previewOutcomes;
        public Game currGameState;

        private Player _owner;

        public PotentialAction(Game game)
        {
            choices = new List<Choice>();
            previewOutcomes = new List<PreviewOutcome>();
            currGameState = game;
        }

        public PotentialAction(Game game, Choice c) : this(game)
        {
            AddChoiceInPlace(c);
        }

        private PotentialAction(List<Choice> choices, List<PreviewOutcome> previewOutcomes, Game currGameState)
        {
            this.choices = new List<Choice>(choices);
            this.previewOutcomes = new List<PreviewOutcome>(previewOutcomes);
            this.currGameState = currGameState;
        }

        private PotentialAction Clone()
        {
            return new PotentialAction(choices, previewOutcomes, currGameState);
        }

        public PotentialAction CloneWithChoice(Choice c)
        {
            var clone = Clone();
            clone.AddChoiceInPlace(c);
            return clone;
        }

        public void AddChoiceInPlace(Choice c)
        {
            var previewedGameState = currGameState.PreviewChoice(c);
            currGameState = previewedGameState.finalGameState;
            choices.Add(c);
            previewOutcomes.AddRange(previewedGameState.outcomes);
        }

        public float GetScore(Player owner, PathfindingMap pathfindingMap)
        {
            _owner = owner;
            
            float score = 0;
            foreach (var po in previewOutcomes)
            {
                score += po.GetScore(owner, pathfindingMap);
            }

            return score;
        }
        
        // for debug purposes
        public List<(PreviewOutcome, float)> OutcomesWithScores(Player owner, PathfindingMap pathfindingMap)
        {
            var ret = new List<(PreviewOutcome, float)>();
            
            _owner = owner;
            
            foreach (var po in previewOutcomes)
            {
                ret.Add((po, po.GetScore(owner, pathfindingMap)));
            }

            return ret;
        }

        public override string ToString()
        {
            string scoreStr = "?";
            if (_owner != null)
                scoreStr = "" + GetScore(_owner, null);
            
            return $"PotentialAction(score={scoreStr},choices={string.Join(",",choices.Select(c => c.ToString()).ToList())})";
        }
    }
}