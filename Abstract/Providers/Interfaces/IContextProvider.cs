﻿using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abstract.Providers.Interfaces
{
    public interface IContextProvider
    {
        IAnimationContext GetAnimationContext();
        IRunContext GetRunContext();
        ISoundContext GetSoundContext();
        IUIController GetUIController();
        INetworkContext GetNetworkContext();
        IOperationsContext GetOperationsContext();
        
        bool IsPreviewing();
        void RegisterPreview(IPreviewAction previewAction);
        List<PreviewOutcome> GetPreviews(Game origGame);
        void ClearPreviews();

        INexusVirtualActor ProvideNexusVirtualActor(Coords coords);
        IMapVirtualActor ProvideMapVirtualActor();
        ITeleporterVirtualActor ProvideTeleporterVirtualActor(int channel);
        ITeleporterVirtualActor ProvideBurrowVirtualActor(Player owner);
        IFighterVirtualActor ProvideFighterVirtualActor(Point pos);
        IHazardVirtualActor ProvideHazardVirtualActor(HazardType type, Coords coords);
        IProjectileVirtualActor ProvideProjectileVirtualActor(ProjectileType type, Point src);
        IMovable ProvideProp(MovablePropType type, Point startingSpot);

        IAchievementTracker GetAchievementTracker();

        ContextType GetContextType();
    }

    public enum ContextType
    {
        Unity,
        InMemory,
        Preview
    }
}