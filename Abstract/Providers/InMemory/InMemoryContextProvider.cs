﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BarnardsStar.Abstract.Contexts.InMemory;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Abstract.VirtualActors.InMemory;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Hazards;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Weapons.ProjectileWeapons;

namespace BarnardsStar.Abstract.Providers.InMemory
{
    public class InMemoryContextProvider : IContextProvider
    {
        private InstantRunContext runCtx = new InstantRunContext();
        
        public IAnimationContext GetAnimationContext()
        {
            return HollowAnimationContext.Instance;
        }

        public IRunContext GetRunContext()
        {
            return runCtx;
        }

        public ISoundContext GetSoundContext()
        {
            return HollowSoundContext.Instance;
        }

        public IUIController GetUIController()
        {
            return HollowUIController.Instance;
        }

        public INetworkContext GetNetworkContext()
        {
            return HollowNetworkContext.Instance;
        }

        public IOperationsContext GetOperationsContext()
        {
            return HollowOperationsContext.Instance;
        }

        public virtual bool IsPreviewing()
        {
            return false;
        }

        public virtual void RegisterPreview(IPreviewAction previewAction)
        { }

        public virtual List<PreviewOutcome> GetPreviews(Game origGame)
        {
            return new List<PreviewOutcome>();
        }

        public virtual void ClearPreviews()
        { }

        public INexusVirtualActor ProvideNexusVirtualActor(Coords coords)
        {
            return new InMemoryNexusVirtualActor(coords);
        }

        public IMapVirtualActor ProvideMapVirtualActor()
        {
            return InMemoryMapVirtualActor.Instance;
        }

        public ITeleporterVirtualActor ProvideTeleporterVirtualActor(int _)
        {
            return new InMemoryTeleporterVirtualActor();
        }

        public ITeleporterVirtualActor ProvideBurrowVirtualActor(Player _)
        {
            return new InMemoryTeleporterVirtualActor();
        }

        public IFighterVirtualActor ProvideFighterVirtualActor(Point pos)
        {
            return new InMemoryFighterVirtualActor(pos);
        }

        public IHazardVirtualActor ProvideHazardVirtualActor(HazardType type, Coords coords)
        {
            return new InMemoryHazardVirtualActor(coords);
        }

        public IProjectileVirtualActor ProvideProjectileVirtualActor(ProjectileType type, Point src)
        {
            return new InMemoryProjectileVirtualActor(src);
        }

        public IMovable ProvideProp(MovablePropType type, Point startingSpot)
        {
            return new InMemoryMovable(startingSpot);
        }

        public IAchievementTracker GetAchievementTracker()
        {
            return HollowAchievementTracker.Instance;
        }

        public virtual ContextType GetContextType()
        {
            return ContextType.InMemory;
        }
    }
}
