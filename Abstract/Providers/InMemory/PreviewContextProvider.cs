﻿using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Providers.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Preview.PreviewActions;
using BarnardsStar.Preview.PreviewOutcomes;
using BarnardsStar.Primitives;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abstract.Providers.InMemory
{
    public class PreviewContextProvider : InMemoryContextProvider
    {
        private Dictionary<string, Coords> fighterStartingCoords;
        private Dictionary<Fighter, List<SingleMove>> movement;
        private Dictionary<IDamageable, DamagePreview> damage;
        private CountingDictionary<Coords> coverDamage;
        private List<PreviewOutcome> otherPreviews;

        public PreviewContextProvider()
        {
            fighterStartingCoords = new Dictionary<string, Coords>();
            movement = new Dictionary<Fighter, List<SingleMove>>();
            damage = new Dictionary<IDamageable, DamagePreview>();
            coverDamage = new CountingDictionary<Coords>();
            otherPreviews = new List<PreviewOutcome>();
        }

        public override bool IsPreviewing()
        {
            return true;
        }

        public void InitFighterPositions(Game game)
        {
            fighterStartingCoords.Clear();
            
            foreach (var p in game.Players)
                foreach (var f in p.LiveFighters)
                    fighterStartingCoords[f.id] = f.coords;
        }

        public override List<PreviewOutcome> GetPreviews(Game origGame)
        {
            var ret = new List<PreviewOutcome>();

            foreach (var move in movement)
            {
                var previewFighter = move.Key;
                var fighter = origGame.FighterById(previewFighter.id);

                PreviewOutcome prev;
                if (fighter != null)
                {
                    if (!fighterStartingCoords.TryGetValue(fighter.id, out var startingCoords))
                    {
                        Logger.Error("Unable to get fighter starting coords: " + fighter.id);
                        startingCoords = fighter.coords;
                    }

                    prev = new MovementPreview(origGame, fighter, startingCoords, move.Value);
                }
                else
                {
                    prev = new SummonMinionPreview(
                        origGame,
                        move.Value[move.Value.Count - 1].coords,
                        previewFighter.Prototype);
                }

                ret.Add(prev);
            }

            foreach (var dmg in damage)
            {
                ret.Add(dmg.Value);
            }

            foreach (var coverDmg in coverDamage)
            {
                ret.Add(new CoverDamagePreview(origGame, coverDmg.Key, coverDmg.Value));
            }
            
            ret.AddRange(otherPreviews);

            return ret;
        }

        public override void ClearPreviews()
        {
            movement.Clear();
            damage.Clear();
            coverDamage.Clear();
            otherPreviews.Clear();
        }

        public override void RegisterPreview(IPreviewAction previewAction)
        {
            switch (previewAction)
            {
                case MovementPreviewAction move:
                    RegisterMovement(move);
                    break;
                case DamagePreview newDmgPreview:
                    if (damage.TryGetValue(newDmgPreview.target, out var damagePreview))
                    {
                        damagePreview.amount += newDmgPreview.amount;
                        damagePreview.ignoreShield = damagePreview.ignoreShield || newDmgPreview.ignoreShield;
                        damagePreview.killsTarget = damagePreview.killsTarget || newDmgPreview.killsTarget;
                    }
                    else
                    {
                        damage[newDmgPreview.target] = newDmgPreview;
                    }
                    break;
                case CoverDamagePreview coverDmg:
                    coverDamage.Add(coverDmg.coords, coverDmg.amount);
                    break;
                
                case PreviewOutcome po:
                    // can go in unchanged
                    otherPreviews.Add(po);
                    break;
                default:
                    Logger.Error("Unexpected preview type: " + previewAction.GetType());
                    break;
            }
        }

        private void RegisterMovement(MovementPreviewAction mpa)
        {
            var self = mpa.self;
            var move = mpa.move;
            var dst = move.coords;

            if (!mpa.forceAdd && move.coords == mpa.self.coords)
                return;
            
            if (movement.TryGetValue(self, out var movesSoFar))
            {
                if (mpa.forceAdd ||
                    // Look up last move in the movesSoFar list, see if we've already got it
                    (movesSoFar.Count > 0 && movesSoFar[movesSoFar.Count - 1].coords != dst))
                {
                    movesSoFar.Add(move);
                }
            }
            else
            {
                movement.Add(self, new List<SingleMove> { move });
            }
        }

        public override ContextType GetContextType()
        {
            return ContextType.Preview;
        }
    }
}
