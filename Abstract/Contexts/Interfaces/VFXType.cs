﻿namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public enum VFXType
    {
        BazookaExplosion,
        BurrowDigEffect,
        BurrowDestroyed,
        DeployCoverLandEffect,
        EarthquakeVFX,
        GlueSpot,
        SummonMinionLandEffect,
        Shield,
        ReflectProjectiles,
        Regeneration,
        ReadyAttack,
        ResetOnKill,
        ShockBurstEffect,
        ShockSquareEffect,
        SpeedBoostVFX,
        TauntedVFX,
        TeleportGunHitEffect,
    }

    public enum VFXColor
    {
        Blue,
        Red,
        Purple,
        Green,
        Yellow,
        Brown
    }
}