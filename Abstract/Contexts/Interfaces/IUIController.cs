using System;
using System.Collections;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IUIController
    {
        IEnumerator StartTurn(bool isReplay = false, bool dontShowTurnIndicator = false, bool showMiniPortraits = true);
        void ShowEndTurnButton(bool show);
        void ShowChooseTeamMenu(Player player);
        void ShowChooseAITurnOrder(Player player);
        IEnumerator SelectUnit(Fighter f);
        void UpdateAfterFighterSpawn();
        void ShowTutorialText(string text);
        void ShowTutorialArrow(Point pointingAt);
        void SetTutorialOverlayTapCallback(MapScenario.TutorialAction callback);
        void SetTutorialHelpButtonCallback(MapScenario.TutorialAction callback);
        void GameOver();
        void SetAbilityButtonFlashing(WhichAbilitySelected which);
        void SetEndTurnButtonFlashing(bool flashing);
        IEnumerator ShowPendingUnlocks();
        void ShowSelfDestructConfirmDialog(Action action);
    }
}