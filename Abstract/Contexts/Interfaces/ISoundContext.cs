namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface ISoundContext
    {
        /// <summary>
        /// Play a sound.
        /// </summary>
        void PlayOneShot(Sound s);
        void PlayOneShotVaryPitch(Sound s);
        void PlayOneShot(Sound s, float pitch);
        
        /// <summary>
        /// Play a sound with the possibility of pausing it later.
        /// </summary>
        /// <param name="s">The sound to play.</param>
        /// <returns>An ID with the track the sound is being played on. This can be passed in later to check if the sound is still playing, or stop playback.</returns>
        int PlayPausable(Sound s);
        
        /// <summary>
        /// Stop playback of a track if it is still being played.
        /// </summary>
        /// <param name="playback">The ID for the track returned by Play or PlayOneOf.</param>
        void Stop(int playback);
    }
}