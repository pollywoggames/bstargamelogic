using BarnardsStar.Games;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IOperationsContext
    {
        void SaveGame(Game game);
        void DeleteGame(string gameId);
        void SaveRoguelikeRun();
        void FinishedRoguelikeGame();
    }
}