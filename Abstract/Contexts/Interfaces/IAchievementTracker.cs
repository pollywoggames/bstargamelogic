using System.Collections.Generic;
using BarnardsStar.Characters;
using BarnardsStar.Games;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IAchievementTracker
    {
        void RegisterAchievementAction(AchievementAction action, int num);
        HashSet<FighterPrototype> GetPendingUnlocks();
        void AcknowledgeUnlock(FighterPrototype f);
        HashSet<FighterPrototype> GetUnlockedFighters();
        bool IsFighterUnlocked(FighterPrototype f);
    }

    public static class Achievements
    {
        public const int
            ActiveDaemonMinionAchievement = 3,
            SummonMinionsAchievement = 20,
            DealDamageWithPushPullAchievement = 5,
            HealByAnyMethodAchievement = 30,
            CrossGaps = 5,
            UseTeleporters = 20;
    }
}