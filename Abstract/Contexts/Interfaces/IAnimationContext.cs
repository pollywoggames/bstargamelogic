﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IAnimationContext
    {
        bool IsReplayMode();

        ICameraController Camera();

        void CursorOff();
        void CursorOn(Fighter f);

        IEnumerator MoveSmoothly(IMovable obj, Point dst, float time);

        IEnumerator MoveParabolic(IMovable obj, Point targetPosition, float numSteps, float gravity);

        IEnumerator BumpObject(IMovable obj);
        IEnumerator ShakeObject(IMovable obj, int frames, float intensity);

        IEnumerator MoveSmoothlyWhileRotating(IMovable obj, Point dst, float targetRotation, float time);

        IEnumerator MoveRotateSquish(IMovable obj, Point dst, float targetRotation, Vector3 targetScale, float time);

        IEnumerator MoveSmoothlyWhileRolling(IMovable f, Point targetPosition, bool left, bool upsideDown, float time);

        IEnumerator AnimateFightersSpawn(List<Fighter> fighters, bool isRespawn);

        IEnumerator AnimateReadyAttack(Fighter attacker, Coords target, bool flashSymbol);

        IEnumerator AnimateTeleport(Fighter fighter, Coords dst, int channel);
        IEnumerator AnimateTeleportOut(Fighter fighter);
        IEnumerator AnimateBurrow(Fighter actor, Coords dst);

        void PlayVFX(VFXType vfxType, Point point);
        void PlayVFX(VFXType vfxType, Point point, VFXColor vfxColor);
        void PlayElectricArcEffect(Point startPoint, Point endPoint, VFXColor color, float durationMultiplier);
    }
}