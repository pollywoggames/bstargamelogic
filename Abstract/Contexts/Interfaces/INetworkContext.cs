﻿using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Games;
using BarnardsStar.Games.Responses;

namespace BarnardsStar.Abstract.Contexts.Interfaces {
    public interface INetworkContext
    {
        IEnumerator SubmitTurn(Game game);

        void AckGameOver(Game game);
    }
}
