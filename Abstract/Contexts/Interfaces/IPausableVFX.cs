using BarnardsStar.Abstract.VirtualActors.Interfaces;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IPausableVFX : IMovable
    {
        void Pause();
        void Play();
    }
}