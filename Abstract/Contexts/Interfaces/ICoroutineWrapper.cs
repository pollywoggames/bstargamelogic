namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface ICoroutineWrapper
    {
        bool IsFinished();
    }
}