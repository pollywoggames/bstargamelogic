using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.InMemory;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface IRunContext
    {
        IEnumerator Run(IEnumerator coroutine);
        void Start(InstantCoroutineWrapper coroutine);
        void Start(IEnumerator coroutine, bool throwExceptions = false);
        IEnumerator All(List<IEnumerator> all);

        object WaitSeconds(float s);
        object WaitOneFrame();

        bool IsUnity();
        float GetTimeScale();
        float GetDeltaTime();
    }
}