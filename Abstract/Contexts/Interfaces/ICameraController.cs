using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.Interfaces
{
    public interface ICameraController
    {
        IEnumerator PanTilPointVisible(Point point);
        IEnumerator PanTilPointsVisible(List<Coords> points);
        IEnumerator PanTilPointsVisible(List<Point> points);
        IEnumerator PanTilPointIsCentered(Point point);

        void Shake(float time, float intensity);
        void Knockback(Point targetDelta);
    }
}