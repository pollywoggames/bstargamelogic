using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;

namespace BarnardsStar.Utilities
{
    public class UnityCoroutineWrapper : ICoroutineWrapper
    {
        private IEnumerator coroutine;
        public bool finished { get; private set; }

        public UnityCoroutineWrapper(IEnumerator coroutine)
        {
            this.coroutine = coroutine;
            finished = false;
        }

        public IEnumerator Run(IRunContext runCtx)
        {
            yield return runCtx.Run(coroutine);
            finished = true;
        }

        public bool IsFinished()
        {
            return finished;
        }
    }
}