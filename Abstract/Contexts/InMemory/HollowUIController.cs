using System;
using System.Collections;
using BarnardsStar.Abilities;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Maps;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowUIController : IUIController
    {
        // No state, so to cut down on memory allocations we just use an instance
        public static HollowUIController Instance = new HollowUIController();

        public IEnumerator StartTurn(bool isReplay = false, bool dontShowTurnIndicator = false, bool showMiniPortraits = true)
        {
            yield break;
        }

        public void ShowEndTurnButton(bool show)
        { }

        public void ShowChooseTeamMenu(Player player)
        { }

        public void ShowChooseAITurnOrder(Player player)
        { }

        public IEnumerator SelectUnit(Fighter f)
        {
            yield break;
        }

        public void UpdateAfterFighterSpawn()
        { }

        public void ShowTutorialText(string text)
        { }

        public void ShowTutorialArrow(Point pointingAt)
        { }

        public void SetTutorialOverlayTapCallback(MapScenario.TutorialAction callback)
        { }

        public void SetTutorialHelpButtonCallback(MapScenario.TutorialAction callback)
        { }

        public void GameOver()
        { }

        public void SetAbilityButtonFlashing(WhichAbilitySelected which)
        { }

        public void SetEndTurnButtonFlashing(bool flashing)
        { }

        public IEnumerator ShowPendingUnlocks()
        {
            yield break;
        }

        public void ShowSelfDestructConfirmDialog(Action action)
        {
            action?.Invoke();
        }
    }
}
