using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Games;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowAchievementTracker : IAchievementTracker
    {
        public static HollowAchievementTracker Instance = new HollowAchievementTracker();
        
        public void RegisterAchievementAction(AchievementAction action, int num)
        {
            
        }

        public HashSet<FighterPrototype> GetPendingUnlocks()
        {
            return new HashSet<FighterPrototype>();
        }

        public void AcknowledgeUnlock(FighterPrototype f)
        {
            
        }

        public HashSet<FighterPrototype> GetUnlockedFighters()
        {
            return new HashSet<FighterPrototype>();
        }

        public bool IsFighterUnlocked(FighterPrototype f)
        {
            return false;
        }
    }
}