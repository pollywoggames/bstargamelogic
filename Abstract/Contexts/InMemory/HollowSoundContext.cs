using System;
using BarnardsStar.Abstract.Contexts.Interfaces;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowSoundContext : ISoundContext
    {
        // No state, so to cut down on memory allocations we just use an instance
        public static HollowSoundContext Instance = new HollowSoundContext();

        private HollowSoundContext()
        { }

        public void PlayOneShot(Sound s)
        { }

        public void PlayOneShotVaryPitch(Sound s)
        { }

        public void PlayOneShot(Sound s, float pitch)
        { }

        public int PlayPausable(Sound s)
        {
            return 0;
        }

        public void Stop(int playback)
        { }
    }
}
