﻿using System.Collections;
using BarnardsStar.Games;
using BarnardsStar.Abstract.Contexts.Interfaces;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowNetworkContext : INetworkContext
    {
        // Previews don't use the network, so everything in this class is a no-op

        public static INetworkContext Instance = new HollowNetworkContext();

        public IEnumerator SubmitTurn(Game game)
        {
            yield break;
        }

        public void AckGameOver(Game game)
        { }
    }
}
