﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowAnimationContext : IAnimationContext
    {
        // No state, so to cut down on memory allocations we just use an instance
        public static HollowAnimationContext Instance = new HollowAnimationContext();
        
        private HollowCameraController camera;

        public HollowAnimationContext()
        {
            camera = new HollowCameraController();
        }

        public bool IsReplayMode()
        {
            return false;
        }

        public ICameraController Camera()
        {
            return camera;
        }

        public void CursorOff()
        { }

        public void CursorOn(Fighter f)
        { }

        public IEnumerator MoveSmoothly(IMovable obj, Point dst, float time)
        {
            var startingPos = obj.GetPosition();

            var delta = dst - startingPos;
            float timeElapsed = 0;
            while (timeElapsed < time)
            {
                var ratio = timeElapsed / time;
                obj.SetPosition(startingPos + delta * ratio);

                yield return null;
                timeElapsed += InstantRunContext.DELTA_TIME;
            }

            yield return null;
            obj.SetPosition(dst);
        }

        public IEnumerator MoveParabolic(IMovable obj, Point targetPosition, float numSteps, float gravity)
        {
            var delta = targetPosition - obj.GetPosition();
            var stepsLeft = numSteps;
            var exactPathTracker = obj.GetPosition();
            while (stepsLeft >= 0)
            {
                exactPathTracker += delta / numSteps;

                obj.SetPosition(new Point(exactPathTracker.x, exactPathTracker.y));

                stepsLeft--;
                yield return new InstantRunContext.RunCtxWaitSeconds(.02f);
            }

            obj.SetPosition(targetPosition);
        }

        public IEnumerator MoveSmoothlyWhileRotating(IMovable obj, Point dst, float targetRotation, float time)
        {
            yield return MoveSmoothly(obj, dst, time);
        }

        public IEnumerator MoveRotateSquish(IMovable obj, Point dst, float targetRotation, Vector3 targetScale, float time)
        {
            yield return MoveSmoothly(obj, dst, time);
        }

        public IEnumerator MoveSmoothlyWhileRolling(IMovable obj, Point targetPosition, bool left, bool upsideDown, float time)
        {
            yield return MoveSmoothly(obj, targetPosition, time);
        }

        public IEnumerator AnimateFightersSpawn(List<Fighter> fighters, bool isRespawn)
        {
            yield break;
        }

        public IEnumerator BumpObject(IMovable obj)
        {
            yield break;
        }

        public IEnumerator ShakeObject(IMovable obj, int frames, float intensity)
        {
            yield break;
        }

        public IEnumerator AnimateReadyAttack(Fighter attacker, Coords target, bool flashSymbol)
        {
            yield break;
        }

        public IEnumerator AnimateTeleport(Fighter fighter, Coords dst, int c)
        {
            fighter.SetPosition(dst.ToPoint(), true);
            yield break;
        }

        public IEnumerator AnimateTeleportOut(Fighter fighter)
        {
            yield break;
        }

        public IEnumerator AnimateBurrow(Fighter actor, Coords dst)
        {
            actor.SetPosition(dst.ToPoint(), true);
            yield break;
        }

        public void PlayVFX(VFXType vfxType, Point point)
        { }

        public void PlayVFX(VFXType vfxType, Point point, VFXColor vfxColor)
        { }

        public void PlayElectricArcEffect(Point startPoint, Point endPoint, VFXColor color, float durationMultiplier)
        { }
    }
}
