using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowCameraController : ICameraController
    {
        public IEnumerator PanTilPointVisible(Point point)
        {
            yield break;
        }

        public IEnumerator PanTilPointsVisible(List<Coords> points)
        {
            yield break;
        }

        public IEnumerator PanTilPointsVisible(List<Point> points)
        {
            yield break;
        }

        public IEnumerator PanTilPointIsCentered(Point point)
        {
            yield break;
        }

        public void Shake(float time, float intensity)
        { }

        public void Knockback(Point targetDelta)
        { }
    }
}
