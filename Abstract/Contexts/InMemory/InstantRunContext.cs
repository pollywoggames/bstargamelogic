using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Utilities;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class InstantRunContext : IRunContext
    {
        public const float FPS = 60f;
        public const float DELTA_TIME = 1f / FPS;
        public const float FAILSAFE_TIME = 10f;
        public const float FAILSAFE_FRAMES = FAILSAFE_TIME / DELTA_TIME;

        public class RunCtxWaitSeconds
        {
            public float seconds;

            public RunCtxWaitSeconds(float seconds)
            {
                this.seconds = seconds;
            }
        }
        
        private List<InstantCoroutineWrapper> coroutines = new List<InstantCoroutineWrapper>();

        // Wrapper around a coroutine that absorbs exceptions
        public IEnumerator Run(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (coroutine.MoveNext() == false) break;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    yield break;
                }
                    
                yield return coroutine.Current;
            }
        }

        public void Start(IEnumerator coroutine, bool throwExceptions = false)
        {
            Start(new InstantCoroutineWrapper(coroutine, throwExceptions));
        }

        public void Start(InstantCoroutineWrapper co)
        {
            // run first frame immediately
            co.ExecuteFrame(DELTA_TIME);
            
            // Add to the list so we keep executing it later
            if (!co.finished)
                coroutines.Add(co);
        }

        public IEnumerator All(List<IEnumerator> all)
        {
            var waitGroup = new WaitGroup(this);
            
            foreach (var c in all)
                waitGroup.Start(c);

            yield return waitGroup.Wait();
        }

        public object WaitSeconds(float s)
        {
            return new RunCtxWaitSeconds(s);
        }

        public object WaitOneFrame()
        {
            return null;
        }

        public bool IsUnity()
        {
            return false;
        }

        public float GetTimeScale()
        {
            return 1f;
        }

        public float GetDeltaTime()
        {
            return DELTA_TIME;
        }

        // Run all current coroutines to completion.
        //
        // Should be used whenever you want to actually run coroutines instantly.
        // (First call Start() on all the coroutines you want to start, then call
        // ExecuteAllCoroutines() to actually run them and wait for all of them to finish.)
        //
        // (Or just call RunInstantly to do both things for you.)
        public void ExecuteAllCoroutines()
        {
            int framesCount = 0;
            while (coroutines.Count > 0 && framesCount <= FAILSAFE_FRAMES)
            {
                // Make a shallow copy of the coroutines list, so that if new coroutines are started
                // in the middle of a frame, it doesn't result in the exception:
                // "System.InvalidOperationException: Collection was modified; enumeration operation may not execute"
                var coroutines_tempCopy = new List<InstantCoroutineWrapper>(coroutines);
                
                // Run a single frame for each coroutine
                foreach (var runCtxCoroutine in coroutines_tempCopy)
                    runCtxCoroutine.ExecuteFrame(DELTA_TIME);

                // Remove any finished coroutines
                coroutines = coroutines.Where(co => !co.finished).ToList();

                framesCount++;
            }

            if (framesCount >= FAILSAFE_FRAMES)
                throw new Exception("InstantRunContext.ExecuteAllCoroutines hit failsafe!");
        }

        public void RunInstantly(IEnumerator coroutine, bool throwExceptions = false)
        {
            Start(coroutine, throwExceptions);
            ExecuteAllCoroutines();
        }
    }
}
