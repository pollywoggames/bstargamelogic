using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Games;

namespace BarnardsStar.Abstract.Contexts.InMemory
{
    public class HollowOperationsContext : IOperationsContext
    {
        public static HollowOperationsContext Instance = new HollowOperationsContext();

        public void SaveGame(Game game)
        { }

        public void DeleteGame(string gameId)
        { }

        public void SaveRoguelikeRun()
        { }

        public void FinishedRoguelikeGame()
        { }
    }
}