using System;
using System.Collections;
using System.Collections.Generic;
using BarnardsStar.Abstract.Contexts.InMemory;
using BarnardsStar.Abstract.Contexts.Interfaces;

namespace BarnardsStar.Utilities
{
    public class InstantCoroutineWrapper : ICoroutineWrapper
    {
        public Stack<IEnumerator> coroutineStack { get; }
        public bool throwsExceptions { get; }
        public InstantRunContext.RunCtxWaitSeconds currWaitObject;

        public bool finished => coroutineStack.Count == 0;

        public InstantCoroutineWrapper(IEnumerator coroutine, bool throwExceptions = false)
        {
            coroutineStack = new Stack<IEnumerator>();
            coroutineStack.Push(coroutine);
            
            throwsExceptions = throwExceptions;
        }

        public void ExecuteFrame(float deltaTime)
        {
            if (finished)
                return;

            if (currWaitObject != null)
            {
                currWaitObject.seconds -= deltaTime;
                
                // check if finished waiting
                if (currWaitObject.seconds <= 0)
                    currWaitObject = null;
                else
                    // still waiting
                    return;
            }

            try
            {
                bool yieldedSubCoroutine;
                do
                {
                    yieldedSubCoroutine = false;
                    
                    // Run coroutine on top of stack
                    var currentCoroutine = coroutineStack.Peek();
                    if (currentCoroutine.MoveNext() == false)
                    {
                        // If the current one is finished, remove it from the stack
                        coroutineStack.Pop();
                        break;
                    }

                    object current = currentCoroutine.Current;
                    if (current is IEnumerator subCoroutine)
                    {
                        // If they 'yield return'ed another coroutine, push that one on the stack
                        // so we can execute it next
                        coroutineStack.Push(subCoroutine);
                        
                        // Set this to true so that we keep looping.
                        // If a sub-coroutine was yielded, we keep looping because we haven't hit a "real" pause
                        // yet (like yield return null or yield return WaitSeconds).
                        yieldedSubCoroutine = true;
                    }
                    else if (current is InstantRunContext.RunCtxWaitSeconds waitSeconds)
                    {
                        currWaitObject = waitSeconds;
                    }
                } while (yieldedSubCoroutine);
            }
            catch (Exception e)
            {
                Logger.Error(e);

                if (throwsExceptions)
                    throw;
            }
        }

        public bool IsFinished()
        {
            return finished;
        }
    }
}