﻿using System;
using System.Numerics;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryMovable : IPausableVFX
    {
        private Point _loc;

        public InMemoryMovable(Point loc)
        {
            _loc = loc;
        }

        public Point GetPosition()
        {
            return _loc;
        }

        public void SetPosition(Point p)
        {
            _loc = p;
        }

        public float GetRotation()
        {
            return 0;
        }

        public void SetRotation(float q)
        { }

        public Vector3 GetScale()
        {
            return Vector3.One;
        }

        public void SetScale(Vector3 scale)
        { }

        public void Destroy()
        { }

        public void Pause()
        { }

        public void Play()
        { }
    }
}
