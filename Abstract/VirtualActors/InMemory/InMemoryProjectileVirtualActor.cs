using System;
using System.Collections;
using System.Numerics;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryProjectileVirtualActor : IProjectileVirtualActor
    {
        private Point loc;

        public InMemoryProjectileVirtualActor(Point loc)
        {
            this.loc = loc;
        }

        public Point GetPosition()
        {
            return loc;
        }

        public void SetPosition(Point p)
        {
            loc = p;
        }

        public float GetRotation()
        {
            return 0;
        }

        public void SetRotation(float q)
        { }

        public Vector3 GetScale()
        {
            return Vector3.One;
        }

        public void SetScale(Vector3 scale)
        { }

        public void Destroy()
        { }

        public IEnumerator PlayExplosion()
        {
            yield break;
        }

        public void ResetExplosion()
        { }
    }
}
