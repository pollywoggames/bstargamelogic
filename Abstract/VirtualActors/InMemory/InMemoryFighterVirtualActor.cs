using System;
using System.Collections;
using System.Numerics;
using BarnardsStar.Abilities;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryFighterVirtualActor : IFighterVirtualActor
    {
        private Point _loc;
        private bool destroyed;

        public static InMemoryFighterVirtualActor CopyFrom(IFighterVirtualActor f)
        {
            return new InMemoryFighterVirtualActor(f.GetPosition());
        }

        public InMemoryFighterVirtualActor(Point loc)
        {
            _loc = loc;
        }
        
        public Point GetPosition()
        {
            return _loc;
        }

        public void SetPosition(Point p)
        {
            _loc = p;
        }

        public void ResetStateForRespawn()
        { }

        public void Hide()
        { }

        public Coords GetCoords()
        {
            return Coords.FromPoint(_loc - Fighter.OffsetOnGrid);
        }

        public void SetCoords(Coords c)
        {
            _loc = Point.FromCoords(c) + Fighter.OffsetOnGrid;
        }

        public void Face(Point p)
        { }

        public bool IsFacingLeft()
        {
            return false;
        }

        public void ShowHealthBar()
        { }

        public void HideHealthBar()
        { }

        public void InitHealthBar()
        { }

        public Point GetMuzzlePosition()
        {
            return _loc;
        }

        public void HideAllIndicators()
        { }

        public void RefreshAllIndicators()
        { }

        public void DamageAnimation(Fighter origin, bool suppressKnockback)
        { }

        public void DeathAnimation()
        { }

        public void HypertunnelAnimation(Coords c, bool isVertical)
        { }

        public void HopLandingAnimation(Coords c)
        { }

        public void ShowStatusEffect(StatusEffect eff)
        { }

        public void ClearAllStatusEffects()
        { }

        public void ClearStatusEffect(StatusEffect eff)
        { }

        public void ReflectProjectileAnimation(Point projectileOrigin)
        { }

        public IEnumerator AnimateThrowWindup(Coords direction, int shakeFrames)
        {
            yield break;
        }

        public IEnumerator AnimateThrowFollowthrough(Coords direction)
        {
            yield break;
        }

        public IEnumerator AnimateHookThrow(Fighter thrower, IMovable hookObj, Coords target)
        {
            yield break;
        }

        public IEnumerator AnimateFallDownHole()
        {
            yield break;
        }

        public IEnumerator AnimateSpawnMinionWindup(Coords target)
        {
            yield break;
        }

        public void ForwardInSortingOrder()
        { }

        public void BackwardInSortingOrder()
        { }

        public void DisableMovingParticles()
        { }

        public void PlayVFX(FighterVFXLocation location, FighterVFXType vfxType)
        { }

        public void StopVFX(FighterVFXLocation location)
        { }

        public void PlayMuzzleFlash()
        { }

        public void SetMuzzleSmokeActive(bool active)
        { }

        public void EnableMovingParticles(Coords delta)
        { }

        public float GetRotation()
        {
            return 0;
        }

        public void SetRotation(float f)
        { }

        public Vector3 GetScale()
        {
            return Vector3.One;
        }

        public void SetScale(Vector3 p)
        { }

        public void Destroy()
        { }

        public void Init(Fighter f, bool startingGame)
        { }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Obliterate()
        {
            destroyed = true;
        }

        public bool IsReal()
        {
            return false;
        }
    }
}
