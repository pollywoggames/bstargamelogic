using System.Numerics;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Hazards;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryHazardVirtualActor : IHazardVirtualActor
    {
        private Coords coords;
        private Point _loc;

        private bool destroyed;

        public InMemoryHazardVirtualActor(Coords coords)
        {
            this.coords = coords;
            this._loc = this.coords.ToPoint();
        }

        public Coords GetCoords()
        {
            return coords;
        }

        public void SetCoords(Coords c)
        {
            coords = c;
            _loc = coords.ToPoint();
        }

        public Point GetOffset()
        {
            return Point.zero;
        }

        public void Init(Hazard h)
        { }

        public void Remove()
        { }

        public void RefreshSprite()
        { }

        public void DoStartEffect()
        { }

        public void DoDamageEffect()
        { }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Obliterate()
        {
            destroyed = true;
        }

        public bool IsReal()
        {
            return false;
        }

        public Point GetPosition()
        {
            return _loc;
        }

        public void SetPosition(Point p)
        {
            _loc = p;
        }

        public float GetRotation()
        {
            return 0;
        }

        public void SetRotation(float q)
        { }

        public Vector3 GetScale()
        {
            return Vector3.One;
        }

        public void SetScale(Vector3 scale)
        { }

        public void Destroy()
        { }
    }
}
