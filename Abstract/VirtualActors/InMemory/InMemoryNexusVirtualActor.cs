using System;
using System.Collections;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryNexusVirtualActor : INexusVirtualActor
    {
        private Coords coords;

        public InMemoryNexusVirtualActor(Coords coords)
        {
            this.coords = coords;
        }

        public bool IsDestroyed()
        {
            return false;
        }

        public void Init(Nexus n)
        { }

        public Coords GetCenterCoords()
        {
            return coords; 
        }

        public void RefreshSprite(Team team, TeamColor teamColor)
        { }

        public void PlayDamagedEffect()
        { }

        public void Hide()
        { }

        public IEnumerator PlayLittleExplosion()
        {
            yield break;
        }

        public IEnumerator PlayBigExplosion()
        {
            yield break;
        }

        public void Obliterate()
        { }

        public bool IsReal()
        {
            return false;
        }
    }
}
