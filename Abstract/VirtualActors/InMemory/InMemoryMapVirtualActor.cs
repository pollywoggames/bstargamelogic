using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Primitives;
using BarnardsStar.Maps;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryMapVirtualActor : IMapVirtualActor
    {
        public static InMemoryMapVirtualActor Instance = new InMemoryMapVirtualActor();

        private bool destroyed;

        public void Init(GameMap map)
        {
            // TODO: don't hardcode this value
            // 1: cone
            map.deployableCoverTileCode = 1;
        }

        public void DeployCover(Coords location)
        { }

        public void DamageCover(Coords location, Point origin)
        { }

        public void DestroyCover(Coords location, Point origin)
        { }

        public void ClearCoverDamageAt(Coords location)
        { }

        public void SetCoverDamageAt(Coords location)
        { }

        public void SetTerrain(Coords location, Terrain terrain)
        { }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Obliterate()
        {
            destroyed = true;
        }

        public bool IsReal()
        {
            return false;
        }
    }
}