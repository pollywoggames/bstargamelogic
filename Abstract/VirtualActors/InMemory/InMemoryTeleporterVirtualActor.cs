using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Abstract.VirtualActors.Interfaces;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.InMemory
{
    public class InMemoryTeleporterVirtualActor : ITeleporterVirtualActor
    {
        private bool destroyed;
        
        public void Init(Teleporter teleporter)
        { }

        public void SetTeleporting(bool teleporting)
        { }

        public void SetAlpha(float alpha)
        { }

        public void SetOnCooldown(bool cooldown)
        { }

        public void AnimateDestroy(IAnimationContext animCtx)
        { }

        public IEnumerator DissolveInOut(bool _in)
        {
            yield break;
        }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Obliterate()
        {
            destroyed = true;
        }

        public bool IsReal()
        {
            return false;
        }
    }
}