using System.Collections;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IFighterVirtualActor : IMovable, IVirtualActor
    {
        void Init(Fighter f, bool startingGame);
        void ResetStateForRespawn();
        void Hide();
        
        Coords GetCoords();
        void SetCoords(Coords c);

        void Face(Point p);
        bool IsFacingLeft();

        void ShowHealthBar();
        void HideHealthBar();
        void InitHealthBar();

        void ForwardInSortingOrder();
        void BackwardInSortingOrder();

        Point GetMuzzlePosition();

        void HideAllIndicators();
        void RefreshAllIndicators();
        
        // --- various animations ---
        void DamageAnimation(Fighter origin, bool suppressKnockback);
        void DeathAnimation(); // for spontaneous death animations when someone surrenders
        void PlayVFX(FighterVFXLocation location, FighterVFXType vfxType);
        void StopVFX(FighterVFXLocation location);
        void PlayMuzzleFlash();
        void SetMuzzleSmokeActive(bool active);
        void EnableMovingParticles(Coords delta);
        void DisableMovingParticles();
        void HypertunnelAnimation(Coords c, bool isVertical);
        void HopLandingAnimation(Coords c);
        void ShowStatusEffect(StatusEffect eff);
        void ClearAllStatusEffects();
        void ClearStatusEffect(StatusEffect eff);
        void ReflectProjectileAnimation(Point projectileOrigin);
        IEnumerator AnimateThrowWindup(Coords direction, int shakeFrames);
        IEnumerator AnimateThrowFollowthrough(Coords direction);
        IEnumerator AnimateHookThrow(Fighter thrower, IMovable hookObj, Coords target);
        IEnumerator AnimateFallDownHole();
        IEnumerator AnimateSpawnMinionWindup(Coords target);
        // ------
    }
}