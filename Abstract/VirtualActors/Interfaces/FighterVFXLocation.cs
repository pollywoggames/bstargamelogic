namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public enum FighterVFXLocation
    {
        Muzzle,
        Ground
    }
}
