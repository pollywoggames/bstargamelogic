using System.Numerics;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IMovable
    {
        Point GetPosition();
        void SetPosition(Point p);

        float GetRotation();
        void SetRotation(float q);

        Vector3 GetScale();
        void SetScale(Vector3 scale);

        void Destroy();
    }
}