namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public enum FighterVFXType
    {
        MuzzleFire,
        
        SmashImpactParticleSystem,
    }
}
