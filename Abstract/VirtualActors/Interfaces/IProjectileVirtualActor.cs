using System.Collections;
using BarnardsStar.Characters;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IProjectileVirtualActor : IMovable
    {
        IEnumerator PlayExplosion();

        void ResetExplosion();
    }
}