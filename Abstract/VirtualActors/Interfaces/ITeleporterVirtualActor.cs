using System.Collections;
using BarnardsStar.Abstract.Contexts.Interfaces;
using BarnardsStar.Maps;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface ITeleporterVirtualActor : IVirtualActor
    {
        void Init(Teleporter teleporter);
        void SetTeleporting(bool teleporting);
        void SetAlpha(float alpha);
        void SetOnCooldown(bool cooldown);
        void AnimateDestroy(IAnimationContext animCtx);
        IEnumerator DissolveInOut(bool _in);
    }
}