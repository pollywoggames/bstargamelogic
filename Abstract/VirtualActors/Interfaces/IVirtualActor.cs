namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IVirtualActor
    {
        // Explicit check for whether a Unity gameobject is destroyed -- I *think* some iOS errors are happening
        // because we check (virtualActor != null) but it doesn't work because it's on a IVirtualActor interface
        // instead of on a Unity GameObject instance directly. So, check IsDestroyed to double check.
        bool IsDestroyed();
        
        // Was going to name this "Destroy" but that's already a Unity keyword.
        // But it does the same thing, just destroys the Unity gameObject
        void Obliterate();

        // Should only return true for Unity version of virtual actors
        bool IsReal();
    }
}