using System.Collections;
using BarnardsStar.Characters;
using BarnardsStar.Players;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface INexusVirtualActor : IVirtualActor
    {
        void Init(Nexus n);
        Coords GetCenterCoords();
        void RefreshSprite(Team team, TeamColor teamColor);
        void PlayDamagedEffect();
        void Hide();

        // Nexus explodes, but there are still other players in the game.
        IEnumerator PlayLittleExplosion();
        
        // Big, dramatic end-of-game explosion coroutine
        IEnumerator PlayBigExplosion();
    }
}