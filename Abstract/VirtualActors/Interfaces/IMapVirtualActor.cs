using BarnardsStar.Primitives;
using BarnardsStar.Maps;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IMapVirtualActor : IVirtualActor
    {
        void Init(GameMap map);
        void DeployCover(Coords location);
        void DamageCover(Coords location, Point origin);
        void DestroyCover(Coords location, Point origin);
        void ClearCoverDamageAt(Coords location);
        void SetCoverDamageAt(Coords location);
        
        void SetTerrain(Coords location, Terrain terrain);
    }
}