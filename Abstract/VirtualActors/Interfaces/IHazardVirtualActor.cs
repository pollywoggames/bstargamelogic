using BarnardsStar.Hazards;
using BarnardsStar.Primitives;

namespace BarnardsStar.Abstract.VirtualActors.Interfaces
{
    public interface IHazardVirtualActor : IVirtualActor, IMovable
    {
        Coords GetCoords();
        void SetCoords(Coords c);
        Point GetOffset();

        void Init(Hazard h);
        void Remove();
        void RefreshSprite();
        void DoStartEffect();
        void DoDamageEffect();
    }
}