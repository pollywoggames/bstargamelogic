using System.Collections.Generic;
using BarnardsStar.Characters;

namespace BarnardsStar.Upgrades
{
    public interface IPotentialUpgrade
    {
        // Index of this upgrade in the list of potentialUpgrades on FighterBlueprint
        public int index { get; set; }
        
        public int GetAbilityIconIdx(FighterPrototype fighterProto);
        public string GetDescription(FighterPrototype fighter, IAbilityNameGetter abilityNameGetter, bool post, bool includeTarget);
        public List<UpgradeType> GetAllUpgrades();
        public bool Matches(UpgradeType upgradeType);
    }
}