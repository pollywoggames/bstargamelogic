namespace BarnardsStar.Upgrades
{
    public interface IAbilityNameGetter
    {
        string GetAbilityName(string abilityTypeName);
    }
}