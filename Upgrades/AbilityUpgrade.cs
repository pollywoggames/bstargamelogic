namespace BarnardsStar.Upgrades
{
    public class AbilityUpgrade
    {
        public int Range;
        public int Damage;
        public int Cooldown;
        public int Misc;

        public bool HasAnyUpgrades()
        {
            return Range > 0 || Damage > 0 || Cooldown > 0 || Misc > 0;
        }
    }
}