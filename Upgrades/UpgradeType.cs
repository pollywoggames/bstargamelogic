using System;
using System.Collections.Generic;
using System.ComponentModel;
using BarnardsStar.Weapons;

namespace BarnardsStar.Upgrades
{
    public enum GeneralUpgradeType
    {
        Health = 0,
        Moves = 1,
        Weapon = 2,
        Ability = 3,
        SwapWeapon = 4,
    }

    public enum AbilityUpgradeType
    {
        None = 0,
        Range = 1,
        Damage = 2,
        Cooldown = 3,
        Misc = 4
    }
    
    [TypeConverter(typeof(UpgradeTypeConverter))]
    public struct UpgradeType : IEquatable<UpgradeType>
    {
        public GeneralUpgradeType mainType;
        public AbilityUpgradeType subType;
        public int abilityIdx;
        public int amount;
        public BSWeapon? newWeapon;

        public UpgradeType(int amount, GeneralUpgradeType type, AbilityUpgradeType subtype = AbilityUpgradeType.None, int abilityIdx = -1, BSWeapon? newWeapon = null)
        {
            this.mainType = type;
            this.subType = subtype;
            this.abilityIdx = abilityIdx;
            this.amount = amount;
            this.newWeapon = newWeapon;

            // some sanity checks
            if (mainType == GeneralUpgradeType.Ability && abilityIdx == -1)
                throw new Exception("Can't make UpgradeType of type Ability and not provide abilityIdx");

            if (mainType == GeneralUpgradeType.SwapWeapon && newWeapon == null)
                throw new Exception("Can't make UpgradeType of type SwapWeapon and not provide newWeapon");
        }

        public override string ToString()
        {
            // human-readable version
            // string subtypeStr = subType == AbilityUpgradeType.None ? "" : $",sub:{subType.ToString()}";
            // string idxStr = abilityIdx == -1 ? "" : $",idx:{abilityIdx}";
            // string newWeaponStr = newWeapon.HasValue ? $",wpn:{newWeapon.Value.ToString()}" : "";
            // return $"UpgradeType({mainType}{subtypeStr}{idxStr}{newWeaponStr},amt:{amount})";
            
            // version that is easier to parse
            int newWeaponInt = newWeapon.HasValue ? (int)newWeapon.Value : -1;
            return $"UpgradeType({(int)mainType},{(int)subType},{abilityIdx},{amount},{newWeaponInt})";
        }

        #region Equality Members
        
        public bool Equals(UpgradeType other)
        {
            return mainType == other.mainType && subType == other.subType && abilityIdx == other.abilityIdx && amount == other.amount && newWeapon == other.newWeapon;
        }

        public override bool Equals(object obj)
        {
            return obj is UpgradeType other && Equals(other);
        }
        
        public static bool operator ==(UpgradeType a, UpgradeType b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(UpgradeType a, UpgradeType b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)mainType;
                hashCode = (hashCode * 397) ^ (int)subType;
                hashCode = (hashCode * 397) ^ abilityIdx;
                hashCode = (hashCode * 397) ^ amount;
                hashCode = (hashCode * 397) ^ newWeapon.GetHashCode();
                return hashCode;
            }
        }
        
        #endregion

        #region Static Members
        
        public static int GetMoveUpgradeAmount(List<UpgradeType> upgrades)
        {
            if (upgrades == null)
                return 0;

            int amount = 0;
            for (var i = 0; i < upgrades.Count; i++)
            {
                var upgrade = upgrades[i];
                if (upgrade.mainType == GeneralUpgradeType.Moves)
                    amount += upgrade.amount;
            }

            return amount;
        }
        
        public static int GetHealthUpgradeAmount(List<UpgradeType> upgrades)
        {
            if (upgrades == null)
                return 0;

            int amount = 0;
            for (var i = 0; i < upgrades.Count; i++)
            {
                var upgrade = upgrades[i];
                if (upgrade.mainType == GeneralUpgradeType.Health)
                    amount += upgrade.amount;
            }

            return amount;
        }
        
        public static BSWeapon? GetSwappedWeapon(List<UpgradeType> upgrades)
        {
            for (var i = 0; i < upgrades.Count; i++)
            {
                var upgrade = upgrades[i];
                if (upgrade.mainType == GeneralUpgradeType.SwapWeapon)
                    return upgrade.newWeapon;
            }

            return null;
        }

        public static AbilityUpgrade GetWeaponUpgrades(List<UpgradeType> upgrades)
        {
            AbilityUpgrade ret = new AbilityUpgrade();
            if (upgrades == null)
                return ret;

            for (var i = 0; i < upgrades.Count; i++)
            {
                var upgrade = upgrades[i];
                if (upgrade.mainType == GeneralUpgradeType.Weapon)
                {
                    switch (upgrade.subType)
                    {
                        case AbilityUpgradeType.Range:  ret.Range += upgrade.amount; break;
                        case AbilityUpgradeType.Damage: ret.Damage += upgrade.amount; break;
                        case AbilityUpgradeType.Misc:   ret.Misc += upgrade.amount; break;
                        default: throw new ArgumentOutOfRangeException();
                    }
                }
            }

            return ret;
        }
        
        public static AbilityUpgrade GetAbilityUpgrades(List<UpgradeType> upgrades, int abilityIdx)
        {
            AbilityUpgrade ret = new AbilityUpgrade();
            if (upgrades == null)
                return ret;

            for (var i = 0; i < upgrades.Count; i++)
            {
                var upgrade = upgrades[i];
                if (upgrade.mainType == GeneralUpgradeType.Ability && upgrade.abilityIdx == abilityIdx)
                {
                    switch (upgrade.subType)
                    {
                        case AbilityUpgradeType.Range:    ret.Range += upgrade.amount; break;
                        case AbilityUpgradeType.Damage:   ret.Damage += upgrade.amount; break;
                        case AbilityUpgradeType.Cooldown: ret.Cooldown += upgrade.amount; break;
                        case AbilityUpgradeType.Misc:     ret.Misc += upgrade.amount; break;
                        default: throw new ArgumentOutOfRangeException();
                    }
                }
            }

            return ret;
        }
        
        #endregion
    }
}