using System.Collections.Generic;
using System.Linq;
using BarnardsStar.Characters;

namespace BarnardsStar.Upgrades
{
    public class MultiPotentialUpgrade : IPotentialUpgrade
    {
        public List<PotentialUpgrade> upgrades;
        public string descriptionOverride;
        
        public int index { get; set; }

        public MultiPotentialUpgrade(List<PotentialUpgrade> upgrades)
        {
            this.upgrades = upgrades;
        }

        public int GetAbilityIconIdx(FighterPrototype fighterProto)
        {
            // just return the icon for the 1st in the list
            return upgrades[0].GetAbilityIconIdx(fighterProto);
        }

        public string GetDescription(FighterPrototype fighter, IAbilityNameGetter ang, bool post, bool includeTarget)
        {
            if (!string.IsNullOrEmpty(descriptionOverride))
                return descriptionOverride;
            
            return string.Join(", ", upgrades.Select(u => u.GetDescription(fighter, ang, post, includeTarget)));
        }

        public List<UpgradeType> GetAllUpgrades()
        {
            return upgrades.Select(u => u.type).ToList();
        }

        public bool Matches(UpgradeType upgradeType)
        {
            for (var i = 0; i < upgrades.Count; i++)
                if (upgrades[i].type == upgradeType)
                    return true;

            return false;
        }
    }
}