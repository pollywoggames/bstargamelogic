using System;
using System.Collections.Generic;
using BarnardsStar.Abilities;
using BarnardsStar.Characters;
using BarnardsStar.Utilities;
using BarnardsStar.Weapons;

namespace BarnardsStar.Upgrades
{
    public class PotentialUpgrade : IPotentialUpgrade
    {
        public const int HealthIconIdx = 19, MoveIconIdx = 246;
        
        public UpgradeType type { get; }
        public string detailsOverride;
        public string nounOverride;
        
        public int index { get; set; }

        public int amount => type.amount;

        public PotentialUpgrade(UpgradeType type, string detailsOverride = null, string nounOverride = null)
        {
            this.type = type;
            this.detailsOverride = detailsOverride;
            this.nounOverride = nounOverride;
        }

        public int GetAbilityIconIdx(FighterPrototype fighterProto)
        {
            return type.mainType switch
            {
                GeneralUpgradeType.Health => HealthIconIdx,
                GeneralUpgradeType.Moves => MoveIconIdx,
                GeneralUpgradeType.SwapWeapon => Weapon.New(type.newWeapon.Value, new Fighter()).spriteIdx,
                _ => GetAbility(fighterProto).GetSpriteIndex(),
            };
        }

        private IAbility WeaponFor(FighterPrototype prototype)
        {
            var fb = AllFighters.Lookup(prototype);
            return Weapon.New(fb.EquippedWeapon, new Fighter());
        }

        private IAbility AbilityFor(FighterPrototype prototype, int abilityIdx)
        {
            var fb = AllFighters.Lookup(prototype);
            return AbstractAbility.New(fb.abilities[abilityIdx], new Fighter());
        }

        private IAbility GetAbility(FighterPrototype proto)
        {
            return type.mainType switch
            {
                GeneralUpgradeType.Weapon => WeaponFor(proto),
                GeneralUpgradeType.Ability => AbilityFor(proto, type.abilityIdx),
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        
        private string DescribeUpgrade(FighterPrototype fighter, IAbilityNameGetter ang, bool includeTarget)
        {
            string target = "";
            if (includeTarget)
            {
                target = type.mainType switch
                {
                    GeneralUpgradeType.Health => "Health",
                    GeneralUpgradeType.Moves => "Movement",
                    GeneralUpgradeType.Weapon => ang.GetAbilityName(GetAbility(fighter).GetTypeName()),
                    GeneralUpgradeType.Ability => ang.GetAbilityName(GetAbility(fighter).GetTypeName()),
                    _ => throw new ArgumentOutOfRangeException()
                };
                target = $"<b>{target}</b>";
            }

            string noun = type.subType switch
            {
                AbilityUpgradeType.Range => "range",
                AbilityUpgradeType.Damage => "damage",
                AbilityUpgradeType.Cooldown => "cooldown",
                AbilityUpgradeType.Misc => "misc",
                _ => null
            };

            if (noun != null)
            {
                if (nounOverride != null)
                    noun = nounOverride;

                if (includeTarget)
                {
                    // don't add an extra space if it's empty
                    if (noun != "") noun = $" {noun}";
                }
                else if (!string.IsNullOrEmpty(noun))
                {
                    noun = TextUtils.CapitalizeFirstLetter(noun);
                }

                return target + noun;
            }
            else
            {
                return target;
            }
        }

        public string GetDescription(FighterPrototype fighter, IAbilityNameGetter abilityNameGetter, bool post, bool includeTarget)
        {
            var fb = AllFighters.Lookup(fighter);
            string fighterNameStr = "";
            if (!post)
            {
                var fighterName = fb.Name;
                fighterNameStr = $"<b>{fighterName}:</b> ";
            }

            if (type.mainType == GeneralUpgradeType.SwapWeapon)
            {
                return fighterNameStr + GetSwapWeaponDescription(fighter, abilityNameGetter, post);
            }

            var amountToUse = amount;
            
            string verb = post ? "increased" : "Increase";
            if (!string.IsNullOrEmpty(nounOverride) || !string.IsNullOrEmpty(detailsOverride))
                verb = post ? "upgraded" : "Upgrade";
            else if (type.subType == AbilityUpgradeType.Cooldown)
            {
                verb = post ? "decreased" : "Decrease";
                amountToUse = -amount;
            }

            string details;
            if (!string.IsNullOrEmpty(detailsOverride))
                details = detailsOverride;
            else if (type.subType != AbilityUpgradeType.Misc)
            {
                int fromAmount;

                switch (type.mainType)
                {
                    case GeneralUpgradeType.Health:
                        fromAmount = fb.Health;
                        break;
                    case GeneralUpgradeType.Moves:
                        fromAmount = fb.MoveSpeed;
                        break;
                    default:
                    {
                        var ability = GetAbility(fighter);
                        fromAmount = type.subType switch
                        {
                            AbilityUpgradeType.Range => ability.GetRange(),
                            AbilityUpgradeType.Damage => ability.GetDamage(),
                            AbilityUpgradeType.Cooldown => ability.GetCooldown(),
                            _ => throw new ArgumentOutOfRangeException()
                        };
                        break;
                    }
                }
                // Use <nobr> tag to prevent wrapping in specific places
                details = $"from <nobr>{fromAmount} to {fromAmount + amountToUse}</nobr>";
            }
            else
                details = $"<nobr>by {amountToUse}<nobr>";

            if (post)
            {
                var desc = DescribeUpgrade(fighter, abilityNameGetter, includeTarget);
                if (desc == "")
                {
                    verb = TextUtils.CapitalizeFirstLetter(verb);
                }
                else
                {
                    desc += " ";
                }
                
                return $"{desc}{verb} {details}";
            }
            else
                return $"{fighterNameStr}{verb} {DescribeUpgrade(fighter, abilityNameGetter, includeTarget)} {details}";
        }

        public List<UpgradeType> GetAllUpgrades()
        {
            return new List<UpgradeType>() { type };
        }

        public bool Matches(UpgradeType upgradeType)
        {
            return type == upgradeType;
        }

        private string GetSwapWeaponDescription(FighterPrototype fighter, IAbilityNameGetter abilityNameGetter, bool post)
        {
            var fb = AllFighters.Lookup(fighter);

            var extraDescription = "";
            if (!post && type.newWeapon == BSWeapon.Magnum)
                extraDescription = " (like Pistol, but can also shoot diagonally)";

            var equippedWeaponStr = abilityNameGetter.GetAbilityName(fb.EquippedWeapon.ToString());
            var newWeaponStr = abilityNameGetter.GetAbilityName(type.newWeapon.Value.ToString());

            if (post)
                return $"Weapon swapped to <b>{newWeaponStr}</b>";
            else
                return $"Swap weapon from <b>{equippedWeaponStr}</b> to <b>{newWeaponStr}</b>{extraDescription}";
        }
    }
}