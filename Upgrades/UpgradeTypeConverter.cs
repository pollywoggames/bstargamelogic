using System;
using System.ComponentModel;
using System.Globalization;
using BarnardsStar.Weapons;

namespace BarnardsStar.Upgrades
{
    public class UpgradeTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string val = (string)value;

            const string UpgradeTypePrefix = "UpgradeType(";
            if (!val.StartsWith(UpgradeTypePrefix) || !val.EndsWith(")"))
            {
                throw new Exception($"Failure to convert string \"{val}\" to UpgradeType - invalid format");
            }

            string members = val.Substring(UpgradeTypePrefix.Length, val.Length - UpgradeTypePrefix.Length - 1);
            var memberStrs = members.Split(',');
            if (memberStrs.Length != 5)
            {
                throw new Exception($"Failure to convert string \"{val}\" to UpgradeType - invalid content");
            }

            int mainType = int.Parse(memberStrs[0]);
            int subType = int.Parse(memberStrs[1]);
            int abilityIdx = int.Parse(memberStrs[2]);
            int amount = int.Parse(memberStrs[3]);
            int newWeapon = int.Parse(memberStrs[4]);

            return new UpgradeType(
                amount,
                (GeneralUpgradeType) mainType,
                (AbilityUpgradeType) subType,
                abilityIdx,
                newWeapon == -1 ? null : (BSWeapon?)newWeapon
            );
        }
    }
}