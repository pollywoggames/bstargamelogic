using System;

#if UNITY_2020_3_OR_NEWER
using UnityEngine;
#else
using BarnardsStar.Attributes;
#endif

namespace BarnardsAscot.bstargamelogic.Notifications
{
    public class IOSNotification : BaseNotification
    {
        public string Title { get; set; }
        
        public string Body { get; set; }
        
        public int? Badge { get; set; }
        
        public NotificationData Data { get; set; }
    }

    [Serializable]
    public class NotificationData
    {
        // If the notification is for it being your turn in a game
        [SerializeField] public string GameId;

        [SerializeField] public NotificationData_GameState GameState;
        
        // If the notification is for a new friend request being received
        [SerializeField] public string FriendInviteId;

        // If the notification is for a friend request being accepted
        [SerializeField] public string FriendId;
        
        // If the notification is for a new game invite
        [SerializeField] public string GameLobbyId;
    }

    [Serializable]
    public enum NotificationData_GameState
    {
        YourTurn,
        TimedOut,
    }
}